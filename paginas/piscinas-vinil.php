<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>
</head>



<body class="bg-produtos">






  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->


  <!--  ==============================================================  -->
  <!---descricao bg empresa -->
  <!--  ==============================================================  --> 
  <div class="container margin-interna">
    <div class="row">
      <div class="col-xs-12 slider-index text-center">
        <h1>NOSSOS PRODUTOS</h1>
        <?php $dados= $obj_site->select_unico("tb_empresa", "idempresa", 2) ?>
        <p><?php Util::imprime($dados[descricao], 1000) ?></p>
      </div>
    </div>
  </div>
  <!--  ==============================================================  -->
  <!-- - descricao bg empresa -->
  <!--  ==============================================================  -->





  
<!-- ======================================================================= -->
<!-- categorias  -->
<!-- ======================================================================= -->
<div class="container">
  <div class="row categorias-index produtos-dentro">
      


      <div class="linha-produtos">
          <div class="categorias-produtos">
              <div class="col-xs-12 bg-cinza bg1prod">
                  <div class="col-xs-3">
                    <h1 class="pull-left top50 right5"><span>SELECIONE O <br> REVESTIMENTO</span></h1> 
                    <h1 class="pull-left top50"><span><i class="fa fa-angle-right"></i></span></h1>
                  </div>

                  <div class="col-xs-9 lista-revestimentos pt50" style="overflow-x: hidden;">
                


                  <div id="carousel" class="flexslider">
                    <ul class="slides ">
                      <?php
                        $result = $obj_site->select("tb_piscinas_vinil");
                        if(mysql_num_rows($result) > 0){
                           $c = 0;
                          while($row = mysql_fetch_array($result)){
                            if ($c == 0) {
                                $btn_orcamento = $row[0];
                              }
                          ?>
                              <li class="touchcarousel-item">
                                  <a data-toggle="tooltip" data-placement="top" title="<?php Util::imprime($row[titulo]); ?>" href="javascript:void(0);" onclick="mudaFoto(<?php echo ++$b; ?>, <?php echo $row[idpiscinavinil] ?> )">
                                    <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem1]", 104, 63, array("data-toggle"=>"tooltip", "data-placement"=>"top", "title"=>"$row[titulo]")) ?>
                                  </a>
                              </li>
                          <?php 
                          }
                        }
                        ?>
                      <!-- items mirrored twice, total of 12 -->
                    </ul>
                  </div>



                  

              </div>

              </div>
          </div>

      </div>
  </div>
</div>
<!-- ======================================================================= -->
<!-- categorias  -->
<!-- ======================================================================= -->



<!-- ======================================================================= -->
<!-- imagens da picinas  -->
<!-- ======================================================================= -->
<div class="container">
  <div class="row top30">
    <div class="col-xs-12 lista_imagens_servico_piscina">
      <div id="container-infos-galeria-grande">
          <?php
          $result = $obj_site->select("tb_piscinas_vinil");
          if(mysql_num_rows($result) > 0){
              while($row = mysql_fetch_array($result)){
              ?>
                  <div id="container-infos-galeria-grande-<?php echo ++$ii; ?>">   
                      <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 1170, 405, array("class"=>"input100")) ?>
                      <img class="input100" src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($row[imagem]) ?>" alt="<?php Util::imprime($row[titulo]) ?>" />                      
                  </div>
              <?php
              }
          }
          ?>
        </div>    
    </div>
  </div>
</div>
<!-- ======================================================================= -->
<!-- imagens da picinas  -->
<!-- ======================================================================= -->







<div class="container">
  <div class="row top20">
    <div class="col-xs-12 desc-prod">

      <div class="pull-left right20">
        
        <button id="btn_add_solicitacao" value="<?php echo $btn_orcamento; ?>" class="btn btn-azul-claro telefone">
          <i class="fa fa-shopping-cart right10"></i> SOLICITE UM ORÇAMENTO
        </button>
   
      </div>

      <div class="">
        <h2 class="pull-left telefone"><i class="fa fa-phone right10"></i> ATENDIMENTO:</h2> 
        <h4 class="pt10"><?php Util::imprime($config[telefone1]); ?></h4>
      </div>


    </div>
  </div>
</div>






<!-- ======================================================================= -->
<!-- descricao  -->
<!-- ======================================================================= -->
<div class="container">
  <div class="row top40">
    <div class="col-xs-12">
     
        <h3><span>DESCRIÇÃO <i class="fa fa-angle-double-down"></i></span></h3>  
        <?php $dados= $obj_site->select_unico("tb_empresa", "idempresa", 12) ?>
        <p class="top10"><?php Util::imprime($dados[descricao]) ?></p>
    </div>
  </div>
</div>
<!-- ======================================================================= -->
<!-- descricao  -->
<!-- ======================================================================= -->








<!-- ======================================================================= -->
<!-- veja tambem  -->
<!-- ======================================================================= -->
<div class="container">
  <div class="row top25">
    <div class="col-xs-12">
      <h3 class="bottom20"><span>VEJA TAMBÉM <i class="fa fa-angle-double-down"></i></span></h3>  

      
      <?php 
      $result = $obj_site->select("tb_produtos", "order by rand() limit 4");
      if(mysql_num_rows($result) > 0){
        while($row = mysql_fetch_array($result)){
        ?>
        

        <div class="col-xs-3 lista-produto">
          <a href="<?php echo Util::caminho_projeto() ?>/produto/<?php Util::imprime($row[url_amigavel]); ?>">
            <img src="<?php echo Util::caminho_projeto() ?>/uploads/tumb_<?php Util::imprime($row[imagem]); ?>" alt="<?php Util::imprime($row[titulo]); ?>" class="input100" >
          </a>
          <h1 class="top10"><?php Util::imprime($row[titulo]); ?></h1>
          <p class="top5 bottom5"><?php Util::imprime($row[descricao], 600); ?></p>
          <a href="<?php echo Util::caminho_projeto() ?>/produto/<?php Util::imprime($row[url_amigavel]); ?>" class="btn btn-azul-pequeno right5 top10">SAIBA MAIS</a>
          <a href="" class="btn btn-azul-pequeno top10">SOLICITE UM ORÇAMENTO</a>
        </div>
        <?php 
        }
      }
      ?>

    </div>
  </div>
</div>
<!-- ======================================================================= -->
<!-- veja tambem  -->
<!-- ======================================================================= -->









<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->
<?php require_once('./includes/rodape.php') ?>
<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->



</body>

</html>




<script>
    $(document).ready(function() {
        $('.FormContato').bootstrapValidator({
            message: 'This value is not valid',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                 nome: {
                    validators: {
                        notEmpty: {
                            
                        }
                    }
                },
                email: {
                    validators: {
                        notEmpty: {
                        
                        },
                        emailAddress: {
                            message: 'Esse endereço de email não é válido'
                        }
                    }
                },
                nota: {
                    validators: {
                        notEmpty: {
                        
                        }
                    }
                },
                comentario: {
                    validators: {
                        notEmpty: {
                            
                        }
                    }
                },
                mensagem: {
                    validators: {
                        notEmpty: {
                            
                        }
                    }
                }
            }
        });
    });
</script>




<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>

<script>
	$(document).ready(function() {
        $(window).load(function() {
          // The slider being synced must be initialized first
          $('#carousel').flexslider({
            animation: "slide",
            controlNav: false,
            animationLoop: false,
            slideshow: false,
            itemWidth: 100,
            itemMargin: 5,
            asNavFor: '#slider'
          });
         
          $('#slider').flexslider({
            animation: "slide",
            controlNav: false,
            animationLoop: false,
            slideshow: false,
            sync: "#carousel"
          });
        });
	 });
</script>



  <!--  ====================================================================================================   -->  
  <!--  TROCA AS IMAGENS   -->
  <!--  ====================================================================================================   -->
  <script type="text/javascript"> 
  function mudaFoto(id, idpiscinavinil)
  {
    var elemento = "#container-infos-galeria-grande-"+id;
      $("#container-infos-galeria-grande div").stop(true, true).fadeOut(500).hide(500);
      $(elemento).stop(true, true).fadeIn(1000).show(500);
      $('#btn_add_solicitacao').val(idpiscinavinil);
  }
  </script>


  <script type="text/javascript">
    $(document).ready(function() {
        $('#btn_add_solicitacao').click(function(event) {
          var id = $('#btn_add_solicitacao').val();
          window.location = '<?php echo Util::caminho_projeto() ?>/add_prod_solicitacao.php?id='+id+'&tipo_orcamento=piscina_vinil';
        });
    });
    </script>