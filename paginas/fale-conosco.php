<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>
</head>



<body class="bg-fale-conosco">






  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->


  <!--  ==============================================================  -->
  <!---descricao bg trabalhe conosco -->
  <!--  ==============================================================  --> 
  <div class="container margin-interna">
    <div class="row">
      <div class="col-xs-12 slider-index text-center">
        <h1>FALE CONOSCO</h1>
        <?php $dados= $obj_site->select_unico("tb_empresa", "idempresa", 10) ?>
        <p><?php Util::imprime($dados[descricao], 1000) ?></p>
      </div>
    </div>
  </div>
  <!--  ==============================================================  -->
  <!-- - descricao bg trabalhe conosco -->
  <!--  ==============================================================  -->



  <!--  ==============================================================  -->
  <!-- formulario-->
  <!--  ==============================================================  -->
  <div class="container">
    <div class="row">
      <div class="col-xs-10 col-xs-offset-1 formulario">

        <div class=" col-xs-5 media top30">
          <div class="media-left media-middle">
            <img class="media-object" src="<?php echo Util::caminho_projeto() ?>/imgs/icon-phone.png" alt="">
          </div>
          <div class="media-body">
            <h2 class="media-heading">
              <?php Util::imprime($config[telefone1]); ?>

              <?php if (!empty($config[telefone2])) { ?>
                <?php Util::imprime($config[telefone2]); ?>
              <?php } ?>

              <?php if (!empty($config[telefone3])) { ?>
                <?php Util::imprime($config[telefone3]); ?>
              <?php } ?>

              <?php if (!empty($config[telefone4])) { ?>
                <?php Util::imprime($config[telefone4]); ?>
              <?php } ?>
            </h2>
          </div>
        </div>


        <div class=" col-xs-7 media top30">
          <div class="media-left media-middle">
            <img class="media-object" src="<?php echo Util::caminho_projeto() ?>/imgs/icon-localizacao.png" alt="">
          </div>
          <div class="media-body">
            <h1 class="media-heading"><?php Util::imprime($config[endereco]); ?></h1>
          </div>
        </div>


        
        <!-- formulario de contatos -->
        <?php
        //  VERIFICO SE E PARA ENVIAR O EMAIL
        if(isset($_POST[nome]))
        {
            $texto_mensagem = "
                                              Nome: ".($_POST[nome])." <br />
                                              Assunto: ".($_POST[assunto])." <br />
                                              Telefone: ".($_POST[telefone])." <br />
                                              Email: ".($_POST[email])." <br />
                                              Mensagem: <br />
                                              ".(nl2br($_POST[mensagem]))."
                                              ";
            Util::envia_email($config[email], utf8_decode($_POST[assunto]), $texto_mensagem, utf8_decode($_POST[nome]), $_POST[email]);
            Util::envia_email($config[email_copia], utf8_decode($_POST[assunto]), $texto_mensagem, utf8_decode($_POST[nome]), $_POST[email]);
            
            Util::alert_bootstrap("Obrigado por entrar em contato.");
            unset($_POST);
        }
        ?>


        <form class="form-inline FormCurriculo" role="form" method="post" enctype="multipart/form-data">
          <div class="top50 bottom25">

            <div class="col-xs-12">
              <!-- formulario orcamento -->
              <div class="top20 bottom80">


                <div class="clearfix"></div>   
                <div class="top15">
                  <div class="col-xs-6 form-group ">
                    <label class="glyphicon glyphicon-user"> <span>Nome</span></label>
                    <input type="text" name="nome" class="form-control fundo-form1 input100" placeholder="">
                  </div>

                  <div class="col-xs-6 form-group ">
                    <label class="glyphicon glyphicon-envelope"> <span>E-mail</span></label>
                    <input type="text" name="email" class="form-control fundo-form1 input100" placeholder="">
                  </div>
                </div>


                <div class="clearfix"></div>   
                <div class="top15">
                  <div class="col-xs-6 form-group">
                    <label class="glyphicon glyphicon-earphone"> <span>Telefone</span></label>
                    <input type="text" name="telefone" class="form-control fundo-form1 input100" placeholder="">
                  </div>

                  <div class="col-xs-6 form-group">
                   <label class="glyphicon glyphicon-star"> <span>Assunto</span></label>
                   <input type="text" name="assunto" class="form-control fundo-form1 input100" placeholder="">
                 </div>
               </div>

               
               <div class="clearfix"></div>    
               <div class="top15">
                <div class="col-xs-12 form-group">
                  <label class="glyphicon glyphicon-pencil"> <span>Sua Mensagem</span></label>
                  <textarea name="mensagem" id="" cols="30" rows="8" class="form-control  fundo-form1 input100" placeholder=""></textarea>
                </div>
              </div>

              <div class="clearfix"></div>
              
              <div class="col-xs-12">
                <div class="pull-left  top30 bottom25">
                  <img src="<?php echo Util::caminho_projeto() ?>/imgs/saiba-como-chegar.png" alt="">
                </div>

                <div class="pull-right  top30 bottom25">
                  <button type="submit" class="btn btn-azul-formulario" name="btn_contato">
                    ENVIAR
                  </button>
                </div>
              </div>


            </div>
            <!-- formulario orcamento -->

          </div>

        </div>
      </form>





    </div>
  </div>
</div>
<!--  ==============================================================  -->
<!-- formulario -->
<!--  ==============================================================  -->


<!--  ==============================================================  -->
<!-- MAPA -->
<!--  ==============================================================  -->
<div class="container-fluid">
  <div class="row top40">
    <iframe src="<?php Util::imprime($config[src_place]); ?>" width="100%" height="600" frameborder="0" style="border:0" allowfullscreen></iframe>
  </div>
</div>

<!--  ==============================================================  -->
<!-- MAPA -->
<!--  ==============================================================  -->




<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->
<?php require_once('./includes/rodape.php') ?>
<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->



</body>

</html>



<script>
  $(document).ready(function() {
    $('.FormCurriculo').bootstrapValidator({
      message: 'This value is not valid',
      feedbackIcons: {
        valid: 'glyphicon glyphicon-ok',
        invalid: 'glyphicon glyphicon-remove',
        validating: 'glyphicon glyphicon-refresh'
      },
      fields: {
       nome: {
        validators: {
          notEmpty: {

          }
        }
      },
      email: {
        validators: {
          notEmpty: {

          },
          emailAddress: {
            message: 'Esse endereço de email não é válido'
          }
        }
      },
      telefone: {
        validators: {
          notEmpty: {

          }
        }
      },
      assunto: {
        validators: {
          notEmpty: {

          }
        }
      },
      escolaridade: {
        validators: {
          notEmpty: {

          }
        }
      },
      cargo: {
        validators: {
          notEmpty: {

          }
        }
      },
      cidade: {
        validators: {
          notEmpty: {

          }
        }
      },
      estado: {
        validators: {
          notEmpty: {

          }
        }
      },
      area: {
        validators: {
          notEmpty: {

          }
        }
      },
      curriculo: {
        validators: {
          notEmpty: {
            message: 'Por favor insira seu currículo'
          },
          file: {
            extension: 'doc,docx,pdf,rtf',
            type: 'application/pdf,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/rtf',
                            maxSize: 5*1024*1024,   // 5 MB
                            message: 'O arquivo selecionado não é valido, ele deve ser (doc,docx,pdf,rtf) e 5 MB no máximo.'
                          }
                        }
                      },
                      mensagem: {
                        validators: {
                          notEmpty: {

                          }
                        }
                      }
                    }
                  });
});
</script>

