<!DOCTYPE html>
<html lang="pt-br">

<head>
    <?php require_once('./includes/head.php'); ?>
</head>



<body>

    




<!-- ======================================================================= -->
<!-- topo    -->
<!-- ======================================================================= -->
<?php require_once('./includes/topo.php') ?>
<!-- ======================================================================= -->
<!-- topo    -->
<!-- ======================================================================= -->







<!--  ==============================================================  -->
<!--  BANNERS  HOME -->
<!--  ==============================================================  -->

<div class="container-fluid padding0">
    <div class="row slider-index">
        <div id="myCarousel" class="carousel slide" data-ride="carousel">
             


              <!-- Indicators -->
              <ol class="carousel-indicators">
                <?php
                  $result = $obj_site->select("tb_banners", "and tipo_banner = 1 ");
                   if(mysql_num_rows($result) > 0){
                    $i = 0;
                     while ($row = mysql_fetch_array($result)) {
                      $imagens[] = $row;
                     ?> 
                        <li data-target="#myCarousel" data-slide-to="<?php echo $i; ?>" class="<?php if($i == 0){ echo "active"; } ?>"></li>
                     <?php 
                      $i++;
                     }
                  }
                  ?>
              </ol>


              <div class="carousel-inner" role="listbox">
                

                <?php 
                if (count($imagens) > 0) {
                  $i = 0;
                  foreach ($imagens as $key => $imagem) {
                  ?>
                      <div class="item <?php if($i == 0){ echo "active"; } ?>">
                        <img class="<?php echo $i ?>-slide" src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($imagem[imagem]); ?>" alt="First slide">
                          <div class="container">
                            <div class="carousel-caption text-center">

                                 <h1><?php Util::imprime($imagem[titulo]); ?></h1>
-                                <p><?php Util::imprime($imagem[legenda]); ?></p>

                              
                                <?php if (!empty($imagem[url])) { ?>
                                  <a class="btn btn-transparente right15" href="<?php Util::imprime($imagem[url]); ?>" role="button">SAIBA MAIS</a>
                                <?php } ?>
                                

                                
                                
                            </div>
                          </div>
                      </div>


                  <?php
                    $i++;
                  }
                }
                ?>


                
             



              <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
              </a>
              <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
              </a>
            </div><!-- /.carousel -->


          </div>
    </div>
</div>
<!--  ==============================================================  -->
<!--  BANNERS  HOME -->
<!--  ==============================================================  -->




<!-- ======================================================================= -->
<!-- categorias  -->
<!-- ======================================================================= -->
<div class="container">
  <div class="row categorias-index">
      <div class="linha-produtos">
          <div class="categorias-produtos">
              <div class="col-xs-12 bg-cinza bg1prod">
                  <ul class="lista-categorias">
                      <li class="destaque">
                          <a href="<?php echo Util::caminho_projeto() ?>/produtos">
                              <img src="<?php echo Util::caminho_projeto() ?>/imgs/icon-estrela.png" alt="">
                              <br>
                              VER TODAS
                          </a>
                      </li>
                      <li>
                            <a href="<?php echo Util::caminho_projeto() ?>/piscinas-vinil">
                                <img src="<?php echo Util::caminho_projeto() ?>/imgs/piscina-vinil.png" alt="PISCINAS DE VINIL">
                                <br>
                                PISCINAS DE VINIL
                            </a>
                        </li>
                      <?php 
                      $result = $obj_site->select("tb_categorias_produtos", "LIMIT 6");
                      if(mysql_num_rows($result) > 0)
                      {
                        while($row = mysql_fetch_array($result))
                        {
                        ?>
                          <li>
                              <a href="<?php echo Util::caminho_projeto() ?>/produtos/<?php Util::imprime($row[url_amigavel]) ?>">
                                  <img src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($row[imagem]); ?>" alt="">
                                  <br>
                                  <?php Util::imprime($row[titulo]) ?>
                              </a>
                          </li>
                        <?php  
                        }
                      }


                      $result = $obj_site->select("tb_categorias_produtos", "LIMIT 6, 1000");
                      if(mysql_num_rows($result) > 0)
                      {
                          ?>
                          <li class="pull-right">
                              <a id="drop3" href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                  <img src="<?php echo Util::caminho_projeto() ?>/imgs/icon-estrela.png" alt="">
                                  <br>
                                  VER MAIS
                                  <span class="caret"></span>
                                </a>
                                <ul class="dropdown-menu lista-categorias-submenu" aria-labelledby="drop3">
                                  <?php while($row = mysql_fetch_array($result)): ?>
                                  <li>
                                      
                                      <a href="<?php echo Util::caminho_projeto() ?>/produtos/<?php Util::imprime($row[url_amigavel]) ?>">
                                        <img src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($row[imagem]); ?>" alt=""><br>
                                        <?php Util::imprime($row[titulo]) ?>
                                      </a>
                                  </li>
                                  <?php endwhile; ?>
                                </ul>
                          </li>
                          <?php
                      }
                      ?>
                      
                  </ul>
              </div>
          </div>

      </div>
  </div>
</div>
<!-- ======================================================================= -->
<!-- categorias  -->
<!-- ======================================================================= -->








<!-- ======================================================================= -->
<!-- produtos  -->
<!-- ======================================================================= -->
<div class="container">
  <div class="row top30">


    <?php
    $result = $obj_site->select("tb_produtos", "ORDER BY RAND() LIMIT 5");
    $i = 0;
    if(mysql_num_rows($result) > 0)
    {
      while ($row = mysql_fetch_array($result)) { 

        if(++$i == 1){
        ?>
          <div class="col-xs-6 lista-produto">
            <a href="<?php echo Util::caminho_projeto() ?>/produto/<?php Util::imprime($row[url_amigavel]); ?>">
              <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 573, 420) ?>
            </a>
            <h1 class="top10"><?php Util::imprime($row[titulo]); ?></h1>
            <p class="top5 bottom5"><?php Util::imprime($row[descricao], 500); ?></p>
            <a href="<?php echo Util::caminho_projeto() ?>/produto/<?php Util::imprime($row[url_amigavel]); ?>" class="btn btn-azul right5 top10">SAIBA MAIS</a>
            <a class="btn btn-azul top10" href="javascript:void(0);" title="Solicite um orçamento" onclick="add_solicitacao(<?php Util::imprime($row[0]) ?>, 'produto')" id="btn_add_solicitacao_<?php Util::imprime($row[0]) ?>, 'produto'">SOLICITE UM ORÇAMENTO</a>
          </div>
        <?php
        }else{

         
          ?>

            <div class="col-xs-3 lista-produto">
              <a href="<?php echo Util::caminho_projeto() ?>/produto/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
                <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 266, 149, array("class"=>"input100")) ?>
              </a>
              <h1 class="top10"><?php Util::imprime($row[titulo]); ?></h1>
            <p class="top5 bottom5"><?php Util::imprime($row[descricao], 500); ?></p>
            <a href="<?php echo Util::caminho_projeto() ?>/produto/<?php Util::imprime($row[url_amigavel]); ?>" class="btn btn-azul-pequeno right5 ">SAIBA MAIS</a>
            <a class="btn btn-azul-pequeno" href="javascript:void(0);" title="Solicite um orçamento" onclick="add_solicitacao(<?php Util::imprime($row[0]) ?>, 'produto')" id="btn_add_solicitacao_<?php Util::imprime($row[0]) ?>, 'produto'">SOLICITE UM ORÇAMENTO</a>
            </div>
          
          <?php
          
     
        ?>

        <?php
        }

      }
    }
    ?>


 
    



  </div>
</div>
<!-- ======================================================================= -->
<!-- produtos  -->
<!-- ======================================================================= -->





<!-- ======================================================================= -->
<!-- CONHECA MAIS A AQUATENDAS  -->
<!-- ======================================================================= -->
<div class="container-fluid">
  <div class="row bg-index-conheca-aquatendas top20">
    <div class="container">


      <div class="row">
        <div class="col-xs-12 text-right top115">
              
              <div class="pull-right text-right left10">
                <i class="fa fa-angle-double-down"></i>
              </div>

              <div class="pull-right">
                <h3>CONHEÇA MAIS</h3>
                <h3><span>A AQUATENDAS</span></h3>  
              </div>
              
        </div>
      </div>
      

      <div class="row">
      
        <div class="col-xs-8 col-xs-offset-4 top75">
          <?php $dados= $obj_site->select_unico("tb_empresa", "idempresa", 8) ?>
          <p><?php Util::imprime($dados[descricao]) ?></p>
          
          
          <h4 class="top25 bottom15">CONHEÇA  OS SERVIÇOS</h4>
          
          <?php
          $result = $obj_site->select("tb_servicos", "ORDER BY RAND() LIMIT 3");
          if(mysql_num_rows($result) > 0)
          {
            while ($row = mysql_fetch_array($result)) { 
            ?>
              <a href="<?php echo Util::caminho_projeto() ?>/servico/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
                <p class="left25"><i class="fa fa-check"></i><?php Util::imprime($row[titulo]); ?></p>  
              </a>
            <?php
            }
          }
          ?>

          
        </div>

      </div>


    </div>
  </div>
</div>
<!-- ======================================================================= -->
<!-- CONHECA MAIS A AQUATENDAS  -->
<!-- ======================================================================= <-->






<!-- ======================================================================= -->
<!-- dicas  -->
<!-- ======================================================================= -->
<div class="container">
  <div class="row top50">
    
    <div class="col-xs-12 linha-dicas bottom25">
      <div class="pull-left padding0">
        <h3>CONFIRA</h3>      
        <h3><span>NOSSAS DICAS</span></h3>  
      </div>
      <div class="pull-right top25">
          <a href="<?php echo Util::caminho_projeto() ?>/dicas" class="btn btn-azul">VER TODAS</a>
      </div>
    </div>


    <!-- lista dicas -->
    <?php
    $result = $obj_site->select("tb_dicas", "ORDER BY RAND() LIMIT 4");
    if(mysql_num_rows($result) > 0)
    {
      while ($row = mysql_fetch_array($result)) { 
      ?>
        <div class="col-xs-3">
          <a href="<?php echo Util::caminho_projeto() ?>/dica/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
            <img src="<?php echo Util::caminho_projeto() ?>/uploads/tumb_<?php Util::imprime($row[imagem]); ?>" alt="" class="input100">
          </a>
          <p><?php Util::imprime($row[titulo]); ?></p>
        </div>
      <?php
      }
    }
    ?>
    <!-- lista dicas -->


  </div>
</div>
<!-- ======================================================================= -->
<!-- dicas  -->
<!-- ======================================================================= -->







<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->
<?php require_once('./includes/rodape.php') ?>
<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->



</body>

</html>
