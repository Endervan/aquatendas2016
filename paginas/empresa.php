<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>
</head>



<body class="bg-empresa">






  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->


  <!--  ==============================================================  -->
  <!---descricao bg empresa -->
  <!--  ==============================================================  --> 
  <div class="container margin-interna">
    <div class="row">
      <div class="col-xs-12 slider-index text-center">
        <h1>NOSSA EMPRESA</h1>
        <?php $dados= $obj_site->select_unico("tb_empresa", "idempresa", 3) ?>
        <p><?php Util::imprime($dados[descricao], 1000) ?></p>
      </div>
    </div>
  </div>
  <!--  ==============================================================  -->
  <!-- - descricao bg empresa -->
  <!--  ==============================================================  -->


  <!--  ==============================================================  -->
  <!-- descricao  CLASSE MEDIA OBJETO-->
  <!--  ==============================================================  -->
  <div class="container">
    <div class="row descricao-empresa">

      <div class="col-xs-12">
        <div class="media top35">
          <div class="media-left">
            <img class="media-object" src="<?php echo Util::caminho_projeto() ?>/imgs/icon-empresa1.png" alt="...">
          </div>
          <div class="media-body">
            <h2 class="media-heading">SOBRE</h2>
          </div>
        </div>

        <?php $dados= $obj_site->select_unico("tb_empresa", "idempresa", 4) ?>
        <p class="top25"><?php Util::imprime($dados[descricao]) ?></p>

      </div>

    


      <!--  ==============================================================  -->
      <!-- carroucel depoimento empresa -->
      <!--  ==============================================================  -->

      <div class="col-xs-12 top45">
        <div class="media">
          <div class="media-left">
            <img class="media-object" src="<?php echo Util::caminho_projeto() ?>/imgs/icon-empresa-qualidade.png" alt="...">
          </div>
          <div class="media-body">
            <h2 class="media-heading">QUALIDADE DOS SERVIÇOS</h2>
          </div>
        </div>

        <?php $dados= $obj_site->select_unico("tb_empresa", "idempresa", 5) ?>
        <p class="top25"><?php Util::imprime($dados[descricao]) ?></p>

      </div>


    </div>
  </div>
  <!--  ==============================================================  -->
  <!-- descricao CLASSE MEDIA OBJETO -->
  <!--  ==============================================================  -->






  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/rodape.php') ?>
  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->



</body>

</html>
