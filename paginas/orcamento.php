<?php
//  EXCLUI UM ITEM
if(isset($_GET[action]))
{
    //  SELECIONO O TIPO
    switch($_GET[tipo])
    {
        case "produto":
            $id = $_GET[id];
            unset($_SESSION[solicitacoes_produtos][$id]);
            sort($_SESSION[solicitacoes_produtos]);
        break;
        case "servico":
            $id = $_GET[id];
            unset($_SESSION[solicitacoes_servicos][$id]);
            sort($_SESSION[solicitacoes_servicos]);
        break;
        case "piscina_vinil":
            $id = $_GET[id];
            unset($_SESSION[piscina_vinil][$id]);
            sort($_SESSION[piscina_vinil]);
        break;
    }

}
?>

<!DOCTYPE html>
<html lang="pt-br">
<head>
    <?php require_once('./includes/head.php'); ?>
</head>
<body class="bg-orcamento">



  <!--  ==============================================================  -->
  <!---descricao bg empresa -->
  <!--  ==============================================================  --> 
  <div class="container margin-interna">
    <div class="row">
      <div class="col-xs-12 slider-index text-center">
        <h1>FAÇA UM ORÇAMENTO</h1>
        <?php $dados= $obj_site->select_unico("tb_empresa", "idempresa", 9) ?>
        <p><?php Util::imprime($dados[descricao], 1000) ?></p>
      </div>
    </div>
  </div>
  <!--  ==============================================================  -->
  <!-- - descricao bg empresa -->
  <!--  ==============================================================  -->





    <!-- topo -->
    <?php require_once('./includes/topo.php') ?>
    <!-- topo -->

<div class="container">
  <div class="row">
    

    <form class="form-inline FormContato" role="form" method="post">

        <?php
        //  VERIFICO SE E PARA CADASTRAR A SOLICITACAO
        if(isset($_POST[nome]))
        {

            //  CADASTRO OS PRODUTOS SOLICITADOS
            for($i=0; $i < count($_POST[qtd]); $i++)
            {
                $dados = $obj_site->select_unico("tb_produtos", "idproduto", $_POST[idproduto][$i]);

                $mensagem .= "
                            <tr>
                                <td><p>". $_POST[qtd][$i] ."</p></td>
                                <td><p>". utf8_encode($dados[titulo]) ."</p></td>
                             </tr>
                            ";
            }


            //  CADASTRO AS PISCINAS DE VINIL
            for($i=0; $i < count($_POST[piscinavinil]); $i++)
            {
                $dados = $obj_site->select_unico("tb_piscinas_vinil", "idpiscinavinil", $_POST[piscinavinil][$i]);

                $mensagem_piscina_vinil .= "
                                            <tr>
                                                <td><p>". $_POST[qtd_piscina][$i] ."</p></td>
                                                <td><p>". utf8_encode($dados[titulo]) ."</p></td>
                                             </tr>
                                            ";
            }




            //  CADASTRO OS PRODUTOS SOLICITADOS
            for($i=0; $i < count($_POST[qtd_servico]); $i++)
            {
                $dados = $obj_site->select_unico("tb_servicos", "idservico", $_POST[idservico][$i]);
                $mensagem_servico .= "
                                        <tr>
                                            <td><p>". $_POST[qtd_servico][$i] ."</p></td>
                                            <td><p>". utf8_encode($dados[titulo]) ."</p></td>
                                         </tr>
                                        ";
            }

            if (count($_POST[categorias]) > 0) {
                foreach($_POST[categorias] as $cat){
                    $desc_cat .= $cat . ' , ';
                }
            }




            




            //  ENVIANDO A MENSAGEM PARA O CLIENTE
            $texto_mensagem = "
                                O seguinte cliente fez uma solicitação pelo site. <br />


                                Nome: $_POST[nome] <br />
                                Email: $_POST[email] <br />
                                Telefone: $_POST[telefone] <br />
                                Celular: $_POST[celular] <br />
                                Bairro: $_POST[bairro] <br />
                                Cidade: $_POST[cidade] <br />
                                Estado: $_POST[estado] <br />
                                Receber orçamento: $_POST[receber_orcamento] <br />
                                Categorias desejadas: $desc_cat <br />
                                Como conheceu nosso site: $_POST[como_conheceu] <br />

                                Mensagem: <br />
                                ".nl2br($_POST[mensagem])." <br />


                                <br />
                                <h2> Produtos selecionados:</h2> <br />

                                <table width='100%' border='0' cellpadding='5' cellspacing='5'>

                                    <tr>
                                          <td><h4>QTD</h4></td>
                                          <td><h4>PRODUTO</h4></td>
                                    </tr>

                                    $mensagem

                                </table>


                                <br />
                                <h2> Piscinas de vinil selecionada(s):</h2> <br />

                                <table width='100%' border='0' cellpadding='5' cellspacing='5'>

                                    <tr>
                                          <td><h4>QTD</h4></td>
                                          <td><h4>PRODUTO</h4></td>
                                    </tr>

                                    $mensagem_piscina_vinil

                                </table>



                                <br />
                                <h2> Serviços selecionados:</h2> <br />

                                <table width='100%' border='0' cellpadding='5' cellspacing='5'>

                                    <tr>
                                          <td><h4>QTD</h4></td>
                                          <td><h4>SERVIÇO</h4></td>
                                    </tr>

                                    $mensagem_servico

                                </table>
                                ";


            Util::envia_email($config[email], utf8_decode($_POST[nome] . " solicitou um orçamento"), $texto_mensagem, utf8_decode($_POST[nome]), $_POST[email]);
            Util::envia_email($config[email_copia], utf8_decode($_POST[nome] . " solicitou um orçamento"), $texto_mensagem, utf8_decode($_POST[nome]), $_POST[email]);
            

            unset($_SESSION[solicitacoes_produtos]);
            unset($_SESSION[solicitacoes_servicos]);
            unset($_SESSION[piscinas_vinil]);
            Util::alert_bootstrap("Orçamento enviado com sucesso. Em breve entraremos em contato.");

        }
        ?>



        <!-- bg-orcamento -->
        <div class="container-fluir">
            <div class="row">
                <div class="bg-orcamento"></div>
            </div>
        </div>
        <!-- bg-orcamento -->




    <?php
    if(count($_SESSION[solicitacoes_produtos]) > 0)
    {
    ?>

        <!-- descricao-empresa -->
        <div class="container">
            <div class="row">
                <div class="col-xs-7 top50 bottom10">
                    <div class="descricao-orcamento">
                        <h3>PRODUTOS SELECIONADOS (<?php echo count($_SESSION[solicitacoes_produtos]) ?>)</h3>
                    </div>
                </div>

                <div class="col-xs-12">
                    <div class="div-tb-lista-itens">

                            <?php
                            for($i=0; $i < count($_SESSION[solicitacoes_produtos]); $i++)
                            {
                              $row = $obj_site->select_unico("tb_produtos", "idproduto", $_SESSION[solicitacoes_produtos][$i]);
                              ?>


                              <!-- itens orcamentos 01 -->
                              <div class="col-xs-12 bottom20 top10">
                                <div class="fundo-cinza1">
                                  <div class="col-xs-2">
                                    <img src="<?php echo Util::caminho_projeto() ?>/uploads/tumb_<?php Util::imprime($row[imagem]) ?>" alt="" height="88" width="100" >
                                  </div>
                                  <div class="col-xs-5 top40">
                                    <p><?php Util::imprime($row[titulo]) ?></p>
                                  </div>
                                  <div class="col-xs-2 top35">
                                    <p>QDT.<input type="text" class="input-lista-prod-orcamentos" name="qtd[]" value="1" data-toggle="tooltip" data-placement="top" title="Digite a quantidade desejada">
                                      <input name="idproduto[]" type="hidden" value="<?php echo $row[0]; ?>"/>
                                    </p>
                                  </div>
                                  <div class="col-xs-3 top30">
                                    <a href="?action=del&id=<?php echo $i; ?>&tipo=produto" data-toggle="tooltip" data-placement="top" title="Excluir">
                                      <i class="fa fa-trash fa-2x top10"></i>
                                    </a>
                                  </div>
                                </div>
                              </div>
                              <!-- itens orcamentos 01-->


                              <?php
                            }
                            ?>
                    </div>

                    <div class="row top15">
                      <div class="col-xs-12">
                        <a href="<?php echo Util::caminho_projeto() ?>/produtos" title="Continuar orçando" class="btn btn-primary">
                          Continuar orçando
                        </a>
                      </div>
                    </div>
                </div>

          </div>
      </div>
    <?php
    }
    ?>





    <!-- piscina vinil -->

    <?php
    if(count($_SESSION[piscina_vinil]) > 0)
    {
    ?>

        <!-- descricao-empresa -->
        <div class="container">
            <div class="row">
                <div class="col-xs-7 top50 bottom10">
                    <div class="descricao-orcamento">
                        <h3>PISCINA VINIL SELECIONADA(S) (<?php echo count($_SESSION[piscina_vinil]) ?>)</h3>
                    </div>
                </div>

                <div class="col-xs-12">
                    <div class="div-tb-lista-itens">

                            <?php
                            for($i=0; $i < count($_SESSION[piscina_vinil]); $i++)
                            {
                              $row = $obj_site->select_unico("tb_piscinas_vinil", "idpiscinavinil", $_SESSION[piscina_vinil][$i]);
                              ?>


                              <!-- itens orcamentos 01 -->
                              <div class="col-xs-12 bottom20 top10">
                                <div class="fundo-cinza1">
                                  <div class="col-xs-2">
                                    <img src="<?php echo Util::caminho_projeto() ?>/uploads/tumb_<?php Util::imprime($row[imagem]) ?>" alt="" height="88" width="100" >
                                  </div>
                                  <div class="col-xs-5 top40">
                                    <p><?php Util::imprime($row[titulo]) ?></p>
                                  </div>
                                  <div class="col-xs-2 top35">
                                    <p>QDT.<input type="text" class="input-lista-prod-orcamentos" name="qtd_piscina[]" value="1" data-toggle="tooltip" data-placement="top" title="Digite a quantidade desejada">
                                      <input name="piscinavinil[]" type="hidden" value="<?php echo $row[0]; ?>"/>
                                    </p>
                                  </div>
                                  <div class="col-xs-3 top30">
                                    <a href="?action=del&id=<?php echo $i; ?>&tipo=piscina_vinil" data-toggle="tooltip" data-placement="top" title="Excluir">
                                      <i class="fa fa-trash fa-2x top10"></i>
                                    </a>
                                  </div>
                                </div>
                              </div>
                              <!-- itens orcamentos 01-->


                              <?php
                            }
                            ?>
                    </div>

                    <div class="row top15">
                      <div class="col-xs-12">
                        <a href="<?php echo Util::caminho_projeto() ?>/piscinas-vinil" title="Continuar orçando" class="btn btn-primary">
                          Continuar orçando
                        </a>
                      </div>
                    </div>
                </div>

          </div>
      </div>
    <?php
    }
    ?>
    <!-- piscina vinil -->







    <!-- servicos desejavel -->

    <?php
    if(count($_SESSION[solicitacoes_servicos]) > 0)
    {
    ?>

        <!-- descricao-empresa -->
        <div class="container">
            <div class="row">
                <div class="col-xs-7 top50 bottom10">
                    <div class="descricao-orcamento">
                        <h3>SERVIÇOS SELECIONADOS (<?php echo count($_SESSION[solicitacoes_servicos]) ?>)</h3>
                    </div>
                </div>

                <div class="col-xs-12">
                    <div class="div-tb-lista-itens">

                            <?php
                            for($i=0; $i < count($_SESSION[solicitacoes_servicos]); $i++)
                            {
                              $row = $obj_site->select_unico("tb_servicos", "idservico", $_SESSION[solicitacoes_servicos][$i]);
                              ?>


                              <!-- itens orcamentos 01 -->
                              <div class="col-xs-12 bottom20 top10">
                                <div class="fundo-cinza1">
                                  <div class="col-xs-2">
                                    <img src="<?php echo Util::caminho_projeto() ?>/uploads/tumb_<?php Util::imprime($row[imagem]) ?>" alt="" height="88" width="100" >
                                  </div>
                                  <div class="col-xs-5 top40">
                                    <p><?php Util::imprime($row[titulo]) ?></p>
                                  </div>
                                  <div class="col-xs-2 top35">
                                    <p>QDT.<input type="text" class="input-lista-prod-orcamentos" name="qtd_servico[]" value="1" data-toggle="tooltip" data-placement="top" title="Digite a quantidade desejada">
                                      <input name="idservico[]" type="hidden" value="<?php echo $row[0]; ?>"/>
                                    </p>
                                  </div>
                                  <div class="col-xs-3 top30">
                                    <a href="?action=del&id=<?php echo $i; ?>&tipo=servico" data-toggle="tooltip" data-placement="top" title="Excluir">
                                      <i class="fa fa-trash fa-2x top10"></i>
                                    </a>
                                  </div>
                                </div>
                              </div>
                              <!-- itens orcamentos 01-->


                              <?php
                            }
                            ?>
                    </div>

                    <div class="row top15">
                      <div class="col-xs-12">
                        <a href="<?php echo Util::caminho_projeto() ?>/servicos" title="Continuar orçando" class="btn btn-primary">
                          Continuar orçando
                        </a>
                      </div>
                    </div>
                </div>

          </div>
      </div>
    <?php
    }
    ?>
    <!-- servicos desejavel -->








      <!-- servicos desejavel -->
        <div class="container">
            <div class="row top30">
                <div class="col-xs-12 top30 bottom10">
                    <div class="descricao-orcamento">
                        <h3>CONFIRME SEUS DADOS</h3>
                    </div>
                </div>

                <!-- formulario orcamento -->
                <div class="FormContato col-xs-8 bottom100">
                    <div class="row">
                      <div class="col-xs-6 form-group  top20">
                      <label class="glyphicon glyphicon-user"> <span>Nome</span></label>
                      <input type="text" name="nome" class="form-control fundo-form1 input100" placeholder="">
                    </div>

                    <div class="col-xs-6 form-group  top20">
                      <label class="glyphicon glyphicon-user"> <span>E-mail</span></label>
                      <input type="text" name="email" class="form-control fundo-form1 input100" placeholder="">
                    </div>
                    </div>

                    <div class="row">
                      <div class="col-xs-6 form-group top20">
                      <label class="glyphicon glyphicon-earphone"> <span>Telefone</span></label>
                      <input type="text" name="telefone" class="form-control fundo-form1 input100" placeholder="">
                    </div>


                    <div class="col-xs-6 form-group  top20">
                    <label class="glyphicon glyphicon-earphone"> <span>Celular</span></label>
                    <input type="text" name="celular" class="form-control fundo-form1 input100" placeholder="">
                  </div>


                    </div>

                    <div class="row">
                       <div class="col-xs-6 form-group  top20">
                         <label <i class="fa fa-globe"></i> <span>Bairro</span></label>
                         <input type="text" name="bairro" class="form-control fundo-form1 input100" placeholder="">
                       </div>


                      <div class="col-xs-6 form-group  top20">
                        <label <i class="fa fa-globe"></i> <span>Cidade</span></label>
                        <input type="text" name="cidade" class="form-control fundo-form1 input100" placeholder="">
                      </div>



                   </div>

                   <div class="row">
                       <div class="col-xs-6 form-group  top20">
                          <label <i class="fa fa-globe"></i><span>Estado</span></label>
                           <input type="text" name="estado" class="form-control fundo-form1 input100" placeholder="">
                       </div>

                       <div class="col-xs-6 form-group  top20">
                          <label class="glyphicon glyphicon-star"> <span>Como conheceu nosso site?</span></label>
                         <select class="form-control input100 top5" name="como_conheceu" >
                          <option value="">Selecione</option>
                          <option>Google</option>
                          <option>Jornais</option>
                          <option>Revistas</option>
                          <option>Sites</option>
                          <option>Fórum</option>
                          <option>Notícias</option>
                          <option>Outros</option>
                        </select>
                       </div>

                   </div>

                    <div class="row">
                      <div class="col-xs-12 form-group  top20">
                      <label class="glyphicon glyphicon-pencil"> <span>Sua Mensagem</span></label>
                      <textarea name="mensagem" id="" cols="30" rows="8" class="form-control  fundo-form1 input100" placeholder=""></textarea>
                    </div>
                    </div>

                    <div class="clearfix"></div>

                    <div class="text-right  top30">
                      <button type="submit" class="btn btn-zul" name="btn_contato">
                        ENVIAR
                      </button>
                    </div>
              </div>
                <!-- formulario orcamento -->

                <!-- pesquisas orcamentos -->
                <div class="col-xs-4 pesquisa-orcamento top20">
                  <p>Tem interesse em receber orçamento de outros produtos:</p>

                  <label class="radio-inline">
                    <input type="radio" name="receber_orcamento" value="SIM"> SIM
                  </label>
                  <label class="radio-inline">
                    <input type="radio" name="receber_orcamento"  value="NÃO"> NÃO
                  </label>
                  

                  <p>Se sim, assinale abaixo</p>


                  <?php
                  $result = $obj_site->select("tb_categorias_produtos");
                  if(mysql_num_rows($result) > 0)
                  {
                    while($row = mysql_fetch_array($result))
                    {
                    ?>
                    <div class="checkbox col-xs-12">
                      <label>
                        <input type="checkbox" name="categorias[]" value="<?php Util::imprime($row[titulo]) ?>">
                        <?php Util::imprime($row[titulo]) ?>
                      </label>
                    </div>
                    <?php
                    }
                }
                ?>

                    <div class="top20"></div>

                </div>
                <!-- pesquisas orcamentos -->

         </div>
       </div>
      <!-- confirme seus dados -->

    </form>


    </div>
</div>

<!-- rodape -->
<?php require_once('./includes/rodape.php') ?>
<!-- rodape -->

</body>
</html>

<script>
  $(document).ready(function() {
    $('.FormContato').bootstrapValidator({
      message: 'This value is not valid',
      feedbackIcons: {
        valid: 'glyphicon glyphicon-ok',
        invalid: 'glyphicon glyphicon-remove',
        validating: 'glyphicon glyphicon-refresh'
      },
      fields: {
       nome: {
        validators: {
          notEmpty: {

          }
        }
      },
      email: {
        validators: {
          notEmpty: {

          },
          emailAddress: {
            message: 'Esse endereço de email não é válido'
          }
        }
      },
      telefone: {
        validators: {
          notEmpty: {

          }
        }
      },
      tipo_pessoa: {
        validators: {
          notEmpty: {

          }
        }
      },
      cidade: {
        validators: {
          notEmpty: {

          }
        }
      },
      estado: {
        validators: {
          notEmpty: {

          }
        }
      },
      bairro: {
        validators: {
          notEmpty: {

          }
        }
      },
      como_conheceu: {
        validators: {
          notEmpty: {

          }
        }
      },
      mensagem: {
        validators: {
          notEmpty: {

          }
        }
      }
    }
  });
  });
</script>








<!-- ---- LAYER SLIDER ---- -->
<link rel="stylesheet" href="<?php echo Util::caminho_projeto() ?>/jquery/touchcarousel/touchcarousel.css"/>
<link rel="stylesheet" href="<?php echo Util::caminho_projeto() ?>/jquery/touchcarousel/black-and-white-skin/black-and-white-skin.css" />
<script src="<?php echo Util::caminho_projeto() ?>/jquery/touchcarousel/jquery.touchcarousel-1.2.min.js"></script>

<script type="text/javascript">
$(document).ready(function() {
	$("#carousel-gallery").touchCarousel({
		itemsPerPage: 1,
		scrollbar: true,
		scrollbarAutoHide: true,
		scrollbarTheme: "dark",
		pagingNav: false,
		snapToItems: true,
		scrollToLast: false,
		useWebkit3d: true,
		loopItems: true
	});
});
</script>
<!-- XXXX LAYER SLIDER XXXX -->




