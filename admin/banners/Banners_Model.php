<?php
ob_start();
session_start();


class Banners_Model extends Dao
{

	private $nome_tabela = "tb_banners";
	private $chave_tabela = "idbanner";
	public $obj_imagem;



	/*	==================================================================================================================	*/
	#	CONSTRUTOR DA CLASSE
	/*	==================================================================================================================	*/
	public function __construct()
	{
		$this->obj_imagem = new Imagem();
		parent::__construct();
	}



	#-----------------------------------------------------------------------------------------------------------------------#
	#	GERA UM SELECT COM TODAS AS UFS
	#-----------------------------------------------------------------------------------------------------------------------#
	public function tipo_banners($name, $valor, $class = '', $titulo = "Selecione", $complemento_javascript = "")
	{
		  $obj_dao = new Dao();
		  //lista de ufs
		  $valor = trim($valor);
		  $lista = array(
						array('id' => '1', 'titulo' => "Index - Banner Principal - Imagem deve ser 1917x992px"),
						array('id' => '2', 'titulo' => "Mobile Index - Imagem deve ser 480x600px"),
						);
		echo '<select name="'.$name.'" id="'.$name.'" class="'.$class.'">';
		echo "<option value=''>$titulo</option>";
		for($i=0; $i < count($lista); $i++)
		{
			//verica se há na lista e check em caso afirmativo
			if($valor == $lista[$i][id])
			{
				?>
				   <option value="<?php Util::imprime($lista[$i][id]); ?>" selected="selected"><?php Util::imprime($lista[$i][titulo]); ?></option>
				<?php
			}
			else
			{
				?>
				<option value="<?php Util::imprime($lista[$i][id]); ?>"><?php Util::imprime($lista[$i][titulo]); ?></option>
				<?php
			}
		}
		echo '</select>';
	}



	/*	==================================================================================================================	*/
	/*	FORMULARIO	*/
	/*	==================================================================================================================	*/
	public function formulario($dados, $tipo_action = "")
	{

		$obj_site = new Site();
		$obj_jquery = new Biblioteca_Jquery();
	?>
    	<!--	====================================================================================================	 -->
        <!--	MASCARAS	-->
        <!--	====================================================================================================	 -->
        <script language="javascript">
			$(document).ready(function() {
				jQuery(function($){
				   $("#data").mask("99/99/9999");

				   //$("#preco_pequena, #preco_media, #preco_grande").maskMoney({showSymbol:false, symbol:"R$", decimal:",", thousands:"."});

				});
			});
		</script>



        <div class="class-form-1">
            <ul>
            	<li>
	            	<p>Tipo de Banner *</p>
	                <?php $this->tipo_banners('tipo_banner', $dados[tipo_banner]) ?>
	            </li>
	            <li>
	            	<p>Título *</p>
	                <input type="text" id="titulo" name="titulo" value="<?php Util::imprime($dados[titulo]) ?>" class="validate(required)"/>
	            </li>

	            <li>
	            	<p>Legenda *</p>
	                <input type="text" id="legenda" name="legenda" value="<?php Util::imprime($dados[legenda]) ?>" class="validate(required)"/>
	            </li>
	            
	            <li>
	            	<p>Url</p>
	                <input type="text" id="url" name="url" value="<?php Util::imprime($dados[url]) ?>" class="validate(url)"/>
	            </li>
	           
	            
                <li>
                	<p>
                    	Imagem: *<br />
                    	<span></span>
                    </p>
                    <input name="imagem" type="file" id="imagem" />
                    <?php
						if(!empty($dados[imagem]) and file_exists("../../uploads/$dados[imagem]")){
					?>
                    <div style="padding:10px; margin:10px 0; background:#EFEFEF;">
                    	<p>Imagem atual</p>
                    	<img src="<?php echo Util::caminho_projeto(); ?>/uploads/<?php Util::imprime($dados[imagem]); ?>" style="max-width:100%;" />
                    </div>
                    <?php
						}
					?>
                </li>
            </ul>
        </div>

    <?php
	}


	/*	==================================================================================================================	*/
	/*	EFETUA O CADASTRO	*/
	/*	==================================================================================================================	*/
	public function cadastra($dados)
	{

		//	VERIFICO SE E PARA CRIAR A IMAGEM DA CAPA
		if($_FILES[imagem][name] != "")
		{
			//	EFETUO O UPLOAD DA IMAGEM
			$dados[imagem] = Util::upload_arquivo("../../uploads", $_FILES[imagem]);
		}

		//	CADASTRA
		$id = parent::insert($this->nome_tabela, $dados);

		Util::script_msg("Cadastro efetuado com sucesso.");
		Util::script_location(dirname($_SERVER['SCRIPT_NAME'])."/cadastra.php");

	}



	/*	==================================================================================================================	*/
	/*	EFETUA A ALTERACAO	*/
	/*	==================================================================================================================	*/
	public function altera($dados)
	{


		//	VERIFICO SE E PARA CRIAR A IMAGEM DA CAPA
		if($_FILES[imagem][name] != "")
		{
			//	EFETUO O UPLOAD DA IMAGEM
			$dados[imagem] = Util::upload_arquivo("../../uploads", $_FILES[imagem]);
		}

		parent::update($this->nome_tabela, $dados[id], $dados);

		//	CADASTRA OS LOCAIS DE ATENDIMENTO
		header("location: index.php");

	}







		#-------------------------------------------------------------------------------------------------#
		#	ALTERO OS DADOS DA TABELA
		#-------------------------------------------------------------------------------------------------#
		private function atualiza_nome_imagem_tabela($id, $coluna, $nome_imagem)
		{
			//	ATUALIZO OS DADOS NA TABELA
			$sql = "UPDATE ". $this->nome_tabela ." SET $coluna = '$nome_imagem' WHERE ". $this->chave_tabela ." = '$id' ";
			parent::executaSQL($sql);
		}


	#-------------------------------------------------------------------------------------------------#
	#	ATUALIZA A ORDEM DO REGISTRO
	#-------------------------------------------------------------------------------------------------#
	public function atualiza_ordem_registro($id, $ordem)
	{
		$sql = "UPDATE ". $this->nome_tabela ." SET ordem = '$ordem' WHERE ". $this->chave_tabela ." = '$id' ";
		parent::executaSQL($sql);
	}





	/*	==================================================================================================================	*/
	/*	ATIVA OU DESATIVA	*/
	/*	==================================================================================================================	*/
	public function ativar_desativar($id, $ativo)
	{
		if($ativo == "SIM")
		{
			$sql = "UPDATE " . $this->nome_tabela. " SET ativo = 'NAO' WHERE " . $this->chave_tabela. " = '$id'";
			parent::executaSQL($sql);

			//	ARMAZENA O LOG
			parent::armazena_log("tb_logs_logins", "DESATIVOU O LOGIN $id", $sql, $_SESSION[login][idlogin]);
		}
		else
		{
			$sql = "UPDATE " . $this->nome_tabela. " SET ativo = 'SIM' WHERE " . $this->chave_tabela. " = '$id'";
			parent::executaSQL($sql);

			//	ARMAZENA O LOG
			parent::armazena_log("tb_logs_logins", "ATIVOU O LOGIN $id", $sql, $_SESSION[login][idlogin]);
		}

	}




	/*	==================================================================================================================	*/
	/*	EXCLUI	*/
	/*	==================================================================================================================	*/
	public function excluir($id)
	{
		//	BUSCA OS DADOS
		$row = $this->select($id);

		$sql = "DELETE FROM " . $this->nome_tabela. " WHERE " . $this->chave_tabela. " = '$id'";
		parent::executaSQL($sql);

		//	ARMAZENA O LOG
		parent::armazena_log("tb_logs_logins", "EXCLUSÃO DO LOGIN $id, NOME: $row[nome], Email: $row[email]", $sql, $_SESSION[login][idlogin]);
	}





	/*	==================================================================================================================	*/
	/*	BUSCA OS DADOS	*/
	/*	==================================================================================================================	*/
	public function select($id = "")
	{
		if($id != "")
		{
			$sql = "
					SELECT
						*
					FROM
						" . $this->nome_tabela. "
					WHERE
						" . $this->chave_tabela. " = '$id'
					";
			return mysql_fetch_array(parent::executaSQL($sql));
		}
		else
		{
			$sql = "
				SELECT
					*
				FROM
					" . $this->nome_tabela. "
				ORDER BY
					titulo
				";
			return parent::executaSQL($sql);
		}

	}







}

?>
