<?php
require_once("../class/Include.class.php");
require_once("login/Login_Model.php");
$obj_login = new Login_Model();







?>
<!doctype html>
<html>


<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../../../favicon.ico">

    <title>Signin Template for Bootstrap</title>

    <!-- Bootstrap core CSS -->
    <link href="http://getbootstrap.com/docs/4.1/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="http://getbootstrap.com/docs/4.1/examples/sign-in/signin.css" rel="stylesheet">
  </head>





<body class="text-center">


    


    <form class="form-signin" action="" method="post">


      <?php 
      if(isset($_POST['email'])){
          if($obj_login->recupera_senha($_POST['email']) == true){
            ?>
            <div class="alert alert-success" role="alert">
              Sua nova senha foi enviado para o e-mail cadastrado.
              <br>
              <a href="index.php">Clique aqui para efetuar seu login.</a>
            </div>
          
          <?php }else{ ?>
          
            <div class="alert alert-danger" role="alert">
               Não foi possível recuperar sua senha. E-mail não encontrado.
            </div>
            <?php
          }
          
      }
      ?>

      <h1 class="h3 mb-3 font-weight-normal">Recuperar senha</h1>
      <p class="mt-1 mb-12 text-muted">Informe o dado solicitado e clique em enviar para receber uma nova senha.</p>
      <label for="inputEmail" class="sr-only">Email address</label>
      <input type="email" id="email" name="email" class="form-control" placeholder="Digite seu email" required autofocus>
      <button class="btn btn-lg btn-primary btn-block mt-3 mb-3"  type="submit">Enviar</button>
      <a class="btn btn-lg btn-secondary btn-block" href="index.php">Voltar</a>
    </form>
  </body>



</html>
