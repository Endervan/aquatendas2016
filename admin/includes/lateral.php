<div id="container-lateral">
   <div id="container-lateral-logo">
     <a href="<?php echo Util::caminho_projeto() ?>/admin/inicial.php" style="text-align: center">
       <img src="<?php echo Util::caminho_projeto() ?>/imgs/logo.png" alt="">
     </a>
   </div>
   <!-- MENU LATERAL -->
   <div id="container-lateral-menu">
      <ul>
          <!-- item menu -->
          <li>
             <div class="holder-topo-menu-1">
                <h1>
                   Configurações
                </h1>
             </div>
             <ul>
                <li>
                   <a href="<?php echo Util::caminho_projeto() ?>/admin/info-pagina/altera.php?action=alterar&id=1" title="Listar">                                Listar                            </a>
                </li>
             </ul>
          </li>
          <!-- item menu -->

          <!-- item menu -->
          <li>
             <div class="holder-topo-menu-1">
                <h1>
                   Banners
                </h1>
             </div>
             <ul>
                <li>
                   <a href="<?php echo Util::caminho_projeto() ?>/admin/banners" title="Listar">                                    Listar                                </a>
                </li>
                <li>
                   <a href="<?php echo Util::caminho_projeto() ?>/admin/banners/cadastra.php"
                   title="Cadastrar">                                    Cadastrar                                </a>
                </li>
             </ul>
          </li>
          <!-- item menu -->


          <!-- item menu -->
          <li>
             <div class="holder-topo-menu-1">
                <h1>
                   Empresa
                </h1>
             </div>
             <ul>
                <li>
                   <a href="<?php echo Util::caminho_projeto() ?>/admin/empresa" title="Listar">                                Listar                            </a>
                </li>
             </ul>
          </li>
          <!-- item menu -->


          <!-- item menu -->
          <li>
             <div class="holder-topo-menu-1">
                <h1>
                   Depoimentos
                </h1>
             </div>
             <ul>
                <li>
                   <a href="<?php echo Util::caminho_projeto() ?>/admin/depoimentos" title="Listar">                                    Listar                                </a>
                </li>
                <li>
                   <a href="<?php echo Util::caminho_projeto() ?>/admin/depoimentos/cadastra.php"
                   title="Cadastrar">                                    Cadastrar                                </a>
                </li>
             </ul>
          </li>
          <!-- item menu -->


          <!-- item menu -->
          <li>
             <div class="holder-topo-menu-1">
                <h1>
                   Parceiros
                </h1>
             </div>
             <ul>
                <li>
                   <a href="<?php echo Util::caminho_projeto() ?>/admin/parceiros" title="Listar">                                    Listar                                </a>
                </li>
                <li>
                   <a href="<?php echo Util::caminho_projeto() ?>/admin/parceiros/cadastra.php"
                   title="Cadastrar">                                    Cadastrar                                </a>
                </li>
             </ul>
          </li>
          <!-- item menu -->


          <!-- item menu -->
          <li>
             <div class="holder-topo-menu-1">
                <h1>
                   Categoria do Produto
                </h1>
             </div>
             <ul>
                <li>
                   <a href="<?php echo Util::caminho_projeto() ?>/admin/categorias_produtos"
                   title="Listar">                                    Listar                                </a>
                </li>
                <li>
                   <a href="<?php echo Util::caminho_projeto() ?>/admin/categorias_produtos/cadastra.php"
                   title="Cadastrar">                                    Cadastrar                                </a>
                </li>
             </ul>
          </li>
          <!-- item menu -->


          <!-- item menu -->
          <li>
             <div class="holder-topo-menu-1">
                <h1>
                   Produtos
                </h1>
             </div>
             <ul>
                <li>
                   <a href="<?php echo Util::caminho_projeto() ?>/admin/produtos" title="Listar">                                    Listar                                </a>
                </li>
                <li>
                   <a href="<?php echo Util::caminho_projeto() ?>/admin/produtos/cadastra.php"
                   title="Cadastrar">                                    Cadastrar                                </a>
                </li>
             </ul>
          </li>
          <!-- item menu -->

          <!-- item menu -->
          <li>
             <div class="holder-topo-menu-1">
                <h1>
                   Avaliação de Produtos
                </h1>
             </div>
             <ul>
                <li>
                   <a href="<?php echo Util::caminho_projeto() ?>/admin/avaliacao-produtos" title="Listar">                                    Listar                                </a>
                </li>
                <li>
                   <a href="<?php echo Util::caminho_projeto() ?>/admin/avaliacao-produtos/cadastra.php"
                   title="Cadastrar">                                    Cadastrar                                </a>
                </li>
             </ul>
          </li>
          <!-- item menu -->

          <!-- item menu -->
          <li>
             <div class="holder-topo-menu-1">
                <h1>
                   Serviços
                </h1>
             </div>
             <ul>
                <li>
                   <a href="<?php echo Util::caminho_projeto() ?>/admin/servicos" title="Listar">                                    Listar                                </a>
                </li>
                <li>
                   <a href="<?php echo Util::caminho_projeto() ?>/admin/servicos/cadastra.php"
                   title="Cadastrar">                                    Cadastrar                                </a>
                </li>
             </ul>
          </li>
          <!-- item menu -->

          <!-- item menu -->
          <li>
             <div class="holder-topo-menu-1">
                <h1>
                   Dicas
                </h1>
             </div>
             <ul>
                <li>
                   <a href="<?php echo Util::caminho_projeto() ?>/admin/dicas" title="Listar">                                    Listar                                </a>
                </li>
                <li>
                   <a href="<?php echo Util::caminho_projeto() ?>/admin/dicas/cadastra.php"
                   title="Cadastrar">                                    Cadastrar                                </a>
                </li>
             </ul>
          </li>
          <!-- item menu -->


          <!-- item menu -->
          <li>
             <div class="holder-topo-menu-1">
                <h1>
                   Comentários Dicas
                </h1>
             </div>
             <ul>
                <li>
                   <a href="<?php echo Util::caminho_projeto() ?>/admin/comentarios-dicas" title="Listar">                                    Listar                                </a>
                </li>
                <li>
                   <a href="<?php echo Util::caminho_projeto() ?>/admin/comentarios-dicas/cadastra.php"
                   title="Cadastrar">                                    Cadastrar                                </a>
                </li>
             </ul>
          </li>
          <!-- item menu -->


          <!-- item menu -->
          <li>
             <div class="holder-topo-menu-1">
                <h1>
                   Portfólio
                </h1>
             </div>
             <ul>
                <li>
                   <a href="<?php echo Util::caminho_projeto() ?>/admin/portifolio" title="Listar">                                    Listar                                </a>
                </li>
                <li>
                   <a href="<?php echo Util::caminho_projeto() ?>/admin/portifolio/cadastra.php"
                   title="Cadastrar">                                    Cadastrar                                </a>
                </li>
             </ul>
          </li>
          <!-- item menu -->


          <!-- item menu -->
          <li>
             <div class="holder-topo-menu-1">
                <h1>
                   Piscinas de vinil
                </h1>
             </div>
             <ul>
                <li>
                   <a href="<?php echo Util::caminho_projeto() ?>/admin/piscina_vinil" title="Listar">                                    Listar                                </a>
                </li>
                <li>
                   <a href="<?php echo Util::caminho_projeto() ?>/admin/piscina_vinil/cadastra.php"
                   title="Cadastrar">                                    Cadastrar                                </a>
                </li>
             </ul>
          </li>
          <!-- item menu -->





      </ul>
   </div>
   <!-- MENU LATERAL -->
</div>
