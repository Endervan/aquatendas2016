<?php
ob_start();
session_start();


class Informacao_Model extends Dao
{

  private $nome_tabela = "tb_configuracoes";
  private $chave_tabela = "idconfiguracao";
  public $obj_imagem;


  /*	==================================================================================================================	*/
  #	CONSTRUTOR DA CLASSE
  /*	==================================================================================================================	*/
  public function __construct()
  {
    $this->obj_imagem = new Imagem();
    parent::__construct();
  }



  /*	==================================================================================================================	*/
  /*	FORMULARIO	*/
  /*	==================================================================================================================	*/
  public function formulario($dados)
  {
    ?>

    <input style="display: none;" type="text" name="endereco" value="<?php Util::imprime($dados[endereco]) ?>" class="form-control fundo-form1 input100" >


    <div class="col-xs-12">
      <div class="alert alert-info">
        TIPO DE SITE
      </div>  
    </div> 

    <div class="col-xs-12 form-group ">
      <label> <span></span></label>
      <?php Util::tipo_site('tipo_site', $dados[tipo_site], 'form-control fundo-form1 input100') ?>
    </div>



    <?php if($dados[tipo_site] == 'carrinho'){ ?>

      <div class="col-xs-12">
        <div class="alert alert-info">
          OPÇÕES DE PAGAMENTO <span></span>
        </div>  
      </div>  
      
      

      <div class="col-xs-4 form-group ">
        <label>PagSeguro <span> Informe o email</span></label>
        <input type="text" name="email_pagseguro" value="<?php Util::imprime($dados[email_pagseguro]) ?>" class="form-control fundo-form1 input100" >
      </div>  

      <div class="col-xs-4 form-group ">
        <label>PayPal <span> Informe o código</span></label>
        <input type="text" name="codigo_paypal" value="<?php Util::imprime($dados[codigo_paypal]) ?>" class="form-control fundo-form1 input100" >
      </div>  
        

      <div class="col-xs-4 form-group ">
        <label>Pagamento com Máquina no local <span></span></label>
        <?php Util::get_sim_nao('pagamento_maquina', $dados[pagamento_maquina], 'form-control fundo-form1 input100'); ?>
      </div>  

    <?php } ?>



    <div class="col-xs-12">
      <div class="alert alert-info">
        MENU
      </div>  
    </div> 


    <div class="col-xs-2 form-group ">
      <label>MENU 1 <span>HOME</span> </label>
      <input type="text" name="menu_1" value="<?php Util::imprime($dados[menu_1]) ?>" class="form-control fundo-form1 input100" >
    </div>

    <div class="col-xs-2 form-group ">
      <label>MENU 2 <span>EMPRESA</span> </label>
      <input type="text" name="menu_2" value="<?php Util::imprime($dados[menu_2]) ?>" class="form-control fundo-form1 input100" >
    </div>

    <div class="col-xs-2 form-group ">
      <label>MENU 3 <span>PRODUTOS</span> </label>
      <input type="text" name="menu_3" value="<?php Util::imprime($dados[menu_3]) ?>" class="form-control fundo-form1 input100" >
      <label>MENU 3 - Singular</label>
      <input type="text" name="menu_3_singular" value="<?php Util::imprime($dados[menu_3_singular]) ?>" class="form-control fundo-form1 input100" >
    </div>

    <div class="col-xs-2 form-group ">
      <label>MENU 4 <span>SERVIÇOS</span> </label>
      <input type="text" name="menu_4" value="<?php Util::imprime($dados[menu_4]) ?>" class="form-control fundo-form1 input100" >
      <label>MENU 4 - Singular</label>
      <input type="text" name="menu_4_singular" value="<?php Util::imprime($dados[menu_4_singular]) ?>" class="form-control fundo-form1 input100" >
    </div>

    <div class="col-xs-2 form-group ">
      <label>MENU 5 <span>DICAS</span> </label>
      <input type="text" name="menu_5" value="<?php Util::imprime($dados[menu_5]) ?>" class="form-control fundo-form1 input100" >
      <label>MENU 5 - Singular</label>
      <input type="text" name="menu_5_singular" value="<?php Util::imprime($dados[menu_5_singular]) ?>" class="form-control fundo-form1 input100" >
    </div>

    <div class="col-xs-2 form-group ">
      <label>MENU 6 <span>CONTATO</span> </label>
      <input type="text" name="menu_6" value="<?php Util::imprime($dados[menu_6]) ?>" class="form-control fundo-form1 input100" >
    </div>


    <div class="clearfix "></div>

    <div class="col-xs-2 form-group ">
      <label>MENU 7 <span>ORÇAMENTO</span> </label>
      <input type="text" name="menu_7" value="<?php Util::imprime($dados[menu_7]) ?>" class="form-control fundo-form1 input100" >
    </div>


    <div class="col-xs-3 form-group ">
      <label>MENU 8 <span>TÍTULO EXTERNO</span> </label>
      <input type="text" name="menu_8_titulo" value="<?php Util::imprime($dados[menu_8_titulo]) ?>" class="form-control fundo-form1 input100" >
    </div>

    <div class="col-xs-7 form-group ">
      <label>MENU 8 <span>LINK EXTERNO</span> </label>
      <input type="text" name="menu_8" value="<?php Util::imprime($dados[menu_8]) ?>" class="form-control fundo-form1 input100" >
    </div>


    <div class="col-xs-12">
      <div class="alert alert-info">
        ÍCONES DO MENU <span>Devem ser retirados do site https://fontawesome.com/v4.7.0/icons/</span>
      </div>  
    </div> 

    <div class="col-xs-2 form-group ">
      <label>MENU 1</label>
      <input type="text" name="icon_menu_mobile_1" value="<?php Util::imprime($dados[icon_menu_mobile_1]) ?>" class="form-control fundo-form1 input100" >
    </div>

    <div class="col-xs-2 form-group ">
      <label>MENU 2</label>
      <input type="text" name="icon_menu_mobile_2" value="<?php Util::imprime($dados[icon_menu_mobile_2]) ?>" class="form-control fundo-form1 input100" >
    </div>

    <div class="col-xs-2 form-group ">
      <label>MENU 3</label>
      <input type="text" name="icon_menu_mobile_3" value="<?php Util::imprime($dados[icon_menu_mobile_3]) ?>" class="form-control fundo-form1 input100" >
    </div>

    <div class="col-xs-2 form-group ">
      <label>MENU 4</label>
      <input type="text" name="icon_menu_mobile_4" value="<?php Util::imprime($dados[icon_menu_mobile_4]) ?>" class="form-control fundo-form1 input100" >
    </div>

    <div class="col-xs-2 form-group ">
      <label>MENU 5</label>
      <input type="text" name="icon_menu_mobile_5" value="<?php Util::imprime($dados[icon_menu_mobile_5]) ?>" class="form-control fundo-form1 input100" >
    </div>

    <div class="col-xs-2 form-group ">
      <label>MENU 6</label>
      <input type="text" name="icon_menu_mobile_6" value="<?php Util::imprime($dados[icon_menu_mobile_6]) ?>" class="form-control fundo-form1 input100" >
    </div>

    <div class="col-xs-2 form-group ">
      <label>MENU 7</label>
      <input type="text" name="icon_menu_mobile_7" value="<?php Util::imprime($dados[icon_menu_mobile_7]) ?>" class="form-control fundo-form1 input100" >
    </div>



    <div class="col-xs-12">
      <div class="alert alert-info">
        TEXTO BOTÕES
      </div>  
    </div> 

    <div class="col-xs-6 form-group ">
      <label>ADICIONAR AO ORÇAMENTO <span>Menu 3 ( produtos )</span></label>
      <input type="text" name="btn_adicionar_orcamento" value="<?php Util::imprime($dados[btn_adicionar_orcamento]) ?>" class="form-control fundo-form1 input100" >
    </div>

    <div class="col-xs-6 form-group ">
      <label>ADICIONAR AO ORÇAMENTO <span>Menu 4 ( Serviços )</span></label>
      <input type="text" name="btn_adicionar_orcamento_2" value="<?php Util::imprime($dados[btn_adicionar_orcamento_2]) ?>" class="form-control fundo-form1 input100" >
    </div>


    

    <div class="col-xs-12">
      <div class="alert alert-info">
        IMAGENS DESKTOP
      </div>  
    </div> 

    <div class="col-xs-3 form-group ">
			<label>Logomarca Desktop <span></span></label>
			<?php
			if(!empty($dados[logomarca_desktop])){
				?>
				<input type="file" name="logomarca_desktop" id="logomarca_desktop" value="<?php Util::imprime($dados[logomarca_desktop]) ?>" class="form-control fundo-form1 input100" />
				<label>Atual:</label>
				<div class="clearfix"></div>
				<img class="col-xs-10" src="<?php echo Util::caminho_projeto(); ?>/uploads/<?php Util::imprime($dados[logomarca_desktop]) ?>" />
			<?php } else { ?>
				<input type="file" name="logomarca_desktop" id="logomarca_desktop" value="<?php Util::imprime($dados[logomarca_desktop]) ?>" class="form-control fundo-form1 input100" />
			<?php } ?>
    </div>


    <div class="col-xs-2 form-group ">
      <label>Altura <span></span></label>
      <input type="number" min="10" name="logo_desktop_height" value="<?php Util::imprime($dados[logo_desktop_height]) ?>" class="form-control fundo-form1 input100" >
    </div>      

    <div class="col-xs-2 form-group ">
      <label>Largura <span></span></label>
      <input type="number" min="10" name="logo_desktop_width" value="<?php Util::imprime($dados[logo_desktop_width]) ?>" class="form-control fundo-form1 input100" >
    </div>  


    <div class="col-xs-2 form-group ">
      <label>Margin Top <span></span></label>
      <input type="number" name="logo_desktop_mt" value="<?php Util::imprime($dados[logo_desktop_mt]) ?>" class="form-control fundo-form1 input100" >
    </div>  


    <div class="col-xs-2 form-group ">
      <label>Margin Left <span></span></label>
      <input type="number" name="logo_desktop_ml" value="<?php Util::imprime($dados[logo_desktop_ml]) ?>" class="form-control fundo-form1 input100" >
    </div>  


        <div class="clearfix"></div>
        <div class="clearfix top20"></div>



    <div class="col-xs-3 form-group ">
			<label>Home Background Empresa <span>382x370px</span></label>
			<?php
			if(!empty($dados[bg_empresa_home])){
				?>
				<input type="file" name="bg_empresa_home" id="bg_empresa_home" value="<?php Util::imprime($dados[bg_empresa_home]) ?>" class="form-control fundo-form1 input100" />
				<label>Atual:</label>
				<div class="clearfix"></div>
				<img class="col-xs-5" src="<?php echo Util::caminho_projeto(); ?>/uploads/<?php Util::imprime($dados[bg_empresa_home]) ?>" />
			<?php } else { ?>
				<input type="file" name="bg_empresa_home" id="bg_empresa_home" value="<?php Util::imprime($dados[bg_empresa_home]) ?>" class="form-control fundo-form1 input100" />
			<?php } ?>
    </div>
    
   
    <div class="col-xs-4 form-group pt0">
			<label>Background Página Empresa <span>1920x400px</span></label>
			<?php
			if(!empty($dados[bg_empresa])){
				?>
				<input type="file" name="bg_empresa" id="bg_empresa" value="<?php Util::imprime($dados[bg_empresa]) ?>" class="form-control fundo-form1 input100" />
				<label>Atual:</label>
				<div class="clearfix"></div>
				<img class="col-xs-5" src="<?php echo Util::caminho_projeto(); ?>/uploads/<?php Util::imprime($dados[bg_empresa]) ?>" />
			<?php } else { ?>
				<input type="file" name="bg_empresa" id="bg_empresa" value="<?php Util::imprime($dados[bg_empresa]) ?>" class="form-control fundo-form1 input100" />
			<?php } ?>
    </div>


      <div class="col-xs-12">
      <div class="alert alert-info">
        IMAGENS MOBILE
      </div>  
    </div> 


   

      <div class="col-xs-3 form-group pt0 ">
          <label>Logomarca <span></span></label>
          <?php
          if(!empty($dados[bg_mobile_paginas])){
              ?>
              <input type="file" name="bg_mobile_paginas" id="bg_mobile_paginas" value="<?php Util::imprime($dados[bg_mobile_paginas]) ?>" class="form-control fundo-form1 input100" />
              <label>Atual:</label>
              <div class="clearfix"></div>
              <img class="col-xs-10" src="<?php echo Util::caminho_projeto(); ?>/uploads/<?php Util::imprime($dados[bg_mobile_paginas]) ?>" />
          <?php } else { ?>
              <input type="file" name="bg_mobile_paginas" id="bg_mobile_paginas" value="<?php Util::imprime($dados[bg_mobile_paginas]) ?>" class="form-control fundo-form1 input100" />
          <?php } ?>
      </div>


    <div class="col-xs-2 form-group ">
      <label>Altura <span></span></label>
      <input type="number" min="10" name="logo_mobile_height" value="<?php Util::imprime($dados[logo_mobile_height]) ?>" class="form-control fundo-form1 input100" >
    </div>      

    <div class="col-xs-2 form-group ">
      <label>Largura <span></span></label>
      <input type="number" min="10" name="logo_mobile_wight" value="<?php Util::imprime($dados[logo_mobile_wight]) ?>" class="form-control fundo-form1 input100" >
    </div>  

    


    <div class="col-xs-2 form-group ">
      <label>Menu Margin Top <span></span></label>
      <input type="number" min="0" name="menu_mobile_mt" value="<?php Util::imprime($dados[menu_mobile_mt]) ?>" class="form-control fundo-form1 input100" >
    </div>      


     <div class="col-xs-3 form-group ">
      <label>Seta Voltar Margin Top <span></span></label>
      <input type="number" min="0" name="seta_voltar_mobile_mt" value="<?php Util::imprime($dados[seta_voltar_mobile_mt]) ?>" class="form-control fundo-form1 input100" >
    </div>  


    <div class="clearfix"></div> 



      <div class="col-xs-4 form-group pt0 top15 ">
          <label>Bg Index <span>360x550px</span></label>
          <?php
          if(!empty($dados[bg_mobile_index])){
              ?>
              <input type="file" name="bg_mobile_index" id="bg_mobile_index" value="<?php Util::imprime($dados[bg_mobile_index]) ?>" class="form-control fundo-form1 input100" />
              <label>Atual:</label>
              <div class="clearfix"></div>
              <img class="col-xs-10" src="<?php echo Util::caminho_projeto(); ?>/uploads/<?php Util::imprime($dados[bg_mobile_index]) ?>" />
          <?php } else { ?>
              <input type="file" name="bg_mobile_index" id="bg_mobile_index" value="<?php Util::imprime($dados[bg_mobile_index]) ?>" class="form-control fundo-form1 input100" />
          <?php } ?>
      </div>


     
  
  
   




    <script>
    $(document).ready(function() {
      $('.FormPrincipal').bootstrapValidator({
        message: 'This value is not valid',
        feedbackIcons: {
          valid: 'fa fa-check',
          invalid: 'fa fa-remove',
          validating: 'fa fa-refresh'
        },
        fields: {

          email: {
            validators: {
              notEmpty: {

              },
              emailAddress: {
                message: 'Esse endereço de email não é válido'
              }
            }
          },

          <?php if($dados[logomarca_desktop] == ''){ ?>
						logomarca_desktop: {
							validators: {
								notEmpty: {
									message: 'Por favor insira Uma Imagem'
								},
								file: {
									extension: 'png',
									maxSize: 1024*1024,   // 2 MB
									message: 'O arquivo selecionado não é valido, ele deve ser (PNG) e 2 MB no máximo.'
								}
							}
            },
            <?php } ?>


            <?php if($dados[tipo_site] == 'carrinho'){ ?>
              pagamento_maquina: {
                validators: {
                  notEmpty: {

                  }
                },
                email_pagseguro: {
                validators: {
                  notEmpty: {

                  },
                  emailAddress: {
                    message: 'Esse endereço de email não é válido'
                  }
                },
            <?php } ?>
            

            <?php if($dados[logomarca_mobile] == ''){ ?>
              logomarca_mobile: {
							validators: {
								notEmpty: {
									message: 'Por favor insira Uma Imagem'
								},
								file: {
									extension: 'png',
									maxSize: 1024*1024,   // 2 MB
									message: 'O arquivo selecionado não é valido, ele deve ser (PNG) e 2 MB no máximo.'
								}
							}
						},
            <?php } ?>



         
            logo_desktop_width: {
            validators: {
              notEmpty: {

              }
            },
            logo_desktop_height: {
            validators: {
              notEmpty: {

              }
            },

            logo_mobile_height: {
            validators: {
              notEmpty: {

              }
            },
            logo_mobile_wight: {
            validators: {
              notEmpty: {

              }
            },


          mensagem: {
            validators: {
              notEmpty: {

              }
            }
          }
        }
      });
    });
    </script>




    <?php
  }




  /*	==================================================================================================================	*/
  /*	FORMULARIO	*/
  /*	==================================================================================================================	*/
  public function formulario_email_smtp($dados)
  {
    ?>

    <div class="col-xs-12 bottom20">
      <h3>Configure o smtp para os email serem enviados pelo gmail.</h3>
    </div>



    <div class="col-xs-12 form-group ">
      <label>Email<span> Informe o email completo exemplo: email@gmail.com</span></label>
      <input type="text" name="email_gmail" id="email_gmail" value="<?php Util::imprime($dados[email_gmail]) ?>" class="form-control fundo-form1 input100"  />
    </div>


    <div class="col-xs-12 form-group ">
      <label>Senha</label>
      <input type="text" name="senha_gmail" id="senha_gmail" value="<?php Util::imprime($dados[senha_gmail]) ?>" class="form-control fundo-form1 input100"  />
    </div>






    <script>
    $(document).ready(function() {
      $('.FormPrincipal').bootstrapValidator({
        message: 'This value is not valid',
        feedbackIcons: {
          valid: 'fa fa-check',
          invalid: 'fa fa-remove',
          validating: 'fa fa-refresh'
        },
        fields: {

          email_gmail: {
            validators: {
              notEmpty: {

              },
              emailAddress: {
                message: 'Esse endereço de email não é válido'
              }
            }
          },

          senha_gmail: {
            validators: {
              notEmpty: {

              }
            }
          }
        }
      });
    });
    </script>




    <?php
  }







  /*	==================================================================================================================	*/
  /*	EFETUA CROP DA IMAGEM	*/
  /*	==================================================================================================================	*/
  public function efetua_crop_imagem($id, $nome_arquivo, $nome_campo, $tamanho_width_tumb, $tamanho_height_tumb, $url_retorno, $msg_sucesso, $tamanho_imagem = 593)
  {
    //	CRIO O CROP DA IMAGEM
    $nome_tabela = $this->nome_tabela;
    $idtabela = $this->chave_tabela;
    $this->obj_imagem->gera_imagem_crop($id, $nome_arquivo, $nome_tabela, $idtabela, $nome_campo, $tamanho_imagem, $tamanho_width_tumb, $tamanho_height_tumb, $url_retorno, $msg_sucesso);
  }



  /*	==================================================================================================================	*/
  /*	EFETUA O CADASTRO	*/
  /*	==================================================================================================================	*/
  public function cadastra($dados)
  {
    //	VERIFICO SE E PARA CRIAR A IMAGEM DA CAPA
    if($_FILES[logomarca_desktop][name] != "")
    {
      //	EFETUO O UPLOAD DA IMAGEM
      Util::upload_arquivo("../../uploads", $_FILES[logomarca_desktop], "3145728");
    }

    //	VERIFICO SE E PARA CRIAR A IMAGEM DA CAPA
    if($_FILES[logomarca_mobile][name] != "")
    {
      //	EFETUO O UPLOAD DA IMAGEM
      Util::upload_arquivo("../../uploads", $_FILES[logomarca_mobile], "3145728");
    }

      //	VERIFICO SE E PARA CRIAR A IMAGEM DA CAPA
      if($_FILES[bg_mobile_paginas][name] != "")
      {
          //	EFETUO O UPLOAD DA IMAGEM
          Util::upload_arquivo("../../uploads", $_FILES[bg_mobile_paginas], "3145728");
      }

      //	VERIFICO SE E PARA CRIAR A IMAGEM DA CAPA
      if($_FILES[bg_mobile_index][name] != "")
      {
          //	EFETUO O UPLOAD DA IMAGEM
          Util::upload_arquivo("../../uploads", $_FILES[bg_mobile_index], "3145728");
      }



       //	VERIFICO SE E PARA CRIAR A IMAGEM DA CAPA
       if($_FILES[bg_mobile_empresa][name] != "")
       {
           //	EFETUO O UPLOAD DA IMAGEM
           $dados[bg_mobile_empresa] =  Util::upload_arquivo("../../uploads", $_FILES[bg_mobile_empresa], "3145728");
       }

    //	CADASTRA O USUARIO
    $id = parent::insert($this->nome_tabela, $_POST);

    //	ARMAZENA O LOG
    parent::armazena_log("tb_logs_logins", "CADASTRO DO CLIENTE $dados[nome]", $sql, $_SESSION[login][idlogin]);


  



    Util::script_msg("Cadastro efetuado com sucesso.");
    Util::script_location(dirname($_SERVER['SCRIPT_NAME'])."/cadastra.php");
  }



  /*	==================================================================================================================	*/
  /*	EFETUA A ALTERACAO	*/
  /*	==================================================================================================================	*/
  public function altera($id, $dados)
  {

    //	VERIFICO SE E PARA CRIAR A IMAGEM DA CAPA
    if($_FILES[logomarca_desktop][name] != "")
    {
      //	EFETUO O UPLOAD DA IMAGEM
      $dados[logomarca_desktop] = Util::upload_arquivo("../../uploads", $_FILES[logomarca_desktop], "3145728");
    }

    //	VERIFICO SE E PARA CRIAR A IMAGEM DA CAPA
    if($_FILES[logomarca_mobile][name] != "")
    {
      //	EFETUO O UPLOAD DA IMAGEM
      $dados[logomarca_mobile] = Util::upload_arquivo("../../uploads", $_FILES[logomarca_mobile], "3145728");
    }

      //	VERIFICO SE E PARA CRIAR A IMAGEM DA CAPA
      if($_FILES[bg_mobile_paginas][name] != "")
      {
          //	EFETUO O UPLOAD DA IMAGEM
          $dados[bg_mobile_paginas] = Util::upload_arquivo("../../uploads", $_FILES[bg_mobile_paginas], "3145728");
      }


    //	VERIFICO SE E PARA CRIAR A IMAGEM DA CAPA
    if($_FILES[bg_empresa][name] != "")
    {
      //	EFETUO O UPLOAD DA IMAGEM
      $dados[bg_empresa] = Util::upload_arquivo("../../uploads", $_FILES[bg_empresa], "3145728");
    }


    //	VERIFICO SE E PARA CRIAR A IMAGEM DA CAPA
    if($_FILES[bg_empresa_home][name] != "")
    {
      //	EFETUO O UPLOAD DA IMAGEM
      $dados[bg_empresa_home] = Util::upload_arquivo("../../uploads", $_FILES[bg_empresa_home], "3145728");
    }

      //	VERIFICO SE E PARA CRIAR A IMAGEM DA CAPA
      if($_FILES[bg_mobile_index][name] != "")
      {
          //	EFETUO O UPLOAD DA IMAGEM
          $dados[bg_mobile_index] = Util::upload_arquivo("../../uploads", $_FILES[bg_mobile_index], "3145728");
      }


       //	VERIFICO SE E PARA CRIAR A IMAGEM DA CAPA
       if($_FILES[bg_mobile_empresa][name] != "")
       {
           //	EFETUO O UPLOAD DA IMAGEM
           $dados[bg_mobile_empresa] = Util::upload_arquivo("../../uploads", $_FILES[bg_mobile_empresa], "3145728");
       }

    parent::update($this->nome_tabela, $id, $dados);


    //	ARMAZENA O LOG
    parent::armazena_log("tb_logs_logins", "ALTERAÇÃO DO CLIENTE $dados[nome]", $sql, $_SESSION[login][idlogin]);



    Util::script_msg("Alterado com sucesso.");
    Util::script_location(dirname($_SERVER['SCRIPT_NAME'])."/index.php");
  }



  /*	==================================================================================================================	*/
  /*	ATIVA OU DESATIVA	*/
  /*	==================================================================================================================	*/
  public function ativar_desativar($id, $ativo)
  {
    if($ativo == "SIM")
    {
      $sql = "UPDATE " . $this->nome_tabela. " SET ativo = 'NAO' WHERE " . $this->chave_tabela. " = '$id'";
      parent::executaSQL($sql);

      //	ARMAZENA O LOG
      parent::armazena_log("tb_logs_logins", "DESATIVOU O LOGIN $id", $sql, $_SESSION[login][idlogin]);
    }
    else
    {
      $sql = "UPDATE " . $this->nome_tabela. " SET ativo = 'SIM' WHERE " . $this->chave_tabela. " = '$id'";
      parent::executaSQL($sql);

      //	ARMAZENA O LOG
      parent::armazena_log("tb_logs_logins", "ATIVOU O LOGIN $id", $sql, $_SESSION[login][idlogin]);
    }

  }




  /*	==================================================================================================================	*/
  /*	EXCLUI	*/
  /*	==================================================================================================================	*/
  public function excluir($id)
  {
    //	BUSCA OS DADOS
    $row = $this->select($id);

    $sql = "DELETE FROM " . $this->nome_tabela. " WHERE " . $this->chave_tabela. " = '$id'";
    parent::executaSQL($sql);

    //	ARMAZENA O LOG
    parent::armazena_log("tb_logs_logins", "EXCLUSÃO DO LOGIN $id, NOME: $row[nome], Email: $row[email]", $sql, $_SESSION[login][idlogin]);
  }







  /*	==================================================================================================================	*/
  /*	VERIFICO SE JA POSSUI O GRUPO CADASTRADO	*/
  /*	==================================================================================================================	*/
  public function verifica($email)
  {
    $sql = "SELECT * FROM " . $this->nome_tabela. " WHERE email = '$email'";
    return mysql_num_rows(parent::executaSQL($sql));
  }




  /*	==================================================================================================================	*/
  /*	VERIFICO SE JA POSSUI O GRUPO CADASTRADO QUANDO ALTERAR	*/
  /*	==================================================================================================================	*/
  public function verifica_altera($email, $id)
  {
    $sql = "SELECT * FROM " . $this->nome_tabela. " WHERE email = '$email' AND " . $this->chave_tabela. " <> '$id'";
    return mysql_num_rows(parent::executaSQL($sql));
  }





  /*	==================================================================================================================	*/
  /*	BUSCA OS DADOS	*/
  /*	==================================================================================================================	*/
  public function select($id = "")
  {
    if($id != "")
    {
      $sql = "
      SELECT
      *
      FROM
      " . $this->nome_tabela. "
      WHERE
      " . $this->chave_tabela. " = '$id'
      ";
      return mysql_fetch_array(parent::executaSQL($sql));
    }
    else
    {
      $sql = "
      SELECT
      *
      FROM
      " . $this->nome_tabela. "
      ORDER BY
      ordem desc
      ";
      return parent::executaSQL($sql);
    }

  }







}

?>
