<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();

// INTERNA
$url = $_GET[get1];

if (!empty($url)) {
    $complemento = "AND url_amigavel = '$url'";
}

$result = $obj_site->select("tb_dicas", $complemento);

if (mysql_num_rows($result) == 0) {
    Util::script_location(Util::caminho_projeto() . "/mobile/dicas/");
}

$dados_dentro = mysql_fetch_array($result);
// BUSCA META TAGS E TITLE
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];
?>


<!doctype html>
<html amp lang="pt-br">
<head>
    <?php require_once("../includes/head.php"); ?>
    <script async custom-element="amp-sidebar" src="https://cdn.ampproject.org/v0/amp-sidebar-0.1.js"></script>

    <style amp-custom>
        <?php require_once("../css/geral.css"); ?>
        <?php require_once("../css/topo_rodape.css"); ?>
        <?php require_once("../css/paginas.css");  //  ARQUIVO DA PAGINA ?>

        <?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 17); ?>
        .bg-interna {
            background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 60px center no-repeat;
            background-size: 100% 128px;
        }
    </style>

    <script async custom-element="amp-bind" src="https://cdn.ampproject.org/v0/amp-bind-0.1.js"></script>
    <script async custom-element="amp-image-lightbox"
            src="https://cdn.ampproject.org/v0/amp-image-lightbox-0.1.js"></script>


</head>

<body class="bg-interna">

<?php
$voltar_para = $obj_site->url_amigavel($config[menu_5]); // link de volta, exemplo produtos, dicas, servicos etc
require_once("../includes/topo.php") ?>


<?php
require_once("../includes/topo.php") ?>


<!-- ======================================================================= -->
<!-- TITULO PAGINA  -->
<!-- ======================================================================= -->
<div class="row">
    <div class="col-12  padding0 localizacao-pagina text-center">
        <h5><?php Util::imprime($banner[legenda_1]); ?></h5>
        <?php if (isset($banner[legenda_2])) : ?>
            <h6><?php Util::imprime($banner[legenda_2]); ?></h6 class="open">
        <?php endif; ?>
        <amp-img class="top10" src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/barra_titulo.png" alt=""
                 height="26" width="320"></amp-img>
    </div>
</div>
<!-- ======================================================================= -->
<!-- TITULO PAGINA  -->
<!-- ======================================================================= -->


<?php require_once("../includes/modulo_5_dentro.php") ?>


<?php require_once("../includes/veja.php") ?>


<?php require_once("../includes/rodape.php") ?>

</body>


</html>
