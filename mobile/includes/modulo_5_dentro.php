<div class="row">


    <div class="col-12 empresa_geral top20">
        <h4><?php echo Util::imprime($dados_dentro[titulo]) ?></h4>
    </div>

    <div class="col-12 top30">
        <div><p><?php echo Util::imprime($dados_dentro[descricao]) ?></p></div>
    </div>


    <div class="col-12 padding0 top30">
        <amp-img on="tap:lightbox20"
                 role="button"
                 tabindex="0"
                 src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($dados_dentro[imagem]) ?>"
                 alt="Home"
                 height="156"
                 layout="responsive"
                 width="360">
        </amp-img>
        <amp-image-lightbox id="lightbox20" layout="nodisplay"></amp-image-lightbox>
    </div>

    <div class="col-12 top30">
        <a class="btn btn-block padding0 btn_escuro"
           on="tap:my-lightbox444"
           role="a"
           tabindex="0">
           <i class="fa fa-phone mr-1" aria-hidden="true"></i>LIGAR AGORA
        </a>
    </div>


</div>