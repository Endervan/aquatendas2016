<div class="row bg_topo">
  <div class="col-2 " style="<?php if(!empty($config[seta_voltar_mobile_mt])){ echo 'margin-top:'.$config[seta_voltar_mobile_mt].'px;';  } ?>">
    <?php
    if(empty($voltar_para)){
      $link_topo = Util::caminho_projeto()."/mobile/";
    }else{
      $link_topo = Util::caminho_projeto()."/mobile/".$voltar_para;
    }
    ?>
    <a href="<?php echo $link_topo  ?>"><i class="fa fa-arrow-left fa-2x btn-topo" aria-hidden="true"></i></a>
  </div>
  <div class="col-8 top5 topo">
    <a href="<?php echo Util::caminho_projeto() ?>/mobile">
      <amp-img src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($config[bg_mobile_paginas]) ?>" alt="Home" height="<?php echo $config[logo_mobile_height] ?>" width="<?php echo $config[logo_mobile_wight] ?>" ></amp-img>
    </a>
  </div>
  <div class="col-2 text-right"  style="<?php if(!empty($config[menu_mobile_mt])){ echo 'margin-top:'.$config[menu_mobile_mt].'px;';  } ?>">
    <button on="tap:sidebar.toggle" class="ampstart-btn caps m2 btn-topo"><i class="fa fa-bars fa-2x" aria-hidden="true"></i></button>
  </div>
</div>



<amp-sidebar id="sidebar" layout="nodisplay" side="left" class="menu-mobile-principal">
  <ul class="menu-mobile">
    
    <?php if(!empty($config[menu_1])): ?>
      <li><a href="<?php echo Util::caminho_projeto() ?>/mobile"> <i class="fa <?php Util::imprime($config[icon_menu_mobile_1]); ?>" aria-hidden="true"></i> <?php Util::imprime($config[menu_1]); ?></a></li>
    <?php endif; ?>
    
    <?php if(!empty($config[menu_2])): ?>
      <li><a href="<?php echo Util::caminho_projeto() ?>/mobile/<?php echo $obj_site->url_amigavel($config[menu_2]) ?>"> <i class="fa <?php Util::imprime($config[icon_menu_mobile_2]); ?>" aria-hidden="true"></i> <?php Util::imprime($config[menu_2]); ?></a></li>
    <?php endif; ?>

    <?php if(!empty($config[menu_3])): ?>    
    <li><a href="<?php echo Util::caminho_projeto() ?>/mobile/<?php echo $obj_site->url_amigavel($config[menu_3]) ?>"> <i class="fa <?php Util::imprime($config[icon_menu_mobile_3]); ?>" aria-hidden="true"></i> <?php Util::imprime($config[menu_3]); ?></a></li>
    <?php endif; ?>

    <?php if(!empty($config[menu_4])): ?>
    <li><a href="<?php echo Util::caminho_projeto() ?>/mobile/<?php echo $obj_site->url_amigavel($config[menu_4]) ?>"> <i class="fa <?php Util::imprime($config[icon_menu_mobile_4]); ?>" aria-hidden="true"></i> <?php Util::imprime($config[menu_4]); ?></a></li>
    <?php endif; ?>
    
    <?php if(!empty($config[menu_5])): ?>
    <li><a href="<?php echo Util::caminho_projeto() ?>/mobile/<?php echo $obj_site->url_amigavel($config[menu_5]) ?>"> <i class="fa <?php Util::imprime($config[icon_menu_mobile_5]); ?>" aria-hidden="true"></i> <?php Util::imprime($config[menu_5]); ?></a></li>
    <?php endif; ?>

    <?php if(!empty($config[menu_6])): ?>
    <li><a href="<?php echo Util::caminho_projeto() ?>/mobile/<?php echo $obj_site->url_amigavel($config[menu_6]) ?>"> <i class="fa <?php Util::imprime($config[icon_menu_mobile_6]); ?>" aria-hidden="true"></i> <?php Util::imprime($config[menu_6]); ?></a></li>
    <?php endif; ?>

      <?php if(!empty($config[menu_8_titulo])): ?>
      <li>
        <a target="_blank" href="<?php Util::imprime($config[menu_8]); ?>">
          <?php Util::imprime($config[menu_8_titulo]); ?>
        </a>
      </li>
      <?php endif; ?>

  </ul>
</amp-sidebar>
