<div class="col-12 produtos_destaques top10 ">
        <p class="linha-topo-bottom"><b>LOCAL DA ENTREGA : <?php Util::imprime( Util::troca_value_nome($_SESSION[id_bairro_entrega], "tb_fretes", "idfrete", "titulo") ) ?></b></p>
    </div>

    <!--  ==============================================================  -->
    <!-- LOCAL DA ENTREGA-->
    <!--  ==============================================================  -->
    <div class="col-12  top10">

          <p class="top10">Selecione o tipo de pagamento desejado.</p>

          <?php if(!empty($config[email_pagseguro])): ?>
            <a href="<?php echo Util::caminho_projeto() ?>/mobile/pagamento/envia.php?tipo_pagamento=pagseguro" class="btn btn_2 top10 btn-block">PagSeguro</a>
          <?php endif; ?>


          <?php if(!empty($config[codigo_paypal])): ?>
            <a href="<?php echo Util::caminho_projeto() ?>/mobile/pagamento/envia.php?tipo_pagamento=paypal" class="btn btn_2 top10 btn-block">PayPal</a>
          <?php endif; ?>

          <?php if($config[pagamento_maquina] == 'SIM'): ?>  
            <a href="<?php echo Util::caminho_projeto() ?>/mobile/pagamento/envia.php?tipo_pagamento=maquineta" class="btn btn_2 top10 btn-block">Para pagamento no local - Via maquineta de cartão</a>
          <?php endif; ?>


  </div>
  <!--  ==============================================================  -->
  <!-- LOCAL DA ENTREGA-->
  <!--  ==============================================================  -->