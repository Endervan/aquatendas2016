
<!-- ======================================================================= -->
<!--  PRODUTOS E SERVICOS -->
<!-- ======================================================================= -->
<div class="row top30 bottom50">
    
    <?php if(!empty($config[menu_3])): ?>
        <div class="col-4">
            <div class="index-bg-icones">
                <a href="<?php echo Util::caminho_projeto(); ?>/mobile/<?php echo $obj_site->url_amigavel($config[menu_3]) ?>">
                    <i class="fa <?php Util::imprime($config[icon_menu_mobile_3]); ?>" aria-hidden="true" ></i>
                    <h6><?php Util::imprime($config[menu_3]); ?></h6>
                </a>
            </div>   
        </div>
    <?php endif; ?>


    <?php if(!empty($config[menu_4])): ?>
        <div class="col-4">
            <div class="index-bg-icones">
                <a href="<?php echo Util::caminho_projeto(); ?>/mobile/<?php echo $obj_site->url_amigavel($config[menu_4]) ?>">
                    <i class="fa <?php Util::imprime($config[icon_menu_mobile_4]); ?>" aria-hidden="true" ></i>
                    <h6><?php Util::imprime($config[menu_4]); ?></h6>
                </a>
            </div>  
        </div>
    <?php else: ?>
    <div class="col-4 ">
        <div class="index-bg-icones">
            <a href="<?php echo Util::caminho_projeto(); ?>/mobile/onde-estamos">
                <i class="fa fa-map-marker" aria-hidden="true"></i>
                <h6>ONDE ESTAMOS</h6>
            </a>
        </div>     
    </div>
    <?php endif; ?>


    <?php if(!empty($config[menu_6])): ?>
    <div class="col-4 ">
        <div class="index-bg-icones">
            <a href="<?php echo Util::caminho_projeto(); ?>/mobile/<?php echo $obj_site->url_amigavel($config[menu_6]) ?>">
                <i class="fa <?php Util::imprime($config[icon_menu_mobile_6]); ?>" aria-hidden="true" ></i>
                <h6><?php Util::imprime($config[menu_6]); ?></h6>
            </a>
        </div>     
    </div>
    <?php endif; ?>
    
    
</div>
<!-- ======================================================================= -->
<!--  PRODUTOS E SERVICOS -->
<!-- ======================================================================= -->

