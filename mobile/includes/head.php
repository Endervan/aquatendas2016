<?php
$result = $obj_site->select("tb_configuracoes","AND idconfiguracao = 1");
$config = mysql_fetch_array($result);

// BUSCA META TAGS E TITLE
$dados_dentro_title = $config;
$description = $dados_dentro_title[description_google];
$keywords = $dados_dentro_title[keywords_google];
$titulo_pagina = $dados_dentro_title[title_google];

?>

  <meta charset="utf-8">
  <title><?php Util::imprime($obj_site->get_title($titulo_pagina)); ?></title>
  <meta name="description" content="<?php Util::imprime($obj_site->get_description($description)); ?>">
  <meta name="keywords" content="<?php Util::imprime($obj_site->get_keywords($keywords)); ?>">
  <link rel="canonical" href="http://example.ampproject.org/article-metadata.html" />
  <meta name="viewport" content="width=device-width,minimum-scale=1">
  <script type="application/ld+json">
    {
      "@context": "http://schema.org",
      "@type": "NewsArticle",
      "headline": "Open-source framework for publishing content",
      "datePublished": "2015-10-07T12:02:41Z",
      "image": [
        "logo.jpg"
      ]
    }
  </script>
  <style amp-boilerplate>body{-webkit-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-moz-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-ms-animation:-amp-start 8s steps(1,end) 0s 1 normal both;animation:-amp-start 8s steps(1,end) 0s 1 normal both}@-webkit-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-moz-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-ms-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-o-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}</style><noscript><style amp-boilerplate>body{-webkit-animation:none;-moz-animation:none;-ms-animation:none;animation:none}</style></noscript>
  <script async src="https://cdn.ampproject.org/v0.js"></script>


<!-- sidebar -->
<!-- <script async custom-element="amp-sidebar" src="https://cdn.ampproject.org/v0/amp-sidebar-0.1.js"></script> -->




<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">

    <?php
    /*    LINK PADRA DA PÁGINA, SEMPRE VAI INSERIR O LINK DA PROPRIA PAGINA
        Preparar sua página para descoberta e distribuição


        62999759801 / 62991707251 - anapolis - centro automoitvo anapolina - Paulo
    */
    ?>

    <script async custom-element="amp-lightbox" src="https://cdn.ampproject.org/v0/amp-lightbox-0.1.js"></script>


<script type="text/javascript" src="<?php echo Util::caminho_projeto() ?>/js/fontawesome-free-5.0.6/on-server/js/fontawesome-all.min.js"></script>
 <link rel="stylesheet" href="<?php echo Util::caminho_projeto() ?>/js/fontawesome-free-5.0.6/on-server/css/fontawesome-all.min.css">