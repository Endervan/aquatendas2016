<div class="row">
    <?php
    $i = 0;
    $result = $obj_site->select("tb_dicas");
    if (mysql_num_rows($result) > 0) {
        while ($row = mysql_fetch_array($result)) {
            ?>
            <div class="col-6 dicas top20">
                <div class="card">
                <a href="<?php echo Util::caminho_projeto() ?>/mobile/<?php echo $obj_site->url_amigavel($config[menu_5_singular]) ?>/<?php Util::imprime($row[url_amigavel]); ?>"
                       title="<?php Util::imprime($row[titulo]); ?>">
                        <amp-img
                                src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($row[imagem]); ?>"
                                width="150"
                                height="100"
                                layout="responsive"
                                alt="<?php echo Util::imprime($row[titulo]) ?>">
                        </amp-img>
                    </a>
                    <div class="card-body bottom10">
                        <h6 class="card-text"><?php Util::imprime($row[titulo]); ?></h6>
                    </div>
                </div>
            </div>
            <?php
            if ($i == 1) {
                echo '<div class="clearfix"></div>';
                $i = 0;
            } else {
                $i++;
            }
        }
    }
    ?>
</div>