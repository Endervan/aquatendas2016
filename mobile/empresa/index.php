<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 2);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];

?>
<!doctype html>
<html amp lang="pt-br">
<head>
  <?php require_once("../includes/head.php"); ?>
  <script async custom-element="amp-sidebar" src="https://cdn.ampproject.org/v0/amp-sidebar-0.1.js"></script>
    <script async custom-element="amp-iframe" src="https://cdn.ampproject.org/v0/amp-iframe-0.1.js"></script>


    <style amp-custom>
  <?php require_once("../css/geral.css"); ?>
  <?php require_once("../css/topo_rodape.css"); ?>
  <?php require_once("../css/paginas.css");  //  ARQUIVO DA PAGINA ?>



  <?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 21); ?>
  .bg-internaOLD{
    background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($config[bg_mobile_empresa]); ?>) top 200px center  no-repeat;
    background-size:  100% 128px;
  }
  </style>

  <script async custom-element="amp-bind" src="https://cdn.ampproject.org/v0/amp-bind-0.1.js"></script>


</head>

<body class="bg-interna">


  <?php require_once("../includes/topo.php") ?>

  <!-- ======================================================================= -->
  <!-- TITULO PAGINA  -->
  <!-- ======================================================================= -->
  <div class="row">
      <div class="col-12  padding0 localizacao-pagina text-center">
          <h5 ><?php Util::imprime($banner[legenda_1]); ?></h5>
          <?php if (isset($banner[legenda_2])) : ?>
              <h6 ><?php Util::imprime($banner[legenda_2]); ?></h6 class="open">
          <?php endif; ?>
          <amp-img class="top10" src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/barra_titulo.png" alt=""
                   height="26" width="320">

          </amp-img>
      </div>
  </div>
  <!-- ======================================================================= -->
  <!-- TITULO PAGINA  -->
  <!-- ======================================================================= -->


  <!--  ==============================================================  -->
  <!--   EMPRESA -->
  <!--  ==============================================================  -->
  <div class="row">
    <div class="col-12 empresa_geral top20">
      <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 4) ?>
    </div>
    <div class="col-12 ">
      <h3 class="text-center">SOBRE</h3>
      <div ><p><?php Util::imprime($row[descricao]); ?></p></div>
    </div>
  </div>

  <div class="row">
    <div class="col-12 top20">
      <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 5) ?>
    </div>
    <div class="col-12 ">
      <h3 class="text-center">QUALIDADE DOS SERVIÇOS</h3>
      <div ><p><?php Util::imprime($row[descricao]); ?></p></div>
    </div>
  </div>
  

  <!--  ==============================================================  -->
  <!--   EMPRESA -->
  <!--  ==============================================================  -->



  <div class="row top25">
    <div class="col-12">
      <a class="btn col-12 padding0 btn_escuro"
      on="tap:my-lightbox444"
      role="a"
      tabindex="0">
      <i class="fa fa-phone mr-1" aria-hidden="true"></i>LIGAR AGORA
    </a>
  </div>


</div>

  <div class="row empresa_geral top35">
      <div class="col-12   text-center">
          <h4>NOSSA LOCALIZAÇÃO</h4>
          <amp-img class="top10" src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/barra_contato.png" alt=""
                   height="24" width="217"></amp-img>

      </div>

      

  </div>




  <?php require_once("../includes/unidades.php"); ?>




<?php require_once("../includes/veja.php") ?>


<?php require_once("../includes/rodape.php") ?>

</body>



</html>
