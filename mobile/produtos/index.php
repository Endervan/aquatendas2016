
<?php
$ids = explode("/", $_GET[get1]);

require_once("../../class/Include.class.php");
$obj_site = new Site();

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo",9);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];

?>
<!doctype html>
<html amp lang="pt-br">
<head>
  <?php require_once("../includes/head.php"); ?>
  <script async custom-element="amp-sidebar" src="https://cdn.ampproject.org/v0/amp-sidebar-0.1.js"></script>

  <style amp-custom>
  <?php require_once("../css/geral.css"); ?>
  <?php require_once("../css/topo_rodape.css"); ?>
  <?php require_once("../css/paginas.css");  //  ARQUIVO DA PAGINA ?>



  <?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 12); ?>
  .bg-interna{
    background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 60px center  no-repeat;
    background-size:  100% 128px;
  }
  </style>


  <script async custom-element="amp-list" src="https://cdn.ampproject.org/v0/amp-list-0.1.js"></script>
  <script async custom-template="amp-mustache" src="https://cdn.ampproject.org/v0/amp-mustache-0.1.js"></script>



</head>

<body class="bg-interna">

  <?php
  $voltar_para = $obj_site->url_amigavel($config[menu_3]); // link de volta, exemplo produtos, dicas, servicos etc
  require_once("../includes/topo.php") ?>




  <!-- ======================================================================= -->
  <!-- CATEGORIAS  -->
  <!-- ======================================================================= -->
  <?php if (empty( $ids[0] )) { ?>
  <div class="row">
        <div class="col-12 top20">
        <div class="list-group">
            <a href="#" class="list-group-item list-group-item-action btn btn-dark btn-block  text-center">
                CATEGORIAS
            </a>
            <?php

            //$result = $obj_site->select("tb_categorias_produtos");
            $sql = "SELECT tcp.* FROM tb_categorias_produtos tcp, tb_produtos tf where tcp.idcategoriaproduto = tf.id_categoriaproduto group by tcp.idcategoriaproduto";
            $result = $obj_site->executaSQL($sql);
            if (mysql_num_rows($result) > 0) {
                $i = 0;
                while ($row1 = mysql_fetch_array($result)) {

                    ?>

                    <a href="<?php echo Util::caminho_projeto() ?>/mobile/<?php echo $obj_site->url_amigavel($config[menu_3]) ?>/<?php Util::imprime($row1[url_amigavel]); ?>"
                       class="list-group-item list-group-item-action" title="<?php Util::imprime($row1[titulo]); ?>">
                        <?php Util::imprime($row1[titulo]); ?>
                    </a>

                    <?php

                    if ($i == 1) {
                        echo '<div class="clearfix"></div>';
                        $i = 0;
                    } else {
                        $i++;
                    }

                }
            }
            ?>
        </div>
        </div>
  </div>
  <?php } ?>
  <!-- ======================================================================= -->
  <!-- CATEGORIAS  -->
  <!-- ======================================================================= -->






  
  <!-- ======================================================================= -->
  <!-- TITULO PAGINA  -->
  <!-- ======================================================================= -->
  <div class="row">
      <div class="col-12 top15 padding0 localizacao-pagina text-center">
          <h5 ><?php Util::imprime($banner[legenda_1]); ?></h5>
          <?php if (isset($banner[legenda_2])) : ?>
              <h6 ><?php Util::imprime($banner[legenda_2]); ?></h6>
          <?php endif; ?>
          <amp-img class="top10" src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/barra_titulo.png" alt=""
                   height="26" width="320"></amp-img>
      </div>
  </div>
  <!-- ======================================================================= -->
  <!-- TITULO PAGINA  -->
  <!-- ======================================================================= -->



<div class="row">
    <div class="col-12 top10">
          <a class="btn btn-block btn_cinza_escuro"
             on="tap:my-lightbox444"
             role="buttom"
             tabindex="0">
             <i class="fa fa-phone mr-1" aria-hidden="true"></i>LIGAR AGORA
          </a>
      </div>

</div>




  <!-- ======================================================================= -->
  <!-- PRODUTOS    -->
  <!-- ======================================================================= -->
  <?php require_once("../includes/modulo_3.php") ?>
  <!-- ======================================================================= -->
  <!-- PRODUTOS    -->
  <!-- ======================================================================= -->




  <!--  ==============================================================  -->
  <!--   VEJA TAMBEM -->
  <!--  ==============================================================  -->
  <?php require_once("../includes/veja.php") ?>
  <!--  ==============================================================  -->
  <!--   VEJA TAMBEM -->
  <!--  ==============================================================  -->


  <?php require_once("../includes/rodape.php") ?>

</body>



</html>
