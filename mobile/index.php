<?php
require_once("../class/Include.class.php");
$obj_site = new Site();
?>
<!doctype html>
<html amp lang="pt-br">
<head>
    <?php require_once("./includes/head.php"); ?>

    <style amp-custom>
        <?php require_once("./css/geral.css"); ?>
        <?php require_once("./css/topo_rodape.css"); ?>
        <?php require_once("./css/paginas.css");  //  ARQUIVO DA PAGINA ?>

        .bg-interna {
            background: #eaeae9 url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($config[bg_mobile_index]); ?>) top 40px center no-repeat;
            -webkit-background-size: 100%;
            -moz-background-size: 100%;
            -o-background-size: 100%;
            background-size: 100%;
        }
    </style>


</head>


<body class="bg-interna">

<div class="row bg-branco">
    <div class="col-12 text-center topo top5 bottom5">
    <amp-img src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($config[bg_mobile_paginas]) ?>" alt="Home" height="<?php echo $config[logo_mobile_height] ?>" width="<?php echo $config[logo_mobile_wight] ?>" ></amp-img>
    </div>
</div>


<div class=" row font-index text-center">
    <div class="col-12 padding0 top30">
        <?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 11); ?>
        <h4 class="text-center"><?php Util::imprime($banner[legenda_1]); ?></h4>
        <?php if (!empty($banner[legenda_2])): ?>
            <h5 class="text-center"><?php Util::imprime($banner[legenda_2]); ?></h5>
        <?php endif; ?>
        <amp-img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/barra_home.png" alt="Home" height="1"
                 width="144"></amp-img>
    </div>
</div>

<!-- ======================================================================= -->
<!--  MENU -->
<!-- ======================================================================= -->
<div class="row">

    <?php if(!empty($config[menu_3])): ?>
        <div class="col-4 top30">
            <div class="index-bg-icones">
                <a href="<?php echo Util::caminho_projeto(); ?>/mobile/<?php echo $obj_site->url_amigavel($config[menu_3]) ?>">
                    <i class="fa <?php Util::imprime($config[icon_menu_mobile_3]); ?>" aria-hidden="true"></i>
                    <h6><?php Util::imprime($config[menu_3]); ?></h6>
                </a>
            </div>   
        </div>
    <?php endif; ?>


    <?php if(!empty($config[menu_4])): ?>
        <div class="col-4 top30">
            <div class="index-bg-icones">
                <a href="<?php echo Util::caminho_projeto(); ?>/mobile/<?php echo $obj_site->url_amigavel($config[menu_4]) ?>">
                    <i class="fa <?php Util::imprime($config[icon_menu_mobile_4]); ?>" aria-hidden="true"></i>
                    <h6><?php Util::imprime($config[menu_4]); ?></h6>
                </a>
            </div>  
        </div>
    <?php endif; ?>

    <?php if(!empty($config[menu_5])): ?>
    <div class="col-4 top30">
        <div class="index-bg-icones">
            <a href="<?php echo Util::caminho_projeto(); ?>/mobile/<?php echo $obj_site->url_amigavel($config[menu_5]) ?>">
                <i class="fa <?php Util::imprime($config[icon_menu_mobile_5]); ?>" aria-hidden="true" ></i>
                <h6><?php Util::imprime($config[menu_5]); ?></h6>
            </a>
        </div>  
    </div>
    <?php endif; ?>




    <?php if(!empty($config[menu_2])): ?>
    <div class="col-4 top30">
        <div class="index-bg-icones">
            <a href="<?php echo Util::caminho_projeto(); ?>/mobile/<?php echo $obj_site->url_amigavel($config[menu_2]) ?>">
                <i class="fa <?php Util::imprime($config[icon_menu_mobile_2]); ?>" aria-hidden="true" ></i>
                <h6><?php Util::imprime($config[menu_2]); ?></h6>
            </a>
        </div>  
    </div>
    <?php endif; ?>



    <?php
    //  calculo para saber se algum modulo esta vazio
    if( empty($config[menu_3]) or  empty($config[menu_4]) or empty($config[menu_5]) ){
        echo '<div class="col-2 top30"></div>';
    }
    ?>




    <?php if(!empty($config[menu_6])): ?>
    <div class="col-4 top30">
        <div class="index-bg-icones">
            <a href="<?php echo Util::caminho_projeto(); ?>/mobile/<?php echo $obj_site->url_amigavel($config[menu_6]) ?>">
                <i class="fa <?php Util::imprime($config[icon_menu_mobile_6]); ?>" aria-hidden="true" ></i>
                <h6><?php Util::imprime($config[menu_6]); ?></h6>
            </a>
        </div>     
    </div>
    <?php endif; ?>

    <div class="col-4 top30">
        <div class="index-bg-icones">
        <a href="<?php echo Util::caminho_projeto(); ?>/mobile/onde-estamos">
                <i class="fa fa-map-marker" aria-hidden="true"></i>
                <h6>ONDE ESTAMOS</h6>
            </a>
        </div>  
    </div>


</div>
<!-- ======================================================================= -->
<!--  MENU -->
<!-- ======================================================================= -->



<?php require_once("./includes/rodape.php") ?>


</body>


</html>