<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 7);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];
?>
<!doctype html>
<html amp lang="pt-br">
<head>
    <?php require_once("../includes/head.php"); ?>
    <script async custom-element="amp-sidebar" src="https://cdn.ampproject.org/v0/amp-sidebar-0.1.js"></script>


    <style amp-custom>
        <?php require_once("../css/geral.css"); ?>
        <?php require_once("../css/topo_rodape.css"); ?>
        <?php require_once("../css/paginas.css");  //  ARQUIVO DA PAGINA ?>

        <?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 18); ?>
        .bg-interna {
            background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 60px center no-repeat;
            background-size: 100% 128px;
        }
    </style>

    <script async custom-element="amp-form" src="https://cdn.ampproject.org/v0/amp-form-0.1.js"></script>
    <script async custom-template="amp-mustache" src="https://cdn.ampproject.org/v0/amp-mustache-0.1.js"></script>
    <script async custom-element="amp-iframe" src="https://cdn.ampproject.org/v0/amp-iframe-0.1.js"></script>


</head>

<body class="bg-interna">


<?php require_once("../includes/topo.php") ?>

<!-- ======================================================================= -->
<!-- TITULO PAGINA  -->
<!-- ======================================================================= -->
<div class="row">
    <div class="col-12  padding0 localizacao-pagina text-center">
        <h5 ><?php Util::imprime($banner[legenda_1]); ?></h5>
        <?php if (isset($banner[legenda_2])) : ?>
            <h6 ><?php Util::imprime($banner[legenda_2]); ?></h6 class="open">
        <?php endif; ?>
        <amp-img class="top10" src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/barra_titulo.png" alt=""
                 height="26" width="320"></amp-img>
    </div>
</div>
<!-- ======================================================================= -->
<!-- TITULO PAGINA  -->
<!-- ======================================================================= -->


<div class="row">
    <div class="col-12 top30">
        <a class="btn btn-block padding0 btn_escuro"
           on="tap:my-lightbox444"
           role="a"
           tabindex="0">
           <i class="fa fa-phone mr-1" aria-hidden="true"></i>LIGAR AGORA
        </a>
    </div>
</div>


<div class="row bottom30 fundo_contatos">
    <!-- ======================================================================= -->
    <!-- FORMULARIO   -->
    <!-- ======================================================================= -->
    <div class="col-12">


        <?php
        //  VERIFICO SE E PARA ENVIAR O EMAIL
        if (isset($_GET[nome])) {
            $texto_mensagem = "
        Assunto: " . ($_GET[assunto]) . " <br />
        Nome: " . ($_GET[nome]) . " <br />
        Email: " . ($_GET[email]) . " <br />
        Telefone: " . ($_GET[telefone]) . " <br />
       Celular: " . ($_GET[celular]) . " <br />


        Mensagem: <br />
        " . (nl2br($_GET[mensagem])) . "
        ";

            if (Util::envia_email($config[email], ("$_GET[nome] solicitou contato pelo site"), ($texto_mensagem),
                ($_GET[nome]), $_GET[email])) {
                Util::envia_email($config[email_copia], ("$_GET[nome] solicitou contato pelo site"), ($texto_mensagem),
                    ($_GET[nome]), $_GET[email]);
                $enviado = 'sim';
                unset($_GET);
            }


        }
        ?>


        <?php if ($enviado == 'sim'): ?>
            <div class="col-12  text-center top40">
                <h4>Email enviado com sucesso.</h4>
                <a class="btn btn_escuro" href="<?php echo Util::caminho_projeto() ?>/mobile/contato">
                    ENVIE OUTRO EMAIL
                </a>
            </div>
        <?php else: ?>


            <form method="get" action="index.php" >


                <div class="ampstart-input top10 inline-block">

                    <div class="relativo">
                        <input type="text" class="input-form input100 block border-none" name="assunto"
                               placeholder="ASSUNTO" required>
                        <span class="fa fa-user-circle form-control-feedback"></span>
                    </div>

                    <div class="relativo">
                        <input type="text" class="input-form input100 block border-none" name="nome" placeholder="NOME"
                               required>
                        <span class="fa fa-user form-control-feedback"></span>
                    </div>

                    <div class="relativo">
                        <input type="email" class="input-form input100 block border-none" name="email"
                               placeholder="EMAIL" required>
                        <span class="fa fa-envelope form-control-feedback"></span>
                    </div>

                    <div class="relativo">
                        <input type="tel" class="input-form input100 block border-none" name="telefone"
                               placeholder="TELEFONE" required>
                        <span class="fa fa-phone form-control-feedback"></span>
                    </div>

                    <div class="relativo">
                        <input type="text" class="input-form input100 block border-none" name="celular"
                               placeholder="CELULAR">
                        <span class="fa fa-mobile-phone form-control-feedback"></span>
                    </div>


                    <div class="relativo">
                        <textarea name="mensagem" placeholder="MENSAGEM"
                                  class="input-form input100 campo-textarea"></textarea>
                        <span class="fa fa-pencil form-control-feedback"></span>
                    </div>

                </div>

                <div class="col-12 padding0">
                    <div class="relativo ">
                        <button type="submit"
                                value="OK"
                                class="ampstart-btn caps btn btn_verde btn_escuro col-12">ENVIAR
                            MENSAGEM
                        </button>
                    </div>
                </div>


                <div submit-success>
                    <template type="amp-mustache">
                        Email enviado com sucesso! {{name}} entraremos em contato o mais breve possível.
                    </template>
                </div>

                <div submit-error>
                    <template type="amp-mustache">
                        Houve um erro, {{name}} por favor tente novamente.
                    </template>
                </div>

            </form>

        <?php endif; ?>
    </div>
    <!-- ======================================================================= -->
    <!-- FORMULARIO   -->
    <!-- ======================================================================= -->

</div>

<div class="row empresa_geral top35">
    <div class="col-12 text-center">
        <h4>NOSSA LOCALIZAÇÃO</h4>
        <amp-img class="top10" src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/barra_contato.png" alt=""
                 height="24" width="217"></amp-img>

    </div>


</div>


 <?php require_once("../includes/unidades.php"); ?>

<?php require_once("../includes/veja.php") ?>


<?php require_once("../includes/rodape.php") ?>

</body>


</html>
