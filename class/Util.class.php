<?php 
ob_start();
@session_start();
require_once($_SERVER['DOCUMENT_ROOT'] . PASTA_PROJETO . "/class/Include.class.php");
require_once($_SERVER['DOCUMENT_ROOT'] . PASTA_PROJETO . "/jquery/ckeditor/ckeditor.php");
require_once("class.phpmailer.php");
require_once($_SERVER['DOCUMENT_ROOT'] . PASTA_PROJETO . '/class/recaptchalib.php');
//require_once($_SERVER['DOCUMENT_ROOT'] . PASTA_PROJETO . "/class/dompdf-0.5.1/dompdf_config.inc.php");



class Util
{


	
	
	
	#-----------------------------------------------------------------------------------------------------------------------#
	#	GERA UM SELECT COM OS EVENTOS DO CALENDARIO
	#-----------------------------------------------------------------------------------------------------------------------#
	static public function tipo_site($name, $valor, $class = '', $titulo = "Selecione", $complemento_javascript = "")
	{
		$obj_dao = new Dao();
		//lista de ufs
		$valor = trim($valor);
		$lista = array(
			array('id' => 'orcamento', 'titulo' => "ORÇAMENTO"),
			array('id' => 'carrinho', 'titulo' => "CARRINHO DE COMPRA"),
		);
		echo '<select name="'.$name.'" id="'.$name.'" class="'.$class.'">';
		echo "<option value=''>$titulo</option>";
		for($i=0; $i < count($lista); $i++)
		{
			//verica se há na lista e check em caso afirmativo
			if($valor == $lista[$i][id])
			{
				?>
				<option value="<?php Util::imprime($lista[$i][id]); ?>" selected="selected"><?php Util::imprime($lista[$i][titulo]); ?></option>
				<?php
			}
			else
			{
				?>
				<option value="<?php Util::imprime($lista[$i][id]); ?>"><?php Util::imprime($lista[$i][titulo]); ?></option>
				<?php
			}
		}
		echo '</select>';
	}


    static public function trata_numero_whatsapp($numero){
        return preg_replace("/[^0-9]/", "", $numero);
    }
	
	
	static public function alert_bootstrap($mensagem, $titulo = "Aviso")
	{
		?>
		<!-- Modal HTML -->
        <div id="myModal" class="modal fade" style="z-index=9999">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" style="color: #D24646;"><?php echo($titulo) ?></h4>
                    </div>
                    <div class="modal-body">
                        <p><?php echo($mensagem) ?></p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                    </div>
                </div>
            </div>
        </div>

        <script type="text/javascript">
        $(document).ready(function(){
            $('#myModal').modal({
			    backdrop: 'static',
			    keyboard: false  // to prevent closing with Esc button (if you want this too)
			})
        });
        </script>
        
		<?php
	}
	
	
	#--------------------------------------------------------------------------------------------------------------#
	#	CRIA IMAGEM CAPTCHA
	#--------------------------------------------------------------------------------------------------------------#
	static public function gera_captcha($theme = 'red', $publickey = "6LciiNkSAAAAAKOiZe5318dAXbe65MsXPdieKbpS")
	{
		//	ESTILO
		?>
        <script type="text/javascript">
		 var RecaptchaOptions = {
			theme : '<?php echo $theme ?>',
			lang: 'pt'
		 };
		 </script>
        <?php
		
		
		echo recaptcha_get_html($publickey);
		
	}
	
	
	
	
	#--------------------------------------------------------------------------------------------------------------#
	#	VALIDA IMAGEM CAPTCHA
	#--------------------------------------------------------------------------------------------------------------#
	static public function valida_captcha($privatekey = "6LfeGd0SAAAAAPn9zN41r9minLRYz0ub2ROx5sFj")
	{
		$resp = recaptcha_check_answer($privatekey, $_SERVER["REMOTE_ADDR"], $_POST["recaptcha_challenge_field"], $_POST["recaptcha_response_field"]);
		return $resp->is_valid;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	#--------------------------------------------------------------------------------------------------------------#
	#	CAMINHO DO SITE
	#--------------------------------------------------------------------------------------------------------------#
	static public function caminho_projeto()
	{
		if( isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on' ) {
            return "https://" . $_SERVER['SERVER_NAME'] . PASTA_PROJETO;
        }else{
            return "http://" . $_SERVER['SERVER_NAME'] . PASTA_PROJETO;
        }
	}
	
	
	
	
	#--------------------------------------------------------------------------------------------------------------#
	#	BUSCA OS DADOS DO GOOGLE MAPS PELO CEP --- FORMATO DO CEP 73050-140
	#--------------------------------------------------------------------------------------------------------------#
	static public function get_dados_google_maps($cep)
	{
		$geocode=file_get_contents("http://maps.google.com/maps/api/geocode/json?address=$cep&sensor=false");
		$output = json_decode($geocode);

		
		//echo $output->results[0]->address_components[1]->long_name;
		
	
		$dados[latitude] = $output->results[0]->geometry->location->lat;
		$dados[longitude] = $output->results[0]->geometry->location->lng;
		
		return $dados;
	}
	
	
	
	
	
	#--------------------------------------------------------------------------------------------------------------#
	#	PEGA A IMAGEM DO VIDEO NO YOUTUBE
	#--------------------------------------------------------------------------------------------------------------#
	public function imagem_youtube($url, $size = 'small')
	{
	   $url = explode('v=',$url);
	   $url = explode('&',$url[1]);
	   
	   
	   $url = $size == 'small' ? ('http://i1.ytimg.com/vi/' . $url[0] . '/default.jpg') : ('http://img.youtube.com/vi/' . $url[0] . '/0.jpg');
	   return $url;
	}
	
	
	
	
	#--------------------------------------------------------------------------------------------------------------#
	#	PEGA A IMAGEM DO VIDEO NO YOUTUBE
	#--------------------------------------------------------------------------------------------------------------#
	public function codigo_video_youtube($url)
	{
	   $url = explode('v=',$url);
	   $url = explode('&',$url[1]);
	   return $url[0];
	}
	
	
	
	
	#--------------------------------------------------------------------------------------------------------------#
	#	LIMPA O TEXTO PARA IMPRIMIR NO FORMULARIO
	#--------------------------------------------------------------------------------------------------------------#
	public function imprime_para_form($texto)
	{
		//$texto_limpo = utf8_encode($texto);
		//echo $texto_limpo;
		echo $texto;
	}
	
	
	#-----------------------------------------------------------------------------------------------------------------------#
	#	CRIA UM JAVASCRIPT PARA O CAMPO OBRIGATORIO
	#-----------------------------------------------------------------------------------------------------------------------#
	public function campo_obrigatorio_js($campos)
	{
		$html = "
				<script>
				function checkMail(mail){
					var er = new RegExp(/^[A-Za-z0-9_\-\.]+@[A-Za-z0-9_\-\.]{2,}\.[A-Za-z0-9]{2,}(\.[A-Za-z0-9])?/);
					if(typeof(mail) == \"string\"){
						if(er.test(mail)){ return true; }
					}else if(typeof(mail) == \"object\"){
						if(er.test(mail.value)){ 
									return true; 
								}
					}else{ 
						return false;
						}
				}
				
				function validaCpf(cpf)
				{ 
				  
					var i; 
					s = cpf; 
					var c = s.substr(0,9); 
					var dv = s.substr(9,2); 
					var d1 = 0; 
					for (i = 0; i < 9; i++) 
					{ 
						d1 += c.charAt(i)*(10-i);
					}	 
					if (d1 == 0)
					{ 
						return false; 
					} 
					d1 = 11 - (d1 % 11); 
					if (d1 > 9) d1 = 0; 
					if (dv.charAt(0) != d1) 
					{ 
						return false; 
					} 
					d1 *= 2; 
					for (i = 0; i < 9; i++) 
					{ 
					d1 += c.charAt(i)*(11-i); 
					} 
					d1 = 11 - (d1 % 11); 
					if (d1 > 9) d1 = 0; 
					if (dv.charAt(1) != d1) 
					{ 
						return false; 
					} 
				  
					return true; 
				  
				}
				function valida_campos_formulario()
				{
				"; 
		
				foreach($campos as $campo)
				{
					if($campo[obr] == 's')
					{
						switch ($campo[tipo])
						{
							////////////////////////////////////////////////////////////////////////
							case "email":
								
								$html.= "
										if (!checkMail(document.getElementById('$campo[nome_campo_form]').value)) 
										{
											alert('". utf8_encode($campo[msgerros]) ."');
											document.getElementById('$campo[nome_campo_form]').focus();
											return false;
										}
										";
							break;
							
							///////////////////////////////////////////////////////////////////////////
							case "cpf":
								
								$html.= "
										if (!validaCpf(document.getElementById('$campo[nome_campo_form]').value)) 
										{
											alert('". utf8_encode($campo[msgerros]) ."');
											document.getElementById('$campo[nome_campo_form]').focus();
											return false;
										}
										";
							break;
							
							///////////////////////////////////////////////////////////////////////////
							default:
								$html.= "
										if (document.getElementById('$campo[nome_campo_form]').value  == '') 
										{
											alert('". utf8_encode($campo[msgerros]) ."');
											document.getElementById('$campo[nome_campo_form]').focus();
											return false;
										}
										";
							break;
						}
						
					}
				}
				
		$html .= "
					return true;
				}
				 </script>
				 ";
		echo $html;
	}
	
	
	
	#--------------------------------------------------------------------------------------------------------------#
	#	LIMPA O TEXTO PARA IMPRIMIR
	#--------------------------------------------------------------------------------------------------------------#

	public function imprime($texto)
	{
		$texto = strip_tags($texto, '<h1>, <h2>, <h3>, <h4>, <h5>, <h6>, <ul>, <li>, <p>, <strong>, <img>, <b>, <br />, <a>, <i>');
		//$texto = html_entity_decode($texto);
		
		//	VERIFICO OS PARAMETRO PASSADO
		$parametros = func_get_args();
		switch(func_num_args())
		{
			case 2:	//CORTA O TEXTO
				
				
				$texto = strip_tags($texto);
				
				
				
				//	LIMPO O TEXTO							
				$texto = stripslashes($texto);
				
				$end_substr = $parametros[1];
				$texto = substr($texto, 0, $end_substr);
				
				$texto = utf8_encode($texto);
				$texto_limpo = $texto;
										   		  

				//	VERIFICO SE O TEXTO É MAIOR QUE O TAMANHO PASSADO
				if(strlen($texto) > $end_substr)
				{
					$texto_limpo .= "...";
				}
			break;
			default:
				$texto = stripslashes($texto);
				$texto = utf8_encode($texto);
				$texto = str_ireplace("<div", "<p", $texto);
				$texto = str_ireplace("</div>", "</p>", $texto);
				$texto = str_ireplace("<p>&nbsp;</p>","",$texto);
				$texto = str_ireplace("<div>&nbsp;</div>","",$texto);
				$texto_limpo = $texto;
			break;
		}
		
		
		
		echo $texto_limpo;

	}
	
	
	
	#--------------------------------------------------------------------------------------------------------------#
	#	CONTADOR DE VISITAS
	#--------------------------------------------------------------------------------------------------------------#
	public function insere_visitas()
	{
		$obj_dao = new Dao();
		$obj_data = new Manipula_Data_Hora();
		
		
		
		$hora = date("H:i:s");
		$data = date("d/m/Y");
		$ip = $_SERVER['REMOTE_ADDR'];
		
		//	BUSCA A ULTIMA MENSAGEM
		$sql = "SELECT * FROM tb_visitas WHERE ip = '$ip' ORDER BY data DESC, hora DESC LIMIT 1";
		$result = $obj_dao->executaSQL($sql);
		
		if(mysql_num_rows($result) > 0)
		{
			$row = mysql_fetch_array($result);
			
			$ano = substr($row[data], 0, 4);
			$mes = substr($row[data], 5, 2);
			$dia = substr($row[data], 8, 2);
			$hora_postada = $row[hora];
			$ip = $row[ip];
		}
		else
		{
			$ano = 2000;
			$mes = date("m");
			$dia = date("d");
			$hora_postada = date("H:i:s");
		}
		
		// VERIFICA SE A DIFERENÇA DA DATA E HORA É DE 3 MIN
		if($obj_data->difDataHora("$dia/$mes/$ano $hora_postada", "$data $hora", "m") > 3)
		{
			$ip = $_SERVER['REMOTE_ADDR'];
			$hora = date("H:i:s");
			
			$sql = "
					INSERT tb_visitas
						(ip, data, hora, ativo) 
					VALUES 
						('$ip', '".date("Ymd")."' ,'$hora', 'SIM')
					";
			$obj_dao->executaSQL($sql);
		}
	}
	
	public function conta_visitas()
	{
		$obj_dao = new Dao();
		
		// CONTA E RETORNA O NUMERO DE REGISTROS(VISITAS) DA TABELA
		$sql = "SELECT COUNT(idvisita) as numero_registros FROM tb_visitas";
		$result = $obj_dao->executaSQL($sql);
		$row = mysql_fetch_array($result);
		return $row['numero_registros'];
	}
	
	
	
	#--------------------------------------------------------------------------------------------------------------#
	#	RETORNA O NOME DO DIRETORIO
	#--------------------------------------------------------------------------------------------------------------#
	public function nome_diretorio()
	{
		return $file = dirname($_SERVER['SCRIPT_NAME']);
	}
	
	
	
	
	
	#--------------------------------------------------------------------------------------------------------------#
	#	ENVIA UMA MSG PARA O USUÁRIO
	#--------------------------------------------------------------------------------------------------------------#
	public function script_msg($msg)
	{
		$msg_final = utf8_encode($msg);
		
		echo "
			<script>
 		 		alert('$msg_final');
  			</script>
  		";
		
	}
	
	
	
	
	#--------------------------------------------------------------------------------------------------------------#
	#	REDIRECIONA O USUARIO PARA UMA PAGINA ESPECIFICA
	#--------------------------------------------------------------------------------------------------------------#
	public function script_location($pagina)
	{
		echo "
			<script>
				window.location.href='$pagina'
			</script>
		";
	}



	#--------------------------------------------------------------------------------------------------------------#
	#	RETORNA PARA A PAGINA ANTERIOR
	#--------------------------------------------------------------------------------------------------------------#
	public function script_go_back()
	{
		echo '
			<script>
				javascript:history.go(-1)
			</script>
		';
		exit();
	}
	
	
	
	
	#--------------------------------------------------------------------------------------------------------------#
	#	RETORNA PARA A PAGINA ANTERIOR
	#--------------------------------------------------------------------------------------------------------------#
	public function corta_texto($texto, $inicial = 0, $final = 0)
	{
		return substr($texto, $inicial, $final);
	}
	
	
	
	
	#--------------------------------------------------------------------------------------------------------------#
	#	EFETUA UPLOAD DE ARQUIVO
	#--------------------------------------------------------------------------------------------------------------#
	public function upload_arquivo($destino_arquivo, $arquivo)
	{ 		
		
			
		$nome_arquivo_final = strtolower(date("dmYhi") . rand(1111111111, 9999999999) . '.' . str_replace(".","", substr($arquivo[name], -4) ) );
		
		// do as permissoes do diretorio
		@chmod($destino_arquivo, 0777);
		// faço o upload
		if(!move_uploaded_file($arquivo['tmp_name'], $destino_arquivo . '/' . $nome_arquivo_final))
		{
			Util::script_msg("ERRO AO ENVIAR O ARQUIVO!");
			Util::script_go_back();
			exit();
		}
		return $nome_arquivo_final;
	}
	
	
	
	
	#-----------------------------------------------------------------------------------------------------------------------#
	#	ATUALIZA UM DADOS EM UMA COLUNA ESPECIFICA DA TABELA
	#-----------------------------------------------------------------------------------------------------------------------#
	public function atualiza_dado_coluna_tabela($nome_tabela, $id_tabela, $valor_id, $nome_coluna, $value)
	{
		$obj_dao = new Dao();
		$sql = "
				UPDATE $nome_tabela
					SET $nome_coluna = '$value'
				WHERE
					$id_tabela = '$valor_id'
				";
		$obj_dao->executaSQL($sql);
	}
	
	
	
	#--------------------------------------------------------------------------------------------------------------#
	#	EFETUA UPLOAD DE UMA IMAGEM
	#	http://webdeveloper.earthweb.com/repository/javascripts/2001/04/41291/byteconverter.htm - CONVERSOR DE BYTES
	#--------------------------------------------------------------------------------------------------------------#
	public function upload_imagem($destino_arquivo, $arquivo)
	{
		$obj_imagem = new Imagem();
		
		//	ZERO AS CONFIGURACOES
		$tamanho_maximo = null;
		$largura_maxima = null;
		$altura_maxima = null;
		$largura_minima = null;
		$altura_minima = null;
		
		
		
		
		//	VERIFICO SE E UMA IMAGEM
		if(@!eregi("^image\/(pjpeg|jpeg|gif|bmp)$", $arquivo["type"]))
		{
			Util::script_msg("Arquivo em formato inválido! A imagem deve ser jpg, jpeg, bmp, gif ou png. Envie outro arquivo");
			Util::script_go_back();
			exit();
		}
		
		
		//	VERIFICO OS PARAMETRO PASSADO
		$parametros = func_get_args();
		
		
		switch(func_num_args())
		{
			case 3:
				$tamanho_maximo = $parametros[2];
			break;
			case 4:
				$tamanho_maximo = $parametros[2];
				$largura_maxima = $parametros[3];
			break;
			case 5:
				$tamanho_maximo = $parametros[2];
				$largura_maxima = $parametros[3];
				$altura_maxima = $parametros[4];
			break;
			case 6:
				$tamanho_maximo = $parametros[2];
				$largura_maxima = $parametros[3];
				$altura_maxima = $parametros[4];
				$largura_minima = $parametros[5];
			break;
			case 7:
				$tamanho_maximo = $parametros[2];
				$largura_maxima = $parametros[3];
				$altura_maxima = $parametros[4];
				$largura_minima = $parametros[5];
				$altura_minima = $parametros[6];
			break;
		}
		
		
		
		
		// VERIFICO O TAMANHO MAXIMO DO ARQUIVO CASO TENHA SIDO PASSADO
		if(!empty($tamanho_maximo))
		{
			if($arquivo["size"] > $tamanho_maximo)
			{
				Util::script_msg("Arquivo em tamanho muito grande! A imagem deve ser de no máximo " . $tamanho_maximo . " bytes. Envie outro arquivo");
				Util::script_go_back();
				exit();
			}
		}
		
		
		
		// PEGO AS DIMENSOES DA IMAGEM
        $tamanhos = getimagesize($arquivo["tmp_name"]);	
		
		
		
		// VERIFICO A LARGURA MAXIMA DO ARQUIVO CASO TENHA SIDO PASSADO
		if(!empty($largura_maxima))
		{
			if($tamanhos[0] > $largura_maxima)
			{
				Util::script_msg("Largura da imagem não deve ultrapassar " . $largura_maxima . " pixels");
			 	Util::script_go_back();
				exit();
			}
		}
		
		
		// VERIFICO A ALTURA MAXIMA DO ARQUIVO CASO TENHA SIDO PASSADO
		if(!empty($altura_maxima))
		{
			if($tamanhos[1] > $altura_maxima)
			{
				Util::script_msg("Altura da imagem não deve ultrapassar " . $altura_maxima . " pixels");
				Util::script_go_back();
				exit();
			}
		}
		
		
		
		
		// VERIFICO A LARGURA MININA DO ARQUIVO CASO TENHA SIDO PASSADO
		if(!empty($largura_minima))
		{
			if($tamanhos[0] < $largura_minima)
			{
				Util::script_msg("Largura da imagem não deve ser menor que " . $largura_minima . " pixels");
			 	Util::script_go_back();
				exit();
			}
		}
		
		
		// VERIFICO A ALTURA MINIMA DO ARQUIVO CASO TENHA SIDO PASSADO
		if(!empty($altura_minima))
		{
			if($tamanhos[1] < $altura_minima)
			{
				Util::script_msg("Altura da imagem não deve ser menor que " . $altura_minima . " pixels");
				Util::script_go_back();
				exit();
			}
		}
		
		
		//	GERO O NOME DO ARQUIVO		
		$nome_arquivo_final = strtolower(date("dmYhi") . rand(1111111111, 9999999999) . '.' . substr($arquivo[name], -4));
		
		// do as permissoes do diretorio
		@chmod($destino_arquivo, 0777);
		// faço o upload
		if(!move_uploaded_file($arquivo['tmp_name'], $destino_arquivo . '/' . $nome_arquivo_final))
		{
			Util::script_msg("ERRO AO ENVIAR O ARQUIVO!");
			Util::script_go_back();
			exit();
		}
		
		//	REDIMENSIONA A IMAGEM
		$obj_imagem->load($destino_arquivo . '/' . $nome_arquivo_final);
		$obj_imagem->redimension_pela_proporcao(1980);
		$obj_imagem->save($destino_arquivo . '/' . $nome_arquivo_final);
		
		return $nome_arquivo_final;
	}
	
	
	
	#-----------------------------------------------------------------------------------------------------------------------#
	#	VERIFICO SE O EMAIL É VALIDO
	#-----------------------------------------------------------------------------------------------------------------------#
	function verifica_email($email)
	{
	   $mail_correcto = 0;
	   //verifico umas coisas
		if ((strlen($email) >= 6) && (substr_count($email,"@") == 1) && (substr($email,0,1) != "@") && (substr($email,strlen($email)-1,1) != "@"))
	   	{
			if ((!strstr($email,"'")) && (!strstr($email,"\"")) && (!strstr($email,"\\")) && (!strstr($email,"\$")) && (!strstr($email," ")))
		  	{
				//vejo se tem caracter .
			 	if (substr_count($email,".")>= 1)
			 	{
					//obtenho a terminação do dominio
					$term_dom = substr(strrchr ($email, '.'),1);
					//verifico que a terminação do dominio seja correcta
				 	if (strlen($term_dom)>1 && strlen($term_dom)<5 && (!strstr($term_dom,"@")) )
				 	{
						//verifico que o de antes do dominio seja correcto
						$antes_dom = substr($email,0,strlen($email) - strlen($term_dom) - 1);
						$caracter_ult = substr($antes_dom,strlen($antes_dom)-1,1);

						if ($caracter_ult != "@" && $caracter_ult != ".")
						{
					   		$mail_correcto = 1;
						}
				 	}
		  		}
	   		}
		}

		return $mail_correcto;
	}
	
	
	
	
	
	
	#--------------------------------------------------------------------------------------------------------------#
	#	APAGA UM ARQUIVO
	#--------------------------------------------------------------------------------------------------------------#
	public function deleta_arquivo($destino_arquivo, $arquivo)
	{ 				
		@chmod($destino_arquivo, 0777);
		if(unlink($destino_arquivo . $arquivo) == false)
		{
			Util::script_msg("ERRO AO APAGAR O ARQUIVO!");
			Util::script_go_back();
			exit();
		}
	}
	
	
	#--------------------------------------------------------------------------------------------------------------#
	#	VERIFICA A EXTENSAO
	#--------------------------------------------------------------------------------------------------------------#
	public function verifica_extensao($arquivo)
	{ 	
		$extensao = explode(".", $arquivo);
		
		//	SELECIONA A EXTENSAO
		switch($extensao[1])
		{
			case "pdf":
			?>
            	<img src="<?php echo Util::caminho_projeto(); ?>/uploads/icon-pdf.jpg" alt="PDF" />
            <?php
			break;
		}
	}
	
	
	#-----------------------------------------------------------------------------------------------------------------------#
	#	ENVIA EMAIL
	#-----------------------------------------------------------------------------------------------------------------------#
	function envia_email($emaildestinatario, $assunto, $mensagem, $nomeremetente, $emailremetente, $comcopia = '', $comcopiaoculta = ''){

		$mensagem .= "<br><br><br>Mensagem enviada pelo site: " . $_SERVER[SERVER_NAME];

		$emails =  explode(",", $emaildestinatario);

		foreach ($emails as $key => $emaildestinatario) {
			$ok = Util::envia_email_smtp($emaildestinatario, $assunto, $mensagem, $nomeremetente, $emailremetente, $comcopia = '', $comcopiaoculta = '');
		}

		return $ok;
	}



		#-----------------------------------------------------------------------------------------------------------------------#
	#	ENVIA EMAIL
	#-----------------------------------------------------------------------------------------------------------------------#
	function envia_email_smtp($emaildestinatario, $assunto, $mensagem, $nomeremetente, $emailremetente, $comcopia = '', $comcopiaoculta = ''){
 
		$mailer = new PHPMailer();
		$mailer->IsSMTP();
		$mailer->SMTPDebug = 0;
		//$mailer->Port = 2525; //Indica a porta de conexão para a saída de e-mails. Utilize obrigatoriamente a porta 587.
		$mailer->Port = 587; //Indica a porta de conexão para a saída de e-mails. Utilize obrigatoriamente a porta 587.

		$mailer->Host = 'smtp.sendgrid.net'; //Onde em 'servidor_de_saida' deve ser alterado por um dos hosts abaixo:
		//Para cPanel: 'localhost';
		//Para Plesk 11 / 11.5: 'smtp.dominio.com.br';

		//Descomente a linha abaixo caso revenda seja 'Plesk 11.5 Linux'
		$mailer->SMTPSecure = 'SSL';

		$mailer->SMTPAuth = true; //Define se haverá ou não autenticação no SMTP
		$mailer->Username = 'apikey'; //Informe o e-mai o completo
		$mailer->Password = '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'; //Senha da caixa postal
		$mailer->FromName = $nomeremetente; // Seu nome
		$mailer->From = $emailremetente; //Obrigatório ser a mesma caixa postal indicada em "username"
		$mailer->addReplyTo($emailremetente, $nomeremetente);
		$mailer->AddAddress($emaildestinatario); //Destinatários
		$mailer->isHTML(true);
		$mailer->CharSet = "UTF-8";

		if (!empty($comcopia)) {
			$mailer->addCC($comcopia); //copia
		}

		if (!empty($comcopiaoculta)) {
			$mailer->addBCC($comcopiaoculta); //copia
		}

		$mailer->Subject = "$assunto";
		$mailer->Body = "$mensagem";

		if(!$mailer->Send()){
			return false;
			//Util::envia_email_2($emaildestinatario, $assunto, $mensagem, $nomeremetente, $emailremetente, $comcopia = '', $comcopiaoculta = '');
			/*
			echo "Mensagem nao enviada";
			echo "Erro: " . $mailer->ErrorInfo; exit;
			*/
		}else{
			return true;
			//print "E-mail enviado! <br />";
		}

	}




	#-----------------------------------------------------------------------------------------------------------------------#

	#	ENVIA EMAIL

	#-----------------------------------------------------------------------------------------------------------------------#

	function envia_email_2($emaildestinatario, $assunto, $mensagem, $nomeremetente, $emailremetente, $comcopia = '', $comcopiaoculta = ''){


		$mailer = new PHPMailer();
		$mailer->IsSMTP();
		$mailer->SMTPDebug = 0;
		$mailer->Port = 587; //Indica a porta de conexão para a saída de e-mails. Utilize obrigatoriamente a porta 587.
		 
		$mailer->Host = 'smtp.gmail.com'; //Onde em 'servidor_de_saida' deve ser alterado por um dos hosts abaixo:
		//Para cPanel: 'localhost';
		//Para Plesk 11 / 11.5: 'smtp.dominio.com.br';
		 
		//Descomente a linha abaixo caso revenda seja 'Plesk 11.5 Linux'
		$mailer->SMTPSecure = 'tls';
		 
		$mailer->SMTPAuth = true; //Define se haverá ou não autenticação no SMTP
		$mailer->Username = GMAIL_USUARIO; //Informe o e-mai o completo
		$mailer->Password = GMAIL_SENHA; //Senha da caixa postal
		$mailer->FromName = $nomeremetente; // Seu nome
		$mailer->From = GMAIL_USUARIO; //Obrigatório ser a mesma caixa postal indicada em "username"
		$mailer->addReplyTo($emailremetente, $nomeremetente);
		$mailer->AddAddress($emaildestinatario); //Destinatários
		$mailer->isHTML(true);

		if (!empty($comcopia)) {
			$mailer->addCC($comcopia); //copia
		}		

		if (!empty($comcopiaoculta)) {
			$mailer->addBCC($comcopiaoculta); //copia
		}		
		
		
		$mailer->Subject = "$assunto";
		$mailer->Body = "$mensagem";
		

		if(!$mailer->Send()){
			Util::envia_email_2($emaildestinatario, $assunto, $mensagem, $nomeremetente, $emailremetente, $comcopia = '', $comcopiaoculta = '');
			/*
			echo "Mensagem nao enviada";
			echo "Erro: " . $mailer->ErrorInfo; exit; 
			*/
		}else{
			return true;
			//print "E-mail enviado! <br />";
		}





	}




	#-----------------------------------------------------------------------------------------------------------------------#

	#	ENVIA EMAIL

	#-----------------------------------------------------------------------------------------------------------------------#

	function envia_email_1($emaildestinatario, $assunto, $mensagem, $nomeremetente, $emailremetente, $comcopia = '', $comcopiaoculta = ''){


		/* Medida preventiva para evitar que outros domínios sejam remetente da sua mensagem. */
		if (eregi('tempsite.ws$|locaweb.com.br$|hospedagemdesites.ws$|websiteseguro.com$', $_SERVER[HTTP_HOST])) {
		        $emailsender='seu@e-mail.com.br';
		} else {
		        $emailsender = "webmaster@" . $_SERVER[HTTP_HOST];
		        //    Na linha acima estamos forçando que o remetente seja 'webmaster@seudominio',
		        // você pode alterar para que o remetente seja, por exemplo, 'contato@seudominio'.
		}
		 
		/* Verifica qual é o sistema operacional do servidor para ajustar o cabeçalho de forma correta. Não alterar */
		if(PHP_OS == "Linux") $quebra_linha = "\n"; //Se for Linux
		elseif(PHP_OS == "WINNT") $quebra_linha = "\r\n"; // Se for Windows
		else die("Este script nao esta preparado para funcionar com o sistema operacional de seu servidor");
		 
		 
		/* Montando o cabeçalho da mensagem */
		$headers = "MIME-Version: 1.1".$quebra_linha;
		$headers .= "Content-type: text/html; charset=iso-8859-1".$quebra_linha;
		// Perceba que a linha acima contém "text/html", sem essa linha, a mensagem não chegará formatada.
		$headers .= "From: ".$emailsender.$quebra_linha;
		$headers .= "Return-Path: " . $emailsender . $quebra_linha;
		// Esses dois "if's" abaixo são porque o Postfix obriga que se um cabeçalho for especificado, deverá haver um valor.
		// Se não houver um valor, o item não deverá ser especificado.
		if(strlen($comcopia) > 0) $headers .= "Cc: ".$comcopia.$quebra_linha;
		if(strlen($comcopiaoculta) > 0) $headers .= "Bcc: ".$comcopiaoculta.$quebra_linha;
		$headers .= "Reply-To: ".$emailremetente.$quebra_linha;
		// Note que o e-mail do remetente será usado no campo Reply-To (Responder Para)
		 
		/* Enviando a mensagem */
		$ok = mail($emaildestinatario, $assunto, $mensagem, $headers, "-r". $emailsender);

		if ($ok) {
			return true;
		}else{
			//return false;
			Util::envia_email_2($emaildestinatario, $assunto, $mensagem, $nomeremetente, $emailremetente, $comcopia = '', $comcopiaoculta = '');

		}


	}

	
	
	
	#-----------------------------------------------------------------------------------------------------------------------#
	#	CRIA UM SELECT A PARTIR DE UMA TABELA NO BANCO DE DADOS
	#-----------------------------------------------------------------------------------------------------------------------#
	public function cria_select_bd($nome_tabela, $id_tabela, $nome_campo_desejado, $nome_select, $value_checked, $class = "",$titulo = 'Selecione', $javascript="",$complemento="")
	{
		$obj_dao = new Dao();
		
		if(!empty($complemento)){
			$complemento = " WHERE $complemento";
		}else{
			$complemento = "";
		}
		
		
		$sql = "SELECT $id_tabela, $nome_campo_desejado FROM $nome_tabela $complemento ORDER BY $nome_campo_desejado";
		$result = $obj_dao->executaSQL($sql);
		echo "<select name='$nome_select' id='$nome_select' class='$class' $javascript>";
		echo "<option value=''>$titulo</option>";
		
		while($row = mysql_fetch_array($result, MYSQL_BOTH))
		{
			if($row[0] != $value_checked)
			{
				echo "<option value='$row[0]'>". utf8_encode($row[1]) ."</option>";
			}
			else
			{
				echo "<option value='$row[0]' selected='selected'>". utf8_encode($row[1]) ."</option>";
			}
		}
		echo "</select>";
	}
	
	
	
	#-----------------------------------------------------------------------------------------------------------------------#
	#	CRIA UM SELECT A PARTIR DE UMA TABELA NO BANCO DE DADOS
	#-----------------------------------------------------------------------------------------------------------------------#
	public function cria_select_bd_com_sql($nome_select, $value_checked, $sql, $javascript)
	{
		$obj_dao = new Dao();
		$result = $obj_dao->executaSQL($sql);
		echo "<select name='$nome_select' id='$nome_select' $javascript >";
		echo "<option value=''>Selecione</option>";
		
		while($row = mysql_fetch_array($result, MYSQL_BOTH))
		{
			if($row[0] != $value_checked)
			{
				echo "<option value='$row[0]'>". utf8_encode($row[1]) ."</option>";
			}
			else
			{
				echo "<option value='$row[0]' selected='selected'>". utf8_encode($row[1]) ."</option>";
			}
		}
		echo "</select>";
	}
	
	
	
	
	
	#-----------------------------------------------------------------------------------------------------------------------#
	#	EXIBE A DATA NA FORMA CORRETA
	#-----------------------------------------------------------------------------------------------------------------------#
	static public function formata_data($data, $tipo = "")
	{
		if ($data != "")
		{
			//	SE O TIPO FOR EXIBIR RETORNA A DATA NO FORMATO 00/00/0000
			if($tipo == 'banco')
			{
				$data = str_replace("-", "", $data);
				$data = str_replace("/", "", $data);
				$nova_data = (substr($data,4,4)).'/'.(substr($data,2,2)).'/'.(substr($data,0,2));
			}
			else	//	RETORNA A DATA NO FORMATO 0000/00/00
			{
				$data = str_replace("-", "", $data);
				$data = str_replace("/", "", $data);
				$nova_data = (substr($data,6,2)).'/'.(substr($data,4,2)).'/'.(substr($data,0,4));
			}
			
			return($nova_data);
		}
	}
	
	
	

	
	
	#-------------------------------------------------------------------------------------------------#
	#	RETORNA O NOME DE UMA COLUNA DA TABELA
	#-------------------------------------------------------------------------------------------------#
	public function troca_value_nome($value, $nome_tabela, $id_tabela, $nome_campo_desejado)
	{
		if(trim($value) != "")
		{
			$obj_dao = new Dao();
			$sql = "
					SELECT $nome_campo_desejado
					FROM $nome_tabela
					WHERE $id_tabela = $value
					";
			$result = $obj_dao->executaSQL($sql);
			$row = mysql_fetch_assoc($result);
			return "$row[$nome_campo_desejado]";
		}
	}
	
	
	
	
	
	#-----------------------------------------------------------------------------------------------------------------------#
	#	GERA UM SELECT COM OS EVENTOS DO CALENDARIO
	#-----------------------------------------------------------------------------------------------------------------------#
	static public function tipos_eventos_calendario($name, $uf, $class = '')
	{
		//lista de ufs
		$uf = trim($uf);
				$lista_uf = array( 
									"Matéria",
									"Atividade",
									"Prova"
									);
									
									
				echo '<select name="'.$name.'" id="'.$name.'" class="'.$class.'">';
				echo '<option value="">Selecione</option>';
				
				for($i=0; $i < count($lista_uf); $i++)
				{
					//verica se há na lista e check em caso afirmativo
					if( $uf == $lista_uf[$i] )
					{
					    echo '<option value="'. utf8_encode($lista_uf[$i]) .'" selected="selected">';
							echo $lista_uf[$i];
						echo '</option>';
					}
					else
					{
						echo '<option value="'. $lista_uf[$i] .'">';
							echo utf8_encode($lista_uf[$i]);
						echo '</option>';
					}
				}

			echo '</select>';
	}
	
	
	
	
	#-----------------------------------------------------------------------------------------------------------------------#
	#	GERA UM SELECT COM OS EVENTOS DO CALENDARIO
	#-----------------------------------------------------------------------------------------------------------------------#
	static public function get_tipo_banner($name, $valor, $class = '', $titulo = "Selecione", $complemento_javascript = "")
	{
		  $obj_dao = new Dao();
		  //lista de ufs
		  $valor = trim($valor);
		  $lista = array(
		  				array('id' => 'Index Topo', 'titulo' => "Index Topo"),
						array('id' => 'Index Slider', 'titulo' => "Index Slider"),
						array('id' => 'Index Busca', 'titulo' => "Index Busca"),
						array('id' => 'Index e Classificados', 'titulo' => "Index e Classificados"),
						);
		echo '<select name="'.$name.'" id="'.$name.'" class="'.$class.'">';
		echo "<option value=''>$titulo</option>";
		for($i=0; $i < count($lista); $i++)
		{
			//verica se há na lista e check em caso afirmativo
			if($valor == $lista[$i][id])
			{
				?>
				   <option value="<?php Util::imprime($lista[$i][id]); ?>" selected="selected"><?php Util::imprime($lista[$i][titulo]); ?></option>
				<?php
			}
			else
			{
				?>
				<option value="<?php Util::imprime($lista[$i][id]); ?>"><?php Util::imprime($lista[$i][titulo]); ?></option>
				<?php
			}
		}
		echo '</select>';
	}
	
	
	
	
	
	
	#-----------------------------------------------------------------------------------------------------------------------#
	#	GERA UM SELECT COM OS EVENTOS DO CALENDARIO
	#-----------------------------------------------------------------------------------------------------------------------#
	static public function get_sim_nao($name, $valor, $class = '', $titulo = "Selecione", $complemento_javascript = "")
	{
		  $obj_dao = new Dao();
		  //lista de ufs
		  $valor = trim($valor);
		  $lista = array(
		  				array('id' => 'SIM', 'titulo' => "SIM"),
						array('id' => 'NAO', 'titulo' => "NÃO"),
						);
		echo '<select name="'.$name.'" id="'.$name.'" class="'.$class.'">';
		echo "<option value=''>$titulo</option>";
		for($i=0; $i < count($lista); $i++)
		{
			//verica se há na lista e check em caso afirmativo
			if($valor == $lista[$i][id])
			{
				?>
				   <option value="<?php Util::imprime($lista[$i][id]); ?>" selected="selected"><?php Util::imprime($lista[$i][titulo]); ?></option>
				<?php
			}
			else
			{
				?>
				<option value="<?php Util::imprime($lista[$i][id]); ?>"><?php Util::imprime($lista[$i][titulo]); ?></option>
				<?php
			}
		}
		echo '</select>';
	}
	
	
	
	
	#-----------------------------------------------------------------------------------------------------------------------#
	#	GERA UM SELECT COM OS EVENTOS DO CALENDARIO
	#-----------------------------------------------------------------------------------------------------------------------#
	static public function get_faixa_tipo_venda_imovel($value)
	{
		switch($value)
		{
			case 'ALUGUEL':
			?>
				<img src="<?php echo Util::caminho_projeto() ?>/imgs/img_aluguel.png" alt="Aluguel" />
			<?php
			break;
			case 'LANCAMENTO':
			?>
				<img src="<?php echo Util::caminho_projeto() ?>/imgs/img_lancamento.png" alt="Lançamento" />
			<?php
			break;
			case 'VENDA':
			?>
				<img src="<?php echo Util::caminho_projeto() ?>/imgs/img_venda.png" alt="Venda" />
			<?php
			break;
		}
	}
	
	
	
	#-----------------------------------------------------------------------------------------------------------------------#
	#	GERA UM SELECT COM OS EVENTOS DO CALENDARIO
	#-----------------------------------------------------------------------------------------------------------------------#
	static public function get_tipo_venda_imovel($name, $valor, $class = '', $titulo = "Selecione", $complemento_javascript = "")
	{
		  $obj_dao = new Dao();
		  //lista de ufs
		  $valor = trim($valor);
		  $lista = array(
		  				array('id' => 'DESTAQUES', 'titulo' => "DESTAQUES"),
						array('id' => 'LANCAMENTO', 'titulo' => "LANÇAMENTO"),
						//array('id' => 'VENDA', 'titulo' => "VENDA"),
						);
		echo '<select name="'.$name.'" id="'.$name.'" class="'.$class.'">';
		echo "<option value=''>$titulo</option>";
		for($i=0; $i < count($lista); $i++)
		{
			//verica se há na lista e check em caso afirmativo
			if($valor == $lista[$i][id])
			{
				?>
				   <option value="<?php Util::imprime($lista[$i][id]); ?>" selected="selected"><?php Util::imprime($lista[$i][titulo]); ?></option>
				<?php
			}
			else
			{
				?>
				<option value="<?php Util::imprime($lista[$i][id]); ?>"><?php Util::imprime($lista[$i][titulo]); ?></option>
				<?php
			}
		}
		echo '</select>';
	}
	
	
	
	
	#-----------------------------------------------------------------------------------------------------------------------#
	#	TRATO OS CAMPOS DO FORMULARIO DE ACORDO COM O BANCO
	#-----------------------------------------------------------------------------------------------------------------------#
	static function trata_dados_formulario($valor)
	{
		return utf8_decode(stripslashes (addslashes(trim(mysql_real_escape_string($valor)))));
		
		//return addslashes(trim($valor));
	}
	
	
	
	#-----------------------------------------------------------------------------------------------------------------------#
	# GERA UM SELECT COM TODAS AS UFS
	#-----------------------------------------------------------------------------------------------------------------------#
	static public function  get_tipos($name, $valor, $class = '', $titulo = "Selecione", $complemento_javascript = "")
	{
		//lista de ufs
		  $valor = trim($valor);
		  $lista = array(
		  				array('id' => 'Novo', 'titulo' => "Novo"),
						array('id' => 'Seminovo', 'titulo' => "Seminovo"),
						);
		echo '<select name="'.$name.'" id="'.$name.'" class="'.$class.'">';
		echo "<option value=''>$titulo</option>";
		for($i=0; $i < count($lista); $i++)
		{
			//verica se há na lista e check em caso afirmativo
			if($valor == $lista[$i][id])
			{
				?>
				   <option value="<?php Util::imprime($lista[$i][id]); ?>" selected="selected"><?php Util::imprime($lista[$i][titulo]); ?></option>
				<?php
			}
			else
			{
				?>
				<option value="<?php Util::imprime($lista[$i][id]); ?>"><?php Util::imprime($lista[$i][titulo]); ?></option>
				<?php
			}
		}
		echo '</select>';
	}
	
	
	#-----------------------------------------------------------------------------------------------------------------------#
	#	GERA UM SELECT COM TODAS AS UFS
	#-----------------------------------------------------------------------------------------------------------------------#
	static public function get_ufs($name, $uf, $class = '')
	{
		//lista de ufs
		$uf = trim($uf);
				$lista_uf = array( "AC",
									"AL",
									"AM",
									"AP",
									"BA",
									"CE",
									"DF",
									"ES",
									"GO",
									"MA",
									"MG",
									"MS",
									"MT",
									"PA",
									"PB",
									"PE",
									"PI",
									"PR",
									"RJ",
									"RN",
									"RO",
									"RR",
									"RS",
									"SC",
									"SE",
									"SP",
									"TO");
									
									
				echo '<select name="'.$name.'" id="'.$name.'" class="'.$class.'">';
				echo '<option value="">Selecione</option>';
				
				for($i=0; $i < count($lista_uf); $i++)
				{
					//verica se há na lista e check em caso afirmativo
					if( $uf == $lista_uf[$i] )
					{
					    echo '<option value="'. $lista_uf[$i] .'" selected="selected">';
							echo $lista_uf[$i];
						echo '</option>';
					}
					else
					{
						echo '<option value="'. $lista_uf[$i] .'">';
							echo $lista_uf[$i];
						echo '</option>';
					}
				}

			echo '</select>';
	}


	#-----------------------------------------------------------------------------------------------------------------------#
	#	GERA UM SELECT COM TODAS AS UFS
	#-----------------------------------------------------------------------------------------------------------------------#
	static public function get_avaliacao($name, $uf, $class = '')
	{
		//lista de ufs
		$uf = trim($uf);
				$lista_uf = array(  "Excelente",
									"Ótimo",
									"Bom",
									"Regular",
									"Ruim");
									
									
				echo '<select name="'.$name.'" id="'.$name.'" class="'.$class.'">';
				echo '<option value="">Selecione</option>';
				
				for($i=0; $i < count($lista_uf); $i++)
				{
					//verica se há na lista e check em caso afirmativo
					if( $uf == $lista_uf[$i] )
					{
					    echo '<option value="'. $lista_uf[$i] .'" selected="selected">';
							echo $lista_uf[$i];
						echo '</option>';
					}
					else
					{
						echo '<option value="'. $lista_uf[$i] .'">';
							echo $lista_uf[$i];
						echo '</option>';
					}
				}

			echo '</select>';
	}


	static function exibe_avaliacao($nota){
		switch ($nota){
			case 'Excelente':
				echo '
					 <i class="fa fa-star"></i>
					 <i class="fa fa-star"></i>
					 <i class="fa fa-star"></i>
					 <i class="fa fa-star"></i>
					 <i class="fa fa-star"></i>
				     ';
			break;
			case 'Ótimo':
				echo '
					 <i class="fa fa-star"></i>
					 <i class="fa fa-star"></i>
					 <i class="fa fa-star"></i>
					 <i class="fa fa-star"></i>
					 <i class="fa fa-star-o"></i>
				     ';
			break;
			case 'Bom':
				echo '
					 <i class="fa fa-star"></i>
					 <i class="fa fa-star"></i>
					 <i class="fa fa-star"></i>
					 <i class="fa fa-star-o"></i>
					 <i class="fa fa-star-o"></i>
				     ';
			break;
			case 'Regular':
				echo '
					 <i class="fa fa-star"></i>
					 <i class="fa fa-star"></i>
					 <i class="fa fa-star-o"></i>
					 <i class="fa fa-star-o"></i>
					 <i class="fa fa-star-o"></i>
				     ';
			break;
			case 'Ruim':
				echo '
					 <i class="fa fa-star"></i>
					 <i class="fa fa-star-o"></i>
					 <i class="fa fa-star-o"></i>
					 <i class="fa fa-star-o"></i>
					 <i class="fa fa-star-o"></i>
				     ';
			break;
		}
	}
	
	
	
	static public function formata_moeda($valor, $tipo = 'exibir')
	{
		//	RETORNA O VALOR NO FORMATO DE MOEDA EX: 1.200,34
		if($tipo == 'exibir')
		{
			$valor_final = number_format($valor, 2, ',', '.');
		}
		else	//	RETORNA NO VALOR DO BANCO EX: 1200.34
		{
			//Remove o . (ponto)
			$novovalor = str_replace(".","","$valor");
			//Substitui a , por . (ponto)
			$valor_final = str_replace(",",".","$novovalor");
		}
		
		return $valor_final;
	}
	
	#-----------------------------------------------------------------------------------------------------------------------#
	# GERA ANOS
	#-----------------------------------------------------------------------------------------------------------------------#
	static public function get_anos($name, $ano, $titulo)
	{
		echo '<select name="'.$name.'" id="'.$name.'" class="">';
		echo "<option value=''>$titulo</option>"; 
		
		
		$ano_selecionado_temp = date("Y");
		
		for($i=0; $i < 30; $i++)
		{
			
			$ano_selecionado = $ano_selecionado_temp - $i;
			
			//verica se há na lista e check em caso afirmativo
			if($ano == $ano_selecionado)
			{
				?>
				   <option value="<?php Util::imprime($ano_selecionado); ?>" selected="selected"><?php Util::imprime($ano_selecionado); ?></option>
				<?php
			}
			else
			{
				?>
				<option value="<?php Util::imprime($ano_selecionado); ?>"><?php Util::imprime($ano_selecionado); ?></option>
				<?php
			}
		}
		echo '</select>';

	}





	#-------------------------------------------------------------------------------------------------------------------#
	#	FAZ UM TEXT AREA COM CKEDITOR
	#-------------------------------------------------------------------------------------------------------------------#
	static public function ckeditor($nome_campo, $value)
	{
		$CKEditor = new CKEditor();
		//$CKEditor->config['toolbar'] = "Full";
		$CKEditor->config['toolbar'] = array(
								  array('Source'/*,'-','Save','NewPage','Preview','-','Templates'*/),
								  array('Cut','Copy','Paste','PasteText','PasteFromWord'/*,'-','Print', 'SpellChecker', 'Scayt'*/),
								  array('Undo','Redo','-','Find','Replace','-','SelectAll','RemoveFormat'),
								  /*array('Form', 'Checkbox', 'Radio', 'TextField', 'Textarea', 'Select', 'Button', 'ImageButton', 'HiddenField'),*/
								  '/',
								  array('Bold','Italic','Underline','Strike','-','Subscript','Superscript'),								  
								  array('NumberedList','BulletedList'/*,'-','Outdent','Indent','Blockquote','CreateDiv'*/),
								  array('JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'),
								  array('Link','Unlink','Anchor'),
								  array('YouTube', 'Image','Flash','Table','HorizontalRule','Smiley','SpecialChar','PageBreak'),
								 
								  array('Styles','Format','Font','FontSize'),
								  array('TextColor','BGColor'),
								  array('Maximize', 'ShowBlocks','-','About')
								  );
		
		$CKEditor->config['filebrowserBrowseUrl'] = 		Util::caminho_projeto() . '/jquery/ckfinder/ckfinder.html';
		$CKEditor->config['filebrowserImageBrowseUrl'] = 	Util::caminho_projeto() . '/jquery/ckfinder/ckfinder.html?type=Images';
		$CKEditor->config['filebrowserFlashBrowseUrl'] = 	Util::caminho_projeto() . '/jquery/ckfinder/ckfinder.html?type=Flash';
		$CKEditor->config['filebrowserUploadUrl'] = 		Util::caminho_projeto() . '/jquery/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files';
		$CKEditor->config['filebrowserImageUploadUrl'] = 	Util::caminho_projeto() . '/jquery/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images';
		$CKEditor->config['filebrowserFlashUploadUrl'] = 	Util::caminho_projeto() . '/jquery/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash';
		$CKEditor->editor("$nome_campo", "$value");
	}
	
	
	
	
	
	static public function mobile_device_detect($iphone=true,$android=true,$opera=true,$blackberry=true,$palm=true,$windows=true,$mobileredirect=false,$desktopredirect=false)
	{

		$mobile_browser   = false; // set mobile browser as false till we can prove otherwise
		$user_agent       = $_SERVER['HTTP_USER_AGENT']; // get the user agent value - this should be cleaned to ensure no nefarious input gets executed
		$accept           = $_SERVER['HTTP_ACCEPT']; // get the content accept value - this should be cleaned to ensure no nefarious input gets executed
		
		switch(true){ // using a switch against the following statements which could return true is more efficient than the previous method of using if statements
		
		case (eregi('ipod',$user_agent)||eregi('iphone',$user_agent)); // we find the words iphone or ipod in the user agent
		  $mobile_browser = $iphone; // mobile browser is either true or false depending on the setting of iphone when calling the function
		  $status = 'Apple';
		  if(substr($iphone,0,4)=='http'){ // does the value of iphone resemble a url
			$mobileredirect = $iphone; // set the mobile redirect url to the url value stored in the iphone value
		  } // ends the if for iphone being a url
		break; // break out and skip the rest if we've had a match on the iphone or ipod
		
		case (eregi('android',$user_agent));  // we find android in the user agent
		  $mobile_browser = $android; // mobile browser is either true or false depending on the setting of android when calling the function
		  $status = 'Android';
		  if(substr($android,0,4)=='http'){ // does the value of android resemble a url
			$mobileredirect = $android; // set the mobile redirect url to the url value stored in the android value
		  } // ends the if for android being a url
		break; // break out and skip the rest if we've had a match on android
		
		case (eregi('opera mini',$user_agent)); // we find opera mini in the user agent
		  $mobile_browser = $opera; // mobile browser is either true or false depending on the setting of opera when calling the function
		  $status = 'Opera';
		  if(substr($opera,0,4)=='http'){ // does the value of opera resemble a rul
			$mobileredirect = $opera; // set the mobile redirect url to the url value stored in the opera value
		  } // ends the if for opera being a url 
		break; // break out and skip the rest if we've had a match on opera
		
		case (eregi('blackberry',$user_agent)); // we find blackberry in the user agent
		  $mobile_browser = $blackberry; // mobile browser is either true or false depending on the setting of blackberry when calling the function
		  $status = 'Blackberry';
		  if(substr($blackberry,0,4)=='http'){ // does the value of blackberry resemble a rul
			$mobileredirect = $blackberry; // set the mobile redirect url to the url value stored in the blackberry value
		  } // ends the if for blackberry being a url 
		break; // break out and skip the rest if we've had a match on blackberry
		
		case (preg_match('/(pre\/|palm os|palm|hiptop|avantgo|fennec|plucker|xiino|blazer|elaine)/i',$user_agent)); // we find palm os in the user agent - the i at the end makes it case insensitive
		  $mobile_browser = $palm; // mobile browser is either true or false depending on the setting of palm when calling the function
		  $status = 'Palm';
		  if(substr($palm,0,4)=='http'){ // does the value of palm resemble a rul
			$mobileredirect = $palm; // set the mobile redirect url to the url value stored in the palm value
		  } // ends the if for palm being a url 
		break; // break out and skip the rest if we've had a match on palm os
		
		case (preg_match('/(iris|3g_t|windows ce|opera mobi|windows ce; smartphone;|windows ce; iemobile)/i',$user_agent)); // we find windows mobile in the user agent - the i at the end makes it case insensitive
		  $mobile_browser = $windows; // mobile browser is either true or false depending on the setting of windows when calling the function
		  $status = 'Windows Smartphone';
		  if(substr($windows,0,4)=='http'){ // does the value of windows resemble a rul
			$mobileredirect = $windows; // set the mobile redirect url to the url value stored in the windows value
		  } // ends the if for windows being a url 
		break; // break out and skip the rest if we've had a match on windows
		
		case (preg_match('/(mini 9.5|vx1000|lge |m800|e860|u940|ux840|compal|wireless| mobi|ahong|lg380|lgku|lgu900|lg210|lg47|lg920|lg840|lg370|sam-r|mg50|s55|g83|t66|vx400|mk99|d615|d763|el370|sl900|mp500|samu3|samu4|vx10|xda_|samu5|samu6|samu7|samu9|a615|b832|m881|s920|n210|s700|c-810|_h797|mob-x|sk16d|848b|mowser|s580|r800|471x|v120|rim8|c500foma:|160x|x160|480x|x640|t503|w839|i250|sprint|w398samr810|m5252|c7100|mt126|x225|s5330|s820|htil-g1|fly v71|s302|-x113|novarra|k610i|-three|8325rc|8352rc|sanyo|vx54|c888|nx250|n120|mtk |c5588|s710|t880|c5005|i;458x|p404i|s210|c5100|teleca|s940|c500|s590|foma|samsu|vx8|vx9|a1000|_mms|myx|a700|gu1100|bc831|e300|ems100|me701|me702m-three|sd588|s800|8325rc|ac831|mw200|brew |d88|htc\/|htc_touch|355x|m50|km100|d736|p-9521|telco|sl74|ktouch|m4u\/|me702|8325rc|kddi|phone|lg |sonyericsson|samsung|240x|x320vx10|nokia|sony cmd|motorola|up.browser|up.link|mmp|symbian|smartphone|midp|wap|vodafone|o2|pocket|kindle|mobile|psp|treo)/i',$user_agent)); // check if any of the values listed create a match on the user agent - these are some of the most common terms used in agents to identify them as being mobile devices - the i at the end makes it case insensitive
		  $mobile_browser = true; // set mobile browser to true
		  $status = 'Mobile matched on piped preg_match';
		break; // break out and skip the rest if we've preg_match on the user agent returned true 
		
		case ((strpos($accept,'text/vnd.wap.wml')>0)||(strpos($accept,'application/vnd.wap.xhtml+xml')>0)); // is the device showing signs of support for text/vnd.wap.wml or application/vnd.wap.xhtml+xml
		  $mobile_browser = true; // set mobile browser to true
		  $status = 'Mobile matched on content accept header';
		break; // break out and skip the rest if we've had a match on the content accept headers
		
		case (isset($_SERVER['HTTP_X_WAP_PROFILE'])||isset($_SERVER['HTTP_PROFILE'])); // is the device giving us a HTTP_X_WAP_PROFILE or HTTP_PROFILE header - only mobile devices would do this
		  $mobile_browser = true; // set mobile browser to true
		  $status = 'Mobile matched on profile headers being set';
		break; // break out and skip the final step if we've had a return true on the mobile specfic headers
		
		case (in_array(strtolower(substr($user_agent,0,4)),array('1207'=>'1207','3gso'=>'3gso','4thp'=>'4thp','501i'=>'501i','502i'=>'502i','503i'=>'503i','504i'=>'504i','505i'=>'505i','506i'=>'506i','6310'=>'6310','6590'=>'6590','770s'=>'770s','802s'=>'802s','a wa'=>'a wa','acer'=>'acer','acs-'=>'acs-','airn'=>'airn','alav'=>'alav','asus'=>'asus','attw'=>'attw','au-m'=>'au-m','aur '=>'aur ','aus '=>'aus ','abac'=>'abac','acoo'=>'acoo','aiko'=>'aiko','alco'=>'alco','alca'=>'alca','amoi'=>'amoi','anex'=>'anex','anny'=>'anny','anyw'=>'anyw','aptu'=>'aptu','arch'=>'arch','argo'=>'argo','bell'=>'bell','bird'=>'bird','bw-n'=>'bw-n','bw-u'=>'bw-u','beck'=>'beck','benq'=>'benq','bilb'=>'bilb','blac'=>'blac','c55/'=>'c55/','cdm-'=>'cdm-','chtm'=>'chtm','capi'=>'capi','cond'=>'cond','craw'=>'craw','dall'=>'dall','dbte'=>'dbte','dc-s'=>'dc-s','dica'=>'dica','ds-d'=>'ds-d','ds12'=>'ds12','dait'=>'dait','devi'=>'devi','dmob'=>'dmob','doco'=>'doco','dopo'=>'dopo','el49'=>'el49','erk0'=>'erk0','esl8'=>'esl8','ez40'=>'ez40','ez60'=>'ez60','ez70'=>'ez70','ezos'=>'ezos','ezze'=>'ezze','elai'=>'elai','emul'=>'emul','eric'=>'eric','ezwa'=>'ezwa','fake'=>'fake','fly-'=>'fly-','fly_'=>'fly_','g-mo'=>'g-mo','g1 u'=>'g1 u','g560'=>'g560','gf-5'=>'gf-5','grun'=>'grun','gene'=>'gene','go.w'=>'go.w','good'=>'good','grad'=>'grad','hcit'=>'hcit','hd-m'=>'hd-m','hd-p'=>'hd-p','hd-t'=>'hd-t','hei-'=>'hei-','hp i'=>'hp i','hpip'=>'hpip','hs-c'=>'hs-c','htc '=>'htc ','htc-'=>'htc-','htca'=>'htca','htcg'=>'htcg','htcp'=>'htcp','htcs'=>'htcs','htct'=>'htct','htc_'=>'htc_','haie'=>'haie','hita'=>'hita','huaw'=>'huaw','hutc'=>'hutc','i-20'=>'i-20','i-go'=>'i-go','i-ma'=>'i-ma','i230'=>'i230','iac'=>'iac','iac-'=>'iac-','iac/'=>'iac/','ig01'=>'ig01','im1k'=>'im1k','inno'=>'inno','iris'=>'iris','jata'=>'jata','java'=>'java','kddi'=>'kddi','kgt'=>'kgt','kgt/'=>'kgt/','kpt '=>'kpt ','kwc-'=>'kwc-','klon'=>'klon','lexi'=>'lexi','lg g'=>'lg g','lg-a'=>'lg-a','lg-b'=>'lg-b','lg-c'=>'lg-c','lg-d'=>'lg-d','lg-f'=>'lg-f','lg-g'=>'lg-g','lg-k'=>'lg-k','lg-l'=>'lg-l','lg-m'=>'lg-m','lg-o'=>'lg-o','lg-p'=>'lg-p','lg-s'=>'lg-s','lg-t'=>'lg-t','lg-u'=>'lg-u','lg-w'=>'lg-w','lg/k'=>'lg/k','lg/l'=>'lg/l','lg/u'=>'lg/u','lg50'=>'lg50','lg54'=>'lg54','lge-'=>'lge-','lge/'=>'lge/','lynx'=>'lynx','leno'=>'leno','m1-w'=>'m1-w','m3ga'=>'m3ga','m50/'=>'m50/','maui'=>'maui','mc01'=>'mc01','mc21'=>'mc21','mcca'=>'mcca','medi'=>'medi','meri'=>'meri','mio8'=>'mio8','mioa'=>'mioa','mo01'=>'mo01','mo02'=>'mo02','mode'=>'mode','modo'=>'modo','mot '=>'mot ','mot-'=>'mot-','mt50'=>'mt50','mtp1'=>'mtp1','mtv '=>'mtv ','mate'=>'mate','maxo'=>'maxo','merc'=>'merc','mits'=>'mits','mobi'=>'mobi','motv'=>'motv','mozz'=>'mozz','n100'=>'n100','n101'=>'n101','n102'=>'n102','n202'=>'n202','n203'=>'n203','n300'=>'n300','n302'=>'n302','n500'=>'n500','n502'=>'n502','n505'=>'n505','n700'=>'n700','n701'=>'n701','n710'=>'n710','nec-'=>'nec-','nem-'=>'nem-','newg'=>'newg','neon'=>'neon','netf'=>'netf','noki'=>'noki','nzph'=>'nzph','o2 x'=>'o2 x','o2-x'=>'o2-x','opwv'=>'opwv','owg1'=>'owg1','opti'=>'opti','oran'=>'oran','p800'=>'p800','pand'=>'pand','pg-1'=>'pg-1','pg-2'=>'pg-2','pg-3'=>'pg-3','pg-6'=>'pg-6','pg-8'=>'pg-8','pg-c'=>'pg-c','pg13'=>'pg13','phil'=>'phil','pn-2'=>'pn-2','pt-g'=>'pt-g','palm'=>'palm','pana'=>'pana','pire'=>'pire','pock'=>'pock','pose'=>'pose','psio'=>'psio','qa-a'=>'qa-a','qc-2'=>'qc-2','qc-3'=>'qc-3','qc-5'=>'qc-5','qc-7'=>'qc-7','qc07'=>'qc07','qc12'=>'qc12','qc21'=>'qc21','qc32'=>'qc32','qc60'=>'qc60','qci-'=>'qci-','qwap'=>'qwap','qtek'=>'qtek','r380'=>'r380','r600'=>'r600','raks'=>'raks','rim9'=>'rim9','rove'=>'rove','s55/'=>'s55/','sage'=>'sage','sams'=>'sams','sc01'=>'sc01','sch-'=>'sch-','scp-'=>'scp-','sdk/'=>'sdk/','se47'=>'se47','sec-'=>'sec-','sec0'=>'sec0','sec1'=>'sec1','semc'=>'semc','sgh-'=>'sgh-','shar'=>'shar','sie-'=>'sie-','sk-0'=>'sk-0','sl45'=>'sl45','slid'=>'slid','smb3'=>'smb3','smt5'=>'smt5','sp01'=>'sp01','sph-'=>'sph-','spv '=>'spv ','spv-'=>'spv-','sy01'=>'sy01','samm'=>'samm','sany'=>'sany','sava'=>'sava','scoo'=>'scoo','send'=>'send','siem'=>'siem','smar'=>'smar','smit'=>'smit','soft'=>'soft','sony'=>'sony','t-mo'=>'t-mo','t218'=>'t218','t250'=>'t250','t600'=>'t600','t610'=>'t610','t618'=>'t618','tcl-'=>'tcl-','tdg-'=>'tdg-','telm'=>'telm','tim-'=>'tim-','ts70'=>'ts70','tsm-'=>'tsm-','tsm3'=>'tsm3','tsm5'=>'tsm5','tx-9'=>'tx-9','tagt'=>'tagt','talk'=>'talk','teli'=>'teli','topl'=>'topl','hiba'=>'hiba','up.b'=>'up.b','upg1'=>'upg1','utst'=>'utst','v400'=>'v400','v750'=>'v750','veri'=>'veri','vk-v'=>'vk-v','vk40'=>'vk40','vk50'=>'vk50','vk52'=>'vk52','vk53'=>'vk53','vm40'=>'vm40','vx98'=>'vx98','virg'=>'virg','vite'=>'vite','voda'=>'voda','vulc'=>'vulc','w3c '=>'w3c ','w3c-'=>'w3c-','wapj'=>'wapj','wapp'=>'wapp','wapu'=>'wapu','wapm'=>'wapm','wig '=>'wig ','wapi'=>'wapi','wapr'=>'wapr','wapv'=>'wapv','wapy'=>'wapy','wapa'=>'wapa','waps'=>'waps','wapt'=>'wapt','winc'=>'winc','winw'=>'winw','wonu'=>'wonu','x700'=>'x700','xda2'=>'xda2','xdag'=>'xdag','yas-'=>'yas-','your'=>'your','zte-'=>'zte-','zeto'=>'zeto','acs-'=>'acs-','alav'=>'alav','alca'=>'alca','amoi'=>'amoi','aste'=>'aste','audi'=>'audi','avan'=>'avan','benq'=>'benq','bird'=>'bird','blac'=>'blac','blaz'=>'blaz','brew'=>'brew','brvw'=>'brvw','bumb'=>'bumb','ccwa'=>'ccwa','cell'=>'cell','cldc'=>'cldc','cmd-'=>'cmd-','dang'=>'dang','doco'=>'doco','eml2'=>'eml2','eric'=>'eric','fetc'=>'fetc','hipt'=>'hipt','http'=>'http','ibro'=>'ibro','idea'=>'idea','ikom'=>'ikom','inno'=>'inno','ipaq'=>'ipaq','jbro'=>'jbro','jemu'=>'jemu','java'=>'java','jigs'=>'jigs','kddi'=>'kddi','keji'=>'keji','kyoc'=>'kyoc','kyok'=>'kyok','leno'=>'leno','lg-c'=>'lg-c','lg-d'=>'lg-d','lg-g'=>'lg-g','lge-'=>'lge-','libw'=>'libw','m-cr'=>'m-cr','maui'=>'maui','maxo'=>'maxo','midp'=>'midp','mits'=>'mits','mmef'=>'mmef','mobi'=>'mobi','mot-'=>'mot-','moto'=>'moto','mwbp'=>'mwbp','mywa'=>'mywa','nec-'=>'nec-','newt'=>'newt','nok6'=>'nok6','noki'=>'noki','o2im'=>'o2im','opwv'=>'opwv','palm'=>'palm','pana'=>'pana','pant'=>'pant','pdxg'=>'pdxg','phil'=>'phil','play'=>'play','pluc'=>'pluc','port'=>'port','prox'=>'prox','qtek'=>'qtek','qwap'=>'qwap','rozo'=>'rozo','sage'=>'sage','sama'=>'sama','sams'=>'sams','sany'=>'sany','sch-'=>'sch-','sec-'=>'sec-','send'=>'send','seri'=>'seri','sgh-'=>'sgh-','shar'=>'shar','sie-'=>'sie-','siem'=>'siem','smal'=>'smal','smar'=>'smar','sony'=>'sony','sph-'=>'sph-','symb'=>'symb','t-mo'=>'t-mo','teli'=>'teli','tim-'=>'tim-','tosh'=>'tosh','treo'=>'treo','tsm-'=>'tsm-','upg1'=>'upg1','upsi'=>'upsi','vk-v'=>'vk-v','voda'=>'voda','vx52'=>'vx52','vx53'=>'vx53','vx60'=>'vx60','vx61'=>'vx61','vx70'=>'vx70','vx80'=>'vx80','vx81'=>'vx81','vx83'=>'vx83','vx85'=>'vx85','wap-'=>'wap-','wapa'=>'wapa','wapi'=>'wapi','wapp'=>'wapp','wapr'=>'wapr','webc'=>'webc','whit'=>'whit','winw'=>'winw','wmlb'=>'wmlb','xda-'=>'xda-',))); // check against a list of trimmed user agents to see if we find a match
		  $mobile_browser = true; // set mobile browser to true
		  $status = 'Mobile matched on in_array';
		break; // break even though it's the last statement in the switch so there's nothing to break away from but it seems better to include it than exclude it
		
		default;
		  $mobile_browser = false; // set mobile browser to false
		  $status = 'Desktop / full capability browser';
		break; // break even though it's the last statement in the switch so there's nothing to break away from but it seems better to include it than exclude it
		
		} // ends the switch 
		
		// tell adaptation services (transcoders and proxies) to not alter the content based on user agent as it's already being managed by this script
		//  header('Cache-Control: no-transform'); // http://mobiforge.com/developing/story/setting-http-headers-advise-transcoding-proxies
		//  header('Vary: User-Agent, Accept'); // http://mobiforge.com/developing/story/setting-http-headers-advise-transcoding-proxies
		
		// if redirect (either the value of the mobile or desktop redirect depending on the value of $mobile_browser) is true redirect else we return the status of $mobile_browser
		if($redirect = ($mobile_browser==true) ? $mobileredirect : $desktopredirect){
                        return true;
                        //header('Location: '.$redirect); // redirect to the right url for this device
			//exit;
		}else{ 
			return $mobile_browser; // will return either true or false 
		}
		
	}
	
	
	
}
?>
