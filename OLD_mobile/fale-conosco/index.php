<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();
?>
<!doctype html>
<html>

<head>
  <?php require_once('../includes/head.php'); ?>

</head>

<body class="bg-fale-conosco">


  <?php require_once('../includes/topo.php'); ?>


  
  <!-- ======================================================================= -->
  <!-- legenda  -->
  <!-- ======================================================================= -->
  <div class="container">
    <div class="row legenda-interna">
      <div class="col-xs-12 text-center">
            <h1>FALE CONOSCO</h1>
          <?php $dados= $obj_site->select_unico("tb_empresa", "idempresa", 10) ?>
          <p><?php Util::imprime($dados[descricao], 1000) ?></p>
      </div>
    </div>
  </div>
  <!-- ======================================================================= -->
  <!-- legenda  -->
  <!-- ======================================================================= -->




  <!-- bg-orcamento -->
  <div class="container">
    <div class="row">
      

      <div class=" col-xs-12 media top30">
          <div class="media-left media-middle">
            <img class="media-object" src="<?php echo Util::caminho_projeto() ?>/imgs/icon-phone.png" alt="">
          </div>
          <div class="media-body">
              
              <div class="col-xs-12">
                <?php Util::imprime($config[telefone1]); ?>
                <a href="tel:+55<?php Util::imprime($config[telefone1]); ?>" title="" class="btn btn-primary pulll-left">
                    LIGAR
                </a>  
              </div>
              
              

              <?php if (!empty($config[telefone2])) { ?>
                <div class="col-xs-12">
                   <?php Util::imprime($config[telefone2]); ?>
                  <a href="tel:+55<?php Util::imprime($config[telefone2]); ?>" title="" class="btn btn-primary pulll-left">
                     LIGAR
                  </a>
                </div>
              <?php } ?>

              <?php if (!empty($config[telefone3])) { ?>
                <div class="col-xs-12 top5">
                   <?php Util::imprime($config[telefone3]); ?>
                  <a href="tel:+55<?php Util::imprime($config[telefone3]); ?>" title="" class="btn btn-primary pulll-left">
                     LIGAR
                  </a>
                </div>
              <?php } ?>


              <?php if (!empty($config[telefone4])) { ?>
                <div class="col-xs-12 top5">
                   <?php Util::imprime($config[telefone4]); ?>
                  <a href="tel:+55<?php Util::imprime($config[telefone4]); ?>" title="" class="btn btn-primary pulll-left">
                     LIGAR
                  </a>
                </div>
              <?php } ?>
            

          </div>
        </div>


        <div class=" col-xs-12 media top30 bottom30">
          <div class="media-left media-middle">
            <img class="media-object" src="<?php echo Util::caminho_projeto() ?>/imgs/icon-localizacao.png" alt="">
          </div>
          <div class="media-body">
            <p class="media-heading"><?php Util::imprime($config[endereco]); ?></p>
          </div>
        </div>

    

      <?php
      //  VERIFICO SE E PARA ENVIAR O EMAIL
      if(isset($_POST[btn_contato]))
      {
        $nome_remetente = ($_POST[nome]);
        $email = ($_POST[email]);
        $assunto = ($_POST[assunto]);
        $telefone = ($_POST[telefone]);
        $mensagem = (nl2br($_POST[mensagem]));
        $texto_mensagem = "
                          Nome: $nome_remetente <br />
                          Assunto: $assunto <br />
                          Telefone: $telefone <br />
                          Email: $email <br />
                          Mensagem: <br />
                          $mensagem
                          ";
        Util::envia_email($config[email], utf8_decode($assunto), $texto_mensagem, utf8_decode($nome_remetente), $email);
        Util::envia_email($config[email_copia], utf8_decode($assunto), $texto_mensagem, utf8_decode($nome_remetente), $email);


        Util::alert_bootstrap("Obrigado por entrar em contato.");
        unset($_POST);
      }
      ?>


      <!-- contatos topo  -->
      <form class="form-inline FormContato" role="form" method="post">
        <div class="col-xs-12">
          <h4>FALE CONOSCO</h4>
          <!-- formulario orcamento -->
          <div class="FormContato top10 bottom20">
            <div class="row">
              <div class="col-xs-6 form-group top20 ">
                <label class="glyphicon glyphicon-user"> <span>Nome</span></label>
                <input type="text" name="nome" class="form-control fundo-form1 input100" placeholder="">
              </div>

              <div class="col-xs-6 form-group top20 ">
                <label class="glyphicon glyphicon-envelope"> <span>E-mail</span></label>
                <input type="text" name="email" class="form-control fundo-form1 input100" placeholder="">
              </div>
            </div>

            <div class="row">
              <div class="col-xs-6 form-group top20">
                <label class="glyphicon glyphicon-earphone"> <span>Telefone</span></label>
                <input type="text" name="telefone" class="form-control fundo-form1 input100" placeholder="">
              </div>

              <div class="col-xs-6 form-group top20">
               <label class="glyphicon glyphicon-star"> <span>Assunto</span></label>
               <input type="text" name="assunto" class="form-control fundo-form1 input100" placeholder="">
             </div>
           </div>

           <div class="row">
            <div class="col-xs-12 form-group top20">
              <label class="glyphicon glyphicon-pencil"> <span>Sua Mensagem</span></label>
              <textarea name="mensagem" id="" cols="30" rows="8" class="form-control  fundo-form1 input100" placeholder=""></textarea>
            </div>
          </div>

          <div class="clearfix"></div>

          <div class="text-right  top15">
            <button type="submit" class="btn btn-azul1" name="btn_contato">
              ENVIAR
            </button>
          </div>
        </div>
        <!-- formulario orcamento -->
      </div>
    </form>
  


    <div class="faleConosco top30">
        <iframe class="top30" src="<?php Util::imprime($config[src_place]) ?>" width="480" height="379" frameborder="0" style="border:0" allowfullscreen></iframe>
      </div>



  </div>
</div>
<!-- bg-orcamento -->





<?php require_once('../includes/rodape.php'); ?>


</body>

</html>



<?php require_once("../includes/js_css.php"); ?>


<script>
  $(document).ready(function() {
    $('.FormContato').bootstrapValidator({
      message: 'This value is not valid',
      feedbackIcons: {
        valid: 'glyphicon glyphicon-ok',
        invalid: 'glyphicon glyphicon-remove',
        validating: 'glyphicon glyphicon-refresh'
      },
      fields: {
       nome: {
        validators: {
          notEmpty: {

          }
        }
      },
      email: {
        validators: {
          notEmpty: {

          },
          emailAddress: {
            message: 'Esse endereço de email não é válido'
          }
        }
      },
      telefone: {
        validators: {
          notEmpty: {

          }
        }
      },
      assunto: {
        validators: {
          notEmpty: {

          }
        }
      },
      cidade: {
        validators: {
          notEmpty: {

          }
        }
      },
      estado: {
        validators: {
          notEmpty: {

          }
        }
      },
      mensagem: {
        validators: {
          notEmpty: {

          }
        }
      }
    }
  });
  });
</script>
