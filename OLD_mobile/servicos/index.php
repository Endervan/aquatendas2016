<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();
?>
<!doctype html>
<html>

<head>
  <?php require_once('../includes/head.php'); ?>
</head>

<body class="bg-servicos">


  <?php require_once('../includes/topo.php'); ?>

  

  

  <!-- ======================================================================= -->
  <!-- legenda  -->
  <!-- ======================================================================= -->
  <div class="container">
    <div class="row legenda-interna">
      <div class="col-xs-12 text-center">
            <h1>NOSSOS SERVIÇOS</h1>
          <?php $dados= $obj_site->select_unico("tb_empresa", "idempresa", 1) ?>
          <p><?php Util::imprime($dados[descricao], 1000) ?></p>
      </div>
    </div>
  </div>
  <!-- ======================================================================= -->
  <!-- legenda  -->
  <!-- ======================================================================= -->





  <!-- ======================================================================= -->
<!-- lista servicos  -->
<!-- ======================================================================= -->
<div class="container">
  <div class="row">
    
      
     <?php
     $result = $obj_site->select("tb_servicos");
     if (mysql_num_rows($result) > 0) {
         while($row = mysql_fetch_array($result)){

        
          ?>
            <div class="col-xs-12 bottom40">
              <div class="media lista-servico bottom60">
                <div class="media-left col-xs-4">
                  <a href="<?php echo Util::caminho_projeto() ?>/mobile/servicos/<?php Util::imprime($row[url_amigavel]); ?>">
                    <img class="media-object input100" src="<?php echo Util::caminho_projeto() ?>/uploads/tumb_<?php Util::imprime($row[imagem]); ?>" alt="<?php Util::imprime($row[titulo]); ?>">
                  </a>
                </div>
                <div class="media-body col-xs-8">
                  <a href="<?php echo Util::caminho_projeto() ?>/mobile/servicos/<?php Util::imprime($row[url_amigavel]); ?>">
                    <h4 class="media-heading"><?php Util::imprime($row[titulo]); ?></h4>
                    <p><?php Util::imprime($row[descricao], 1000); ?></p>
                  </a>
                  <div class="text-right top10">
                   <a class="btn btn-azul-claro" href="javascript:void(0);" title="Solicite um orçamento" onclick="add_solicitacao(<?php Util::imprime($row[0]) ?>, 'servico')" id="btn_add_solicitacao_<?php Util::imprime($row[0]) ?>, 'servico'">
                     <i class="fa fa-shopping-cart right10"></i>
                     SOLICITE UM ORÇAMENTO
                   </a> 
                  </div>
                </div>
              </div>
            </div>
          
          
        <?php 
        }
      }
      ?>




        
        
      

    </div>
  </div>
</div>
<!-- ======================================================================= -->
<!-- lista servicos  -->
<!-- ======================================================================= -->








<?php require_once('../includes/rodape.php'); ?>


</body>

</html>

<?php require_once("../includes/js_css.php"); ?>