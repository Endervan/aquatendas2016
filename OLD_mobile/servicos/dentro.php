<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();


// INTERNA DE PRODUTOS
$url = $_GET[get1];


if(!empty($url))
{
  $complemento = "AND url_amigavel = '$url'";
}


$result = $obj_site->select("tb_servicos",$complemento);
if(mysql_num_rows($result)==0)
{
  Util::script_location(Util::caminho_projeto()."/mobile/servicos/");
}

$dados_dentro = mysql_fetch_array($result);

// BUSCA META TAGS E TITLE
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];


?>


<!doctype html>
<html>

<head>
  <?php require_once('../includes/head.php'); ?>
</head>

<body class="bg-servicos">


  <?php require_once('../includes/topo.php'); ?>

  

  <!-- ======================================================================= -->
  <!-- legenda  -->
  <!-- ======================================================================= -->
  <div class="container">
    <div class="row legenda-interna">
      <div class="col-xs-12 text-center">
            <h1>NOSSOS SERVIÇOS</h1>
          <?php $dados= $obj_site->select_unico("tb_empresa", "idempresa", 1) ?>
          <p><?php Util::imprime($dados[descricao], 1000) ?></p>
      </div>
    </div>
  </div>
  <!-- ======================================================================= -->
  <!-- legenda  -->
  <!-- ======================================================================= -->


 

  <!-- flex slider -->
  <div class="container ">
    <div class="row slider-prod-dentro">

      <!-- descricao do produto -->
      <div class="col-xs-12 slider-produtos ">

        <h4 class="bottom20"><?php Util::imprime($dados_dentro[titulo]) ?></h4>

        <?php $obj_site->redimensiona_imagem("../uploads/$dados_dentro[imagem]", 446, 287); ?>

        <p class="bottom35"><?php Util::imprime($dados_dentro[descricao]) ?></p>

        
        <div class="top25 text-center">
          
          <a href="<?php echo Util::caminho_projeto() ?>/mobile/servicos" class="btn btn-azul-claro right15" role="button"><i class="fa fa-angle-double-left"></i> VOLTAR</a>

          <a class="btn btn-azul-claro " href="javascript:void(0);" title="Solicite um orçamento" onclick="add_solicitacao(<?php Util::imprime($dados_dentro[0]) ?>, 'servico')" id="btn_add_solicitacao_<?php Util::imprime($dados_dentro[0]) ?>, 'servico'">
             <i class="fa fa-shopping-cart right10"></i>
             SOLICITE UM ORÇAMENTO
           </a> 
        </div>


      </div>
    </div>
  </div>





<?php require_once('../includes/rodape.php'); ?>


</body>

</html>


<?php require_once("../includes/js_css.php"); ?>






  <!-- FlexSlider -->
  <script defer src="http://masmidia.com.br/clientes/san-remo/jquery/woothemes-FlexSlider-83b3cae/jquery.flexslider.js"></script>
  <!-- Syntax Highlighter -->
  <script type="text/javascript" src="<?php echo Util::caminho_projeto() ?>/jquery/woothemes-FlexSlider-83b3cae/demo/js/shCore.js"></script>
  <script type="text/javascript" src="<?php echo Util::caminho_projeto() ?>/jquery/woothemes-FlexSlider-83b3cae/demo/js/shBrushXml.js"></script>
  <script type="text/javascript" src="<?php echo Util::caminho_projeto() ?>/jquery/woothemes-FlexSlider-83b3cae/demo/js/shBrushJScript.js"></script>

  <!-- Optional FlexSlider Additions -->
  <script src="<?php echo Util::caminho_projeto() ?>/jquery/woothemes-FlexSlider-83b3cae/demo/js/jquery.easing.js"></script>
  <script src="<?php echo Util::caminho_projeto() ?>/jquery/woothemes-FlexSlider-83b3cae/demo/js/jquery.mousewheel.js"></script>
  <script defer src="<?php echo Util::caminho_projeto() ?>/jquery/woothemes-FlexSlider-83b3cae/demo/js/demo.js"></script>


  <!-- Syntax Highlighter -->
  <link href="<?php echo Util::caminho_projeto() ?>/jquery/woothemes-FlexSlider-83b3cae/css/shCore.css" rel="stylesheet" type="text/css" />
  <link href="<?php echo Util::caminho_projeto() ?>/jquery/woothemes-FlexSlider-83b3cae/css/shThemeDefault.css" rel="stylesheet" type="text/css" />


  <!-- Demo CSS -->
  <link rel="stylesheet" href="<?php echo Util::caminho_projeto() ?>/jquery/woothemes-FlexSlider-83b3cae/css/flexslider.css" type="text/css" media="screen" />


  <script>
    $(window).load(function() {
          // The slider being synced must be initialized first
          $('#carousel').flexslider({
            animation: "slide",
            controlNav: false,
            animationLoop: false,
            slideshow: false,
            itemWidth: 51,
            itemMargin:7,
            asNavFor: '#slider'
          });

          $('#slider').flexslider({
            animation: "slide",
            controlNav: false,
            animationLoop: false,
            slideshow: false,
            sync: "#carousel"
          });
        });
  </script>

  <!-- star rating -->
  <script>
    jQuery(document).ready(function () {

      $('.avaliacao').rating({
        min: 0,
        max: 5,
        step: 1,
        size: 'xs',
        showClear: false,
        disabled: true,
        clearCaption: 'Seja o primeiro a avaliar.',
        starCaptions: {
          0.5: 'Half Star',
          1: 'Ruim',
          1.5: 'One & Half Star',
          2: 'Regular',
          2.5: 'Two & Half Stars',
          3: 'Bom',
          3.5: 'Three & Half Stars',
          4: 'Ótimo',
          4.5: 'Four & Half Stars',
          5: 'Excelente'
        }
      });


    });
  </script>
  <!-- star rating -->


  <!-- adicionando ou diminuindo quantidade de produto -->
  <script>
    $(document).ready(function() {
      $("#mais").click(function(){

        if($("#mudeValor").val() < 10){
          $('#mudeValor').val(parseInt($('#mudeValor').val())+1); return false;
        } else {
          $('#limite').html('limite maximo 10');
          return false;
        }

      });
      $("#menos").click(function(){
        $('#limite').html('');
        if($("#mudeValor").val()!=0){$('#mudeValor').val(parseInt($('#mudeValor').val())-1);} return false;
      });
    });
  </script>





<script>
    $(document).ready(function() {
        $('.FormContato').bootstrapValidator({
            message: 'This value is not valid',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                 nome: {
                    validators: {
                        notEmpty: {
                            
                        }
                    }
                },
                email: {
                    validators: {
                        notEmpty: {
                        
                        },
                        emailAddress: {
                            message: 'Esse endereço de email não é válido'
                        }
                    }
                },
                nota: {
                    validators: {
                        notEmpty: {
                        
                        }
                    }
                },
                assunto: {
                    validators: {
                        notEmpty: {
                            
                        }
                    }
                },
                comentario: {
                    validators: {
                        notEmpty: {
                            
                        }
                    }
                }
            }
        });
    });
</script>






