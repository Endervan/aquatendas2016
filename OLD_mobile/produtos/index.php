<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();
?>
<!doctype html>
<html>
<head>
	<?php require_once('../includes/head.php'); ?>
</head>

<body class="bg-produtos">


	<?php require_once('../includes/topo.php'); ?>



  

  <!-- ======================================================================= -->
  <!-- legenda  -->
  <!-- ======================================================================= -->
  <div class="container">
    <div class="row legenda-interna">
      <div class="col-xs-12 text-center">
            <h1>NOSSOS PRODUTOS</h1>
	        <?php $dados= $obj_site->select_unico("tb_empresa", "idempresa", 2) ?>
	        <p><?php Util::imprime($dados[descricao], 1000) ?></p>
      </div>
    </div>
  </div>
  <!-- ======================================================================= -->
  <!-- legenda  -->
  <!-- ======================================================================= -->





  <!-- ======================================================================= -->
  <!-- categorias -->
  <!-- ======================================================================= -->
  <div class="container margin-cat-index">
    
    

    <div class="row sombra-categorias top30">
      <div class="col-xs-12">
        
        <div id="carousel-gallery" class="touchcarousel">
                  <ul class="touchcarousel-container lista-categorias">
                      <li class="destaque touchcarousel-item">
                        <a href="<?php echo Util::caminho_projeto() ?>/mobile/produtos">
                            <img src="<?php echo Util::caminho_projeto() ?>/imgs/icon-estrela.png" alt="">
                            <br>
                            VER TODAS
                        </a>
                    </li>
                    <li class="touchcarousel-item">
                        <a href="<?php echo Util::caminho_projeto() ?>/mobile/piscinas-vinil">
                            <img src="<?php echo Util::caminho_projeto() ?>/imgs/piscina-vinil.png" alt="PISCINAS DE VINIL">
                            <br>
                            PISCINAS DE VINIL
                        </a>
                      </li>

                      <?php 
                      $result = $obj_site->select("tb_categorias_produtos", "ORDER BY rand() LIMIT 12");
                      if(mysql_num_rows($result) > 0)
                      {
                        while($row = mysql_fetch_array($result))
                        {
                        ?>  
                          <li class="touchcarousel-item">
                            <a href="<?php echo Util::caminho_projeto() ?>/mobile/produtos/?cat=<?php Util::imprime($row[url_amigavel]) ?>">
                                <img src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($row[imagem]); ?>" alt="">
                                <br>
                                <?php Util::imprime($row[titulo]) ?>
                            </a>
                        </li>
                              
                        <?php 
                        }
                      }
                      ?>
                  </ul>
              </div>



      </div>
    </div>
  </div>
  <!-- ======================================================================= -->
  <!-- categorias -->
  <!-- ======================================================================= -->





  <!-- ======================================================================= -->
  <!-- produtos -->
  <!-- ======================================================================= -->
  <div class="container">
    <div class="row top50">
       <?php 
            //  titulo
            if (isset($_POST[busca_topo])) {
                $complemento .= "AND titulo LIKE '%$_POST[busca_topo]%'";
            }

            //  categoria
            if (isset($_GET[cat])) {
                $id_categoria = $obj_site->get_id_url_amigavel("tb_categorias_produtos", "idcategoriaproduto", $_GET[cat]);
                $complemento .= "AND id_categoriaproduto = '$id_categoria' ";
            }


        $result = $obj_site->select("tb_produtos", $complemento);
        if (mysql_num_rows($result) == 0) {
          echo "<p class='bg-danger top20 bottom30' style='padding: 20px;'>Nenhum produto encontrado.</p>";
        }else{
          $i = 1;
            while($row = mysql_fetch_array($result)){
        ?>
        <div class="col-xs-6">
          <div class="lista-produto">
            <a href="<?php echo Util::caminho_projeto() ?>/mobile/produtos/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
              <img src="<?php echo Util::caminho_projeto() ?>/uploads/tumb_<?php Util::imprime($row[imagem]); ?>" alt="" class="input100" >
            </a>
            <h1><?php Util::imprime($row[titulo]); ?></h1>
            <p><?php Util::imprime($row[descricao], 110); ?></p>
            <a href="<?php echo Util::caminho_projeto() ?>/mobile/produtos/<?php Util::imprime($row[url_amigavel]); ?>" class="btn btn-azul-pequeno top5 right5">SAIBA MAIS</a>
            <a class="btn btn-azul-pequeno top5" href="javascript:void(0);" title="Solicite um orçamento" onclick="add_solicitacao(<?php Util::imprime($row[0]) ?>, 'produto')" id="btn_add_solicitacao_<?php Util::imprime($row[0]) ?>, 'produto'">SOLICITE UM ORÇAMENTO</a>
          </div>
        </div>
              <?php 

                if ($i == 2) {
                  $i = 1;
                  echo '<div class="clearfix"></div>';
                }else{
                  $i++;
                }


              }
            }
            ?>
    </div>
  </div>
  <!-- ======================================================================= -->
  <!-- produtos -->
  <!-- ======================================================================= -->






	






	<!-- rodape -->
	<?php require_once('../includes/rodape.php') ?>
	<!-- rodape -->

</body>
</html>




<?php require_once("../includes/js_css.php"); ?>


 <!-- ---- LAYER SLIDER ---- -->
<link rel="stylesheet" href="<?php echo Util::caminho_projeto() ?>/jquery/touchcarousel/touchcarousel.css"/>
<link rel="stylesheet" href="?php echo Util::caminho_projeto() ?>/jquery/touchcarousel/black-and-white-skin/black-and-white-skin.css" />
<script src="<?php echo Util::caminho_projeto() ?>/jquery/touchcarousel/jquery.touchcarousel-1.2.min.js"></script>

<script type="text/javascript">
$(document).ready(function() {
	$("#carousel-gallery").touchCarousel({
		itemsPerPage: 3,
		scrollbar: false,
		scrollbarAutoHide: true,
		scrollbarTheme: "dark",
		pagingNav: true,
		snapToItems: true,
		scrollToLast: false,
		useWebkit3d: true,
		loopItems: true
	});
});
</script>
<!-- XXXX LAYER SLIDER XXXX -->