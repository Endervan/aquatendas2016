<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();


// INTERNA DE PRODUTOS
$url = $_GET[get1];


if(!empty($url))
{
  $complemento = "AND url_amigavel = '$url'";
}


$result = $obj_site->select("tb_dicas", $complemento);
if(mysql_num_rows($result)==0)
{
  Util::script_location(Util::caminho_projeto()."/mobile/dicas/");
}

$dados_dentro = mysql_fetch_array($result);

// BUSCA META TAGS E TITLE
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];


?>



<!doctype html>
<html>

<head>
  <?php require_once('../includes/head.php'); ?>


</head>

<body class="bg-dicas">


  <?php require_once('../includes/topo.php'); ?>

  
    <!-- ======================================================================= -->
  <!-- legenda  -->
  <!-- ======================================================================= -->
  <div class="container">
    <div class="row legenda-interna">
      <div class="col-xs-12 text-center">
            <h1>NOSSAS DICAS</h1>
          <?php $dados= $obj_site->select_unico("tb_empresa", "idempresa", 7) ?>
          <p><?php Util::imprime($dados[descricao], 1000) ?></p>
      </div>
    </div>
  </div>
  <!-- ======================================================================= -->
  <!-- legenda  -->
  <!-- ======================================================================= -->






  <!-- ======================================================================= -->
  <!-- categorias  dicas-->
  <!-- ======================================================================= -->
  <div class="container">
    <div class="row">
      <div class="col-xs-12 categorias-produtos">
        <div class="col-xs-3 text-right top20">
          <h1>FILTRAR DICAS:</h1>
        </div>


        <div class="col-xs-9 top20">
          <form action="<?php echo Util::caminho_projeto() ?>/mobile/dicas/" method="post">
            <div class="input-group">
              <input type="text" class="form-control" name="busca_dica" placeholder="TERMO DA BUSCA">
              <span class="input-group-btn">
                <button class="btn btn-default btn-busca" type="submit"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></button>
              </span>
            </div><!-- /input-group -->
          </form>      
        </div>
      </div>
    </div>
  </div>
  <!-- ======================================================================= -->
  <!-- categorias  dicas-->
  <!-- ======================================================================= -->


  

  <!-- ======================================================================= -->
  <!-- descricao -->
  <!-- ======================================================================= -->
  <div class="container">
    <div class="row top25">
      <div class="col-xs-12">
        
        <a href="<?php echo Util::caminho_projeto() ?>/mobile/dicas" class="btn btn-azul-pequeno">VOLTAR</a>

        <h4 class="top15 bottom15"><?php Util::imprime($dados_dentro[titulo]); ?></h4>

        <?php if(!empty($dados_dentro[imagem])): ?>
        <img src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($dados_dentro[imagem]); ?>" alt="" class="input100">
        <?php endif; ?>
        
        <p class="bottom15"><?php Util::imprime($dados_dentro[descricao]); ?></p>
      </div>
    </div>
  </div>
  <!-- ======================================================================= -->
  <!-- descricao -->
  <!-- ======================================================================= -->







  <!-- ======================================================================= -->
  <!-- comentarios -->
  <!-- ======================================================================= -->
  <div class="container bottom25 top30">
    <div class="row comentarios">
      
      <?php
      $result = $obj_site->select("tb_comentarios_dicas", "AND id_dica = '$dados_dentro[0]' ORDER BY data DESC");
      ?>


      <div class="col-xs-12">
        <h4 class="top5">COMENTÁRIOS(<?php echo mysql_num_rows($result) ?>)</h4>
      </div>
     

      
      <!-- descricao comentario -->
      <div class="col-xs-12">
        <?php
        if(mysql_num_rows($result) > 0)
        {
            while($row = mysql_fetch_array($result))
            {
            ?>
              <div class="col-xs-12 top35">
                <div class="jumbotron descricao-comentario">
                  <h1><?php Util::imprime($row[nome]) ?></h1>
                  <p class="top20 bottom35"><?php Util::imprime($row[comentario]) ?></p>

                </div>
              </div>
            <?php  
            }
        }else{
          echo '<div class="clearfix"></div> <p class="bg-warning top20">Nenhum comentário. Seja o primeiro a comentar.</p>';
        }
        ?>
      </div>
      <!-- descricao comentario -->

    </div>
  </div>
  <!-- ======================================================================= -->
  <!-- comentarios -->
  <!-- ======================================================================= -->


  <!-- ======================================================================= -->
  <!-- form comentario -->
  <!-- ======================================================================= -->
<div class="container top30 bottom40">
  <?php 
  if (isset($_POST[comentario])) {
    $obj_site->insert("tb_comentarios_dicas");
    Util::alert_bootstrap("Muito obrigado pelo seu comentário.");
  }
  ?>
  <form class="form-inline FormContato fundo-form" role="form" method="post">

        <div class="row">
            <div class="col-xs-12 form-group">
                <label class="glyphicon glyphicon-user"> Nome</label>
                <input type="text" name="nome" class="form-control input100" placeholder="">
            </div>
            <div class="col-xs-12 form-group">
                <label class="glyphicon glyphicon-envelope"> E-mail</label>
                <input type="text" name="email" class="form-control input100" placeholder="">
            </div>
            <div class="col-xs-12 form-group">
                <label class="fa fa-star"> Nota</label>
                <select name="nota" id="nota" class="form-control input100">
                    <option value="">Selecione</option>
                    <option value="5">Excelente</option>
                    <option value="4">Ótimo</option>
                    <option value="3">Bom</option>
                    <option value="2">Regular</option>
                    <option value="1">Ruim</option>
                </select>
            </div>

            <div class="col-xs-12 top20 form-group">
                <label class="glyphicon glyphicon-pencil"> Comentário</label>
                <textarea name="comentario" id="" cols="30" rows="10" class="form-control input100"></textarea>
            </div>

            <div class="clearfix"></div>

            <input type="hidden" name="id_dica" value="<?php echo $dados_dentro[0] ?>">
            <input type="hidden" name="ativo" value="NAO">
            <input type="hidden" name="data" value="<?php echo date("d/m/Y") ?>">

            <div class="text-right top30">
                <button type="submit" class="btn btn-default">
                    ENVIAR
                </button>
            </div>
          </div>

      </form>
</div>
<!-- ======================================================================= -->
<!-- form comentario -->
<!-- ======================================================================= -->


  <?php require_once('../includes/rodape.php'); ?>


</body>

</html>




<?php require_once("../includes/js_css.php"); ?>





<script>
    $(document).ready(function() {
        $('.FormContato').bootstrapValidator({
            message: 'This value is not valid',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                 nome: {
                    validators: {
                        notEmpty: {
                            
                        }
                    }
                },
                email: {
                    validators: {
                        notEmpty: {
                        
                        },
                        emailAddress: {
                            message: 'Esse endereço de email não é válido'
                        }
                    }
                },
                nota: {
                    validators: {
                        notEmpty: {
                        
                        }
                    }
                },
                assunto: {
                    validators: {
                        notEmpty: {
                            
                        }
                    }
                },
                comentario: {
                    validators: {
                        notEmpty: {
                            
                        }
                    }
                }
            }
        });
    });
</script>










