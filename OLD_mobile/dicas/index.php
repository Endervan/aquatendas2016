<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();
?>
<!doctype html>
<html>

<head>
  <?php require_once('../includes/head.php'); ?>


</head>

<body class="bg-dicas">


  <?php require_once('../includes/topo.php'); ?>

  



   <!-- ======================================================================= -->
  <!-- legenda  -->
  <!-- ======================================================================= -->
  <div class="container">
    <div class="row legenda-interna">
      <div class="col-xs-12 text-center">
            <h1>NOSSAS DICAS</h1>
          <?php $dados= $obj_site->select_unico("tb_empresa", "idempresa", 7) ?>
          <p><?php Util::imprime($dados[descricao], 1000) ?></p>
      </div>
    </div>
  </div>
  <!-- ======================================================================= -->
  <!-- legenda  -->
  <!-- ======================================================================= -->






  <!-- ======================================================================= -->
  <!-- categorias  dicas-->
  <!-- ======================================================================= -->
  <div class="container">
    <div class="row">
      <div class="col-xs-12 categorias-produtos">
        <div class="col-xs-3 text-right top20">
          <h1>FILTRAR DICAS:</h1>
        </div>


        <div class="col-xs-9 top20">
          <form action="<?php echo Util::caminho_projeto() ?>/mobile/dicas/" method="post">
            <div class="input-group">
              <input type="text" class="form-control" name="busca_dica" placeholder="TERMO DA BUSCA">
              <span class="input-group-btn">
                <button class="btn btn-default btn-busca" type="submit"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></button>
              </span>
            </div><!-- /input-group -->
          </form>      
        </div>
      </div>
    </div>
  </div>
  <!-- ======================================================================= -->
  <!-- categorias  dicas-->
  <!-- ======================================================================= -->




   <!-- ======================================================================= -->
  <!-- dicas  -->
  <!-- ======================================================================= -->
  <div class="container">
    <div class="row top50">
      

      <?php
      //  FILTRA PELO TITULO
      if(isset($_POST[busca_dica])):
        $complemento = "AND titulo LIKE '%$_POST[busca_dica]%'";
      endif;

      $result = $obj_site->select("tb_dicas", $complemento);
      if(mysql_num_rows($result) == 0)
      {
        echo "<h2 class='bg-info' style='padding: 20px; margin: 20px;'>Nenhuma dica encontrada.</h2>";
      }else{
          $i = 0;
          while($row = mysql_fetch_array($result)){
          ?>
            <div class="col-xs-6 bottom30">
              <a href="<?php echo Util::caminho_projeto() ?>/mobile/dicas/<?php Util::imprime($row[url_amigavel]); ?>">
                <img class="input100" src="<?php echo Util::caminho_projeto() ?>/uploads/tumb_<?php Util::imprime($row[imagem]); ?>" alt="<?php Util::imprime($row[titulo]); ?>">
              </a>
              <p><?php Util::imprime($row[titulo]); ?></p>
            </div>
          <?php 
            if (++$i == 2) {
              echo '<div class="clearfix"></div>';
              $i = 0;
            }
          }
      }
      ?>
    



    </div>
  </div>
  <!-- ======================================================================= -->
  <!-- dicas  -->
  <!-- ======================================================================= -->





  <?php require_once('../includes/rodape.php'); ?>


</body>

</html>






<?php require_once("../includes/js_css.php"); ?>
