<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();

?>


<!doctype html>
<html>

<head>
  <?php require_once('../includes/head.php'); ?>


</head>

<body class="bg-empresa">



  <!-- ======================================================================= -->
  <!-- topo  -->
  <!-- ======================================================================= -->
  <?php require_once('../includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo  -->
  <!-- ======================================================================= -->





  <!-- ======================================================================= -->
  <!-- legenda  -->
  <!-- ======================================================================= -->
  <div class="container">
    <div class="row legenda-interna">
      <div class="col-xs-12 text-center">
            <h1>NOSSA EMPRESA</h1>
            <?php $dados= $obj_site->select_unico("tb_empresa", "idempresa", 3) ?>
            <p><?php Util::imprime($dados[descricao], 1000) ?></p>
      </div>
    </div>
  </div>
  <!-- ======================================================================= -->
  <!-- legenda  -->
  <!-- ======================================================================= -->




  <!-- ======================================================================= -->
  <!-- desc empresa  -->
  <!-- ======================================================================= -->
  <div class="container">
    <div class="row bottom40">
      <div class="col-xs-12">
        
        <div class="media top35">
            <div class="media-left">
              <img class="media-object" src="<?php echo Util::caminho_projeto() ?>/imgs/icon-empresa1.png" alt="" height="25" >
            </div>
            <div class="media-body">
              <h3 class="media-heading">SOBRE</h3>
            </div>
          </div>

          <?php $dados= $obj_site->select_unico("tb_empresa", "idempresa", 4) ?>
          <p class="top15"><?php Util::imprime($dados[descricao]) ?></p>

        </div>

      

        <div class="col-xs-12 top45">
          <div class="media">
            <div class="media-left">
              <img class="media-object" src="<?php echo Util::caminho_projeto() ?>/imgs/icon-empresa-qualidade.png" height="25" alt="">
            </div>
            <div class="media-body">
              <h3 class="media-heading">QUALIDADE DOS SERVIÇOS</h3>
            </div>
          </div>

          <?php $dados= $obj_site->select_unico("tb_empresa", "idempresa", 5) ?>
          <p class="top15"><?php Util::imprime($dados[descricao]) ?></p>

        </div>
    




    </div>
  </div>
  <!-- ======================================================================= -->
  <!-- desc empresa  -->
  <!-- ======================================================================= -->






  <!-- ======================================================================= -->
  <!-- rodape  -->
  <!-- ======================================================================= -->
  <?php require_once('../includes/rodape.php') ?>
  <!-- ======================================================================= -->
  <!-- rodape  -->
  <!-- ======================================================================= -->

</body>

</html>




<?php require_once("../includes/js_css.php"); ?>