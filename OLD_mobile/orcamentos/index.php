<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();


//  EXCLUI UM ITEM
if(isset($_GET[action]))
{
    //  SELECIONO O TIPO
    switch($_GET[tipo])
    {
        case "produto":
            $id = $_GET[id];
            unset($_SESSION[solicitacoes_produtos][$id]);
            sort($_SESSION[solicitacoes_produtos]);
        break;
        case "servico":
            $id = $_GET[id];
            unset($_SESSION[solicitacoes_servicos][$id]);
            sort($_SESSION[solicitacoes_servicos]);
        break;
        case "piscina_vinil":
            $id = $_GET[id];
            unset($_SESSION[piscina_vinil][$id]);
            sort($_SESSION[piscina_vinil]);
        break;
    }

}


?>



<!doctype html>
<html>

<head>
  <?php require_once('../includes/head.php'); ?>


</head>

<body class="bg-orcamentos">


  <?php require_once('../includes/topo.php'); ?>

 

   
  <!-- ======================================================================= -->
  <!-- legenda  -->
  <!-- ======================================================================= -->
  <div class="container">
    <div class="row legenda-interna">
      <div class="col-xs-12 text-center">
            <h1>FAÇA UM ORÇAMENTO</h1>
          <?php $dados= $obj_site->select_unico("tb_empresa", "idempresa", 9) ?>
          <p><?php Util::imprime($dados[descricao], 1000) ?></p>
      </div>
    </div>
  </div>
  <!-- ======================================================================= -->
  <!-- legenda  -->
  <!-- ======================================================================= -->







<form class="form-inline FormOrcamento" role="form" method="post">


<?php
    //  VERIFICO SE E PARA CADASTRAR A SOLICITACAO
    if(isset($_POST[nome]))
    {

        //  CADASTRO OS PRODUTOS SOLICITADOS
        for($i=0; $i < count($_POST[qtd]); $i++)
        {
            $dados = $obj_site->select_unico("tb_produtos", "idproduto", $_POST[idproduto][$i]);

            $mensagem .= "
                        <tr>
                            <td><p>". $_POST[qtd][$i] ."</p></td>
                            <td><p>". utf8_encode($dados[titulo]) ."</p></td>
                         </tr>
                        ";
        }


        //  CADASTRO AS PISCINAS DE VINIL
            for($i=0; $i < count($_POST[piscinavinil]); $i++)
            {
                $dados = $obj_site->select_unico("tb_piscinas_vinil", "idpiscinavinil", $_POST[piscinavinil][$i]);

                $mensagem_piscina_vinil .= "
                                            <tr>
                                                <td><p>". $_POST[qtd_piscina][$i] ."</p></td>
                                                <td><p>". utf8_encode($dados[titulo]) ."</p></td>
                                             </tr>
                                            ";
            }



        //  CADASTRO OS PRODUTOS SOLICITADOS
        for($i=0; $i < count($_POST[qtd_servico]); $i++)
        {
            $dados = $obj_site->select_unico("tb_servicos", "idservico", $_POST[idservico][$i]);
            $mensagem_servico .= "
                                    <tr>
                                        <td><p>". $_POST[qtd_servico][$i] ."</p></td>
                                        <td><p>". utf8_encode($dados[titulo]) ."</p></td>
                                     </tr>
                                    ";
        }

        if (count($_POST[categorias]) > 0) {
            foreach($_POST[categorias] as $cat){
                $desc_cat .= $cat . ' , ';
            }
        }




        //  ENVIANDO A MENSAGEM PARA O CLIENTE
        $texto_mensagem = "
                            O seguinte cliente fez uma solicitação pelo site. <br />


                            Nome: $_POST[nome] <br />
                            Email: $_POST[email] <br />
                            Telefone: $_POST[telefone] <br />
                            Celular: $_POST[celular] <br />
                            Bairro: $_POST[bairro] <br />
                            Cidade: $_POST[cidade] <br />
                            Estado: $_POST[estado] <br />
                            Receber orçamento: $_POST[receber_orcamento] <br />
                            Categorias desejadas: $desc_cat <br />
                            Como conheceu nosso site: $_POST[como_conheceu] <br />

                            Mensagem: <br />
                            ". nl2br($_POST[mensagem])." <br />


                            <br />
                            <h2> Produtos selecionados:</h2> <br />

                            <table width='100%' border='0' cellpadding='5' cellspacing='5'>

                                <tr>
                                      <td><h4>QTD</h4></td>
                                      <td><h4>PRODUTO</h4></td>
                                </tr>

                                $mensagem

                            </table>


                            <br />
                            <h2> Piscinas de vinil selecionada(s):</h2> <br />

                            <table width='100%' border='0' cellpadding='5' cellspacing='5'>

                                <tr>
                                      <td><h4>QTD</h4></td>
                                      <td><h4>PRODUTO</h4></td>
                                </tr>

                                $mensagem_piscina_vinil

                            </table>



                            <br />
                            <h2> Serviços selecionados:</h2> <br />

                            <table width='100%' border='0' cellpadding='5' cellspacing='5'>

                                <tr>
                                      <td><h4>QTD</h4></td>
                                      <td><h4>SERVIÇO</h4></td>
                                </tr>

                                $mensagem_servico

                            </table>
                            ";

                    
    
        Util::envia_email($config[email], utf8_decode($_POST[nome] . " solicitou um orçamento"), $texto_mensagem, utf8_decode($_POST[nome]), $_POST[email]);
        Util::envia_email($config[email_copia], utf8_decode($_POST[nome] . " solicitou um orçamento"), $texto_mensagem, utf8_decode($_POST[nome]), $_POST[email]);
            
        unset($_SESSION[solicitacoes_produtos]);
        unset($_SESSION[solicitacoes_servicos]);
        unset($_SESSION[piscinas_vinil]);
        Util::alert_bootstrap("Orçamento enviado com sucesso. Em breve entraremos em contato.");

    }
    ?>






<?php
if(count($_SESSION[solicitacoes_produtos]) > 0)
{
?>

  <!-- orcamentos gerais  -->
  <div class="container top25">
    <div class="row orcamentos">
      <div class="col-xs-12">
        <h2>PRODUTOS SELECIONADOS (<?php echo count($_SESSION[solicitacoes_produtos]) ?>)</h2>
      </div>


       
        <!-- itens e descricao carrinho -->
        <div class="col-xs-12">
          <table class="table top10 tb-lista-itens">
            <thead>
              <tr>
                <th class="text-center">ITEM</th>
                <th>DESCRIÇÃO</th>
                <th class="text-center">QDT.</th>
                <th class="text-center">EXCLUIR</th>
              </tr>
            </thead>


            <tbody>
              <?php
              for($i=0; $i < count($_SESSION[solicitacoes_produtos]); $i++)
              {
                $row = $obj_site->select_unico("tb_produtos", "idproduto", $_SESSION[solicitacoes_produtos][$i]);
                ?>
                  <tr>
                    <td>
                      <img src="<?php echo Util::caminho_projeto() ?>/uploads/tumb_<?php Util::imprime($row[imagem]) ?>" alt="" height="70" width="100" >
                    </td>
                    <td><?php Util::imprime($row[titulo]) ?></td>
                    <td class="text-center">
                      <input type="text" class="input-lista-prod-orcamentos" name="qtd[]" value="1" data-toggle="tooltip" data-placement="top" title="Digite a quantidade desejada">
                      <input name="idproduto[]" type="hidden" value="<?php echo $row[0]; ?>"/>
                    </td>
                    <td class="text-center">
                      <a href="?action=del&id=<?php echo $i; ?>&tipo=produto" data-toggle="tooltip" data-placement="top" title="Excluir">
                        <i class="fa fa-trash  top10"></i>
                      </a>
                    </td>
                  </tr>
                <?php 
                }
                ?>
            </tbody>


          </table>

          <div class="row top15">
            <div class="col-xs-12">
              <a href="<?php echo Util::caminho_projeto() ?>/mobile/produtos" title="Continuar orçando" class="btn btn-primary">
                Continuar orçando
              </a>
            </div>
          </div>

        </div>
        <!-- itens e descricao carrinho -->
      

    </div>
  </div>
  <!-- orcamentos gerais  -->
<?php 
}
?>





<!-- piscina vinil -->

<?php
if(count($_SESSION[piscina_vinil]) > 0)
{
?>

  <!-- orcamentos gerais  -->
  <div class="container top25">
    <div class="row orcamentos">
      <div class="col-xs-12">
        <h2>PISCINAS DE VINIL SELECIONADA(S) (<?php echo count($_SESSION[piscina_vinil]) ?>)</h2>
      </div>


       
        <!-- itens e descricao carrinho -->
        <div class="col-xs-12">
          <table class="table top10 tb-lista-itens">
            <thead>
              <tr>
                <th class="text-center">ITEM</th>
                <th>DESCRIÇÃO</th>
                <th class="text-center">QDT.</th>
                <th class="text-center">EXCLUIR</th>
              </tr>
            </thead>


            <tbody>
              <?php
              for($i=0; $i < count($_SESSION[piscina_vinil]); $i++)
              {
                $row = $obj_site->select_unico("tb_piscinas_vinil", "idpiscinavinil", $_SESSION[piscina_vinil][$i]);
                ?>
                  <tr>
                    <td>
                      <img src="<?php echo Util::caminho_projeto() ?>/uploads/tumb_<?php Util::imprime($row[imagem]) ?>" alt="" height="70" width="100" >
                    </td>
                    <td><?php Util::imprime($row[titulo]) ?></td>
                    <td class="text-center">
                      <p>QDT.<input type="text" class="input-lista-prod-orcamentos" name="qtd_piscina[]" value="1" data-toggle="tooltip" data-placement="top" title="Digite a quantidade desejada">
                      <input name="piscinavinil[]" type="hidden" value="<?php echo $row[0]; ?>"/>
                    </td>
                    <td class="text-center">
                      <a href="?action=del&id=<?php echo $i; ?>&tipo=piscina_vinil" data-toggle="tooltip" data-placement="top" title="Excluir">
                        <i class="fa fa-trash  top10"></i>
                      </a>
                    </td>
                  </tr>
                <?php 
                }
                ?>
            </tbody>


          </table>

          <div class="row top15">
            <div class="col-xs-12">
              <a href="<?php echo Util::caminho_projeto() ?>/mobile/servicos" title="Continuar orçando" class="btn btn-primary">
                Continuar orçando
              </a>
            </div>
          </div>

        </div>
        <!-- itens e descricao carrinho -->
      

    </div>
  </div>
  <!-- orcamentos gerais  -->
<?php 
}
?>



<!-- piscina vinil -->







<?php
if(count($_SESSION[solicitacoes_servicos]) > 0)
{
?>

  <!-- orcamentos gerais  -->
  <div class="container top25">
    <div class="row orcamentos">
      <div class="col-xs-12">
        <h2>SERVIÇOS SELECIONADOS (<?php echo count($_SESSION[solicitacoes_servicos]) ?>)</h2>
      </div>


       
        <!-- itens e descricao carrinho -->
        <div class="col-xs-12">
          <table class="table top10 tb-lista-itens">
            <thead>
              <tr>
                <th class="text-center">ITEM</th>
                <th>DESCRIÇÃO</th>
                <th class="text-center">QDT.</th>
                <th class="text-center">EXCLUIR</th>
              </tr>
            </thead>


            <tbody>
              <?php
              for($i=0; $i < count($_SESSION[solicitacoes_servicos]); $i++)
              {
                $row = $obj_site->select_unico("tb_servicos", "idservico", $_SESSION[solicitacoes_servicos][$i]);
                ?>
                  <tr>
                    <td>
                      <img src="<?php echo Util::caminho_projeto() ?>/uploads/tumb_<?php Util::imprime($row[imagem]) ?>" alt="" height="70" width="100" >
                    </td>
                    <td><?php Util::imprime($row[titulo]) ?></td>
                    <td class="text-center">
                      <input type="text" class="input-lista-prod-orcamentos" name="qtd_servico[]" value="1" data-toggle="tooltip" data-placement="top" title="Digite a quantidade desejada">
                      <input name="idservico[]" type="hidden" value="<?php echo $row[0]; ?>"/>
                    </td>
                    <td class="text-center">
                      <a href="?action=del&id=<?php echo $i; ?>&tipo=servico" data-toggle="tooltip" data-placement="top" title="Excluir">
                        <i class="fa fa-trash  top10"></i>
                      </a>
                    </td>
                  </tr>
                <?php 
                }
                ?>
            </tbody>


          </table>

          <div class="row top15">
            <div class="col-xs-12">
              <a href="<?php echo Util::caminho_projeto() ?>/mobile/servicos" title="Continuar orçando" class="btn btn-primary">
                Continuar orçando
              </a>
            </div>
          </div>

        </div>
        <!-- itens e descricao carrinho -->
      

    </div>
  </div>
  <!-- orcamentos gerais  -->
<?php 
}
?>














  <!-- FORMULARIO -->
  <div class="container top40">
    <div class="row orcamentos">
      <div class="col-xs-12">
        <h2>CONFIRME SEUS DADOS</h2>  
      </div>


      <!-- formulario orcamento -->
      <div class="FormContato col-xs-12 top20 bottom25">
        <div class="row">
          <div class="col-xs-6 form-group ">
            <label class="glyphicon glyphicon-user"> <span>Nome</span></label>
            <input type="text" name="nome" class="form-control fundo-form1 input100" placeholder="">
          </div>

          <div class="col-xs-6 form-group ">
            <label class="glyphicon glyphicon-user"> <span>E-mail</span></label>
            <input type="text" name="email" class="form-control fundo-form1 input100" placeholder="">
          </div>
        </div>

        <div class="row">
          <div class="col-xs-6 form-group">
            <label class="glyphicon glyphicon-earphone"> <span>Telefone</span></label>
            <input type="text" name="telefone" class="form-control fundo-form1 input100" placeholder="">
          </div>


          <div class="col-xs-6 form-group">
            <label> <i class="fa fa-mobile"></i> <span>Celular</span></label>
            <input type="text" name="celular" class="form-control fundo-form1 input100" placeholder="">
          </div>


        </div>

        <div class="row">
         <div class="col-xs-6 form-group">
           <label> <i class="fa fa-home"></i> <span>Bairro</span></label>
           <input type="text" name="bairro" class="form-control fundo-form1 input100" placeholder="">
         </div>


         <div class="col-xs-6 form-group">
          <label> <i class="fa fa-globe"></i> <span>Cidade</span></label>
          <input type="text" name="cidade" class="form-control fundo-form1 input100" placeholder="">
        </div>
      </div>

      <div class="row">
       <div class="col-xs-6 form-group">
        <label> <i class="fa fa-globe"></i><span>Estado</span></label>
        <input type="text" name="estado" class="form-control fundo-form1 input100" placeholder="">
      </div>

      <div class="col-xs-6 form-group">
        <label class="glyphicon glyphicon-star"> <span>Tipo de pessoa</span></label>
        <input type="text" name="tipo_pessoa" class="form-control fundo-form1 input100" placeholder="">
      </div>

    </div>

    <div class="row">
      <div class="col-xs-12 form-group">
        <label class="glyphicon glyphicon-pencil"> <span>Sua Mensagem</span></label>
        <textarea name="mensagem" id="" cols="30" rows="8" class="form-control  fundo-form1 input100" placeholder=""></textarea>
      </div>
    </div>


    <!-- pesquisas orcamentos -->
            <div class="col-xs-12 pesquisa-orcamento">
              <p>Tem interesse em receber orçamento de outros produtos:</p>

              <label class="radio-inline">
                <input type="radio" name="receber_orcamento" value="SIM"> SIM
              </label>
              <label class="radio-inline">
                <input type="radio" name="receber_orcamento"  value="NÃO"> NÃO
              </label>
              <p>Se sim, assinale abaixo</p>

              <?php
              $result = $obj_site->select("tb_categorias_produtos", "LIMIT 4");
              if(mysql_num_rows($result) > 0)
              {
                while($row = mysql_fetch_array($result))
                {
                ?>
                <div class="checkbox col-xs-12">
                  <label>
                    <input type="checkbox" name="categorias[]" value="<?php Util::imprime($row[titulo]) ?>">
                    <?php Util::imprime($row[titulo]) ?>
                  </label>
                </div>
                <?php
                }
            }
            ?>

                <div class="top10 row"></div>

                  <p>Como conheceu nosso site?</p>

                  <select class="form-control input-lg" name="como_conheceu">
                    <option value="">Selecione</option>
                    <option>Jornais</option>
                    <option>Revistas</option>
                    <option>Sites</option>
                    <option>Fórum</option>
                    <option>Notícias</option>
                  </select>
            </div>
            <!-- pesquisas orcamentos -->

    
  </div>
  <!-- formulario orcamento -->


    
 
    <div class="text-right bottom30">
      <button type="submit" class="btn btn-azul" name="btn_contato">
        ENVIAR
      </button>
    </div>


</div>
</div>



</form>


<?php require_once('../includes/rodape.php'); ?>


</body>

</html>


<?php require_once("../includes/js_css.php"); ?>



<script>
  $(document).ready(function() {
    $('.FormOrcamento').bootstrapValidator({
      message: 'This value is not valid',
      feedbackIcons: {
        valid: 'glyphicon glyphicon-ok',
        invalid: 'glyphicon glyphicon-remove',
        validating: 'glyphicon glyphicon-refresh'
      },
      fields: {
       nome: {
        validators: {
          notEmpty: {

          }
        }
      },
      email: {
        validators: {
          notEmpty: {

          },
          emailAddress: {
            message: 'Esse endereço de email não é válido'
          }
        }
      },
      telefone: {
        validators: {
          notEmpty: {

          }
        }
      },
      tipo_pessoa: {
        validators: {
          notEmpty: {

          }
        }
      },
      cidade: {
        validators: {
          notEmpty: {

          }
        }
      },
      estado: {
        validators: {
          notEmpty: {

          }
        }
      },
      bairro: {
        validators: {
          notEmpty: {

          }
        }
      },
      mensagem: {
        validators: {
          notEmpty: {

          }
        }
      }
    }
  });
  });
</script>
