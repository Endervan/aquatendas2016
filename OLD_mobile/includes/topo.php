<div class="container topo">
  <div class="row  top5">

    <!--  ==============================================================  -->
    <!-- logo -->
    <!--  ==============================================================  -->
    <div class="col-xs-4 top10">
      <a href="<?php echo Util::caminho_projeto() ?>/mobile" title="Home">
        <img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/logo.png" alt="Home">
      </a>
    </div> 

    <!--  ==============================================================  -->
    <!-- logo -->
    <!--  ==============================================================  -->



    <div class=" col-xs-8 menu-topo">

      <!-- contatos topo  -->   
      <div class="media-left media-middle">
        <img class="media-object" src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/icon-phone.png" alt="">
      </div>
      <div class="media-body">
        <h4 class="media-heading sub-menu-telefone">
          
          
          <div class="dropdown">
            <a href="javascript:void(0);" class="btn btn-azul" data-toggle="dropdown">CLIQUE E LIGUE AGORA</a>

            <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel1">
                
                <?php if (!empty($config[telefone1])): ?>
                  <li>
                    <a href="tel:+55<?php Util::imprime($config[telefone1]); ?>">
                      <i class="fa fa-phone"></i> 
                      <?php Util::imprime($config[telefone1]); ?>
                    </a>
                  </li>   
                <?php endif ?>

                <?php if (!empty($config[telefone2])): ?>
                  <li>
                    <a href="tel:+55<?php Util::imprime($config[telefone2]); ?>">
                      <i class="fa fa-whatsapp"></i> 
                      <?php Util::imprime($config[telefone2]); ?>
                    </a>
                  </li>   
                <?php endif ?>

                <?php if (!empty($config[telefone3])): ?>
                  <li>
                    <a href="tel:+55<?php Util::imprime($config[telefone3]); ?>">
                      <i class="fa fa-whatsapp"></i> 
                      <?php Util::imprime($config[telefone3]); ?>
                    </a>
                  </li>   
                <?php endif ?>
                
            </ul>
          </div>

        </h4>
      </div>
      <!-- contatos topo  -->



      <!-- menu  -->
      <div class=" dropdown top5 pull-left right5">
        <a id="dLabel"  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/menu-topo.png" alt="">
        </a>
        <ul class="dropdown-menu sub-menu" aria-labelledby="dLabel">
          <li><a href="<?php echo Util::caminho_projeto() ?>/mobile/">HOME</a></li>
          <li><a href="<?php echo Util::caminho_projeto() ?>/mobile/empresa">EMPRESA</a></li>
          <li><a href="<?php echo Util::caminho_projeto() ?>/mobile/produtos">PRODUTOS</a></li>
          <li><a href="<?php echo Util::caminho_projeto() ?>/mobile/servicos">SERVIÇOS</a></li>
          <li><a href="<?php echo Util::caminho_projeto() ?>/mobile/dicas">DICAS</a></li>
          <li><a href="<?php echo Util::caminho_projeto() ?>/mobile/fale-conosco">FALE CONOSCO</a></li>
          <li><a href="<?php echo Util::caminho_projeto() ?>/mobile/trabalhe-conosco">TRABALHE CONOSCO</a></li>
        </ul>
      </div>
      <!-- menu  -->

      <!-- botao pesquisa -->
      <div class=" dropdown top5 pull-left right5">
        <a href="#" class="dropdown-toggle btn btn-azul-claro" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
          <i class="fa fa-search right10"></i>
          <span class="caret"></span></a>

          <ul class="dropdown-menu form-busca-topo pull-right">
            <form class="navbar-form navbar-left" action="<?php echo Util::caminho_projeto() ?>/mobile/produtos/" method="post">
              <div class="form-group col-xs-8 pull-left">
                <input type="text" class="form-control" placeholder="O que está procurando?" name="busca_topo">
              </div>
              <button type="submit" class="btn btn-azul-claro2">Buscar</button>
            </form>
          </ul>
        </div>

        <!-- botao pesquisas -->

        <!-- botao carrinho de compra -->
        <div class=" dropdown top5 pull-right right5">
          <a href="#" class="dropdown-toggle btn btn-azul-claro" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
            <i class="fa fa-shopping-cart right10"></i>
            <span class="caret"></span></a>

            <div class="dropdown-menu topo-meu-orcamento pull-right" aria-labelledby="dLabel">

                              

                               <?php
                                if(count($_SESSION[solicitacoes_produtos]) > 0)
                                {
                                    echo '<h6 class="bottom20">MEUS PRODUTOS('. count($_SESSION[solicitacoes_produtos]) .')</h6>';

                                    for($i=0; $i < count($_SESSION[solicitacoes_produtos]); $i++)
                                    {
                                        $row = $obj_site->select_unico("tb_produtos", "idproduto", $_SESSION[solicitacoes_produtos][$i]);
                                        ?>
                                        <div class="lista-itens-carrinho col-xs-12">
                                            <div class="col-xs-2">
                                                <img src="<?php echo Util::caminho_projeto() ?>/uploads/tumb_<?php Util::imprime($row[imagem]) ?>" height="46" width="29" alt="">    
                                            </div>
                                            <div class="col-xs-8">
                                                <h1><?php Util::imprime($row[titulo]) ?></h1>
                                            </div>
                                            <div class="col-xs-1">
                                                <a href="<?php echo Util::caminho_projeto() ?>/mobile/orcamentos/?action=del&id=<?php echo $i; ?>&tipo=produto" data-toggle="tooltip" data-placement="top" title="Excluir"> <i class="glyphicon glyphicon-remove"></i> </a>
                                            </div>
                                        </div>
                                        <?php  
                                    }
                                }
                                ?>



                                <?php
                                if(count($_SESSION[solicitacoes_servicos]) > 0)
                                {

                                  echo '<h6 class="bottom20">MEUS SERVIÇOS('. count($_SESSION[solicitacoes_servicos]) .')</h6>';

                                    for($i=0; $i < count($_SESSION[solicitacoes_servicos]); $i++)
                                    {
                                        $row = $obj_site->select_unico("tb_servicos", "idservico", $_SESSION[solicitacoes_servicos][$i]);
                                        ?>
                                        <div class="lista-itens-carrinho col-xs-12">
                                            <div class="col-xs-2">
                                                <img src="<?php echo Util::caminho_projeto() ?>/uploads/tumb_<?php Util::imprime($row[imagem]) ?>" height="46" width="29" alt="">    
                                            </div>
                                            <div class="col-xs-8">
                                                <h1><?php Util::imprime($row[titulo]) ?></h1>
                                            </div>
                                            <div class="col-xs-1">
                                                <a href="<?php echo Util::caminho_projeto() ?>/mobile/orcamentos/?action=del&id=<?php echo $i; ?>&tipo=produto" data-toggle="tooltip" data-placement="top" title="Excluir"> <i class="glyphicon glyphicon-remove"></i> </a>
                                            </div>
                                        </div>
                                        <?php  
                                    }
                                }
                                ?>






                               

                                <div class="text-right bottom20">
                                    <a href="<?php echo Util::caminho_projeto() ?>/mobile/orcamentos" title="Finalizar" class="btn btn-primary" >
                                        FINALIZAR
                                    </a>
                                </div>


                            </div>



          </div>

          <!-- botao carrinho de compra -->


        </div>

      </div>
    </div>






