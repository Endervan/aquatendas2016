<?php
require_once("../class/Include.class.php");
$obj_site = new Site();
?>
<!doctype html>
<html>

<head>
  <?php require_once('./includes/head.php'); ?>


 
  
</head>

<body>
  <?php require_once('./includes/topo.php'); ?>
  
	

	<!-- ======================================================================= -->
	<!-- slider	-->
	<!-- ======================================================================= -->	
	<div class="container">
	    <div class="row slider-index">
	      <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
	        <!-- Indicators -->
	        <ol class="carousel-indicators">
	          <?php
                  $result = $obj_site->select("tb_banners", "and tipo_banner = 2 ");
                   if(mysql_num_rows($result) > 0){
                    $i = 0;
                     while ($row = mysql_fetch_array($result)) {
                      $imagens[] = $row;
                     ?> 
                        <li data-target="#carousel-example-generic" data-slide-to="<?php echo $i; ?>" class="<?php if($i == 0){ echo "active"; } ?>"></li>
                     <?php 
                      $i++;
                     }
                  }
                  ?>
	        </ol>

	        <!-- Wrapper for slides -->
	        <div class="carousel-inner" role="listbox">
				
				<?php 
                if (count($imagens) > 0) {
                  $i = 0;
                  foreach ($imagens as $key => $imagem) {
                  ?>
                      	<div class="item <?php if($i == 0){ echo "active"; } ?>">
				            <img src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($imagem[imagem]); ?>" alt="First slide">
				            <div class="carousel-caption text-center">
			                   <h1><?php Util::imprime($imagem[titulo]); ?></h1>
                                <p><?php Util::imprime($imagem[legenda]); ?></p>
			                    <?php if (!empty($imagem[url])) { ?>
			                    	<a class="btn btn-transparente right15" href="<?php Util::imprime($imagem[url]); ?>" role="button">SAIBA MAIS</a>
			                    <?php } ?>
			                </div>
				        </div>
					
                   <?php
                   $i++;
                  }
                }
                ?>


	        </div>
			
			
			<?php /* ?>
	     	<a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
              </a>
              <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
              </a>
            <?php */ ?>  
			
			
	      </div>
	    </div>
	  </div>
	<!-- ======================================================================= -->
	<!-- slider	-->
	<!-- ======================================================================= -->



	
	<!-- ======================================================================= -->
	<!-- categorias	-->
	<!-- ======================================================================= -->
	<div class="container margin-cat-index">
		
		<div class="row">
	        <div class="col-xs-12">
	              
	             
	              <div class="text-right pull-left">
	                <h6>CONHEÇA NOSSA</h6>
	                <h6><span>LINHA DE PRODUTOS</span></h6>  
	              </div>

	              <div class="pull-left left5">
	              	<i class="fa fa-angle-double-down seta-indicacao"></i>
	              </div>
	              
	        </div>
	      </div>

		<div class="row sombra-categorias">
			<div class="col-xs-12">
				
				<div id="carousel-gallery" class="touchcarousel  black-and-white">
	                <ul class="touchcarousel-container lista-categorias">
	                    <li class="destaque touchcarousel-item">
			                  <a href="<?php echo Util::caminho_projeto() ?>/mobile/produtos">
			                      <img src="<?php echo Util::caminho_projeto() ?>/imgs/icon-estrela.png" alt="">
			                      <br>
			                      VER TODAS
			                  </a>
			              </li>
			              <li class="touchcarousel-item">
	                        <a href="<?php echo Util::caminho_projeto() ?>/mobile/piscinas-vinil">
	                            <img src="<?php echo Util::caminho_projeto() ?>/imgs/piscina-vinil.png" alt="PISCINAS DE VINIL">
	                            <br>
	                            PISCINAS DE VINIL
	                        </a>
	                      </li>

	                    <?php 
	                    $result = $obj_site->select("tb_categorias_produtos", "ORDER BY rand() LIMIT 12");
	                    if(mysql_num_rows($result) > 0)
	                    {
	                      while($row = mysql_fetch_array($result))
	                      {
	                      ?>  
	                        <li class="touchcarousel-item">
			                      <a href="<?php echo Util::caminho_projeto() ?>/mobile/produtos/?cat=<?php Util::imprime($row[url_amigavel]) ?>">
			                          <img src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($row[imagem]); ?>" alt="">
			                          <br>
			                          <?php Util::imprime($row[titulo]) ?>
			                      </a>
			                  </li>
	                            
	                      <?php 
	                      }
	                    }
	                    ?>
	                </ul>
	            </div>



			</div>
		</div>
	</div>
	<!-- ======================================================================= -->
	<!-- categorias	-->
	<!-- ======================================================================= -->




	<!-- ======================================================================= -->
	<!-- produtos	-->
	<!-- ======================================================================= -->
	<div class="container">
		<div class="row top50">
			 <?php 
             $result = $obj_site->select("tb_produtos", "ORDER BY rand() LIMIT 6");
             if(mysql_num_rows($result) > 0){
             	$i = 0;
              while($row = mysql_fetch_array($result)){
              ?> 
				<div class="col-xs-6">
					<div class="lista-produto">
						<a href="<?php echo Util::caminho_projeto() ?>/mobile/produtos/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
							<img src="<?php echo Util::caminho_projeto() ?>/uploads/tumb_<?php Util::imprime($row[imagem]); ?>" alt="" class="input100" >
						</a>
						<h1><?php Util::imprime($row[titulo]); ?></h1>
						<p><?php Util::imprime($row[descricao], 110); ?></p>
						<a href="<?php echo Util::caminho_projeto() ?>/mobile/produtos/<?php Util::imprime($row[url_amigavel]); ?>" class="btn btn-azul-pequeno top5 right5">SAIBA MAIS</a>
						<a class="btn btn-azul-pequeno top5" href="javascript:void(0);" title="Solicite um orçamento" onclick="add_solicitacao(<?php Util::imprime($row[0]) ?>, 'produto')" id="btn_add_solicitacao_<?php Util::imprime($row[0]) ?>, 'produto'">SOLICITE UM ORÇAMENTO</a>
					</div>
				</div>
              <?php 

              	if ($i == 1) {
              		$i = 0;
              		echo '<div class="clearfix"></div>';
              	}else{
              		$i++;
              	}


              }
            }
            ?>
		</div>
	</div>
	<!-- ======================================================================= -->
	<!-- produtos	-->
	<!-- ======================================================================= -->





	<!-- ======================================================================= -->
	<!-- conheca mais	-->
	<!-- ======================================================================= -->
	<div class="container bg-conheca">
		
		<div class="row">
	        <div class="col-xs-12">
	             
	              <div class="pull-right left5">
	              	<i class="fa fa-angle-double-down seta-azul"></i>
	              </div>

	               <div class="text-right pull-right">
	                <h5>CONHEÇA MAIS</h5>
	                <h5><span>A AQUATENDAS</span></h5>  
	              </div>
	              
	        </div>
	      </div>


		<div class="row top50">
			<div class="col-xs-7 col-xs-offset-5">
				<?php $dados= $obj_site->select_unico("tb_empresa", "idempresa", 8) ?>
          		<p><?php Util::imprime($dados[descricao]) ?></p>

				<a href="<?php echo Util::caminho_projeto() ?>/mobile/empresa" class="btn btn-transparente top5">VER MAIS</a>

			</div>
		</div>


	</div>
	<!-- ======================================================================= -->
	<!-- conheca mais	-->
	<!-- ======================================================================= -->



	
<!-- ======================================================================= -->
<!-- dicas  -->
<!-- ======================================================================= -->
<div class="container">
  <div class="row top10">
    
    <div class="col-xs-12 linha-dicas bottom25">
      <div class="pull-left padding0">
        <h3>CONFIRA</h3>      
        <h3><span>NOSSAS DICAS</span></h3>  
      </div>
      <div class="pull-right top25">
          <a href="<?php echo Util::caminho_projeto() ?>/mobile/dicas" class="btn btn-azul-pequeno">VER TODAS</a>
      </div>
    </div>


    <!-- lista dicas -->
    <?php
    $result = $obj_site->select("tb_dicas", "ORDER BY RAND() LIMIT 2");
    if(mysql_num_rows($result) > 0)
    {
      while ($row = mysql_fetch_array($result)) { 
      ?>
        <div class="col-xs-6">
          <a href="<?php echo Util::caminho_projeto() ?>/mobile/dicas/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
            <img src="<?php echo Util::caminho_projeto() ?>/uploads/tumb_<?php Util::imprime($row[imagem]); ?>" alt="" class="input100">
          	<p><?php Util::imprime($row[titulo]); ?></p>
         </a>
        </div>
      <?php
      }
    }
    ?>
    <!-- lista dicas -->


  </div>
</div>
<!-- ======================================================================= -->
<!-- dicas  -->
<!-- ======================================================================= -->








  <?php require_once('./includes/rodape.php'); ?>






</body>

</html>




<?php require_once("./includes/js_css.php"); ?>


 <!-- ---- LAYER SLIDER ---- -->
        <link rel="stylesheet" href="<?php echo Util::caminho_projeto() ?>/jquery/touchcarousel/touchcarousel.css"/>
        <link rel="stylesheet" href="?php echo Util::caminho_projeto() ?>/jquery/touchcarousel/black-and-white-skin/black-and-white-skin.css" />
        <script src="<?php echo Util::caminho_projeto() ?>/jquery/touchcarousel/jquery.touchcarousel-1.2.min.js"></script>

        <script type="text/javascript">
        $(document).ready(function() {
            $("#carousel-gallery").touchCarousel({
                itemsPerPage: 3,
                scrollbar: false,
                scrollbarAutoHide: true,
                scrollbarTheme: "dark",
                pagingNav: true,
                snapToItems: true,
                scrollToLast: false,
                useWebkit3d: true,
                loopItems: true
            });
        });
        </script>
        <!-- XXXX LAYER SLIDER XXXX -->