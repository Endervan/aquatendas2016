<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();
?>
<!doctype html>
<html>
<head>
	<?php require_once('../includes/head.php'); ?>

</head>

<body class="bg-empresa">


	<?php require_once('../includes/topo.php'); ?>



  

  <!-- ======================================================================= -->
  <!-- legenda  -->
  <!-- ======================================================================= -->
  <div class="container">
    <div class="row legenda-interna">
      <div class="col-xs-12 text-center">
            <h1>NOSSOS PRODUTOS</h1>
	        <?php $dados= $obj_site->select_unico("tb_empresa", "idempresa", 2) ?>
	        <p><?php Util::imprime($dados[descricao], 1000) ?></p>
      </div>
    </div>
  </div>
  <!-- ======================================================================= -->
  <!-- legenda  -->
  <!-- ======================================================================= -->





  <!-- ======================================================================= -->
  <!-- categorias -->
  <!-- ======================================================================= -->
  <div class="container margin-cat-index">
    
    

    <div class="row sombra-categorias top30">
      <div class="col-xs-12">
        
        <div id="carousel-gallery" class="touchcarousel  black-and-white">
                  <ul class="touchcarousel-container lista-categorias">
                      <?php 
                      $result = $obj_site->select("tb_piscinas_vinil");
                      if(mysql_num_rows($result) > 0)
                      {
                        while($row = mysql_fetch_array($result))
                        {
                        ?>  
                          <li class="touchcarousel-item">
                            <a href="javascript:void(0);" onclick="mudaFoto(<?php echo ++$b; ?>, <?php echo $row[idpiscinavinil] ?> )">
                                <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem1]", 104, 63) ?>
                                <br>
                                <?php Util::imprime($row[titulo]) ?>
                            </a>
                        </li>
                              
                        <?php 
                        }
                      }
                      ?>
                  </ul>
              </div>

      </div>
    </div>
  </div>
  <!-- ======================================================================= -->
  <!-- categorias -->
  <!-- ======================================================================= -->



  


<!-- ======================================================================= -->
<!-- imagens da picinas  -->
<!-- ======================================================================= -->
<div class="container">
  <div class="row top30">
    <div class="col-xs-12 lista_imagens_servico_piscina">
      <div id="container-infos-galeria-grande">
          <?php
          $result = $obj_site->select("tb_piscinas_vinil");
          if(mysql_num_rows($result) > 0){
              while($row = mysql_fetch_array($result)){
              ?>
                  <div id="container-infos-galeria-grande-<?php echo ++$ii; ?>">   
                      <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 1170, 405, array("class"=>"input100", "style"=>"height: 260px;")) ?>
                  </div>
              <?php
              }
          }
          ?>
        </div>    
    </div>
  </div>
</div>
<!-- ======================================================================= -->
<!-- imagens da picinas  -->
<!-- ======================================================================= -->







<div class="container">
  <div class="row top20">
    <div class="col-xs-12 desc-prod">

      <div class="right20">
        
        <button id="btn_add_solicitacao" value="<?php echo $btn_orcamento; ?>" class="btn btn-azul-claro telefone">
          <i class="fa fa-shopping-cart right10"></i> SOLICITE UM ORÇAMENTO
        </button>
   
      </div>


      <div class="clearfix bottom10 top15">
        <h2 class="pull-left top5 telefone"><i class="fa fa-phone right10"></i> ATENDIMENTO:</h2> 
         <h4 class="top10 pull-left"><?php Util::imprime($config[telefone1]); ?></h4>

         <a class="pull-right top5" href="tel:+55<?php Util::imprime($config[telefone1]); ?>" title="Solicite um orçamento">
          <h2>CHAMAR</h2>
        </a>

      </div>



    </div>
    



  </div>
</div>










<!-- ======================================================================= -->
<!-- descricao  -->
<!-- ======================================================================= -->
<div class="container">
  <div class="row top40">
    <div class="col-xs-12">
     
        <h3><span>DESCRIÇÃO <i class="fa fa-angle-double-down"></i></span></h3>  
        <?php $dados= $obj_site->select_unico("tb_empresa", "idempresa", 12) ?>
        <p class="top10"><?php Util::imprime($dados[descricao]) ?></p>
    </div>
  </div>
</div>
<!-- ======================================================================= -->
<!-- descricao  -->
<!-- ======================================================================= -->











<!-- ======================================================================= -->
<!-- veja tambem  -->
<!-- ======================================================================= -->
<div class="container">
  <div class="row top25">
    <div class="">
      <div class="col-xs-12">
        <h3 class="bottom20"><span>VEJA TAMBÉM <i class="fa fa-angle-double-down"></i></span></h3>    
      </div>
      

      
      <?php 
      $result = $obj_site->select("tb_produtos", "order by rand() limit 2");
      if(mysql_num_rows($result) > 0){
        while($row = mysql_fetch_array($result)){
        ?>
        

        <div class="col-xs-6">
          <div class="lista-produto">
            <a href="<?php echo Util::caminho_projeto() ?>/mobile/produtos/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
              <img src="<?php echo Util::caminho_projeto() ?>/uploads/tumb_<?php Util::imprime($row[imagem]); ?>" alt="" class="input100" >
            </a>
            <h1><?php Util::imprime($row[titulo]); ?></h1>
            <p><?php Util::imprime($row[descricao], 110); ?></p>
            <a href="<?php echo Util::caminho_projeto() ?>/mobile/produtos/<?php Util::imprime($row[url_amigavel]); ?>" class="btn btn-azul-pequeno top5 right5">SAIBA MAIS</a>
            <a class="btn btn-azul-pequeno top5" href="javascript:void(0);" title="Solicite um orçamento" onclick="add_solicitacao(<?php Util::imprime($row[0]) ?>, 'produto')" id="btn_add_solicitacao_<?php Util::imprime($row[0]) ?>, 'produto'">SOLICITE UM ORÇAMENTO</a>
          </div>
        </div>
        <?php 
        }
      }
      ?>

    </div>
  </div>
</div>
<!-- ======================================================================= -->
<!-- veja tambem  -->
<!-- ======================================================================= -->





	






	<!-- rodape -->
	<?php require_once('../includes/rodape.php') ?>
	<!-- rodape -->

</body>
</html>



<?php require_once("../includes/js_css.php"); ?>



<!-- ---- LAYER SLIDER ---- -->
<link rel="stylesheet" href="<?php echo Util::caminho_projeto() ?>/jquery/touchcarousel/touchcarousel.css"/>
<link rel="stylesheet" href="?php echo Util::caminho_projeto() ?>/jquery/touchcarousel/black-and-white-skin/black-and-white-skin.css" />
<script src="<?php echo Util::caminho_projeto() ?>/jquery/touchcarousel/jquery.touchcarousel-1.2.min.js"></script>

<script type="text/javascript">
$(document).ready(function() {
	$("#carousel-gallery").touchCarousel({
		itemsPerPage: 3,
		scrollbar: false,
		scrollbarAutoHide: true,
		scrollbarTheme: "dark",
		pagingNav: true,
		snapToItems: true,
		scrollToLast: false,
		useWebkit3d: true,
		loopItems: true
	});
});
</script>
<!-- XXXX LAYER SLIDER XXXX -->


<!--  ====================================================================================================   -->  
<!--  TROCA AS IMAGENS   -->
<!--  ====================================================================================================   -->
<script type="text/javascript"> 
function mudaFoto(id, idpiscinavinil)
{
  var elemento = "#container-infos-galeria-grande-"+id;
	$("#container-infos-galeria-grande div").stop(true, true).fadeOut(500).hide(500);
	$(elemento).stop(true, true).fadeIn(1000).show(500);
	$('#btn_add_solicitacao').val(idpiscinavinil);
}
</script>


<script type="text/javascript">
  $(document).ready(function() {
	  $('#btn_add_solicitacao').click(function(event) {
		var id = $('#btn_add_solicitacao').val();
		window.location = '<?php echo Util::caminho_projeto() ?>/mobile/add_prod_solicitacao.php?id='+id+'&tipo_orcamento=piscina_vinil';
	  });
  });
  </script>





<script>
    $(document).ready(function() {
        $('.FormContato').bootstrapValidator({
            message: 'This value is not valid',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                 nome: {
                    validators: {
                        notEmpty: {
                            
                        }
                    }
                },
                email: {
                    validators: {
                        notEmpty: {
                        
                        },
                        emailAddress: {
                            message: 'Esse endereço de email não é válido'
                        }
                    }
                },
                nota: {
                    validators: {
                        notEmpty: {
                        
                        }
                    }
                },
                comentario: {
                    validators: {
                        notEmpty: {
                            
                        }
                    }
                },
                mensagem: {
                    validators: {
                        notEmpty: {
                            
                        }
                    }
                }
            }
        });
    });
</script>

