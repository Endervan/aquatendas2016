<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();
?>
<!doctype html>
<html>

<head>
  <?php require_once('../includes/head.php'); ?>

</head>

<body class="bg-trabalhe-conosco">


  <?php require_once('../includes/topo.php'); ?>



  <!-- ======================================================================= -->
  <!-- legenda  -->
  <!-- ======================================================================= -->
  <div class="container">
    <div class="row legenda-interna">
      <div class="col-xs-12 text-center">
            <h1>TRABALHE CONOSCO</h1>
          <?php $dados= $obj_site->select_unico("tb_empresa", "idempresa", 11) ?>
          <p><?php Util::imprime($dados[descricao], 1000) ?></p>
      </div>
    </div>
  </div>
  <!-- ======================================================================= -->
  <!-- legenda  -->
  <!-- ======================================================================= -->



  <!-- bg-orcamento -->
  <div class="container">
    <div class="row">
      
      <div class=" col-xs-12 media top30">
          <div class="media-left media-middle">
            <img class="media-object" src="<?php echo Util::caminho_projeto() ?>/imgs/icon-phone.png" alt="">
          </div>
          <div class="media-body">
              
              <div class="col-xs-12">
                <?php Util::imprime($config[telefone1]); ?>
                <a href="tel:+55<?php Util::imprime($config[telefone1]); ?>" title="" class="btn btn-primary pulll-left">
                    LIGAR
                </a>  
              </div>
              
              

              <?php if (!empty($config[telefone2])) { ?>
                <div class="col-xs-12 top5">
                   <?php Util::imprime($config[telefone2]); ?>
                  <a href="tel:+55<?php Util::imprime($config[telefone2]); ?>" title="" class="btn btn-primary pulll-left">
                     LIGAR
                  </a>
                </div>
              <?php } ?>

              <?php if (!empty($config[telefone3])) { ?>
                <div class="col-xs-12 top5">
                   <?php Util::imprime($config[telefone3]); ?>
                  <a href="tel:+55<?php Util::imprime($config[telefone3]); ?>" title="" class="btn btn-primary pulll-left">
                     LIGAR
                  </a>
                </div>
              <?php } ?>


              <?php if (!empty($config[telefone4])) { ?>
                <div class="col-xs-12 top5">
                   <?php Util::imprime($config[telefone4]); ?>
                  <a href="tel:+55<?php Util::imprime($config[telefone4]); ?>" title="" class="btn btn-primary pulll-left">
                     LIGAR
                  </a>
                </div>
              <?php } ?>
            

          </div>
        </div>


        <div class=" col-xs-12 media top30 bottom30">
          <div class="media-left media-middle">
            <img class="media-object" src="<?php echo Util::caminho_projeto() ?>/imgs/icon-localizacao.png" alt="">
          </div>
          <div class="media-body">
            <p class="media-heading"><?php Util::imprime($config[endereco]); ?></p>
          </div>
        </div>


      <!-- trabalhe conosco  -->

      <?php
      //  VERIFICO SE E PARA ENVIAR O EMAIL
      if(isset($_POST[nome]))
      {
        $nome_remetente = Util::trata_dados_formulario($_POST[nome]);
        $assunto = Util::trata_dados_formulario($_POST[assunto]);
        $email = Util::trata_dados_formulario($_POST[email]);
        $telefone = Util::trata_dados_formulario($_POST[telefone]);
        $escolaridade = Util::trata_dados_formulario($_POST[escolaridade]);
        $cargo = Util::trata_dados_formulario($_POST[cargo]);
        $area = Util::trata_dados_formulario($_POST[area]);
        $mensagem = Util::trata_dados_formulario(nl2br($_POST[mensagem]));

        if(!empty($_FILES[curriculo][name])):
          $nome_arquivo = Util::upload_arquivo("../../uploads", $_FILES[curriculo]);
          $texto = "Anexo: ";
          $texto .= "Clique ou copie e cole o link abaixo no seu navegador de internet para visualizar o arquivo.<br>";
          $texto .= "<a href='".Util::caminho_projeto()."/uploads/$nome_arquivo' target='_blank'>".Util::caminho_projeto()."/uploads/$nome_arquivo</a>";
        endif;

              $texto_mensagem = "
                                Nome: $nome_remetente <br />
                                Assunto: $assunto <br />
                                Telefone: $telefone <br />
                                Email: $email <br />
                                Escolaridade: $escolaridade <br />
                                Cargo: $cargo <br />
                                Área: $area <br />
                                Mensagem: <br />
                                $texto    <br><br>
                                $mensagem
                                ";
              Util::envia_email($config[email], "CURRÍCULO PELO SITE ".$_SERVER[SERVER_NAME], $texto_mensagem, $nome_remetente, $email);
              Util::envia_email($config[email_copia], "CURRÍCULO PELO SITE ".$_SERVER[SERVER_NAME], $texto_mensagem, $nome_remetente, $email);
              Util::alert_bootstrap("Obrigado por entrar em contato.");
              unset($_POST);
      }
      ?>

      <form class="form-inline FormCurriculo" role="form" method="post" enctype="multipart/form-data">
        <div class="container">
          <div class="row bottom25">
            <div class="col-xs-12 fale-conosco">
              <h4>TRABALHE CONOSCO</h4>
              <!-- formulario orcamento -->
              <div class="FormContato top20 bottom80">
                <div class="row top10">
                  <div class="col-xs-6 form-group ">
                    <label class="glyphicon glyphicon-user"> <span>Nome</span></label>
                    <input type="text" name="nome" class="form-control fundo-form1 input100" placeholder="">
                  </div>

                  <div class="col-xs-6 form-group ">
                    <label class="glyphicon glyphicon-user"> <span>E-mail</span></label>
                    <input type="text" name="email" class="form-control fundo-form1 input100" placeholder="">
                  </div>
                </div>

                <div class="row top10">
                  <div class="col-xs-6 form-group">
                    <label class="glyphicon glyphicon-earphone"> <span>Telefone</span></label>
                    <input type="text" name="telefone" class="form-control fundo-form1 input100" placeholder="">
                  </div>

                  <div class="col-xs-6 form-group">
                   <label class="glyphicon glyphicon-star"> <span>Assunto</span></label>
                   <input type="text" name="assunto" class="form-control fundo-form1 input100" placeholder="">
                 </div>
               </div>


               <div class="row top10">
                <div class="col-xs-6 form-group">
                  <label class="glyphicon glyphicon-file"> <span>Currículo</span></label>
                  <input type="file" name="curriculo" class="form-control fundo-form1 input100" placeholder="">
                </div>


                <div class="col-xs-6 form-group">
                  <label class="glyphicon glyphicon-book"> <span>Escolaridade</span></label>
                  <input type="text" name="escolaridade" class="form-control fundo-form1 input100" placeholder="">
                </div>
              </div>

              <div class="row top10">
                <div class="col-xs-6 form-group">
                  <label class="glyphicon glyphicon-lock"> <span>Cargo</span></label>
                  <input type="text" name="cargo" class="form-control fundo-form1 input100" placeholder="">
                </div>

                <div class="col-xs-6 form-group">
                  <label class="glyphicon glyphicon-briefcase"> <span>Area</span></label>
                  <input type="text" name="area" class="form-control fundo-form1 input100" placeholder="">
                </div>
              </div>

              <div class="row top10">
                <div class="col-xs-12 form-group">
                  <label class="glyphicon glyphicon-pencil"> <span>Sua Mensagem</span></label>
                  <textarea name="mensagem" id="" cols="30" rows="8" class="form-control  fundo-form1 input100" placeholder=""></textarea>
                </div>
              </div>

              <div class="clearfix"></div>

              <div class="text-right  top30">
                <button type="submit" class="btn btn-azul1" name="btn_contato">
                  ENVIAR
                </button>
              </div>
            </div>
            <!-- formulario orcamento -->

          </div>

        </div>
      </div>
    </form>
    <!-- formulario de contatos -->


    <div class="faleConosco">
        <iframe src="<?php Util::imprime($config[src_place]) ?>" width="480" height="379" frameborder="0" style="border:0" allowfullscreen></iframe>
      </div>


  </div>
</div>




<?php require_once('../includes/rodape.php'); ?>


</body>

</html>

<?php require_once("../includes/js_css.php"); ?>

<script>
  $(document).ready(function() {
    $('.FormCurriculo').bootstrapValidator({
      message: 'This value is not valid',
      feedbackIcons: {
        valid: 'glyphicon glyphicon-ok',
        invalid: 'glyphicon glyphicon-remove',
        validating: 'glyphicon glyphicon-refresh'
      },
      fields: {
       nome: {
        validators: {
          notEmpty: {

          }
        }
      },
      email: {
        validators: {
          notEmpty: {

          },
          emailAddress: {
            message: 'Esse endereço de email não é válido'
          }
        }
      },
      telefone: {
        validators: {
          notEmpty: {

          }
        }
      },
      assunto: {
        validators: {
          notEmpty: {

          }
        }
      },
      escolaridade: {
        validators: {
          notEmpty: {

          }
        }
      },
      cargo: {
        validators: {
          notEmpty: {

          }
        }
      },
      area: {
        validators: {
          notEmpty: {

          }
        }
      },
      curriculo: {
        validators: {
          notEmpty: {
            message: 'Por favor insira seu currículo'
          },
          file: {
            extension: 'doc,docx,pdf,rtf',
            type: 'application/pdf,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/rtf',
                            maxSize: 5*1024*1024,   // 5 MB
                            message: 'O arquivo selecionado não é valido, ele deve ser (doc,docx,pdf,rtf) e 5 MB no máximo.'
                          }
                        }
                      },
                      mensagem: {
                        validators: {
                          notEmpty: {

                          }
                        }
                      }
                    }
                  });
});
</script>
