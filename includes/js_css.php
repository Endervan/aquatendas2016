
<!-- Bootstrap core CSS -->
<link href="<?php echo Util::caminho_projeto(); ?>/dist/css/bootstrap.min.css" rel="stylesheet">


<!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
<!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
<script src="<?php echo Util::caminho_projeto() ?>/dist/js/ie-emulation-modes-warning.js"></script>

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->



<link href="<?php echo Util::caminho_projeto() ?>/css/style.css" rel="stylesheet">




<!-- Custom styles for this template -->
<link href="<?php echo Util::caminho_projeto() ?>/dist/css/non-responsive.css" rel="stylesheet">




<!--    ==============================================================  -->
<!--    ROYAL SLIDER    -->
<!--    ==============================================================  -->

<!-- slider JS files -->
<link href="<?php echo Util::caminho_projeto() ?>/jquery/cc-royalslider-9.2.0/royalslider/royalslider.css" rel="stylesheet">
<script  src="<?php echo Util::caminho_projeto() ?>/jquery/cc-royalslider-9.2.0/royalslider/jquery-1.8.0.min.js"></script>
<script src="<?php echo Util::caminho_projeto() ?>/jquery/cc-royalslider-9.2.0/royalslider/jquery.royalslider.min.js"></script>






<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="<?php echo Util::caminho_projeto() ?>/dist/js/bootstrap.min.js"></script>
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script src="<?php echo Util::caminho_projeto() ?>/dist/js/ie10-viewport-bug-workaround.js"></script>






<!-- Bootstrap Validator
https://github.com/nghuuphuoc/bootstrapvalidator
================================================== -->
<link rel="stylesheet" href="<?php echo Util::caminho_projeto() ?>/dist/css/bootstrapValidator.min.css"/>
 <!-- Include FontAwesome CSS if you want to use feedback icons provided by FontAwesome -->
<link rel="stylesheet" href="<?php echo Util::caminho_projeto() ?>/dist/css/font-awesome.min.css"/>


<!-- Either use the compressed version (recommended in the production site) -->
<script type="text/javascript" src="<?php echo Util::caminho_projeto() ?>/dist/js/bootstrapValidator.min.js"></script>

<!-- Or use the original one with all validators included -->
<script type="text/javascript" src="<?php echo Util::caminho_projeto() ?>/dist/js/bootstrapValidator.js"></script>

<!-- language -->
<script type="text/javascript" src="<?php echo Util::caminho_projeto() ?>/src/js/language/pt_BR.js"></script>


<link rel="stylesheet" href="<?php echo Util::caminho_projeto() ?>/dist/css/bootstrap-lightbox.min.css">
<script src="<?php echo Util::caminho_projeto() ?>/dist/js/bootstrap-lightbox.min.js"></script>



<!-- Rating
http://plugins.krajee.com/star-rating
================================================== -->
<link href="<?php echo Util::caminho_projeto() ?>/dist//css/star-rating.min.css" media="all" rel="stylesheet" type="text/css" />
<script src="<?php echo Util::caminho_projeto() ?>/dist//js/star-rating.min.js" type="text/javascript"></script>

<script>
jQuery(document).ready(function () {

    $('.avaliacao').rating({
          min: 0,
          max: 5,
          step: 1,
          size: 'xs',
          showClear: false,
          disabled: true,
          clearCaption: 'Seja o primeiro a avaliar.',
          starCaptions: {
                            0.5: 'Half Star',
                            1: 'Ruim',
                            1.5: 'One & Half Star',
                            2: 'Regular',
                            2.5: 'Two & Half Stars',
                            3: 'Bom',
                            3.5: 'Three & Half Stars',
                            4: 'Ótimo',
                            4.5: 'Four & Half Stars',
                            5: 'Excelente'
                        }
       });


});
</script>






<script>
    $('#myAffix').affix({
      offset: {
        top: 100,
        bottom: function () {
          return (this.bottom = $('.footer').outerHeight(true))
        }
      }
    })
</script>




<script>

    $(function () {
      $('[data-toggle="tooltip"]').tooltip()
    })
</script>




<script>
$(document).ready(function() {
    $('.FormBusca')
        .bootstrapValidator({
            container: 'tooltip',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                busca_topo: {
                    validators: {
                        notEmpty: {
                            message: 'Por favor, preêncha esse campo.'
                        }
                    }
                }
            }
        })
        .on('error.field.bv', function(e, data) {
            // Get the tooltip
            var $parent = data.element.parents('.form-group'),
                $icon   = $parent.find('.form-control-feedback[data-bv-icon-for="' + data.field + '"]'),
                title   = $icon.data('bs.tooltip').getTitle();

            // Destroy the old tooltip and create a new one positioned to the right
            $icon.tooltip('destroy').tooltip({
                html: true,
                placement: 'right',
                title: title,
                container: 'body'
            });
        });

    // Reset the Tooltip container form
    $('#resetButton').on('click', function(e) {
        var fields = $('#tooltipContainerForm').data('bootstrapValidator').getOptions().fields,
            $parent, $icon;

        for (var field in fields) {
            $parent = $('[name="' + field + '"]').parents('.form-group');
            $icon   = $parent.find('.form-control-feedback[data-bv-icon-for="' + field + '"]');
            $icon.tooltip('destroy');
        }

        // Then reset the form
        $('#tooltipContainerForm').data('bootstrapValidator').resetForm(true);
    });
});
</script>




<script>
$(document).ready(function() {
    $('.FormBuscaRodape')
        .bootstrapValidator({
            container: 'tooltip',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                busca_rodape: {
                    validators: {
                        notEmpty: {
                            message: 'Por favor, preêncha esse campo.'
                        }
                    }
                }
            }
        })
        .on('error.field.bv', function(e, data) {
            // Get the tooltip
            var $parent = data.element.parents('.form-group'),
                $icon   = $parent.find('.form-control-feedback[data-bv-icon-for="' + data.field + '"]'),
                title   = $icon.data('bs.tooltip').getTitle();

            // Destroy the old tooltip and create a new one positioned to the right
            $icon.tooltip('destroy').tooltip({
                html: true,
                placement: 'right',
                title: title,
                container: 'body'
            });
        });

    // Reset the Tooltip container form
    $('#resetButton').on('click', function(e) {
        var fields = $('#tooltipContainerForm').data('bootstrapValidator').getOptions().fields,
            $parent, $icon;

        for (var field in fields) {
            $parent = $('[name="' + field + '"]').parents('.form-group');
            $icon   = $parent.find('.form-control-feedback[data-bv-icon-for="' + field + '"]');
            $icon.tooltip('destroy');
        }

        // Then reset the form
        $('#tooltipContainerForm').data('bootstrapValidator').resetForm(true);
    });
});
</script>




<!--    ====================================================================================================     -->
<!--    ADICIONA PRODUTO AOO CARRINHO    -->
<!--    ====================================================================================================     -->
<script type="text/javascript">
function add_solicitacao(id, tipo_orcamento)
{
    window.location = '<?php echo Util::caminho_projeto() ?>/add_prod_solicitacao.php?id='+id+'&tipo_orcamento='+tipo_orcamento;
}
</script>





<!-- FlexSlider -->
<script defer src="<?php echo Util::caminho_projeto() ?>/jquery/woothemes-FlexSlider-83b3cae/jquery.flexslider.js"></script>
<!-- Syntax Highlighter -->
<script type="text/javascript" src="<?php echo Util::caminho_projeto() ?>/jquery/woothemes-FlexSlider-83b3cae/demo/js/shCore.js"></script>
<script type="text/javascript" src="<?php echo Util::caminho_projeto() ?>/jquery/woothemes-FlexSlider-83b3cae/demo/js/shBrushXml.js"></script>
<script type="text/javascript" src="<?php echo Util::caminho_projeto() ?>/jquery/woothemes-FlexSlider-83b3cae/demo/js/shBrushJScript.js"></script>

<!-- Optional FlexSlider Additions -->
<script src="<?php echo Util::caminho_projeto() ?>/jquery/woothemes-FlexSlider-83b3cae/demo/js/jquery.easing.js"></script>
<script src="<?php echo Util::caminho_projeto() ?>/jquery/woothemes-FlexSlider-83b3cae/demo/js/jquery.mousewheel.js"></script>
<script defer src="<?php echo Util::caminho_projeto() ?>/jquery/woothemes-FlexSlider-83b3cae/demo/js/demo.js"></script>


<!-- Syntax Highlighter -->
<link href="<?php echo Util::caminho_projeto() ?>/jquery/woothemes-FlexSlider-83b3cae/css/shCore.css" rel="stylesheet" type="text/css" />
<link href="<?php echo Util::caminho_projeto() ?>/jquery/woothemes-FlexSlider-83b3cae/css/shThemeDefault.css" rel="stylesheet" type="text/css" />


<!-- Demo CSS -->
<link rel="stylesheet" href="<?php echo Util::caminho_projeto() ?>/jquery/woothemes-FlexSlider-83b3cae/css/flexslider.css" type="text/css" media="screen" />








  <!-- ---- LAYER SLIDER ---- -->
    <link rel="stylesheet" href="<?php echo Util::caminho_projeto() ?>/jquery/touchcarousel/touchcarousel.css"/>
    <link rel="stylesheet" href="<?php echo Util::caminho_projeto() ?>/jquery/touchcarousel/black-and-white-skin/black-and-white-skin.css" />
    <script src="<?php echo Util::caminho_projeto() ?>/jquery/touchcarousel/jquery.touchcarousel-1.2.min.js"></script>

    <script type="text/javascript">
    $(document).ready(function() {
        $("#carousel-gallery").touchCarousel({
            itemsPerPage: 1,
            scrollbar: true,
            scrollbarAutoHide: true,
            scrollbarTheme: "dark",
            pagingNav: false,
            snapToItems: true,
            scrollToLast: false,
            useWebkit3d: true,
            loopItems: true,
            autoplay: true
        });
    });
    </script>
    <!-- XXXX LAYER SLIDER XXXX -->




<!--    ====================================================================================================     -->
<!--    CLASSE DE FONTS DO SITE http://fortawesome.github.io/Font-Awesome/icons/    -->
<!--    ====================================================================================================     -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">




<script>
$(document).ready(function() {
    $('.FormBusca')
        .bootstrapValidator({
            container: 'tooltip',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                busca_produtos: {
                    validators: {
                        notEmpty: {
                            message: 'Por favor, preêncha esse campo.'
                        }
                    }
                }
            }
        })
        .on('error.field.bv', function(e, data) {
            // Get the tooltip
            var $parent = data.element.parents('.form-group'),
                $icon   = $parent.find('.form-control-feedback[data-bv-icon-for="' + data.field + '"]'),
                title   = $icon.data('bs.tooltip').getTitle();

            // Destroy the old tooltip and create a new one positioned to the right
            $icon.tooltip('destroy').tooltip({
                html: true,
                placement: 'right',
                title: title,
                container: 'body'
            });
        });

    // Reset the Tooltip container form
    $('#resetButton').on('click', function(e) {
        var fields = $('#tooltipContainerForm').data('bootstrapValidator').getOptions().fields,
            $parent, $icon;

        for (var field in fields) {
            $parent = $('[name="' + field + '"]').parents('.form-group');
            $icon   = $parent.find('.form-control-feedback[data-bv-icon-for="' + field + '"]');
            $icon.tooltip('destroy');
        }

        // Then reset the form
        $('#tooltipContainerForm').data('bootstrapValidator').resetForm(true);
    });
});
</script>