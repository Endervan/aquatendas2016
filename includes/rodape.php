<div class="clearfix"></div>
<div class="top50"></div>
<div class="container-fluid rodape-cinza">
	<div class="row">

		<!-- menu -->
		<div class="container bg-menu-rodape top40">
			<div class="row">
				<div class="col-xs-12">
					<ul class="menu-rodape">
						<li>
							<a href="<?php echo Util::caminho_projeto() ?>/">HOME</a></li>
							<li><a href="<?php echo Util::caminho_projeto() ?>/empresa">EMPRESA</a></li>
							<li><a href="<?php echo Util::caminho_projeto() ?>/produtos">PRODUTOS</a></li>
							<li><a href="<?php echo Util::caminho_projeto() ?>/servicos">SERVIÇOS</a></li>
							<li><a href="<?php echo Util::caminho_projeto() ?>/dicas">DICAS</a></li>
							<li><a href="<?php echo Util::caminho_projeto() ?>/trabalhe-conosco">TRABALHE CONOSCO</a></li>
							<li><a href="<?php echo Util::caminho_projeto() ?>/fale-conosco">FALE CONOSCO</a></li>
					</ul>
				</div>
			</div>
		</div>
		<!-- menu -->

		<!-- logo, endereco, telefone -->
		<div class="container top40 bottom20">
			<div class="row">
				<div class="col-xs-3">
					<a href="<?php echo Util::caminho_projeto() ?>">
						<img src="<?php echo Util::caminho_projeto() ?>/imgs/logo-rodape.png" alt="">
					</a>
				</div>
				<div class="col-xs-7">

					<p class="bottom15"><i class="glyphicon glyphicon-home"></i><?php Util::imprime($config[endereco]); ?></p>
					
					<p class="bottom15">
						<i class="glyphicon glyphicon-phone"></i> 
						<?php Util::imprime($config[telefone1]); ?>
						
						<?php if (!empty($config[telefone2])) { ?>
			                / <?php Util::imprime($config[telefone2]); ?>
			            <?php } ?>

			            <?php if (!empty($config[telefone3])) { ?>
			                / <?php Util::imprime($config[telefone3]); ?>
			            <?php } ?>

			            <?php if (!empty($config[telefone4])) { ?>
			                / <?php Util::imprime($config[telefone4]); ?>
			            <?php } ?>

					</p>

					
				</div>

				<div class="col-xs-2">

					<?php if ($config[google_plus] != "") { ?>
						<a class="pull-left top25 left50" href="<?php Util::imprime($config[google_plus]); ?>" title="Google Plus" target="_blank" >
							<p class=""><i class="fa fa-google-plus"></i></p>
						</a>
					<?php } ?>


					<a href="http://www.homewebbrasil.com.br" target="_blank">
						<img src="<?php echo Util::caminho_projeto() ?>/imgs/logo-masmidia.png"  alt="">
					</a>

				</div>
			</div>
		</div>
		<!-- logo, endereco, telefone -->


	</div>
</div>


<div class="container-fluid rodape-preto">
	<div class="row">
		<div>
			<h5 class="text-center top15 bottom15">© Copyright AQUATENDAS</h5>
		</div>
	</div>
</div>


