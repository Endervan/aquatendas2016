<div class="container">
<nav class="navbar navbar-default navbar-static-top">
      

		

		<div class="col-xs-3">
	        <div class="navbar-header">
	          <!-- The mobile navbar-toggle button can be safely removed since you do not need it in a non-responsive implementation -->
	          <a class="navbar-brand" href="<?php echo Util::caminho_projeto() ?>">
				<img src="<?php echo Util::caminho_projeto() ?>/imgs/logo.png" alt="">
	          </a>
	        </div>
        </div>


        <div class="col-xs-9">

        	<div class="col-xs-12 top10">
        		
        		
	            <div class="col-xs-9 text-right">
	            	<?php if (!empty($config[telefone1])) { ?>
	                	<a class="btn btn-azul-claro right15" href="javascript:void(0);">
	                		<i class="fa fa-phone"></i> 
	                		<?php Util::imprime($config[telefone1]); ?>
	                	</a>
	                <?php } ?>

	                <?php if (!empty($config[telefone2])) { ?>
	                	<a class="btn btn-azul-claro right15" href="javascript:void(0);">
	                		<i class="fa fa-whatsapp"></i>
	                		<?php Util::imprime($config[telefone2]); ?>
                		</a>
	                <?php } ?>

	                <?php if (!empty($config[telefone3])) { ?>
	                	<a class="btn btn-azul-claro" href="javascript:void(0);">
	                		<i class="fa fa-whatsapp"></i>
	                		<?php Util::imprime($config[telefone3]); ?>
                		</a>
	                <?php } ?>

	            </div>  
	              
	            

	            <div class="dropdown pull-right">
	              <a href="#" class="dropdown-toggle btn btn-azul-claro" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
	              <i class="fa fa-shopping-cart right10"></i>
	              <span class="right10">MEU ORÇAMENTO</span>
	              <span class="caret"></span></a>
	              
	              <div class="dropdown-menu topo-meu-orcamento pull-right" aria-labelledby="dLabel">



			          <?php
			          if(count($_SESSION[solicitacoes_produtos]) > 0)
			          {
			              echo '<h6 class="bottom20 top20">MEUS PRODUTOS('. count($_SESSION[solicitacoes_produtos]) .')</h6>';

			              for($i=0; $i < count($_SESSION[solicitacoes_produtos]); $i++)
			              {
			                  $row = $obj_site->select_unico("tb_produtos", "idproduto", $_SESSION[solicitacoes_produtos][$i]);
			                  ?>
			                  <div class="lista-itens-carrinho clearfix">
			                      <div class="col-xs-2">
			                          <img src="<?php echo Util::caminho_projeto() ?>/uploads/tumb_<?php Util::imprime($row[imagem]) ?>" height="46" width="29" alt="">
			                      </div>
			                      <div class="col-xs-9">
			                          <h4><?php Util::imprime($row[titulo]) ?></h4>
			                      </div>
			                      <div class="col-xs-1">
			                          <a href="<?php echo Util::caminho_projeto() ?>/orcamento/?action=del&id=<?php echo $i; ?>&tipo=produto" data-toggle="tooltip" data-placement="top" title="Excluir"> <i class="glyphicon glyphicon-remove"></i> </a>
			                      </div>
			                  </div>
			                  <?php
			              }
			          }
			          ?>




			          <?php
			          if(count($_SESSION[solicitacoes_servicos]) > 0)
			          {
			              echo '<h6 class="bottom20">MEUS SERVIÇOS('. count($_SESSION[solicitacoes_servicos]) .')</h6>';

			              for($i=0; $i < count($_SESSION[solicitacoes_produtos]); $i++)
			              {
			                  $row = $obj_site->select_unico("tb_servicos", "idservico", $_SESSION[solicitacoes_servicos][$i]);
			                  ?>
			                  <div class="lista-itens-carrinho clearfix">
			                      <div class="col-xs-2">
			                          <img src="<?php echo Util::caminho_projeto() ?>/uploads/tumb_<?php Util::imprime($row[imagem]) ?>" height="46" width="29" alt="">
			                      </div>
			                      <div class="col-xs-9">
			                          <h4><?php Util::imprime($row[titulo]) ?></h4>
			                      </div>
			                      <div class="col-xs-1">
			                          <a href="<?php echo Util::caminho_projeto() ?>/orcamento/?action=del&id=<?php echo $i; ?>&tipo=servico" data-toggle="tooltip" data-placement="top" title="Excluir"> <i class="glyphicon glyphicon-remove"></i> </a>
			                      </div>
			                  </div>
			                  <?php
			              }
			          }
			          ?>

					  <div class="text-right bottom20">
						<a href="<?php echo Util::caminho_projeto() ?>/orcamento" title="Finalizar" class="btn btn-azul">
						  FINALIZAR
						</a>
					  </div>


					</div>
	            </div>


        	</div>
			
	
			<div class="col-xs-12 top30">
	        	<!-- Note that the .navbar-collapse and .collapse classes have been removed from the #navbar -->
		        <div id="navbar">
		          <ul class="nav navbar-nav">
		            <li class="<?php if(Url::getURL( 0 ) == ""){ echo "active"; } ?>">
		            	<a href="<?php echo Util::caminho_projeto() ?>">
		            		<img src="<?php echo Util::caminho_projeto() ?>/imgs/icon-home.png" alt="">
		            		<span class="clearfix">HOME</span>
	            		</a>
	            	</li>
	            	<li class="<?php if(Url::getURL( 0 ) == "empresa"){ echo "active"; } ?>">
		            	<a href="<?php echo Util::caminho_projeto() ?>/empresa">
		            		<img src="<?php echo Util::caminho_projeto() ?>/imgs/icon-empresa.png" alt="">
		            		<span class="clearfix">EMPRESA</span>
	            		</a>
	            	</li>
	            	<li class="<?php if(Url::getURL( 0 ) == "produtos" or Url::getURL( 0 ) == "produto"){ echo "active"; } ?>">
		            	<a href="<?php echo Util::caminho_projeto() ?>/produtos">
		            		<img src="<?php echo Util::caminho_projeto() ?>/imgs/icon-produtos.png" alt="">
		            		<span class="clearfix">PRODUTOS</span>
	            		</a>
	            	</li>
	            	<li class="<?php if(Url::getURL( 0 ) == "servicos" or Url::getURL( 0 ) == "servico"){ echo "active"; } ?>">
		            	<a href="<?php echo Util::caminho_projeto() ?>/servicos">
		            		<img src="<?php echo Util::caminho_projeto() ?>/imgs/icon-servicos.png" alt="">
		            		<span class="clearfix">SERVIÇOS</span>
	            		</a>
	            	</li>
	            	<li class="<?php if(Url::getURL( 0 ) == "dicas" or Url::getURL( 0 ) == "dica"){ echo "active"; } ?>">
		            	<a href="<?php echo Util::caminho_projeto() ?>/dicas">
		            		<img src="<?php echo Util::caminho_projeto() ?>/imgs/icon-dicas.png" alt="">
		            		<span class="clearfix">DICAS</span>
	            		</a>
	            	</li>
	            	<li class="<?php if(Url::getURL( 0 ) == "fale-conosco"){ echo "active"; } ?>">
		            	<a href="<?php echo Util::caminho_projeto() ?>/fale-conosco">
		            		<img src="<?php echo Util::caminho_projeto() ?>/imgs/icon-fale-conosco.png" alt="">
		            		<span class="clearfix">FALE CONOSCO</span>
	            		</a>
	            	</li>
		            <li class="<?php if(Url::getURL( 0 ) == "trabalhe-conosco"){ echo "active"; } ?>">
		            	<a href="<?php echo Util::caminho_projeto() ?>/trabalhe-conosco">
		            		<img src="<?php echo Util::caminho_projeto() ?>/imgs/icon-trabalhe-conosco.png" alt="">
		            		<span class="clearfix">TRABALHE CONOSCO</span>
	            		</a>
	            	</li>

		            
		          </ul>		          
		          
		        </div><!--/.nav-collapse -->
	        </div>
        	
        </div>

        





    </nav>
      </div>



