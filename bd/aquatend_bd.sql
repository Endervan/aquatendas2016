-- phpMyAdmin SQL Dump
-- version 4.0.10.7
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tempo de Geração: 28/07/2016 às 21:05
-- Versão do servidor: 5.5.48-37.8
-- Versão do PHP: 5.4.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Banco de dados: `aquatend_bd`
--

-- --------------------------------------------------------

--
-- Estrutura para tabela `tb_avaliacoes_produtos`
--

CREATE TABLE IF NOT EXISTS `tb_avaliacoes_produtos` (
  `idavaliacaoproduto` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `comentario` longtext COLLATE utf8_unicode_ci,
  `ativo` varchar(3) COLLATE utf8_unicode_ci DEFAULT 'NAO',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `id_produto` int(11) DEFAULT NULL,
  `data` date DEFAULT NULL,
  `nota` int(11) DEFAULT NULL,
  PRIMARY KEY (`idavaliacaoproduto`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=29 ;

--
-- Fazendo dump de dados para tabela `tb_avaliacoes_produtos`
--

INSERT INTO `tb_avaliacoes_produtos` (`idavaliacaoproduto`, `nome`, `email`, `comentario`, `ativo`, `ordem`, `url_amigavel`, `id_produto`, `data`, `nota`) VALUES
(26, 'Marcio', 'marciomas@gmail.com', 'Um excelente produto, recomendo a todos.', 'SIM', 0, ' ', 7, '2016-01-28', 5),
(28, ' ', ' ', ' ', 'NAO', 0, ' ', 7, '2016-01-28', 0);

-- --------------------------------------------------------

--
-- Estrutura para tabela `tb_banners`
--

CREATE TABLE IF NOT EXISTS `tb_banners` (
  `idbanner` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `imagem` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ativo` varchar(255) COLLATE utf8_unicode_ci DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `tipo_banner` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `legenda` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url_btn_orcamento` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`idbanner`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=8 ;

--
-- Fazendo dump de dados para tabela `tb_banners`
--

INSERT INTO `tb_banners` (`idbanner`, `titulo`, `imagem`, `ativo`, `ordem`, `tipo_banner`, `url_amigavel`, `url`, `legenda`, `url_btn_orcamento`) VALUES
(1, 'PISCINAS EM VINIL', '2903201609258201253650.jpg', 'SIM', NULL, '1', 'piscinas-em-vinil', 'http://aquatendas.com.br/piscinas-vinil', 'QUALIDADE, CONFORTO E RAPIDEZ EM SEU PROJETO', 'http://aquatendas.com.br/aquatendas2016/orcamento'),
(3, 'PISCINAS DE VINIL', '2903201610468779584864.jpg', 'SIM', 1, '2', 'piscinas-de-vinil', 'http://aquatendas.com.br/mobile/piscinas-vinil/', 'QUALIDADE, CONFORTO E RAPIDEZ NO SEU PROJETO', NULL),
(4, 'ILUMINAÇÃO LED PISCINA', '1604201612364860573729.jpg', 'SIM', NULL, '1', 'iluminacao-led-piscina', 'http://aquatendas.com.br/produtos/iluminacao', 'MAIS BELEZA, REQUINTE E ECONOMIA', NULL),
(5, 'PRODUTOS PARA PISCINAS', '2903201606257729269340.jpg', 'SIM', NULL, '1', 'produtos-para-piscinas', 'http://aquatendas.com.br/produtos/quimicos', 'SUA PISCINA MAIS LIMPA COM AS MELHORES MARCAS', NULL),
(6, 'ILUMINAÇÃO LED PISCINA', '1604201612555409723180.jpg', 'SIM', 2, '2', 'iluminacao-led-piscina', 'http://aquatendas.com.br/mobile/produtos/?cat=iluminacao', 'MAIS BELEZA, REQUINTE E ECONOMIA', NULL),
(7, 'PRODUTOS PARA PISCINAS', '2903201610239261558349.jpg', 'SIM', NULL, '2', 'produtos-para-piscinas', 'http://aquatendas.com.br/mobile/produtos/?cat=quimicos', 'SUA PISCINA MAIS LIMPA COM AS MELHORES MARCAS', NULL);

-- --------------------------------------------------------

--
-- Estrutura para tabela `tb_categorias_produtos`
--

CREATE TABLE IF NOT EXISTS `tb_categorias_produtos` (
  `idcategoriaproduto` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `desCategoria_Dica_Modelcricao` longtext COLLATE utf8_unicode_ci,
  `imagem` varchar(45) COLLATE utf8_unicode_ci DEFAULT 'imagem_nao_disponivel.jpg',
  `ativo` varchar(3) COLLATE utf8_unicode_ci DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `keywords_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`idcategoriaproduto`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=74 ;

--
-- Fazendo dump de dados para tabela `tb_categorias_produtos`
--

INSERT INTO `tb_categorias_produtos` (`idcategoriaproduto`, `titulo`, `desCategoria_Dica_Modelcricao`, `imagem`, `ativo`, `ordem`, `url_amigavel`, `title_google`, `keywords_google`, `description_google`) VALUES
(57, 'PISCINAS', NULL, '2701201602021288111244.png', 'SIM', NULL, 'piscinas', NULL, NULL, NULL),
(59, 'ACESSÓRIOS', NULL, '2701201602041208570733.png', 'SIM', NULL, 'acessorios', NULL, NULL, NULL),
(60, 'CASCATAS', NULL, '2701201602041311380045.png', 'SIM', NULL, 'cascatas', NULL, NULL, NULL),
(61, 'FILTROS', NULL, '2701201602041273827844.png', 'SIM', NULL, 'filtros', NULL, NULL, NULL),
(62, 'QUÍMICOS', NULL, '2701201602041291193930.png', 'SIM', NULL, 'quimicos', NULL, NULL, NULL),
(70, 'AQUECEDORES', NULL, '2203201612533791852970.png', 'SIM', NULL, 'aquecedores', NULL, NULL, NULL),
(71, 'ILUMINAÇÃO', NULL, '2203201604051391296171.png', 'SIM', NULL, 'iluminacao', NULL, NULL, NULL),
(72, 'ESCADAS', NULL, '2203201605209121044373.png', 'SIM', NULL, 'escadas', NULL, NULL, NULL),
(73, 'SAUNAS', NULL, '3003201603217918150126.png', 'SIM', NULL, 'saunas', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura para tabela `tb_comentarios_dicas`
--

CREATE TABLE IF NOT EXISTS `tb_comentarios_dicas` (
  `idcomentariodica` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `comentario` longtext COLLATE utf8_unicode_ci,
  `ativo` varchar(3) COLLATE utf8_unicode_ci DEFAULT 'NAO',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `id_dica` int(11) DEFAULT NULL,
  `data` date DEFAULT NULL,
  PRIMARY KEY (`idcomentariodica`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura para tabela `tb_comentarios_piscinas_vinil`
--

CREATE TABLE IF NOT EXISTS `tb_comentarios_piscinas_vinil` (
  `idcomentariopiscinavinil` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `comentario` longtext COLLATE utf8_unicode_ci,
  `ativo` varchar(3) COLLATE utf8_unicode_ci DEFAULT 'NAO',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `id_piscinavinil` int(11) DEFAULT NULL,
  `data` date DEFAULT NULL,
  `nota` int(11) DEFAULT NULL,
  PRIMARY KEY (`idcomentariopiscinavinil`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=32 ;

--
-- Fazendo dump de dados para tabela `tb_comentarios_piscinas_vinil`
--

INSERT INTO `tb_comentarios_piscinas_vinil` (`idcomentariopiscinavinil`, `nome`, `email`, `comentario`, `ativo`, `ordem`, `url_amigavel`, `id_piscinavinil`, `data`, `nota`) VALUES
(29, 'Maria Aparecida', 'maria@masmidia.com.br', 'Uma excelente piscina, adorei a instalação.', 'SIM', 0, ' ', 0, '2016-02-10', 5),
(30, ' ', ' ', ' ', 'NAO', 0, ' ', 0, '2016-02-10', 0),
(31, ' ', ' ', ' ', 'NAO', 0, ' ', 0, '2016-02-10', 0);

-- --------------------------------------------------------

--
-- Estrutura para tabela `tb_comentarios_produtos`
--

CREATE TABLE IF NOT EXISTS `tb_comentarios_produtos` (
  `idcomentarioproduto` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `comentario` longtext COLLATE utf8_unicode_ci,
  `ativo` varchar(3) COLLATE utf8_unicode_ci DEFAULT 'NAO',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `id_produto` int(11) DEFAULT NULL,
  `data` date DEFAULT NULL,
  PRIMARY KEY (`idcomentarioproduto`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura para tabela `tb_configuracoes`
--

CREATE TABLE IF NOT EXISTS `tb_configuracoes` (
  `idconfiguracao` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `titulo` varchar(255) DEFAULT NULL,
  `title_google` varchar(255) NOT NULL,
  `description_google` varchar(255) NOT NULL,
  `keywords_google` varchar(255) NOT NULL,
  `ativo` varchar(3) NOT NULL DEFAULT 'SIM',
  `ordem` int(10) unsigned NOT NULL,
  `url_amigavel` varchar(255) NOT NULL,
  `endereco` varchar(255) NOT NULL,
  `telefone1` varchar(255) NOT NULL,
  `telefone2` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `src_place` varchar(255) NOT NULL,
  `horario_1` varchar(255) DEFAULT NULL,
  `horario_2` varchar(255) DEFAULT NULL,
  `email_copia` varchar(255) DEFAULT NULL,
  `telefone3` varchar(255) DEFAULT NULL,
  `telefone4` varchar(255) DEFAULT NULL,
  `google_plus` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`idconfiguracao`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Fazendo dump de dados para tabela `tb_configuracoes`
--

INSERT INTO `tb_configuracoes` (`idconfiguracao`, `titulo`, `title_google`, `description_google`, `keywords_google`, `ativo`, `ordem`, `url_amigavel`, `endereco`, `telefone1`, `telefone2`, `email`, `src_place`, `horario_1`, `horario_2`, `email_copia`, `telefone3`, `telefone4`, `google_plus`) VALUES
(1, 'Aquatendas 2016', 'Aquatendas Piscinas de vinil  Brasília - DF - Construção e manutenção de piscinas, produtos e acessórios', 'Aquatendas - Piscinas e Acessórios - Construção de piscinas em vinil e azulejo, aquecimento solar e elétrico, filtros, bombas, produtos químicos, acessórios, capas, lonas para tendas e palcos e reformas em geral.', 'Revestimento de Vinil , Capas para Piscinas, Tendas, Toldos, Manutenção e Reforma de Piscinas, Piscinas de Fibra e de Vinil, Acessórios para  Piscinas, Escadas , Duchas e Cascatas, Bombas e Filtros', 'SIM', 0, 'aquatendas-2016', 'R 10 - Chácara 138 Lote 6 A, Vicente Pires -  Brasília - DF', '(61) 3302-4517', '(61) 8464-9697', 'aquatendas@gmail.com', 'https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d245795.5082910491!2d-47.93735785!3d-15.72176215!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1spt-BR!2sbr!4v1453991657890', NULL, NULL, 'junior@homewebbrasil.com.br, marciomas@gmail.com, angela.homeweb@gmail.com', '(61) 9903-5263', '', 'https://plus.google.com/u/0/b/106485846105819215402/106485846105819215402');

-- --------------------------------------------------------

--
-- Estrutura para tabela `tb_depoimentos`
--

CREATE TABLE IF NOT EXISTS `tb_depoimentos` (
  `iddepoimento` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(255) DEFAULT NULL,
  `depoimento` longtext,
  `ativo` varchar(3) DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) DEFAULT NULL,
  `imagem` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`iddepoimento`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Fazendo dump de dados para tabela `tb_depoimentos`
--

INSERT INTO `tb_depoimentos` (`iddepoimento`, `titulo`, `depoimento`, `ativo`, `ordem`, `url_amigavel`, `imagem`) VALUES
(7, 'TESTE', '<p>\r\n	TESTE</p>', 'SIM', NULL, 'teste', NULL);

-- --------------------------------------------------------

--
-- Estrutura para tabela `tb_dicas`
--

CREATE TABLE IF NOT EXISTS `tb_dicas` (
  `iddica` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `descricao` longtext COLLATE utf8_unicode_ci,
  `imagem` varchar(45) COLLATE utf8_unicode_ci DEFAULT 'imagem_nao_disponivel.jpg',
  `ativo` varchar(3) COLLATE utf8_unicode_ci DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `keywords_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `data` date DEFAULT NULL,
  PRIMARY KEY (`iddica`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=42 ;

--
-- Fazendo dump de dados para tabela `tb_dicas`
--

INSERT INTO `tb_dicas` (`iddica`, `titulo`, `descricao`, `imagem`, `ativo`, `ordem`, `url_amigavel`, `title_google`, `keywords_google`, `description_google`, `data`) VALUES
(38, 'Como iluminar a piscina de casa', '<p>\r\n	Ter uma piscina bem iluminada em casa &eacute; garantia de beleza e funcionalidade na &aacute;rea externa. As luzes que decoram permitem tamb&eacute;m aproveitar o espa&ccedil;o durante a noite, com esportes ou apenas momentos de relax. E existem v&aacute;rias op&ccedil;&otilde;es para o projeto luminot&eacute;cnico, sendo o LED e a fibra &oacute;tica as mais reconhecidas do mercado. A primeira alternativa consome menos energia e exige baixa manuten&ccedil;&atilde;o &ndash; h&aacute; refletores de 3,5 watts, contra os 50 watts das l&acirc;mpadas dicroicas.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	O sistema LED conta ainda com grande variedade de cores e refletores potentes de at&eacute; 300 watts (devendo ser instalados na parede da piscina a cada 48 m&sup2;). &ldquo;A ilumina&ccedil;&atilde;o da piscina tem que ser adequada ao tamanho do tanque e &agrave; cor do revestimento interno, para que haja luz na totalidade&rdquo;, afirma Adriana Noya, arquiteta. E, caso n&atilde;o seja poss&iacute;vel colocar as luzes dentro da piscina, o ideal &eacute; posicionar os refletores na dire&ccedil;&atilde;o da &aacute;gua. &ldquo;As pessoas erram muito ao instalar os fachos na dire&ccedil;&atilde;o do im&oacute;vel, pois isso ofusca quem est&aacute; na &aacute;gua&rdquo;, diz. Al&eacute;m disso, &eacute; essencial ressaltar que quanto mais escuro for o revestimento da piscina, mais claridade ele absorver&aacute; do ambiente.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Outro aspecto fundamental no projeto luminot&eacute;cnico &eacute; iluminar as &aacute;reas de risco da piscina &ndash; escadas e degraus, por exemplo &ndash;, al&eacute;m de criar contrastes de luz e sombra nas quinas do tanque. Lembre-se tamb&eacute;m de instalar o dispositivo DR (Diferencial Residual), garantindo a interrup&ccedil;&atilde;o autom&aacute;tica de energia no caso de choques. A escolha do colorido das luzes &eacute; mais um ponto a ser estudado. Tonalidades de vermelho ou laranja trazem a impress&atilde;o de calor ao local, j&aacute; as cores azul e verde deixam a sensa&ccedil;&atilde;o de que a &aacute;gua est&aacute; fresca e gelada.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	A ilumina&ccedil;&atilde;o com fibra &oacute;tica &eacute; outra possibilidade na hora de trazer luz &agrave; piscina. Uma de suas principais vantagens &eacute; o baixo risco de acidentes el&eacute;tricos. O sistema tecnol&oacute;gico &eacute; seguro e exige a constru&ccedil;&atilde;o de uma caixa para o abrigo dos equipamentos, n&atilde;o sendo interessante usar condu&iacute;tes ou caixas de passagens de instala&ccedil;&otilde;es antigas. A fibra &oacute;tica tem vantagens tamb&eacute;m na quest&atilde;o est&eacute;tica. O modelo permite criar, por exemplo, o visual de &ldquo;estrelas&rdquo; no ch&atilde;o da piscina, gra&ccedil;as ao uso de proje&ccedil;&otilde;es e sa&iacute;das pontuais de luz. E n&atilde;o &eacute; s&oacute; isso. O tanque pode ganhar ainda a ilumina&ccedil;&atilde;o no contorno, garantindo mais destaque ao formato da piscina.</p>\r\n<p>\r\n	Leia mais http://www.lopes.com.br/blog/decoracao-paisagismo/iluminacao/como-iluminar-piscina-de-casa/#ixzz43fMVp5VC</p>', '2203201605395401159193..jpg', 'SIM', NULL, 'como-iluminar-a-piscina-de-casa', NULL, NULL, NULL, NULL),
(39, 'Dicas na hora de construir sua piscina', '<p>\r\n	<strong>POR ONDE COME&Ccedil;AR???</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Primeiro Passo:Contratar um arquiteto ou Empresa especializada em constru&ccedil;&atilde;o de piscinas, para elaborar o projeto de acordo com o formato que deseja e espa&ccedil;o que tem dispon&iacute;vel. O profissional ir&aacute; desenhar o modelo arquitet&ocirc;nico e dar sugest&otilde;es de acordo com o que necessita. Ele tamb&eacute;m orientar&aacute; na constru&ccedil;&atilde;o da alvenaria e instala&ccedil;&otilde;es El&eacute;tricas e Hidr&aacute;ulicas. Conselho se for contratar por exemplo um pedreiro verifique se j&aacute; instalou piscinas , obtenha refer&ecirc;ncias e etc....pois existem muitos que dizem saber e depois &eacute; s&oacute; problema ,transtornos e preju&iacute;zos ao inv&eacute;s de Lazer, compensa investir um pouco a mais neste item primnordial para o bom funcionamento da piscina.&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Segundo Passo: Pesquise os pre&ccedil;os , de posse do projeto ou do tamanho que pensa em construir,&nbsp;</p>\r\n<p>\r\n	Terceiro Passo: Acompanhe a constru&ccedil;&atilde;o. N&atilde;o necessitar&aacute; ficar o tempo todo na obra, mas uma supervis&atilde;o de vez em quando &eacute; recomendado, assim verificar&aacute; se tudo est&aacute; saindo conforme o projeto, mesmo tendo um Profissional de sua confian&ccedil;a acompanhando a obra.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>O QUE SE DEVE LEVAR EM CONTA:</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Primeiramente o espa&ccedil;o dispon&iacute;vel para instala&ccedil;&atilde;o.Deve-se pensar que al&eacute;m da piscina voc&ecirc; necessita deixar uma &aacute;rea para lazer ao redor da piscina, mesmo que pequena, onde poder&aacute; colocar por exemplo mesinhas , cadeiras e etc. Uma dica e n&atilde;o necess&aacute;riamente REGRA, seria o dobro do tamanho da piscina, exemplo, para uma piscina de 3x5 mts ou 15m2, deveria deixar um terreno ou espa&ccedil;o de 30 m2.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>A PROFUNDIDADE DA PISCINA:</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Profundidade - Antigamente, eram comuns piscinas grandes e bem profundas.Hoje,a profundidade diminuiu deixando-as mais confort&aacute;veis para o uso e tamb&eacute;m mais econ&ocirc;micas. A natureza agradece! #sustent&aacute;vel</p>\r\n<p>\r\n	* Lembre-se: a &aacute;gua fica uns 10cm abaixo da borda.</p>\r\n<p>\r\n	Piscina convencional (150cm) - &Eacute; a mais tradicional, a &aacute;gua fica no pesco&ccedil;o. &Eacute; usada para nadar, mas para &quot;pular de ponta&quot; deve ter 180cm.</p>\r\n<p>\r\n	Piscina familiar (120cm) - &Eacute; uma piscina para ficar conversando, curtindo a fam&iacute;lia e os amigos. Considero a profundidade pr&aacute;tica.</p>\r\n<p>\r\n	Piscina infantil (90cm) - &Eacute; a profundidade tradicional para crian&ccedil;as.</p>\r\n<p>\r\n	Prainha (50cm) - Serve para adultos e crian&ccedil;as, &eacute; aquele degrau que d&aacute; para sentar e ficar conversando.&nbsp;</p>\r\n<p>\r\n	Lava-p&eacute;s (30cm) - &Eacute; a profundidade que tamb&eacute;m serve para colocar as cadeiras de tomar sol, fica uma del&iacute;cia!</p>\r\n<p>\r\n	* Essas s&atilde;o apenas sugest&otilde;es de profundidade, n&atilde;o existe uma regra espec&iacute;fica. &Eacute; claro que voc&ecirc; n&atilde;o precisa ter todas elas numa piscina s&oacute;, veja a que mais de ad&eacute;qua ao seu uso e seu gosto.</p>\r\n<p>\r\n	Revestimentos - S&atilde;o muitas op&ccedil;&otilde;es e aqui o custo pode definir a escolha. As pastilhas de vidro s&atilde;o as mais bonitas e tamb&eacute;m as mais caras, mas quanto menor o tamanho, maiores as chances de descolar, por isso a m&atilde;o de obra deve ser de qualidade. Minha escolha pessoal &eacute; a pastilha cer&acirc;mica, mais barata e tamb&eacute;m muito bonita.</p>\r\n<p>\r\n	Podem ser: cer&acirc;mica, azulejo, pastilhas cer&acirc;micas (Atlas, Jatob&aacute;), pastilha de vidro (Vidrotil, Colormix), vinil.</p>\r\n<p>\r\n	Cores - A cor azul &eacute; a mais usada, variando de claro a escuro. Observo uma tend&ecirc;ncia com piscinas de uma cor s&oacute; em tons mais escuros, eu gosto mais das cores claras pois o c&eacute;u &eacute; que reflete na &aacute;gua alem do que quanto mais escuro menos reflex&atilde;o dos lumens das luzes do refletor, se tivermos 10% de escuro perdemos 10% na ilumina&ccedil;&atilde;o e assim por diante sem contar que perdemos a profundidade e fica dif&iacute;cil pra indentificar qualquer objeto que caia na piscina , j&aacute; as cores esverdeadas acho que combinam num contesto mais natural em lugares inseridos de mata.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>OUTRO DETALHE MUITO IMPORTANTE:</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Um detalhe que as vezes passa desapercebido, &eacute; ONDE EST&Aacute; O SOL. N&atilde;o adiantar&aacute; investir na constru&ccedil;&atilde;o da piscina e constru-i-la em local que o sol n&atilde;o apare&ccedil;a. Portanto n&atilde;o se esque&ccedil;a de verificar o local mais apropriado.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>PISCINA DE CONCRETO, FIBRA OU VINIL???</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Concreto:Estas piscinas chegam a custar at&eacute; 4 vezes mais que outros modelos .Compensa&ccedil;&atilde;o podem ter a forma e tamanho desejados.&nbsp;</p>\r\n<p>\r\n	Fibra: Modelos definidos pelo fabricante, necessita fazer uma caixa de alvenaria e instal&aacute;-la dentro da mesma, por virem prontas, considere o valor do transporte at&eacute; o local, e sendo invi&aacute;vel dependendo do terreno e local, que se encontram com dif&iacute;cil acesso por exemplo numa cobertura . Custam em m&eacute;dia duas vezes no m&iacute;nimo mais que as de vinil.</p>\r\n<p>\r\n	<strong>Vinil: As mais usadas atualmente, pela versatilidade, custo reduzido, f&aacute;cil manuten&ccedil;&atilde;o , em compara&ccedil;&atilde;o com outros modelos. Nas piscinas de vinil deve-se ter cuidado na utiliza&ccedil;&atilde;o da mesma, observe ou alerte para n&atilde;o se utiliz&aacute;-la com objetos pontiagudos ou cortantes, para n&atilde;o perfurar ou rasgar o vinil. Assim ter&aacute; vida longa para sua piscina.</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Fonte:&nbsp;http://www.guiadaconstrucao1.com.br/capa.asp?idpagina=122</p>', '3001201601491236205540..jpg', 'SIM', NULL, 'dicas-na-hora-de-construir-sua-piscina', NULL, NULL, NULL, NULL),
(40, 'Verão está no fim, mas manutenção de piscina precisa', '<p>\r\n	Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>', '3001201601501243792975..jpg', 'NAO', NULL, 'verao-esta-no-fim-mas-manutencao-de-piscina-precisa', NULL, NULL, NULL, NULL),
(41, 'Como limpar piscina: dicas práticas', '<p>\r\n	&ndash; &Eacute; de extrema import&acirc;ncia limpar todo o contorno da piscina. Caso esteja coberta com uma lona, elimine toda a &aacute;gua e sujeira que fica acumulada. Em seguida retire as folhas e a sujidade sempre no sentido oposto &agrave; localiza&ccedil;&atilde;o da piscina.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	&ndash; Aplique algumas gotas de qualquer produto espec&iacute;fico para o rebordo da piscina. Existem no mercado bons produtos para limpar os rebordos, biodegrad&aacute;veis e que n&atilde;o estragam os azulejos. Estes produtos podem ser aplicados sem luvas.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	&ndash; Esvazie o dep&oacute;sito do filtro e limpe-o para que n&atilde;o corra o risco de ficar entupido. N&atilde;o se esque&ccedil;a de verificar o conjunto de filtragem da sua piscina, para ver se o mesmo est&aacute; a funcionar bem e correctamente.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	&ndash; Escolha um dia da semana para verificar se n&atilde;o h&aacute; folhas e qualquer tipo de sujeira maior que possa prejudicar o fluxo da &aacute;gua. Os filtros est&atilde;o localizados no fundo da piscina.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Material de limpeza das piscinas</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	&ndash; Com a Rede para folhas, limpe bem a superf&iacute;cie da &aacute;gua.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	&ndash; Escove as paredes da piscina pelo menos uma vez na semana. Isso ajuda a eliminar dep&oacute;sitos de algas, antes que comecem a criar manchas. Uma dica importante: use escovas mais duras para piscinas de concreto armado e as macias para piscinas de fibra de vidro e em pisos emborrachados. Se for caso da piscina ser em azulejo, n&atilde;o use nada abrasivo, pois pode arranhar e danificar a superf&iacute;cie.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	&ndash; Depois de usar as escovas, use o aspirador e remova do fundo da piscina aqueles peda&ccedil;os pequenos que ca&iacute;ram durante a escova&ccedil;&atilde;o. Opte por aspirar uma vez na semana, para manter a &aacute;gua limpa.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<strong>Como manter piscina limpa</strong></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	&ndash; Por fim, o cloro. Na verdade o cloro serve como desinfectante. Sendo assim, a sua aplica&ccedil;&atilde;o &eacute; indispens&aacute;vel. Mas tenha aten&ccedil;&atilde;o em usar conforme as instru&ccedil;&otilde;es do fabricante.</p>', '2203201606153585587131..jpg', 'SIM', NULL, 'como-limpar-piscina-dicas-praticas', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura para tabela `tb_empresa`
--

CREATE TABLE IF NOT EXISTS `tb_empresa` (
  `idempresa` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `titulo` varchar(80) NOT NULL,
  `descricao` longtext NOT NULL,
  `ativo` varchar(3) NOT NULL DEFAULT 'SIM',
  `ordem` int(10) unsigned NOT NULL,
  `title_google` varchar(255) NOT NULL,
  `keywords_google` varchar(255) NOT NULL,
  `description_google` varchar(255) NOT NULL,
  `url_amigavel` varchar(255) NOT NULL,
  `colaboradores_especializados` int(11) DEFAULT NULL,
  `trabalhos_entregues` int(11) DEFAULT NULL,
  `trabalhos_entregues_este_mes` int(11) DEFAULT NULL,
  PRIMARY KEY (`idempresa`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Fazendo dump de dados para tabela `tb_empresa`
--

INSERT INTO `tb_empresa` (`idempresa`, `titulo`, `descricao`, `ativo`, `ordem`, `title_google`, `keywords_google`, `description_google`, `url_amigavel`, `colaboradores_especializados`, `trabalhos_entregues`, `trabalhos_entregues_este_mes`) VALUES
(1, 'Serviços - Legenda', '<p>\r\n	Profissionais qualificados e agilidade na entrega dos servi&ccedil;os</p>', 'SIM', 0, '', '', '', 'servicos--legenda', NULL, NULL, NULL),
(2, 'Produtos - Legenda', '<p>\r\n	Mais de 09 anos no mercado oferecendo produtos de qualidade</p>', 'SIM', 0, '', '', '', 'produtos--legenda', NULL, NULL, NULL),
(3, 'Empresa - Legenda', '<p>\r\n	Atuando no segmento de piscinas, lonas e acess&oacute;rios.</p>', 'SIM', 0, '', '', '', 'empresa--legenda', NULL, NULL, NULL),
(4, 'Empresa - Sobre', '<p style="text-align: justify;">\r\n	A Aquatendas Piscinas &eacute; uma empresa que atua a mais de 9 anos no segmento de constru&ccedil;&atilde;o e reformas de piscinas de vinil e de azulejos visando a satisfa&ccedil;&atilde;o dos nossos clientes. Trabalhamos com profissionais qualificados e com produtos de qualidade, &nbsp;proporcionando agilidade&nbsp;na entrega dos servi&ccedil;os. Oferecemos tamb&eacute;m ampla linha de produtos e equipamentos e acess&oacute;rios para sua piscina e ambiente, tais como: aquecimento solar e el&eacute;trico, capas t&eacute;rmica, prote&ccedil;&atilde;o e tela, acess&oacute;rios em geral, produtos qu&iacute;micos, cascatas, escadas, e outros mais.</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	A Aquatendas Piscinas &nbsp;oferece diferentes modelos, estampas ou alvenaria, de piscinas de vinil e azulejo executando os projetos escolhidos pelo cliente.</p>', 'SIM', 0, '', '', '', 'empresa--sobre', NULL, NULL, NULL),
(5, 'Empresa - Qualidade dos serviços', '<p>\r\n	A Aquatendas Piscinas conta com profissionais qualificados e produtos para a constru&ccedil;&atilde;o, reforma e manuten&ccedil;&atilde;o de sua piscina em vinil ou azulejo, seja residencial, em condom&iacute;nios, ch&aacute;caras e fazendas, em clubes e academias.</p>', 'SIM', 0, '', '', '', 'empresa--qualidade-dos-servicos', NULL, NULL, NULL),
(6, 'Portfólio - Legenda', 'Legenda portfólio dolor sit amet, consectetur adipisicing elit', 'SIM', 0, '', '', '', '', NULL, NULL, NULL),
(7, 'Dicas - Legenda', '<p>\r\n	Confira algumas dicas para constru&ccedil;&atilde;o e manuten&ccedil;&atilde;o de piscinas</p>', 'SIM', 0, '', '', '', 'dicas--legenda', NULL, NULL, NULL),
(8, 'Index - Conheça mais a Aquatendas', '<p>\r\n	A Aquatendas Piscinas &eacute; uma empresa que atua a mais de 9 anos no segmento de constru&ccedil;&atilde;o e reformas de piscinas de vinil e de azulejos visando a satisfa&ccedil;&atilde;o dos nossos clientes. Trabalhamos com profissionais qualificados e com produtos de qualidade, &nbsp;proporcionando agilidade&nbsp;na entrega dos servi&ccedil;os. Oferecemos tamb&eacute;m ampla linha de produtos e equipamentos e acess&oacute;rios para sua piscina e ambiente, tais como: aquecimento solar e el&eacute;trico, capas t&eacute;rmica, prote&ccedil;&atilde;o e tela, acess&oacute;rios em geral, produtos qu&iacute;micos, cascatas, escadas, e outros mais.</p>', 'SIM', 0, '', '', '', 'index--conheca-mais-a-aquatendas', NULL, NULL, NULL),
(9, 'Orçamento - Legenda', '<p>\r\n	Solicite um or&ccedil;amento e uma visita de nossos profissionais</p>', 'SIM', 0, '', '', '', 'orcamento--legenda', NULL, NULL, NULL),
(10, 'Fale conosco - Legenda', '<p>\r\n	Entre em contato com a Aquatendas Piscinas</p>', 'SIM', 0, '', '', '', 'fale-conosco--legenda', NULL, NULL, NULL),
(11, 'Trabalhe conosco - Legenda', '<p>\r\n	Fa&ccedil;a parte de nossa equipe de colaboradores</p>', 'SIM', 0, '', '', '', 'trabalhe-conosco--legenda', NULL, NULL, NULL),
(12, 'Piscinal de vinil - Descrição', '<p>\r\n	H&aacute; muitas vantagens para a instala&ccedil;&atilde;o de uma piscina de vinil. Piscinas de vinil s&atilde;o muito flex&iacute;veis e n&atilde;o pode rachar como uma piscina de concreto. Voc&ecirc; nunca ter&aacute; que redesenhar uma piscina de vinil. O revestimento de vinil faz com que todas as superf&iacute;cies de sua piscina se tornem suaves. Por causa da flexibilidade com as paredes, uma piscina de vinil oferece ilimitadas formas de design. Uma piscina de vinil tamb&eacute;m &eacute; menos caro do que uma piscina de fibra de vidro ou piscina de concreto.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Os revestimentos de vinil n&atilde;o s&atilde;o limitados em cores e padr&otilde;es como costumavam ser. Ao longo do tempo, mudando o revestimento em sua piscina, far&aacute; melhorar dramaticamente o efeito visual em seu ambiente. O custo do revestimento depender&aacute; da sua forma, tamanho, espessura, e a escolha padr&atilde;o.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Como em todas as piscinas em ambientes cobertos, uma variedade de recursos podem ser adicionados &agrave; sua op&ccedil;&atilde;o por vinil, incluindo spas, cachoeiras, decks, jatos laminares, efeitos de ilumina&ccedil;&atilde;o e aquecedores de piscina.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Aquatendas Piscinas vem h&aacute; mais de nove anos atuando na constru&ccedil;&atilde;o e instala&ccedil;&atilde;o de piscina de vinil de grande, m&eacute;dio ou pequeno porte, formado por profissionais com grande experi&ecirc;ncia. Possibilitamos o menor pre&ccedil;o e a melhor qualidade de nossos produtos, oferecemos parceria e solu&ccedil;&otilde;es para os projetos dos clientes.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Consulte a Aquatendas Piscinas, traga seu projeto e solicite or&ccedil;amento da constru&ccedil;&atilde;o de piscinas e instala&ccedil;&atilde;o de revestimentos de vinil dispon&iacute;veis para sua obra.</p>', 'SIM', 0, '', '', '', 'piscinal-de-vinil--descricao', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura para tabela `tb_galerias_portifolios`
--

CREATE TABLE IF NOT EXISTS `tb_galerias_portifolios` (
  `idgaleriaportifolio` int(11) NOT NULL AUTO_INCREMENT,
  `imagem` varchar(255) DEFAULT NULL,
  `ativo` varchar(3) DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) DEFAULT NULL,
  `id_portifolio` int(11) DEFAULT NULL,
  PRIMARY KEY (`idgaleriaportifolio`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=80 ;

--
-- Fazendo dump de dados para tabela `tb_galerias_portifolios`
--

INSERT INTO `tb_galerias_portifolios` (`idgaleriaportifolio`, `imagem`, `ativo`, `ordem`, `url_amigavel`, `id_portifolio`) VALUES
(56, '2801201603021200738207.jpg', 'SIM', NULL, NULL, 7),
(57, '2801201603021232320941.jpg', 'SIM', NULL, NULL, 7),
(58, '2801201603021116541054.jpg', 'SIM', NULL, NULL, 7),
(59, '2801201603021384773400.jpg', 'SIM', NULL, NULL, 7),
(60, '2801201603021211824913.jpg', 'SIM', NULL, NULL, 7),
(61, '2801201603031133705918.jpg', 'SIM', NULL, NULL, 8),
(62, '2801201603031191229581.jpg', 'SIM', NULL, NULL, 8),
(63, '2801201603031253263167.jpg', 'SIM', NULL, NULL, 8),
(64, '2801201603031277500028.jpg', 'SIM', NULL, NULL, 8),
(65, '2801201603031365426813.jpg', 'SIM', NULL, NULL, 8),
(66, '2801201603031408059643.jpeg', 'SIM', NULL, NULL, 9),
(67, '2801201603031164317371.jpg', 'SIM', NULL, NULL, 9),
(68, '2801201603031348094918.jpg', 'SIM', NULL, NULL, 9),
(69, '2801201603031273527546.jpg', 'SIM', NULL, NULL, 9),
(70, '2801201603031136693175.jpg', 'SIM', NULL, NULL, 9),
(71, '2801201603031157967284.jpg', 'SIM', NULL, NULL, 9),
(72, '2801201603031354125587.jpg', 'SIM', NULL, NULL, 9),
(73, '2801201603031395310957.jpeg', 'SIM', NULL, NULL, 9),
(74, '2801201603031403609508.jpg', 'SIM', NULL, NULL, 9),
(75, '2801201603031138290044.jpg', 'SIM', NULL, NULL, 10),
(76, '2801201603031358200589.jpg', 'SIM', NULL, NULL, 10),
(77, '2801201603031258047521.jpg', 'SIM', NULL, NULL, 10),
(78, '2801201603031149065834.jpg', 'SIM', NULL, NULL, 10),
(79, '2801201603031115735593.jpg', 'SIM', NULL, NULL, 10);

-- --------------------------------------------------------

--
-- Estrutura para tabela `tb_galerias_produtos`
--

CREATE TABLE IF NOT EXISTS `tb_galerias_produtos` (
  `id_galeriaproduto` int(11) NOT NULL AUTO_INCREMENT,
  `imagem` varchar(255) DEFAULT NULL,
  `ativo` varchar(3) DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) DEFAULT NULL,
  `id_produto` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_galeriaproduto`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=231 ;

--
-- Fazendo dump de dados para tabela `tb_galerias_produtos`
--

INSERT INTO `tb_galerias_produtos` (`id_galeriaproduto`, `imagem`, `ativo`, `ordem`, `url_amigavel`, `id_produto`) VALUES
(115, '3001201601191124391193.jpg', 'SIM', NULL, NULL, 8),
(126, '3001201601191253916754.jpg', 'SIM', NULL, NULL, 8),
(168, '2003201602348014425014.jpg', 'SIM', NULL, NULL, 7),
(172, '2203201604112572013301.jpg', 'SIM', NULL, NULL, 10),
(173, '2203201604135930488030.jpg', 'SIM', NULL, NULL, 10),
(174, '2203201604182077685486.jpg', 'SIM', NULL, NULL, 11),
(175, '2203201604188735433117.jpg', 'SIM', NULL, NULL, 11),
(176, '2203201604234720280596.jpg', 'SIM', NULL, NULL, 12),
(177, '2203201604234716540322.jpg', 'SIM', NULL, NULL, 12),
(178, '2203201604265614290572.jpg', 'SIM', NULL, NULL, 13),
(179, '2203201604266962379577.jpg', 'SIM', NULL, NULL, 13),
(182, '2203201604304529227626.jpg', 'SIM', NULL, NULL, 14),
(183, '2203201604327442631175.jpg', 'SIM', NULL, NULL, 15),
(184, '2203201604332800788705.jpg', 'SIM', NULL, NULL, 15),
(185, '2203201604361766249537.jpg', 'SIM', NULL, NULL, 16),
(186, '2203201604361628849357.jpg', 'SIM', NULL, NULL, 16),
(187, '2203201604388639433466.jpg', 'SIM', NULL, NULL, 17),
(188, '2203201604383433055649.jpg', 'SIM', NULL, NULL, 17),
(189, '2203201604412981405224.jpg', 'SIM', NULL, NULL, 18),
(190, '2203201604417222834796.jpg', 'SIM', NULL, NULL, 18),
(196, '2203201605039892698137.jpg', 'SIM', NULL, NULL, 22),
(197, '2203201605038900757316.jpg', 'SIM', NULL, NULL, 22),
(198, '2203201605037106970676.jpg', 'SIM', NULL, NULL, 22),
(199, '2203201605161743234205.jpg', 'SIM', NULL, NULL, 22),
(200, '2203201605218396552159.jpg', 'SIM', NULL, NULL, 23),
(201, '2203201605228539671181.jpg', 'SIM', NULL, NULL, 23),
(202, '2203201605245540615891.jpg', 'SIM', NULL, NULL, 24),
(203, '2203201605325942788678.jpg', 'SIM', NULL, NULL, 25),
(205, '2203201606203014454431.jpg', 'SIM', NULL, NULL, 27),
(206, '2203201606206851726820.jpg', 'SIM', NULL, NULL, 27),
(207, '2203201606239814488117.jpg', 'SIM', NULL, NULL, 28),
(209, '2503201607265752555926.jpg', 'SIM', NULL, NULL, 7),
(210, '2503201607399233615969.jpg', 'SIM', NULL, NULL, 7),
(211, '2903201611276437665149.jpg', 'SIM', NULL, NULL, 7),
(212, '2903201611276624576958.jpg', 'SIM', NULL, NULL, 7),
(213, '2903201611273196801443.jpg', 'SIM', NULL, NULL, 7),
(214, '2903201611277575654528.jpg', 'SIM', NULL, NULL, 7),
(215, '2903201611272880838670.jpg', 'SIM', NULL, NULL, 7),
(216, '2903201611278390440555.jpg', 'SIM', NULL, NULL, 7),
(218, '3003201604488952948670.jpg', 'SIM', NULL, NULL, 26),
(221, '3003201605054033150341.jpg', 'SIM', NULL, NULL, 19),
(222, '3003201605082048252245.jpg', 'SIM', NULL, NULL, 20),
(223, '3003201605101507269719.jpg', 'SIM', NULL, NULL, 29),
(224, '3003201605111277368685.jpg', 'SIM', NULL, NULL, 30),
(225, '3003201603269590357906.jpg', 'SIM', NULL, NULL, 31),
(226, '3003201603272288259520.jpg', 'SIM', NULL, NULL, 32),
(227, '0804201609306187896711.jpg', 'SIM', NULL, NULL, 8),
(229, '1804201604091555552184.jpg', 'SIM', NULL, NULL, 9),
(230, '1804201604106820692076.jpg', 'SIM', NULL, NULL, 21);

-- --------------------------------------------------------

--
-- Estrutura para tabela `tb_logins`
--

CREATE TABLE IF NOT EXISTS `tb_logins` (
  `idlogin` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(45) DEFAULT NULL,
  `senha` varchar(45) DEFAULT NULL,
  `ativo` varchar(3) DEFAULT 'SIM',
  `id_grupologin` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  PRIMARY KEY (`idlogin`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Fazendo dump de dados para tabela `tb_logins`
--

INSERT INTO `tb_logins` (`idlogin`, `nome`, `senha`, `ativo`, `id_grupologin`, `email`) VALUES
(1, 'HomeWeb', 'e10adc3949ba59abbe56e057f20f883e', 'SIM', 0, 'atendimento.sites@homewebbrasil.com.br'),
(2, 'Aquatendas', '01dd1ae4648731ee67149ce3b3e5316c', 'SIM', 0, 'aquatendas@gmail.com');

-- --------------------------------------------------------

--
-- Estrutura para tabela `tb_logs_logins`
--

CREATE TABLE IF NOT EXISTS `tb_logs_logins` (
  `idloglogin` int(11) NOT NULL AUTO_INCREMENT,
  `operacao` longtext,
  `consulta_sql` longtext,
  `data` date DEFAULT NULL,
  `hora` time DEFAULT NULL,
  `id_login` int(11) NOT NULL,
  PRIMARY KEY (`idloglogin`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2006 ;

--
-- Fazendo dump de dados para tabela `tb_logs_logins`
--

INSERT INTO `tb_logs_logins` (`idloglogin`, `operacao`, `consulta_sql`, `data`, `hora`, `id_login`) VALUES
(1634, 'CADASTRO DO CLIENTE ', '', '2016-01-27', '14:02:53', 7),
(1635, 'CADASTRO DO CLIENTE ', '', '2016-01-27', '14:03:53', 7),
(1636, 'CADASTRO DO CLIENTE ', '', '2016-01-27', '14:04:03', 7),
(1637, 'CADASTRO DO CLIENTE ', '', '2016-01-27', '14:04:13', 7),
(1638, 'CADASTRO DO CLIENTE ', '', '2016-01-27', '14:04:26', 7),
(1639, 'CADASTRO DO CLIENTE ', '', '2016-01-27', '14:04:39', 7),
(1640, 'ALTERAÇÃO DO CLIENTE ', '', '2016-01-27', '14:30:30', 7),
(1641, 'EXCLUSÃO DO LOGIN 56, NOME: , Email: ', 'DELETE FROM tb_categorias_produtos WHERE idcategoriaproduto = ''56''', '2016-01-27', '14:30:55', 7),
(1642, 'CADASTRO DO CLIENTE ', '', '2016-01-27', '14:33:45', 7),
(1643, 'CADASTRO DO CLIENTE ', '', '2016-01-27', '14:33:53', 7),
(1644, 'CADASTRO DO CLIENTE ', '', '2016-01-27', '14:34:00', 7),
(1645, 'CADASTRO DO CLIENTE ', '', '2016-01-27', '14:34:06', 7),
(1646, 'CADASTRO DO CLIENTE ', '', '2016-01-27', '14:34:15', 7),
(1647, 'CADASTRO DO CLIENTE ', '', '2016-01-27', '14:34:25', 7),
(1648, 'CADASTRO DO CLIENTE ', '', '2016-01-27', '14:34:31', 7),
(1649, 'DESATIVOU O LOGIN 63', 'UPDATE tb_categorias_produtos SET ativo = ''NAO'' WHERE idcategoriaproduto = ''63''', '2016-01-27', '14:47:12', 7),
(1650, 'DESATIVOU O LOGIN 64', 'UPDATE tb_categorias_produtos SET ativo = ''NAO'' WHERE idcategoriaproduto = ''64''', '2016-01-27', '14:47:32', 7),
(1651, 'DESATIVOU O LOGIN 69', 'UPDATE tb_categorias_produtos SET ativo = ''NAO'' WHERE idcategoriaproduto = ''69''', '2016-01-27', '14:47:35', 7),
(1652, 'DESATIVOU O LOGIN 68', 'UPDATE tb_categorias_produtos SET ativo = ''NAO'' WHERE idcategoriaproduto = ''68''', '2016-01-27', '14:47:38', 7),
(1653, 'DESATIVOU O LOGIN 67', 'UPDATE tb_categorias_produtos SET ativo = ''NAO'' WHERE idcategoriaproduto = ''67''', '2016-01-27', '14:47:42', 7),
(1654, 'DESATIVOU O LOGIN 66', 'UPDATE tb_categorias_produtos SET ativo = ''NAO'' WHERE idcategoriaproduto = ''66''', '2016-01-27', '14:47:45', 7),
(1655, 'DESATIVOU O LOGIN 65', 'UPDATE tb_categorias_produtos SET ativo = ''NAO'' WHERE idcategoriaproduto = ''65''', '2016-01-27', '14:47:49', 7),
(1656, 'EXCLUSÃO DO LOGIN 63, NOME: , Email: ', 'DELETE FROM tb_categorias_produtos WHERE idcategoriaproduto = ''63''', '2016-01-27', '20:58:13', 7),
(1657, 'EXCLUSÃO DO LOGIN 69, NOME: , Email: ', 'DELETE FROM tb_categorias_produtos WHERE idcategoriaproduto = ''69''', '2016-01-27', '20:58:19', 7),
(1658, 'EXCLUSÃO DO LOGIN 64, NOME: , Email: ', 'DELETE FROM tb_categorias_produtos WHERE idcategoriaproduto = ''64''', '2016-01-27', '20:58:22', 7),
(1659, 'EXCLUSÃO DO LOGIN 68, NOME: , Email: ', 'DELETE FROM tb_categorias_produtos WHERE idcategoriaproduto = ''68''', '2016-01-27', '20:58:25', 7),
(1660, 'EXCLUSÃO DO LOGIN 67, NOME: , Email: ', 'DELETE FROM tb_categorias_produtos WHERE idcategoriaproduto = ''67''', '2016-01-27', '20:58:27', 7),
(1661, 'EXCLUSÃO DO LOGIN 66, NOME: , Email: ', 'DELETE FROM tb_categorias_produtos WHERE idcategoriaproduto = ''66''', '2016-01-27', '20:58:30', 7),
(1662, 'EXCLUSÃO DO LOGIN 65, NOME: , Email: ', 'DELETE FROM tb_categorias_produtos WHERE idcategoriaproduto = ''65''', '2016-01-27', '20:58:32', 7),
(1663, 'CADASTRO DO CLIENTE ', '', '2016-01-27', '21:02:15', 7),
(1664, 'CADASTRO DO CLIENTE ', '', '2016-01-28', '14:04:36', 7),
(1665, 'CADASTRO DO CLIENTE ', '', '2016-01-28', '14:04:54', 7),
(1666, 'CADASTRO DO CLIENTE ', '', '2016-01-28', '14:05:21', 7),
(1667, 'ALTERAÇÃO DO CLIENTE ', '', '2016-01-28', '14:50:50', 7),
(1668, 'ALTERAÇÃO DO CLIENTE ', '', '2016-01-28', '14:51:00', 7),
(1669, 'ALTERAÇÃO DO CLIENTE ', '', '2016-01-28', '14:51:10', 7),
(1670, 'ALTERAÇÃO DO CLIENTE ', '', '2016-01-28', '14:51:30', 7),
(1671, 'ALTERAÇÃO DO CLIENTE ', '', '2016-01-28', '14:51:47', 7),
(1672, 'ALTERAÇÃO DO CLIENTE ', '', '2016-01-28', '14:51:54', 7),
(1673, 'ALTERAÇÃO DO CLIENTE ', '', '2016-01-28', '14:53:22', 7),
(1674, 'ALTERAÇÃO DO CLIENTE ', '', '2016-01-28', '14:53:34', 7),
(1675, 'ALTERAÇÃO DO CLIENTE ', '', '2016-01-28', '14:53:45', 7),
(1676, 'CADASTRO DO CLIENTE ', '', '2016-01-28', '15:00:14', 7),
(1677, 'CADASTRO DO CLIENTE ', '', '2016-01-28', '15:00:28', 7),
(1678, 'CADASTRO DO CLIENTE ', '', '2016-01-28', '15:00:43', 7),
(1679, 'CADASTRO DO CLIENTE ', '', '2016-01-28', '15:00:57', 7),
(1680, 'CADASTRO DO CLIENTE ', '', '2016-01-30', '13:16:28', 7),
(1681, 'CADASTRO DO CLIENTE ', '', '2016-01-30', '13:16:49', 7),
(1682, 'CADASTRO DO CLIENTE ', '', '2016-01-30', '13:17:17', 7),
(1683, 'CADASTRO DO CLIENTE ', '', '2016-01-30', '13:17:41', 7),
(1684, 'CADASTRO DO CLIENTE ', '', '2016-01-30', '13:18:08', 7),
(1685, 'CADASTRO DO CLIENTE ', '', '2016-01-30', '13:18:43', 7),
(1686, 'CADASTRO DO CLIENTE ', '', '2016-01-30', '13:19:09', 7),
(1687, 'CADASTRO DO CLIENTE ', '', '2016-01-30', '13:47:15', 7),
(1688, 'CADASTRO DO CLIENTE ', '', '2016-01-30', '13:47:37', 7),
(1689, 'CADASTRO DO CLIENTE ', '', '2016-01-30', '13:47:59', 7),
(1690, 'CADASTRO DO CLIENTE ', '', '2016-01-30', '13:48:21', 7),
(1691, 'ALTERAÇÃO DO CLIENTE ', '', '2016-01-30', '13:49:49', 7),
(1692, 'ALTERAÇÃO DO CLIENTE ', '', '2016-01-30', '13:49:59', 7),
(1693, 'ALTERAÇÃO DO CLIENTE ', '', '2016-01-30', '13:50:07', 7),
(1694, 'ALTERAÇÃO DO CLIENTE ', '', '2016-01-30', '13:50:22', 7),
(1695, 'EXCLUSÃO DO LOGIN 58, NOME: , Email: ', 'DELETE FROM tb_categorias_produtos WHERE idcategoriaproduto = ''58''', '2016-02-10', '14:23:32', 1),
(1696, 'CADASTRO DO CLIENTE ', '', '2016-02-10', '19:21:09', 1),
(1697, 'CADASTRO DO CLIENTE ', '', '2016-02-10', '19:21:49', 1),
(1698, 'CADASTRO DO CLIENTE ', '', '2016-02-10', '19:22:04', 1),
(1699, 'CADASTRO DO CLIENTE ', '', '2016-02-10', '19:22:13', 1),
(1700, 'ALTERAÇÃO DO CLIENTE ', '', '2016-02-10', '19:41:52', 1),
(1701, 'ALTERAÇÃO DO CLIENTE ', '', '2016-02-10', '19:42:14', 1),
(1702, 'ALTERAÇÃO DO CLIENTE ', '', '2016-02-10', '19:42:25', 1),
(1703, 'ALTERAÇÃO DO CLIENTE ', '', '2016-02-10', '19:42:36', 1),
(1704, 'ALTERAÇÃO DO CLIENTE ', '', '2016-02-10', '19:42:47', 1),
(1705, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-15', '22:10:57', 1),
(1706, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-15', '22:14:20', 1),
(1707, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-15', '22:15:47', 1),
(1708, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-15', '22:16:09', 1),
(1709, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-15', '22:16:23', 1),
(1710, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-15', '22:16:31', 1),
(1711, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-15', '22:21:07', 1),
(1712, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-20', '14:20:56', 1),
(1713, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-20', '14:31:39', 1),
(1714, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-22', '12:43:06', 1),
(1715, 'DESATIVOU O LOGIN 60', 'UPDATE tb_categorias_produtos SET ativo = ''NAO'' WHERE idcategoriaproduto = ''60''', '2016-03-22', '12:44:51', 1),
(1716, 'CADASTRO DO CLIENTE ', '', '2016-03-22', '12:52:14', 1),
(1717, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-22', '12:53:07', 1),
(1718, 'DESATIVOU O LOGIN 61', 'UPDATE tb_categorias_produtos SET ativo = ''NAO'' WHERE idcategoriaproduto = ''61''', '2016-03-22', '12:53:59', 1),
(1719, 'EXCLUSÃO DO LOGIN 4, NOME: , Email: ', 'DELETE FROM tb_depoimentos WHERE iddepoimento = ''4''', '2016-03-22', '12:55:30', 1),
(1720, 'EXCLUSÃO DO LOGIN 5, NOME: , Email: ', 'DELETE FROM tb_depoimentos WHERE iddepoimento = ''5''', '2016-03-22', '12:55:33', 1),
(1721, 'EXCLUSÃO DO LOGIN 6, NOME: , Email: ', 'DELETE FROM tb_depoimentos WHERE iddepoimento = ''6''', '2016-03-22', '12:55:36', 1),
(1722, 'CADASTRO DO CLIENTE ', '', '2016-03-22', '12:56:10', 1),
(1723, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-22', '13:09:54', 1),
(1724, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-22', '13:21:38', 1),
(1725, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-22', '13:22:53', 1),
(1726, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-22', '13:23:46', 1),
(1727, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-22', '13:24:28', 1),
(1728, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-22', '13:25:03', 1),
(1729, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-22', '13:25:31', 1),
(1730, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-22', '13:26:00', 1),
(1731, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-22', '13:32:11', 1),
(1732, 'EXCLUSÃO DO LOGIN 6, NOME: Andr?, Email: andre@masmidia.com.br', 'DELETE FROM tb_comentarios_dicas WHERE idcomentariodica = ''6''', '2016-03-22', '13:33:09', 1),
(1733, 'EXCLUSÃO DO LOGIN 7, NOME: Joana, Email: joana@masmidia.com.br', 'DELETE FROM tb_comentarios_dicas WHERE idcomentariodica = ''7''', '2016-03-22', '13:33:12', 1),
(1734, 'CADASTRO DO CLIENTE ', '', '2016-03-22', '16:03:30', 1),
(1735, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-22', '16:04:24', 1),
(1736, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-22', '16:05:07', 1),
(1737, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-22', '16:05:53', 1),
(1738, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-22', '16:09:03', 1),
(1739, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-22', '16:10:52', 1),
(1740, 'ATIVOU O LOGIN 61', 'UPDATE tb_categorias_produtos SET ativo = ''SIM'' WHERE idcategoriaproduto = ''61''', '2016-03-22', '16:14:23', 1),
(1741, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-22', '16:18:21', 1),
(1742, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-22', '16:19:29', 1),
(1743, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-22', '16:20:07', 1),
(1744, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-22', '16:23:29', 1),
(1745, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-22', '16:23:40', 1),
(1746, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-22', '16:25:41', 1),
(1747, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-22', '16:28:18', 1),
(1748, 'CADASTRO DO CLIENTE ', '', '2016-03-22', '16:32:36', 1),
(1749, 'CADASTRO DO CLIENTE ', '', '2016-03-22', '16:35:56', 1),
(1750, 'CADASTRO DO CLIENTE ', '', '2016-03-22', '16:37:36', 1),
(1751, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-22', '16:38:42', 1),
(1752, 'CADASTRO DO CLIENTE ', '', '2016-03-22', '16:41:04', 1),
(1753, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-22', '16:41:32', 1),
(1754, 'CADASTRO DO CLIENTE ', '', '2016-03-22', '16:44:39', 1),
(1755, 'CADASTRO DO CLIENTE ', '', '2016-03-22', '16:49:39', 1),
(1756, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-22', '16:50:52', 1),
(1757, 'CADASTRO DO CLIENTE ', '', '2016-03-22', '16:52:51', 1),
(1758, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-22', '16:57:04', 1),
(1759, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-22', '16:57:53', 1),
(1760, 'ATIVOU O LOGIN 60', 'UPDATE tb_categorias_produtos SET ativo = ''SIM'' WHERE idcategoriaproduto = ''60''', '2016-03-22', '16:58:00', 1),
(1761, 'CADASTRO DO CLIENTE ', '', '2016-03-22', '16:59:25', 1),
(1762, 'CADASTRO DO CLIENTE ', '', '2016-03-22', '17:20:22', 1),
(1763, 'CADASTRO DO CLIENTE ', '', '2016-03-22', '17:21:14', 1),
(1764, 'CADASTRO DO CLIENTE ', '', '2016-03-22', '17:24:14', 1),
(1765, 'CADASTRO DO CLIENTE ', '', '2016-03-22', '17:29:50', 1),
(1766, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-22', '17:39:42', 1),
(1767, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-22', '17:40:44', 1),
(1768, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-22', '17:41:16', 1),
(1769, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-22', '17:43:50', 1),
(1770, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-22', '17:44:11', 1),
(1771, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-22', '17:49:56', 1),
(1772, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-22', '17:51:19', 1),
(1773, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-22', '17:51:40', 1),
(1774, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-22', '17:52:13', 1),
(1775, 'DESATIVOU O LOGIN 40', 'UPDATE tb_dicas SET ativo = ''NAO'' WHERE iddica = ''40''', '2016-03-22', '17:52:28', 1),
(1776, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-22', '18:15:29', 1),
(1777, 'CADASTRO DO CLIENTE ', '', '2016-03-22', '18:17:13', 1),
(1778, 'CADASTRO DO CLIENTE ', '', '2016-03-22', '18:20:25', 1),
(1779, 'CADASTRO DO CLIENTE ', '', '2016-03-22', '18:21:55', 1),
(1780, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-22', '20:00:20', 1),
(1781, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-22', '20:37:03', 1),
(1782, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-22', '20:37:18', 1),
(1783, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-22', '20:37:27', 1),
(1784, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-22', '20:47:14', 1),
(1785, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-22', '22:36:39', 1),
(1786, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-22', '22:37:04', 1),
(1787, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-22', '22:37:14', 1),
(1788, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-22', '22:37:42', 1),
(1789, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-22', '22:37:56', 1),
(1790, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-22', '22:38:59', 1),
(1791, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-22', '22:41:31', 1),
(1792, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-22', '22:41:58', 1),
(1793, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-22', '22:47:19', 1),
(1794, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-22', '22:47:49', 1),
(1795, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-22', '22:50:09', 1),
(1796, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-22', '22:50:46', 1),
(1797, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-22', '22:53:04', 1),
(1798, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-22', '22:56:28', 1),
(1799, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-22', '23:01:14', 1),
(1800, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-22', '23:08:06', 1),
(1801, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-22', '23:10:47', 1),
(1802, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-22', '23:11:14', 1),
(1803, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-22', '23:11:24', 1),
(1804, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-22', '23:14:17', 1),
(1805, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-22', '23:19:02', 1),
(1806, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-22', '23:22:07', 1),
(1807, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-22', '23:55:37', 1),
(1808, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-22', '23:58:06', 1),
(1809, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-22', '23:58:16', 1),
(1810, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-23', '00:00:32', 1),
(1811, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-23', '00:02:01', 1),
(1812, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-23', '00:03:52', 1),
(1813, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-23', '00:05:35', 1),
(1814, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-23', '00:05:54', 1),
(1815, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-23', '00:07:48', 1),
(1816, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-23', '00:08:41', 1),
(1817, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-23', '00:11:07', 1),
(1818, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-23', '00:14:08', 1),
(1819, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-23', '00:17:55', 1),
(1820, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-23', '00:25:01', 1),
(1821, 'EXCLUSÃO DO LOGIN 2, NOME: , Email: ', 'DELETE FROM tb_banners WHERE idbanner = ''2''', '2016-03-23', '12:01:40', 1),
(1822, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-23', '12:37:33', 1),
(1823, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-23', '12:43:38', 1),
(1824, 'DESATIVOU O LOGIN 39', 'UPDATE tb_servicos SET ativo = ''NAO'' WHERE idservico = ''39''', '2016-03-23', '12:43:43', 1),
(1825, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-23', '13:42:07', 1),
(1826, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-24', '15:41:28', 1),
(1827, 'DESATIVOU O LOGIN 76', 'UPDATE tb_piscinas_vinil SET ativo = ''NAO'' WHERE idpiscinavinil = ''76''', '2016-03-24', '15:43:14', 1),
(1828, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-25', '19:17:57', 1),
(1829, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-25', '19:22:54', 1),
(1830, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-25', '21:39:52', 1),
(1831, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-25', '21:40:27', 1),
(1832, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-25', '21:41:03', 1),
(1833, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-25', '21:56:37', 1),
(1834, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-26', '00:13:31', 1),
(1835, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-28', '15:46:15', 1),
(1836, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-29', '23:29:09', 1),
(1837, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-29', '23:33:43', 1),
(1838, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-29', '23:35:10', 1),
(1839, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-30', '04:47:55', 1),
(1840, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-30', '04:54:08', 1),
(1841, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-30', '04:59:25', 1),
(1842, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-30', '05:04:06', 1),
(1843, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-30', '05:06:04', 1),
(1844, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-30', '05:08:23', 1),
(1845, 'CADASTRO DO CLIENTE ', '', '2016-03-30', '05:10:10', 1),
(1846, 'CADASTRO DO CLIENTE ', '', '2016-03-30', '05:11:34', 1),
(1847, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-30', '05:13:51', 1),
(1848, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-30', '05:14:39', 1),
(1849, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-30', '11:10:25', 1),
(1850, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-30', '11:11:22', 1),
(1851, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-30', '11:13:20', 1),
(1852, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-30', '11:13:42', 1),
(1853, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-30', '11:14:13', 1),
(1854, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-30', '11:18:56', 1),
(1855, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-30', '11:22:14', 1),
(1856, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-30', '11:23:40', 1),
(1857, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-30', '11:24:43', 1),
(1858, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-30', '11:25:23', 1),
(1859, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-30', '11:26:58', 1),
(1860, 'ATIVOU O LOGIN 76', 'UPDATE tb_piscinas_vinil SET ativo = ''SIM'' WHERE idpiscinavinil = ''76''', '2016-03-30', '11:29:14', 1),
(1861, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-30', '11:30:35', 1),
(1862, 'CADASTRO DO CLIENTE ', '', '2016-03-30', '11:35:32', 1),
(1863, 'DESATIVOU O LOGIN 77', 'UPDATE tb_piscinas_vinil SET ativo = ''NAO'' WHERE idpiscinavinil = ''77''', '2016-03-30', '11:37:39', 1),
(1864, 'ATIVOU O LOGIN 77', 'UPDATE tb_piscinas_vinil SET ativo = ''SIM'' WHERE idpiscinavinil = ''77''', '2016-03-30', '11:38:12', 1),
(1865, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-30', '11:46:08', 1),
(1866, 'CADASTRO DO CLIENTE ', '', '2016-03-30', '11:50:07', 1),
(1867, 'CADASTRO DO CLIENTE ', '', '2016-03-30', '11:53:30', 1),
(1868, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-30', '11:59:54', 1),
(1869, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-30', '12:00:33', 1),
(1870, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-30', '12:04:23', 1),
(1871, 'DESATIVOU O LOGIN 77', 'UPDATE tb_piscinas_vinil SET ativo = ''NAO'' WHERE idpiscinavinil = ''77''', '2016-03-30', '12:05:21', 1),
(1872, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-30', '12:07:04', 1),
(1873, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-30', '12:07:25', 1),
(1874, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-30', '12:07:51', 1),
(1875, 'ATIVOU O LOGIN 77', 'UPDATE tb_piscinas_vinil SET ativo = ''SIM'' WHERE idpiscinavinil = ''77''', '2016-03-30', '12:08:43', 1),
(1876, 'CADASTRO DO CLIENTE ', '', '2016-03-30', '15:21:14', 1),
(1877, 'CADASTRO DO CLIENTE ', '', '2016-03-30', '15:25:16', 1),
(1878, 'CADASTRO DO CLIENTE ', '', '2016-03-30', '15:27:25', 1),
(1879, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-30', '15:29:20', 1),
(1880, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-30', '15:29:48', 1),
(1881, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-30', '15:30:13', 1),
(1882, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-06', '12:00:16', 1),
(1883, 'DESATIVOU O LOGIN 74', 'UPDATE tb_piscinas_vinil SET ativo = ''NAO'' WHERE idpiscinavinil = ''74''', '2016-04-06', '12:00:52', 1),
(1884, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-06', '12:40:46', 1),
(1885, 'ATIVOU O LOGIN 74', 'UPDATE tb_piscinas_vinil SET ativo = ''SIM'' WHERE idpiscinavinil = ''74''', '2016-04-06', '12:41:16', 1),
(1886, 'ATIVOU O LOGIN 74', 'UPDATE tb_piscinas_vinil SET ativo = ''SIM'' WHERE idpiscinavinil = ''74''', '2016-04-06', '12:42:25', 1),
(1887, 'DESATIVOU O LOGIN 74', 'UPDATE tb_piscinas_vinil SET ativo = ''NAO'' WHERE idpiscinavinil = ''74''', '2016-04-06', '12:42:51', 1),
(1888, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-06', '13:18:20', 1),
(1889, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-06', '13:28:39', 1),
(1890, 'ATIVOU O LOGIN 74', 'UPDATE tb_piscinas_vinil SET ativo = ''SIM'' WHERE idpiscinavinil = ''74''', '2016-04-06', '13:30:14', 1),
(1891, 'DESATIVOU O LOGIN 79', 'UPDATE tb_piscinas_vinil SET ativo = ''NAO'' WHERE idpiscinavinil = ''79''', '2016-04-06', '13:30:32', 1),
(1892, 'DESATIVOU O LOGIN 73', 'UPDATE tb_piscinas_vinil SET ativo = ''NAO'' WHERE idpiscinavinil = ''73''', '2016-04-06', '13:30:43', 1),
(1893, 'DESATIVOU O LOGIN 75', 'UPDATE tb_piscinas_vinil SET ativo = ''NAO'' WHERE idpiscinavinil = ''75''', '2016-04-06', '13:30:54', 1),
(1894, 'DESATIVOU O LOGIN 77', 'UPDATE tb_piscinas_vinil SET ativo = ''NAO'' WHERE idpiscinavinil = ''77''', '2016-04-06', '13:31:09', 1),
(1895, 'DESATIVOU O LOGIN 78', 'UPDATE tb_piscinas_vinil SET ativo = ''NAO'' WHERE idpiscinavinil = ''78''', '2016-04-06', '13:31:22', 1),
(1896, 'DESATIVOU O LOGIN 74', 'UPDATE tb_piscinas_vinil SET ativo = ''NAO'' WHERE idpiscinavinil = ''74''', '2016-04-06', '13:33:52', 1),
(1897, 'DESATIVOU O LOGIN 76', 'UPDATE tb_piscinas_vinil SET ativo = ''NAO'' WHERE idpiscinavinil = ''76''', '2016-04-06', '13:33:59', 1),
(1898, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-06', '14:04:24', 1),
(1899, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-06', '14:55:37', 1),
(1900, 'ATIVOU O LOGIN 74', 'UPDATE tb_piscinas_vinil SET ativo = ''SIM'' WHERE idpiscinavinil = ''74''', '2016-04-06', '15:07:37', 1),
(1901, 'DESATIVOU O LOGIN 74', 'UPDATE tb_piscinas_vinil SET ativo = ''NAO'' WHERE idpiscinavinil = ''74''', '2016-04-06', '15:09:15', 1),
(1902, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-06', '20:16:25', 1),
(1903, 'ATIVOU O LOGIN 76', 'UPDATE tb_piscinas_vinil SET ativo = ''SIM'' WHERE idpiscinavinil = ''76''', '2016-04-06', '20:20:56', 1),
(1904, 'ATIVOU O LOGIN 74', 'UPDATE tb_piscinas_vinil SET ativo = ''SIM'' WHERE idpiscinavinil = ''74''', '2016-04-06', '20:21:08', 1),
(1905, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-07', '00:23:55', 1),
(1906, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-07', '00:45:51', 1),
(1907, 'ATIVOU O LOGIN 73', 'UPDATE tb_piscinas_vinil SET ativo = ''SIM'' WHERE idpiscinavinil = ''73''', '2016-04-07', '03:30:44', 1),
(1908, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-07', '03:33:23', 1),
(1909, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-07', '03:53:09', 1),
(1910, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-07', '03:54:20', 1),
(1911, 'DESATIVOU O LOGIN 73', 'UPDATE tb_piscinas_vinil SET ativo = ''NAO'' WHERE idpiscinavinil = ''73''', '2016-04-07', '09:09:39', 1),
(1912, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-07', '12:34:17', 1),
(1913, 'ATIVOU O LOGIN 73', 'UPDATE tb_piscinas_vinil SET ativo = ''SIM'' WHERE idpiscinavinil = ''73''', '2016-04-07', '12:34:28', 1),
(1914, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-07', '12:52:11', 1),
(1915, 'ATIVOU O LOGIN 79', 'UPDATE tb_piscinas_vinil SET ativo = ''SIM'' WHERE idpiscinavinil = ''79''', '2016-04-07', '13:29:04', 1),
(1916, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-07', '13:39:53', 1),
(1917, 'ATIVOU O LOGIN 78', 'UPDATE tb_piscinas_vinil SET ativo = ''SIM'' WHERE idpiscinavinil = ''78''', '2016-04-07', '13:40:08', 1),
(1918, 'EXCLUSÃO DO LOGIN 75, NOME: , Email: ', 'DELETE FROM tb_piscinas_vinil WHERE idpiscinavinil = ''75''', '2016-04-07', '20:44:53', 1),
(1919, 'EXCLUSÃO DO LOGIN 77, NOME: , Email: ', 'DELETE FROM tb_piscinas_vinil WHERE idpiscinavinil = ''77''', '2016-04-07', '20:44:58', 1),
(1920, 'CADASTRO DO CLIENTE ', '', '2016-04-07', '20:57:46', 1),
(1921, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-08', '21:29:58', 1),
(1922, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-08', '21:32:41', 1),
(1923, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-08', '23:11:24', 1),
(1924, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-08', '23:11:50', 1),
(1925, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-08', '23:25:04', 1),
(1926, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-09', '00:04:26', 1),
(1927, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-09', '03:55:27', 1),
(1928, 'CADASTRO DO CLIENTE ', '', '2016-04-09', '04:00:38', 1),
(1929, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-09', '04:01:23', 1),
(1930, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-09', '04:04:08', 1),
(1931, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-09', '04:05:00', 1),
(1932, 'CADASTRO DO CLIENTE ', '', '2016-04-09', '04:22:02', 1),
(1933, 'CADASTRO DO CLIENTE ', '', '2016-04-09', '04:29:08', 1),
(1934, 'CADASTRO DO CLIENTE ', '', '2016-04-09', '04:34:21', 1),
(1935, 'CADASTRO DO CLIENTE ', '', '2016-04-09', '04:47:30', 1),
(1936, 'CADASTRO DO CLIENTE ', '', '2016-04-09', '04:52:51', 1),
(1937, 'CADASTRO DO CLIENTE ', '', '2016-04-09', '04:58:53', 1),
(1938, 'CADASTRO DO CLIENTE ', '', '2016-04-09', '08:40:39', 1),
(1939, 'CADASTRO DO CLIENTE ', '', '2016-04-09', '09:10:12', 1),
(1940, 'CADASTRO DO CLIENTE ', '', '2016-04-09', '09:16:02', 1),
(1941, 'CADASTRO DO CLIENTE ', '', '2016-04-09', '09:29:17', 1),
(1942, 'CADASTRO DO CLIENTE ', '', '2016-04-09', '09:36:36', 1),
(1943, 'CADASTRO DO CLIENTE ', '', '2016-04-09', '10:21:28', 1),
(1944, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-13', '10:05:50', 1),
(1945, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-13', '13:49:27', 1),
(1946, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-13', '15:02:12', 1),
(1947, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-13', '15:03:13', 1),
(1948, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-13', '15:05:16', 1),
(1949, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-13', '15:07:02', 1),
(1950, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-13', '15:48:15', 1),
(1951, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-16', '01:26:04', 1),
(1952, 'DESATIVOU O LOGIN 83', 'UPDATE tb_piscinas_vinil SET ativo = ''NAO'' WHERE idpiscinavinil = ''83''', '2016-04-16', '01:29:50', 1),
(1953, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-16', '01:41:14', 1),
(1954, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-16', '01:43:41', 1),
(1955, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-16', '01:45:55', 1),
(1956, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-16', '01:49:22', 1),
(1957, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-16', '07:23:14', 1),
(1958, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-16', '16:50:51', 1),
(1959, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-16', '17:50:43', 1),
(1960, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-16', '18:01:00', 1),
(1961, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-16', '18:11:53', 1),
(1962, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-16', '18:15:48', 1),
(1963, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-16', '18:23:26', 1),
(1964, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-16', '18:26:46', 1),
(1965, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-16', '18:27:53', 1),
(1966, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-16', '18:31:58', 1),
(1967, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-16', '18:34:38', 1),
(1968, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-16', '18:39:05', 1),
(1969, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-16', '18:41:25', 1),
(1970, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-16', '18:45:16', 1),
(1971, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-16', '18:48:39', 1),
(1972, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-16', '18:56:40', 1),
(1973, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-16', '19:03:11', 1),
(1974, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-16', '19:07:44', 1),
(1975, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-16', '19:09:57', 1),
(1976, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-16', '19:16:57', 1),
(1977, 'CADASTRO DO CLIENTE ', '', '2016-04-16', '19:25:02', 1),
(1978, 'EXCLUSÃO DO LOGIN 94, NOME: , Email: ', 'DELETE FROM tb_piscinas_vinil WHERE idpiscinavinil = ''94''', '2016-04-16', '19:29:24', 1),
(1979, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-16', '19:37:05', 1),
(1980, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-16', '19:37:56', 1),
(1981, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-16', '19:42:41', 1),
(1982, 'EXCLUSÃO DO LOGIN 73, NOME: , Email: ', 'DELETE FROM tb_piscinas_vinil WHERE idpiscinavinil = ''73''', '2016-04-16', '19:45:37', 1),
(1983, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-17', '13:38:29', 1),
(1984, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-17', '13:46:45', 1),
(1985, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-18', '03:39:26', 1),
(1986, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-18', '03:41:04', 1),
(1987, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-18', '03:42:14', 1),
(1988, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-18', '03:43:37', 1),
(1989, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-18', '03:44:48', 1),
(1990, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-18', '03:45:48', 1),
(1991, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-18', '03:48:09', 1),
(1992, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-18', '03:49:24', 1),
(1993, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-18', '03:53:32', 1),
(1994, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-18', '03:55:02', 1),
(1995, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-18', '03:56:22', 1),
(1996, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-18', '03:58:07', 1),
(1997, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-18', '03:59:19', 1),
(1998, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-18', '04:00:44', 1),
(1999, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-18', '04:01:43', 1),
(2000, 'EXCLUSÃO DO LOGIN 83, NOME: , Email: ', 'DELETE FROM tb_piscinas_vinil WHERE idpiscinavinil = ''83''', '2016-04-18', '04:02:25', 1),
(2001, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-18', '04:05:25', 1),
(2002, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-18', '04:07:10', 1),
(2003, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-18', '04:08:43', 1),
(2004, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-19', '12:22:24', 1),
(2005, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-04', '16:33:57', 1);

-- --------------------------------------------------------

--
-- Estrutura para tabela `tb_piscinas_vinil`
--

CREATE TABLE IF NOT EXISTS `tb_piscinas_vinil` (
  `idpiscinavinil` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `titulo` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `imagem` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `preco` double NOT NULL,
  `descricao` longtext COLLATE utf8_unicode_ci NOT NULL,
  `id_categoriaproduto` int(10) unsigned NOT NULL,
  `ativo` varchar(3) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'SIM',
  `ordem` int(10) unsigned NOT NULL,
  `title_google` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `keywords_google` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description_google` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `codigo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `marca` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `imagem1` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`idpiscinavinil`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=94 ;

--
-- Fazendo dump de dados para tabela `tb_piscinas_vinil`
--

INSERT INTO `tb_piscinas_vinil` (`idpiscinavinil`, `titulo`, `imagem`, `preco`, `descricao`, `id_categoriaproduto`, `ativo`, `ordem`, `title_google`, `keywords_google`, `description_google`, `codigo`, `marca`, `url_amigavel`, `imagem1`) VALUES
(74, 'Golfinhos 3D', '1704201601466820906210..jpg', 0, '', 0, 'SIM', 1, '', '', '', NULL, NULL, 'golfinhos-3d', '0804201611117412761491..jpg'),
(76, 'Pastilha Master', '1804201603398943817160..jpg', 0, '', 0, 'SIM', 2, '', '', '', NULL, NULL, 'pastilha-master', '0804201611119179353771..jpg'),
(78, 'Miami Marmore', '1804201603444499869536..jpg', 0, '', 0, 'SIM', 3, '', '', '', NULL, NULL, 'miami-marmore', '1604201607429408878733..jpg'),
(79, 'Miami', '1804201603431122912896..jpg', 0, '', 0, 'SIM', 4, '', '', '', NULL, NULL, 'miami', '0904201604042919743694..jpg'),
(80, 'Golfinhos STD', '1804201603426624003615..jpg', 0, '', 0, 'SIM', 6, '', '', '', NULL, NULL, 'golfinhos-std', '0904201604016113513902..jpg'),
(81, 'Golfinhos com Marmore', '1804201603418107329073..jpg', 0, '', 0, 'SIM', 7, '', '', '', NULL, NULL, 'golfinhos-com-marmore', '0904201604006830588732..jpg'),
(82, 'Miami Standard', '1804201603456687955512..jpg', 0, '', 0, 'SIM', 5, '', '', '', NULL, NULL, 'miami-standard', '1604201601435270912945..jpg'),
(84, 'Pastilha de Vidro Fundo Azul', '1804201603485308017714..jpg', 0, '', 0, 'SIM', 8, '', '', '', NULL, NULL, 'pastilha-de-vidro-fundo-azul', '0904201604347565614303..jpg'),
(85, 'Pastilha de Vidro Fundo Azul Marmore', '1804201603493064626786..jpg', 0, '', 0, 'SIM', 9, '', '', '', NULL, NULL, 'pastilha-de-vidro-fundo-azul-marmore', '0904201604472349503549..jpg'),
(86, 'Pastilha de Vidro Fundo Azul Standard', '1704201601381823722028..jpg', 0, '', 0, 'SIM', 10, '', '', '', NULL, NULL, 'pastilha-de-vidro-fundo-azul-standard', '0904201604525421056896..jpg'),
(87, 'Pastilha de Vidro Fundo Branco', '1804201603532137741334..jpg', 0, '', 0, 'SIM', 11, '', '', '', NULL, NULL, 'pastilha-de-vidro-fundo-branco', '0904201604585435345354..jpg'),
(88, 'Pastilha de Vidro Fundo Branco Marmore', '1804201603557724002653..jpg', 0, '', 0, 'SIM', 12, '', '', '', NULL, NULL, 'pastilha-de-vidro-fundo-branco-marmore', '0904201608409622572892..jpg'),
(89, 'Pastilha de Vidro Fundo Branco Standard', '1804201603568959746580..jpg', 0, '', 0, 'SIM', 13, '', '', '', NULL, NULL, 'pastilha-de-vidro-fundo-branco-standard', '0904201609102169612877..jpg'),
(90, 'Pool Master', '1804201603585084354248..jpg', 0, '', 0, 'SIM', 14, '', '', '', NULL, NULL, 'pool-master', '1604201601458211963040..jpg'),
(91, 'Pool Master Barrado', '1804201603595429877299..jpg', 0, '', 0, 'SIM', 15, '', '', '', NULL, NULL, 'pool-master-barrado', '0904201609295961992144..jpg'),
(92, 'Pool Master Marmore', '1804201604008377601102..jpg', 0, '', 0, 'SIM', 16, '', '', '', NULL, NULL, 'pool-master-marmore', '0904201609366938419900..jpg'),
(93, 'Pool Master Standard', '1804201604013774186898..jpg', 0, '', 0, 'SIM', 17, '', '', '', NULL, NULL, 'pool-master-standard', '0904201610212935759213..jpg');

-- --------------------------------------------------------

--
-- Estrutura para tabela `tb_portifolios`
--

CREATE TABLE IF NOT EXISTS `tb_portifolios` (
  `idportifolio` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(255) DEFAULT NULL,
  `imagem` varchar(255) DEFAULT NULL,
  `descricao` longtext,
  `title_google` varchar(255) DEFAULT NULL,
  `keywords_google` longtext,
  `description_google` longtext,
  `ativo` varchar(3) DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`idportifolio`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Fazendo dump de dados para tabela `tb_portifolios`
--

INSERT INTO `tb_portifolios` (`idportifolio`, `titulo`, `imagem`, `descricao`, `title_google`, `keywords_google`, `description_google`, `ativo`, `ordem`, `url_amigavel`) VALUES
(7, 'Portfólio 1', '2801201603001231950844..jpg', '<p>\r\n	Descri&ccedil;&atilde;o aqui&nbsp;Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>', NULL, NULL, NULL, 'SIM', NULL, 'portfolio-1'),
(8, 'Portfólio 2', '2801201603001392287771..jpg', '<p>\r\n	Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>', NULL, NULL, NULL, 'SIM', NULL, 'portfolio-2'),
(9, 'Portfólio 3', '2801201603001158711652..jpg', '<p>\r\n	3&nbsp;Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>', NULL, NULL, NULL, 'SIM', NULL, 'portfolio-3'),
(10, 'Portfólio 5', '2801201603001168013040..jpg', '<p>\r\n	5&nbsp;Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>', NULL, NULL, NULL, 'SIM', NULL, 'portfolio-5');

-- --------------------------------------------------------

--
-- Estrutura para tabela `tb_produtos`
--

CREATE TABLE IF NOT EXISTS `tb_produtos` (
  `idproduto` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `descricao` longtext COLLATE utf8_unicode_ci NOT NULL,
  `title_google` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `keywords_google` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description_google` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ativo` varchar(3) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'SIM',
  `ordem` int(10) unsigned NOT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `id_categoriaproduto` int(10) unsigned NOT NULL,
  `marca` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `apresentacao` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `avaliacao` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`idproduto`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=33 ;

--
-- Fazendo dump de dados para tabela `tb_produtos`
--

INSERT INTO `tb_produtos` (`idproduto`, `titulo`, `imagem`, `descricao`, `title_google`, `keywords_google`, `description_google`, `ativo`, `ordem`, `url_amigavel`, `id_categoriaproduto`, `marca`, `apresentacao`, `avaliacao`) VALUES
(7, 'Piscinas de Vinil', '0804201609327890038005..jpg', '<p>\r\n	H&aacute; muitas vantagens para a instala&ccedil;&atilde;o de uma piscina de vinil. Piscinas de vinil s&atilde;o muito flex&iacute;veis e n&atilde;o pode rachar como uma piscina de concreto. Voc&ecirc; nunca ter&aacute; que redesenhar uma piscina de vinil. O revestimento de vinil faz com que todas as superf&iacute;cies de sua piscina se tornem suaves. Por causa da flexibilidade com as paredes, uma piscina de vinil oferece ilimitadas formas de design. Uma piscina de vinil tamb&eacute;m &eacute; menos caro do que uma piscina de fibra de vidro ou piscina de concreto.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Os revestimentos de vinil n&atilde;o s&atilde;o limitados em cores e padr&otilde;es como costumavam ser. Ao longo do tempo, mudando o revestimento em sua piscina, far&aacute; melhorar dramaticamente o efeito visual em seu ambiente. O custo do revestimento depender&aacute; da sua forma, tamanho, espessura, e a escolha padr&atilde;o.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Como em todas as piscinas em ambientes cobertos, uma variedade de recursos podem ser adicionados &agrave; sua op&ccedil;&atilde;o por vinil, incluindo spas, cachoeiras, decks, jatos laminares, efeitos de ilumina&ccedil;&atilde;o e aquecedores de piscina.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Aquatendas Piscinas vem h&aacute; mais de nove anos atuando na constru&ccedil;&atilde;o e instala&ccedil;&atilde;o de piscina de vinil de grande, m&eacute;dio ou pequeno porte, formado por profissionais com grande experi&ecirc;ncia. Possibilitamos o menor pre&ccedil;o e a melhor qualidade de nossos produtos, oferecemos parceria e solu&ccedil;&otilde;es para os projetos dos clientes.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Consulte a Aquatendas Piscinas, traga seu projeto e solicite or&ccedil;amento da constru&ccedil;&atilde;o de piscinas e instala&ccedil;&atilde;o de revestimentos de vinil dispon&iacute;veis para sua obra.</p>', '', '', '', 'SIM', 0, 'piscinas-de-vinil', 57, 'SANSUY E CIPAVINIL', NULL, NULL),
(8, 'Piscinas de Azulejo', '0804201609292638105038..jpg', '<p>\r\n	Aquatendas Piscinas vem h&aacute; mais de nove anos atuando na constru&ccedil;&atilde;o e instala&ccedil;&atilde;o de piscina de azulejo de grande, m&eacute;dio ou pequeno porte, formado por profissionais com grande experi&ecirc;ncia. Possibilitamos o menor pre&ccedil;o e agilidade na execu&ccedil;&atilde;o de projetos dos clientes.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Consulte a Aquatendas Piscinas, traga seu projeto e solicite or&ccedil;amento para a constru&ccedil;&atilde;o de sua piscina em azulejo de acordo com o seu projeto.</p>', '', '', '', 'SIM', 0, 'piscinas-de-azulejo', 57, 'OUTRAS', NULL, NULL),
(9, 'Capas de Proteção', '1804201604054644347321..jpg', '<div>\r\n	A capa de prote&ccedil;&atilde;o para piscina &eacute; instalada tanto para proteger contra quedas de pessoas ou animais como tamb&eacute;m para mante-l&aacute; limpa. Esta rede &nbsp;com o tratamento UV (Raios Ultra violeta), onde dar&aacute; mais durabilidade.</div>\r\n<div>\r\n	Consulte a Aquatendas Piscinas e conhe&ccedil;a os modelos dispon&iacute;veis.</div>\r\n<p>\r\n	&bull; 30% mais leve;</p>\r\n<p>\r\n	&bull; 87% mais resistente;</p>\r\n<p>\r\n	&bull; F&aacute;cil instala&ccedil;&atilde;o;</p>\r\n<p>\r\n	&bull; Secagem r&aacute;pida;</p>\r\n<p>\r\n	&bull; Resistente a cloro;</p>\r\n<p>\r\n	&bull; Anti-uv, anti-fungos e anti-bact&eacute;rias.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	* Este produto n&atilde;o deve ser considerado como um salva vidas, ele somente ajuda na preven&ccedil;&atilde;o de acidentes.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Consulte a Aquatendas Piscinas e conhe&ccedil;a os modelos e dimens&otilde;es dispon&iacute;veis para todo tipo de tamanho de piscina.</p>\r\n<div>\r\n	&nbsp;</div>', '', '', '', 'SIM', 1, 'capas-de-protecao', 59, 'OUTRAS', NULL, NULL),
(10, 'Capa Térmica', '2203201604109355285109..jpg', '<p>\r\n	A capa t&eacute;rmica &eacute; ideal para manter a temperatura da sua piscina sem grandes perdas de calor especialmente durante a noite, pois sem o calor solar aquecendo a &aacute;gua esta perde boa parte de sua temperatura.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Consulte a Aquatendas Piscinas e conhe&ccedil;a os modelos e dimens&otilde;es dispon&iacute;veis para todo tipo de tamanho de piscina.</p>', '', '', '', 'SIM', 1, 'capa-termica', 59, 'ATCO', NULL, NULL),
(11, 'Filtros Jacuzzi', '2203201604189826425421..jpg', '<p>\r\n	A Jacuzzi&reg; &eacute; refer&ecirc;ncia mundial quando o assunto &eacute; filtra&ccedil;&atilde;o.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	S&atilde;o mais de 50 anos de mercado, apresentando sempre lan&ccedil;amentos com inova&ccedil;&otilde;es tecnol&oacute;gicas que reduzem o consumo de &aacute;gua e diminuem a frequ&ecirc;ncia de manuten&ccedil;&atilde;o.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Seja para projeto e constru&ccedil;&atilde;o ou manuten&ccedil;&atilde;o e limpeza, a Jacuzzi&reg; possui uma linha de filtros e equipamentos para piscina que atende &agrave;s necessidades tanto de piscinas de uso residencial quanto as de uso coletivo, o que garante uma &aacute;gua pura, cristalina e saud&aacute;vel.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Fabricados com a mais alta tecnologia, s&atilde;o reconhecidos por sua performance, durabilidade e qualidade.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Consulte a Aquatendas Piscinas e conhe&ccedil;a os modelos dispon&iacute;veis.</p>', '', '', '', 'SIM', 1, 'filtros-jacuzzi', 61, 'JACUZZI', NULL, NULL),
(12, 'Filtros Sibrape', '2203201604231245760201..jpg', '<p>\r\n	Os filtros Sibrape&trade; possuem v&aacute;lvulas diferenciadas, que possibilitam maior vaz&atilde;o, filtram muito mais &aacute;gua e consomem menos energia, gerando qualidade e economia para o usu&aacute;rio. Os tanques s&atilde;o rotomoldados em material termopl&aacute;stico, totalmente &agrave; prova de corros&atilde;o. A espessura do tanque &eacute; a maior do mercado, assegurando maior vida &uacute;til do equipamento.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	A combina&ccedil;&atilde;o perfeita para filtros de piscinas. Garantem o baixo consumo de energia e s&atilde;o &agrave; prova de corros&atilde;o, garantindo uma longa vida &uacute;til em compara&ccedil;&atilde;o com a concorr&ecirc;ncia.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Consulte a Aquatendas Piscinas e conhe&ccedil;a os modelos dispon&iacute;veis.</p>', '', '', '', 'SIM', 1, 'filtros-sibrape', 61, 'SIBRAPE PANTAIR', NULL, NULL),
(13, 'Filtros Dancor', '2203201604255789958184..jpg', '<p style="text-align: justify;">\r\n	Todos os modelos de filtros Dancor s&atilde;o fabricados em material termopl&aacute;stico, pelo processo de roto moldagem, enquanto seus sitemas internos de distrbui&ccedil;&atilde;o, de drenagem e as v&aacute;lvulas seletoras de seis posi&ccedil;&otilde;es, mais a posi&ccedil;&atilde;o LIVRE, s&atilde;o produzidas em termopl&aacute;sticos industriais: ABS, PVC e ABS com fibra de vidro.</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	A areia de filtragem &eacute; permanente, sendo selecionada pela Dancor, n&atilde;o necessitando ser substitu&iacute;da. O man&ocirc;metro &eacute; acoplado no bocal da bomba, na v&aacute;lvula seletora, para indicar o momento certo em que dever&aacute; ser feita &agrave; retro lavagem da mesma.</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Consulte a Aquatendas Piscinas e conhe&ccedil;a os modelos dispon&iacute;veis.</p>', '', '', '', 'SIM', 1, 'filtros-dancor', 61, 'DANCOR', NULL, NULL),
(14, 'Cloro Hth', '2203201604285330854078..jpg', '<p>\r\n	A linha de cloro hth&reg; age na elimina&ccedil;&atilde;o imediata de todos os germes (inclusive fungos causadores de frieira e candid&iacute;ase), ele libera &iacute;ons minerais na &aacute;gua que clarificam continuamente a piscina atrav&eacute;s da elimina&ccedil;&atilde;o de micropart&iacute;culas na filtra&ccedil;&atilde;o e previne a &aacute;gua verde. O resultado &eacute; uma &aacute;gua t&atilde;o pura e cristalina que brilha.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Consulte a Aquatendas Piscinas e conhe&ccedil;a a linha de cloro hth&reg; dispon&iacute;vel:</p>\r\n<p>\r\n	Cloro Concentrado Tradicional</p>\r\n<p>\r\n	Cloro Aditivado Mineral Brilliance&trade; 10em1</p>\r\n<p>\r\n	Cloro Flutuador</p>\r\n<p>\r\n	Cloro Granulado</p>\r\n<div>\r\n	&nbsp;</div>', '', '', '', 'SIM', 1, 'cloro-hth', 62, 'HTh', NULL, NULL),
(15, 'Cloro NeoClor', '2203201604321134599496..jpg', '<p>\r\n	Cloro Neoclor&reg; para piscina 100% de produto ativo na sua composi&ccedil;&atilde;o a base de DICLORO ISOCIANURATO DE S&Oacute;DIO, org&acirc;nico, que possui agentes protetores contra a degrada&ccedil;&atilde;o do sol. N&atilde;o deixa nenhum res&iacute;duo insol&uacute;vel que possa deixar a &aacute;gua turva ou prejudicar o filtro. Pode ser usado nas dosagens recomendadas, sem restri&ccedil;&otilde;es, em piscinas de fibra e vinil, pintura ou azulejos, chegando a oferecer 2 a 3 vezes mais economia do que as concorrentes.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Consulte a Aquatendas Piscinas e conhe&ccedil;a a linha de Cloro Neoclor&reg; dispon&iacute;vel.</p>', '', '', '', 'SIM', 1, 'cloro-neoclor', 62, 'NEOCLOR', NULL, NULL),
(16, 'Peneiras', '2203201604357014891546..jpg', '<p>\r\n	Peneira de Limpeza e Manuten&ccedil;&atilde;o</p>\r\n<p>\r\n	Peneira Cata Folha Pl&aacute;stica para Piscinas</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Consulte a Aquatendas Piscinas e conhe&ccedil;a os modelos e dimens&otilde;es dispon&iacute;veis.</p>', '', '', '', 'SIM', 1, 'peneiras', 59, 'OUTRAS', NULL, NULL),
(17, 'Rodo Aspirador', '2203201604378054552703..jpg', '<p>\r\n	A fun&ccedil;&atilde;o espec&iacute;fica dos aspiradores, al&eacute;m de proporcionar ampla movimenta&ccedil;&atilde;o na &aacute;gua, &eacute; de eliminar por completo sujeira e detritos acumulados no fundo da piscina, que succionada atrav&eacute;s do aspirador permite uma limpeza uniforme , inclusive nas regi&otilde;es menos favorecidas como cantos e superf&iacute;cies rasas da piscina. A Sodramar fabrica uma variedade de modelos que se adequam a situa&ccedil;&otilde;es e tipos de piscinas.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Consulte a Aquatendas Piscinas e conhe&ccedil;a os modelos dispon&iacute;veis.</p>', '', '', '', 'SIM', 1, 'rodo-aspirador', 59, 'OUTRAS', NULL, NULL),
(18, 'Kit de Limpeza', '2203201604418000774669..jpg', '<p>\r\n	Cuide de sua piscina semanalmente com um kit de limpeza.</p>\r\n<p>\r\n	Acess&oacute;rios fortes, resistentes, confeccionados com material de excelente qualidade, contendo:</p>\r\n<p>\r\n	&bull; Aspirador modelo Slim&nbsp;</p>\r\n<p>\r\n	&bull; Peneira Slim</p>\r\n<p>\r\n	&bull; Escova Slim</p>\r\n<p>\r\n	&bull; Ponteiras de borracha</p>\r\n<p>\r\n	&bull; Adaptador de mangueira</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Consulte a Aquatendas Piscinas e conhe&ccedil;a os kits de limpeza dispon&iacute;veis.</p>', '', '', '', 'SIM', 1, 'kit-de-limpeza', 59, 'OUTRAS', NULL, NULL),
(19, 'Hiper LED cromado', '3003201605068605135372..jpg', '<p>\r\n	Hiper LED RGB lat&atilde;o cromado - SODRAMAR</p>\r\n<p>\r\n	Um show de luz e beleza em sua piscina!</p>\r\n<p>\r\n	Vantagens da Ilumina&ccedil;&atilde;o em Led: &nbsp;</p>\r\n<p>\r\n	- Alta economia de energia;&nbsp;</p>\r\n<p>\r\n	- Baixa emiss&atilde;o de calor;&nbsp;</p>\r\n<p>\r\n	- Maior efici&ecirc;ncia luminosa ;&nbsp;</p>\r\n<p>\r\n	- N&atilde;o emite radia&ccedil;&atilde;o infravermelha;&nbsp;</p>\r\n<p>\r\n	- N&atilde;o emite radia&ccedil;&atilde;o ultravioleta;&nbsp;</p>\r\n<p>\r\n	- Ilumina&ccedil;&atilde;o dirigida; proporciona efeito de destaque;&nbsp;</p>\r\n<p>\r\n	- F&aacute;cil Instala&ccedil;&atilde;o;&nbsp;</p>\r\n<p>\r\n	- Vida M&eacute;dia de 30.000 hs.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Consulte a Aquatendas Piscinas e conhe&ccedil;a nossa linha de ilumina&ccedil;&atilde;o LED RGB dispon&iacute;vel.</p>', '', '', '', 'SIM', 1, 'hiper-led-cromado', 71, 'SODRAMAR', NULL, NULL),
(20, 'Hiper LED plástico', '3003201605088095355998..jpg', '<p>\r\n	Hiper LED RGB Pl&aacute;stico - SODRAMAR</p>\r\n<p>\r\n	Um show de luz e beleza em sua piscina!</p>\r\n<p>\r\n	Vantagens da Ilumina&ccedil;&atilde;o em Led: &nbsp;</p>\r\n<p>\r\n	- Alta economia de energia;&nbsp;</p>\r\n<p>\r\n	- Baixa emiss&atilde;o de calor;&nbsp;</p>\r\n<p>\r\n	- Maior efici&ecirc;ncia luminosa ;&nbsp;</p>\r\n<p>\r\n	- N&atilde;o emite radia&ccedil;&atilde;o infravermelha;&nbsp;</p>\r\n<p>\r\n	- N&atilde;o emite radia&ccedil;&atilde;o ultravioleta;&nbsp;</p>\r\n<p>\r\n	- Ilumina&ccedil;&atilde;o dirigida; proporciona efeito de destaque;&nbsp;</p>\r\n<p>\r\n	- F&aacute;cil Instala&ccedil;&atilde;o;&nbsp;</p>\r\n<p>\r\n	- Vida M&eacute;dia de 30.000 hs.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Consulte a Aquatendas Piscinas e conhe&ccedil;a nossa linha de ilumina&ccedil;&atilde;o LED RGB dispon&iacute;vel.</p>', '', '', '', 'SIM', 1, 'hiper-led-plastico', 71, 'SODRAMAR', NULL, NULL),
(21, 'Telas de Proteção', '1804201604085033113857..jpg', '<p>\r\n	A tela de prote&ccedil;&atilde;o para piscina &eacute; instalada para proteger contra quedas de pessoas ou animais. Esta tela recebe tratamento UV (Raios Ultra violeta), onde dar&aacute; mais durabilidade a tela.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Consulte a Aquatendas Piscinas e conhe&ccedil;a os modelos dispon&iacute;veis.</p>', '', '', '', 'SIM', 1, 'telas-de-protecao', 59, 'OUTRAS', NULL, NULL),
(22, 'Cascatas em Inox', '2203201604599021067081..jpg', '<p>\r\n	As Cascatas em inox Librainox &eacute; produzida em a&ccedil;o inoxid&aacute;vel escovado fosco de primeira qualidade (AISI 304) n&atilde;o havendo perigo de ferrugem e corros&atilde;o. Com design moderno ela decora e embeleza sua &aacute;rea de lazer promovendo momentos de relaxamento f&iacute;sico e mental, al&eacute;m de contribuir no processo de oxigena&ccedil;&atilde;o da &aacute;gua de sua piscina.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Consulte a Aquatendas Piscinas e conhe&ccedil;a os modelos dispon&iacute;veis.</p>', '', '', '', 'SIM', 1, 'cascatas-em-inox', 60, 'LIBRAINOX', NULL, NULL),
(23, 'Escadas de Inox', '2203201605211940151353..jpg', '<p>\r\n	As escadas para piscinas Librainox, foram desenvolvidas para atender os mais diversos tipos de instala&ccedil;&otilde;es, tornando acess&iacute;vel aos banhistas a entrada e sa&iacute;da da piscina com muito conforto e seguran&ccedil;a.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Consulte a Aquatendas Piscinas e conhe&ccedil;a os modelos dispon&iacute;veis.</p>\r\n<p>\r\n	&nbsp;</p>', '', '', '', 'SIM', 1, 'escadas-de-inox', 72, 'LIBRAINOX', NULL, NULL),
(24, 'Corrimão de Inox', '2203201605246129755704..jpg', '<p>\r\n	Corrim&atilde;o para escadas de piscinas em A&ccedil;o Inox 304&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Consulte a Aquatendas Piscinas e conhe&ccedil;a os modelos e dimens&otilde;es dispon&iacute;veis.</p>', '', '', '', 'SIM', 1, 'corrimao-de-inox', 72, 'LIBRAINOX', NULL, NULL),
(25, 'Enrolador de Capas', '2203201605291723501264..jpg', '<p>\r\n	Enrolador Para Capa T&eacute;rmica para Piscina ATCO</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Maior praticidade para colocar e retirar a capa da piscina</p>\r\n<p>\r\n	Adapta-se em qualquer formato de piscina</p>\r\n<p>\r\n	Leve - F&aacute;cil de Carregar</p>\r\n<p>\r\n	Aumenta a vida &uacute;til da capa</p>\r\n<p>\r\n	F&aacute;cil de instalar</p>\r\n<p>\r\n	Volante confort&aacute;vel</p>\r\n<p>\r\n	Composto de A&ccedil;o inox, tubo de aluminio tratado, pl&aacute;stico com tratamento anti UV, resistentes ao sol, ao cloro e outros produtos para tratamento da piscina</p>\r\n<p>\r\n	Seu desenho moderno garante adequa&ccedil;&atilde;o ao ambiente da piscina.</p>\r\n<p>\r\n	Garantia contra defeitos de fabrica&ccedil;&atilde;o: 90 dias.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Consulte a Aquatendas Piscinas e conhe&ccedil;a os modelos dispon&iacute;veis.</p>', '', '', '', 'SIM', 1, 'enrolador-de-capas', 59, 'ATCO', NULL, NULL),
(26, 'Aquecedores Solar', '3003201604475110253029..jpg', '<p>\r\n	Com um Aquecedor Solar sua piscina vai deixar de ser atra&ccedil;&atilde;o somente no ver&atilde;o. Com muita economia, usando a energia do sol e sem usar tecnologias que prejudicam o meio ambiente, o aquecedor solar proporciona o uso da piscina praticamente o ano todo.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Aquecedor solar de piscina de baixo custo e com consumo de energia praticamente desprez&iacute;vel. Podendo ser instalado em qualquer piscina.. Proporciona significativa redu&ccedil;&atilde;o dos custos comparando-se a outros sistemas convencionais de aquecimento.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Consulte a Aquatendas Piscinas e conhe&ccedil;a nossos equipamentos de aquecimento solar para piscinas.</p>', '', '', '', 'SIM', 0, 'aquecedores-solar', 70, 'JELLY FISH', NULL, NULL),
(27, 'Aquecedores Elétricos', '3003201604599730797066..jpg', '<p>\r\n	Aque&ccedil;a sua piscina e deixe sua temperatura agrad&aacute;vel todos os dias do ano, at&eacute; mesmo no ver&atilde;o a &aacute;gua de uma piscina se torna gelada, pela diferen&ccedil;a &nbsp;de temperatura &nbsp;entre a &aacute;gua e o corpo do banhista. Uma piscina aquecida proporciona um lazer agrad&aacute;vel a qualquer momento do dia para sua fam&iacute;lia, imagine a sensa&ccedil;&atilde;o de relaxamento ao sentar no spa da sua piscina aquecida, depois de um expediente cansativo e estressante de trabalho.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Consulte a Aquatendas Piscinas e conhe&ccedil;a nossos equipamentos de aquecimento el&eacute;trico para piscinas.</p>\r\n<div>\r\n	&nbsp;</div>', '', '', '', 'SIM', 0, 'aquecedores-eletricos', 70, 'SODRAMAR', NULL, NULL),
(28, 'Algicidas Hth', '2203201606214157629087..gif', '<p>\r\n	Algicida hth&reg; Eliminador de Algas</p>\r\n<p>\r\n	Indicado para prevenir o surgimento de algas, pode ser usado no mesmo dia que o cloro. &Eacute; um algicida de &uacute;ltima gera&ccedil;&atilde;o e com exclusiva f&oacute;rmula sem cobre: n&atilde;o mancha o revestimento e n&atilde;o deixa o cabelo esverdeado.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Consulte a Aquatendas Piscinas e conhe&ccedil;a a linha completa de Algicida hth&reg; dispon&iacute;vel.</p>', '', '', '', 'SIM', 0, 'algicidas-hth', 62, 'HTH', NULL, NULL),
(29, 'Led SMD', '3003201605109565253439..jpg', '<p>\r\n	Um show de luz e beleza em sua piscina!</p>\r\n<p>\r\n	Vantagens da Ilumina&ccedil;&atilde;o em Led: &nbsp;</p>\r\n<p>\r\n	- Alta economia de energia;&nbsp;</p>\r\n<p>\r\n	- Baixa emiss&atilde;o de calor;&nbsp;</p>\r\n<p>\r\n	- Maior efici&ecirc;ncia luminosa ;&nbsp;</p>\r\n<p>\r\n	- N&atilde;o emite radia&ccedil;&atilde;o infravermelha;&nbsp;</p>\r\n<p>\r\n	- N&atilde;o emite radia&ccedil;&atilde;o ultravioleta;&nbsp;</p>\r\n<p>\r\n	- Ilumina&ccedil;&atilde;o dirigida; proporciona efeito de destaque;&nbsp;</p>\r\n<p>\r\n	- F&aacute;cil Instala&ccedil;&atilde;o;&nbsp;</p>\r\n<p>\r\n	- Vida M&eacute;dia de 30.000 hs.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Consulte a Aquatendas Piscinas e conhe&ccedil;a nossa linha de ilumina&ccedil;&atilde;o LED RGB dispon&iacute;vel.</p>', '', '', '', 'SIM', 0, 'led-smd', 71, 'SODRAMAR', NULL, NULL),
(30, 'Led Pool Pratic', '3003201605118594412004..jpg', '<p>\r\n	Um show de luz e beleza em sua piscina!</p>\r\n<p>\r\n	Vantagens da Ilumina&ccedil;&atilde;o em Led: &nbsp;</p>\r\n<p>\r\n	- Alta economia de energia;&nbsp;</p>\r\n<p>\r\n	- Baixa emiss&atilde;o de calor;&nbsp;</p>\r\n<p>\r\n	- Maior efici&ecirc;ncia luminosa ;&nbsp;</p>\r\n<p>\r\n	- N&atilde;o emite radia&ccedil;&atilde;o infravermelha;&nbsp;</p>\r\n<p>\r\n	- N&atilde;o emite radia&ccedil;&atilde;o ultravioleta;&nbsp;</p>\r\n<p>\r\n	- Ilumina&ccedil;&atilde;o dirigida; proporciona efeito de destaque;&nbsp;</p>\r\n<p>\r\n	- F&aacute;cil Instala&ccedil;&atilde;o;&nbsp;</p>\r\n<p>\r\n	- Vida M&eacute;dia de 30.000 hs.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Consulte a Aquatendas Piscinas e conhe&ccedil;a nossa linha de ilumina&ccedil;&atilde;o LED RGB dispon&iacute;vel.</p>', '', '', '', 'SIM', 0, 'led-pool-pratic', 71, 'SODRAMAR', NULL, NULL),
(31, 'Sauna Universal', '3003201603256050722284..jpg', '<p>\r\n	Gerador de vapor com voltagem universal</p>\r\n<p>\r\n	A Sodramar &eacute; lider na fabrica&ccedil;&atilde;o de equipamentos e acess&oacute;rios para Sauna a vapor. Consulte a Aquatendas Piscinas e conhe&ccedil;a os modelos dispon&iacute;veis para voc&ecirc; aproveitar o m&aacute;ximo da Sauna com seguran&ccedil;a e qualidade</p>', '', '', '', 'SIM', 0, 'sauna-universal', 73, 'SODRAMAR', NULL, NULL),
(32, 'Sauna Compact Line', '3003201603273985098480..jpg', '<p>\r\n	Gerador de vapor Compact Line Inox</p>\r\n<p>\r\n	A Sodramar &eacute; lider na fabrica&ccedil;&atilde;o de equipamentos e acess&oacute;rios para Sauna a vapor. Consulte a Aquatendas Piscinas e conhe&ccedil;a os modelos dispon&iacute;veis para voc&ecirc; aproveitar o m&aacute;ximo da Sauna com seguran&ccedil;a e qualidade</p>\r\n<div>\r\n	&nbsp;</div>', '', '', '', 'SIM', 0, 'sauna-compact-line', 73, 'SODRAMAR', NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura para tabela `tb_servicos`
--

CREATE TABLE IF NOT EXISTS `tb_servicos` (
  `idservico` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `descricao` longtext COLLATE utf8_unicode_ci,
  `imagem` varchar(45) COLLATE utf8_unicode_ci DEFAULT 'imagem_nao_disponivel.jpg',
  `ativo` varchar(3) COLLATE utf8_unicode_ci DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `keywords_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `id_categoriaservico` int(10) unsigned NOT NULL,
  `imagem_1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem_2` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`idservico`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=40 ;

--
-- Fazendo dump de dados para tabela `tb_servicos`
--

INSERT INTO `tb_servicos` (`idservico`, `titulo`, `descricao`, `imagem`, `ativo`, `ordem`, `url_amigavel`, `title_google`, `keywords_google`, `description_google`, `id_categoriaservico`, `imagem_1`, `imagem_2`) VALUES
(37, 'INSTALAÇÃO DE PISCINAS', '<p>\r\n	Aquatendas Piscinas vem h&aacute; mais de nove anos atuando na constru&ccedil;&atilde;o e instala&ccedil;&atilde;o de piscina de vinil ou azulejo de grande, m&eacute;dio ou pequeno porte, formado por profissionais com grande experi&ecirc;ncia. Possibilitamos o menor pre&ccedil;o e a melhor qualidade de nossos produtos, oferecemos parceria e solu&ccedil;&otilde;es para projetos dos clientes.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Consulte a Aquatendas Piscinas, traga seu projeto e solicite or&ccedil;amento da constru&ccedil;&atilde;o e materiais, como revestimentos de vinil e azulejos, dispon&iacute;veis para sua obra.</p>', '2801201602041225579006..jpg', 'SIM', NULL, 'instalacao-de-piscinas', '', '', '', 0, '', ''),
(38, 'REFORMA DE PISCINAS', '<p>\r\n	Aquatendas Piscinas &eacute; especializada na reforma de piscinas de vinil ou azulejo de grande, m&eacute;dio ou pequeno porte, formado por profissionais com grande experi&ecirc;ncia. Aplicamos para cada tipo de piscina t&eacute;cnicas diferentes de reforma, seja na estrutura de alvenaria como a troca e substitui&ccedil;&atilde;o de revestimentos de vinil. Possibilitamos o menor pre&ccedil;o e agilidade de entrega da reforma.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Consulte a Aquatendas Piscinas e solicite or&ccedil;amento para reforma de sua piscina.</p>', '2801201602041259910792..jpg', 'SIM', NULL, 'reforma-de-piscinas', '', '', '', 0, '', ''),
(39, 'LIMPEZA DE PISCINAS', '<p>\r\n	Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>', '2801201602051257444057..jpg', 'NAO', NULL, 'limpeza-de-piscinas', '', '', '', 0, '', '');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
